macro(loom_basic_setup)
    set(CMAKE_MODULE_PATH "${CMAKE_SOURCE_DIR}/cmake" ${CMAKE_MODULE_PATH})
    set(CMAKE_PREFIX_PATH "${CMAKE_SOURCE_DIR}/cmake" ${CMAKE_PREFIX_PATH})

    message(STATUS "Setting up conan.")
    if (NOT EXISTS ${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)
        message(FATAL_ERROR "Conan needs to be executed: conan install <path_to_your_conanfile.txt>")
    endif ()
    include(${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)
    conan_basic_setup(TARGETS)

    find_program(CCACHE_PROGRAM ccache)
    if (CCACHE_PROGRAM)
        message(STATUS "Enabled ccache")
        set_property(GLOBAL PROPERTY RULE_LAUNCH_COMPILE "${CCACHE_PROGRAM}")
    endif ()

    if (ANDROID AND NOT TARGET pthread)
        # Threading is included in android per default. Since some of the boost targets imported by conan seem to
        # hard-code their dependencies we solve this by adding an empty target.
        add_library(pthread INTERFACE)
    endif ()

    # Reset the unit tests
    set(LOOM_TEST_UNITS "" CACHE INTERNAL "Test units to include")

    # Reset the tests
    message("Resetting test cache")
    set(LOOM_TEST_SOURCES "" CACHE INTERNAL "LOOM_TEST_SOURCES")
    set(LOOM_TEST_LIBRARIES "" CACHE INTERNAL "LOOM_TEST_LIBRARIES")
    set(LOOM_TEST_INCLUDE_DIRS "" CACHE INTERNAL "LOOM_TEST_INCLUDE_DIRS")
    option(LOOM_AGGREGATE_TESTS "Create a target for aggregated tests" ON)
endmacro()

macro(loom_set_target_defaults TARGET_NAME)
    set_target_properties(${TARGET_NAME}
            PROPERTIES
            ARCHIVE_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/lib/"
            LIBRARY_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/lib/"
            RUNTIME_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/bin/"
            )

    set_target_properties(${TARGET_NAME}
            PROPERTIES
            CXX_STANDARD 17
            CXX_STANDARD_REQUIRED YES
            CXX_EXTENSIONS NO
            POSITION_INDEPENDENT_CODE ON
            )

    target_include_directories(${TARGET_NAME}
            PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/include)

    target_include_directories(${TARGET_NAME}
            INTERFACE ${CMAKE_CURRENT_SOURCE_DIR}/include)

    get_target_property(target_type ${TARGET_NAME} TYPE)
    if (target_type STREQUAL "SHARED_LIBRARY")
        target_compile_definitions(${TARGET_NAME} PRIVATE LOOM_SHARED)
        target_compile_definitions(${TARGET_NAME} PRIVATE LOOM_EXPORT)
    endif ()
endmacro()

macro(loom_setup_tests)
    cmake_parse_arguments(TEST "" "" "LIBS;COMMON" ${ARGN})

    set(libs ${TEST_LIBS})
    set(standalone_libs ${libs} loom-testing-main)
    set(common_sources ${TEST_COMMON})

    foreach (unit ${TEST_UNPARSED_ARGUMENTS})
        string(REGEX MATCH ".*.cpp" is_test ${unit})

        if (is_test)
            string(REGEX REPLACE "(.cpp|test(s)?\\/|Test(s)?)" "" test_target ${unit})
            string(REGEX REPLACE "\\/" "_" test_target ${test_target})

            set(test_target ${PROJECT_NAME}_${test_target})
            add_executable(test_${test_target} ${unit} ${common_sources})
            target_link_libraries(test_${test_target} ${standalone_libs})
            loom_set_target_defaults(test_${test_target})

            add_test(NAME ${test_target} COMMAND test_${test_target})

            if (LOOM_AGGREGATE_TESTS)
                set(test_sources "${unit};${common_sources}")
                list(TRANSFORM test_sources PREPEND "${CMAKE_CURRENT_SOURCE_DIR}/")

                set(LOOM_TEST_SOURCES "${LOOM_TEST_SOURCES};${test_sources}" CACHE INTERNAL "LOOM_TEST_SOURCES")
                set(LOOM_TEST_LIBRARIES "${LOOM_TEST_LIBRARIES};${libs}" CACHE INTERNAL "LOOM_TEST_LIBRARIES")
                set(LOOM_TEST_INCLUDE_DIRS "${LOOM_TEST_INCLUDE_DIRS};${CMAKE_CURRENT_SOURCE_DIR}/include" CACHE INTERNAL "LOOM_TEST_INCLUDE_DIRS")
            endif ()

        endif ()
    endforeach ()
endmacro()
