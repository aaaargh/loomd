# A custom FindPackage configuration for RxCpp that downloads it from
# github without the need of submodules.

if (NOT TARGET RxCpp::RxCpp)
    include(FetchContent)
    FetchContent_Declare(
            RxCpp
            GIT_REPOSITORY https://github.com/ReactiveX/RxCpp.git
    )

    #FetchContent_MakeAvailable(RxCpp)

    FetchContent_GetProperties(RxCpp)
    if(NOT rxcpp_POPULATED)
        FetchContent_Populate(RxCpp)
    endif()

    add_library(RxCpp::RxCpp INTERFACE IMPORTED)

    target_include_directories(RxCpp::RxCpp INTERFACE
            ${rxcpp_SOURCE_DIR}/Rx/v2/src
           )
endif()
