# A custom FindPackage configuration for Loguru that downloads it from
# github without the need of submodules.

if (NOT TARGET Loguru::Loguru)
include(FetchContent)
FetchContent_Declare(
        loguru
        GIT_REPOSITORY https://github.com/emilk/loguru.git
)
# FetchContent_MakeAvailable(loguru)

FetchContent_GetProperties(loguru)
if(NOT loguru_POPULATED)
    FetchContent_Populate(loguru)
endif()

add_library(Loguru
        ${loguru_SOURCE_DIR}/loguru.cpp
        )

set_property(TARGET Loguru PROPERTY POSITION_INDEPENDENT_CODE ON)

target_compile_definitions(Loguru PUBLIC LOGURU_WITH_STREAMS=1)

target_link_libraries(Loguru PUBLIC dl)

target_include_directories(Loguru INTERFACE
        "${loguru_SOURCE_DIR}/"
        )

add_library(Loguru::Loguru ALIAS Loguru)
endif()
