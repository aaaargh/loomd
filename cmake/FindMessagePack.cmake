# A custom FindPackage configuration for msgpack that downloads it from
# github without the need of submodules.

if (NOT TARGET MessagePack::MessagePack)
    include(FetchContent)
    FetchContent_Declare(
            msgpack
            GIT_REPOSITORY https://github.com/msgpack/msgpack-c.git
            GIT_TAG cpp_master
    )

    FetchContent_GetProperties(msgpack)
    if(NOT msgpack_POPULATED)
        FetchContent_Populate(msgpack)
    endif()

    add_library(MessagePack::MessagePack INTERFACE IMPORTED)
    target_include_directories(MessagePack::MessagePack INTERFACE
            "${msgpack_SOURCE_DIR}/include"
            )
endif ()