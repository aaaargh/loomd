# A custom FindPackage configuration for the Argh! command line parser

if (NOT TARGET Argh::Argh)
    include(FetchContent)
    FetchContent_Declare(
            argh
            GIT_REPOSITORY https://github.com/adishavit/argh.git
    )
    # FetchContent_MakeAvailable(loguru)

    FetchContent_GetProperties(argh)
    if (NOT argh_POPULATED)
        FetchContent_Populate(argh)
    endif ()

    add_library(Argh::Argh INTERFACE IMPORTED)

    target_include_directories(Argh::Argh INTERFACE
            "${argh_SOURCE_DIR}/"
            )
endif ()
