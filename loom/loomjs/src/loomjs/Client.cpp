//
// Created by void on 06/06/2020.
//

#include <loomjs/Client.h>
#include <loomjs/ConfigMap.h>

#include <loom/System.h>

#include <cassert>

namespace loomjs {

namespace detail {

struct EventPropagator : public loom::ClientEventListener {
    ~EventPropagator() override {
        for (auto &listener : activeListeners) {
            listener.Release();
        }
    }

    void onSubjectAnnounced(loom::Client &client, const loom::ConfigMap &subject, bool isUpdate) override {
        for (auto &listener : activeListeners) {
            listener.BlockingCall([subject, isUpdate](Napi::Env env, Napi::Function callback) {
                auto eventObj = Napi::Object::New(env);
                eventObj["type"] = "announced";
                eventObj["isUpdate"] = isUpdate;
                eventObj["subject"] = convert<Napi::Object>(subject, env);
                callback.Call({eventObj});
            });
        }
    }

    void onSubjectDropped(loom::Client &client, const loom::ConfigMap &subject) override {
        for (auto &listener : activeListeners) {
            listener.BlockingCall([subject](Napi::Env env, Napi::Function callback) {
                auto eventObj = Napi::Object::New(env);
                eventObj["type"] = "dropped";
                eventObj["subject"] = convert<Napi::Object>(subject, env);
                callback.Call({eventObj});
            });
        }
    }

    void onAuthenticationFailed(loom::Client &client, const std::string &reason) override {
        // ...
    }

    void onAuthenticated(loom::Client &client, const loom::ConfigMap &identity) override {
        // ...
    }

    void onConnected(loom::Client &client) override {
        // ...
    }

    void onConnectionLost(loom::Client &client, const std::exception_ptr &error) override {
        // ...
    }

    std::vector<Napi::ThreadSafeFunction> activeListeners;
};

}

Napi::FunctionReference Client::constructor;

Napi::Object Client::Init(Napi::Env env, Napi::Object exports) {
    Napi::HandleScope scope(env);

    Napi::Function func =
            DefineClass(env,
                        "Client",
                        {
                                InstanceMethod("asyncStart", &Client::asyncStart),
                                InstanceMethod("stop", &Client::stop),
                                InstanceMethod("authenticate", &Client::authenticate),
                                InstanceMethod("querySubjects", &Client::querySubjects),
                                InstanceMethod("announceSubject", &Client::announceSubject),
                                InstanceMethod("subscribeToSubjects", &Client::subscribeToSubjects)
                        });

    constructor = Napi::Persistent(func);
    constructor.SuppressDestruct();

    exports.Set("Client", func);
    return exports;
}

Client::Client(const Napi::CallbackInfo &info) : ObjectWrap(info) {
    Napi::Env env = info.Env();
    Napi::HandleScope scope(env);

    int length = info.Length();

    if (length <= 0 || !info[0].IsString()) {
        Napi::TypeError::New(env, "String expected").ThrowAsJavaScriptException();
        return;
    }

    if (length <= 1 || !info[1].IsNumber()) {
        Napi::TypeError::New(env, "Number expected").ThrowAsJavaScriptException();
        return;
    }

    auto daemonAddress = info[0].ToString().Utf8Value();
    auto daemonPort = info[1].ToNumber().Int32Value();

    client = loom::System::get().createTCPClient(daemonAddress, daemonPort);
    client->setEventListener(std::make_unique<detail::EventPropagator>());
}

struct EventLoopWorker : Napi::AsyncWorker {
    explicit EventLoopWorker(Napi::Function &callback) : AsyncWorker(callback) {}

    void Execute() override {
        loom::System::get().messagePump();
    }
};

void Client::asyncStart(const Napi::CallbackInfo &info) {
    Napi::Env env = info.Env();
    Napi::HandleScope scope(env);

    int length = info.Length();

    Napi::Function fun;

    size_t argn = 0;
    if (length <= argn || !info[argn].IsFunction()) {
        Napi::TypeError::New(env, "Function expected").ThrowAsJavaScriptException();
        return;
    }
    auto executeCb = info[argn].As<Napi::Function>();

    if (length <= ++argn || !info[argn].IsFunction()) {
        Napi::TypeError::New(env, "Function expected").ThrowAsJavaScriptException();
        return;
    }
    auto shutdownCb = info[argn].As<Napi::Function>();

    auto *worker = new EventLoopWorker(shutdownCb);
    worker->Queue();

    executeCb.Call({});
}

void Client::stop(const Napi::CallbackInfo &info) {
    loom::System::get().stop();
}

Napi::Value Client::authenticate(const Napi::CallbackInfo &info) {
    Napi::Env env = info.Env();
    Napi::HandleScope scope(env);

    int length = info.Length();

    if (length <= 0 || !info[0].IsObject()) {
        Napi::TypeError::New(env, "Object expected").ThrowAsJavaScriptException();
        return env.Undefined();
    }

    Napi::Promise::Deferred deferred = Napi::Promise::Deferred::New(info.Env());

    auto identity = convert<loom::ConfigMap>(info[0].ToObject());

    auto tsfn = Napi::ThreadSafeFunction::New(
            env,
            Napi::Function::New(env, [](const Napi::CallbackInfo &cbinfo) { return cbinfo.Env().Undefined(); }),
            "Client.authenticate()",
            0,
            1);

    client->authenticate(identity, [tsfn, deferred]() mutable {
        tsfn.BlockingCall([deferred](Napi::Env env, Napi::Function) {
            deferred.Resolve(env.Undefined());
        });
        tsfn.Release();
    }, [tsfn, deferred](const auto &error) mutable {
        std::string reason;

        try {
            std::rethrow_exception(error);
        } catch (const std::exception &exception) {
            reason = exception.what();
        }

        tsfn.BlockingCall([reason = std::move(reason), deferred](Napi::Env env, Napi::Function) {
            deferred.Reject(Napi::String::From(env, reason));
        });
        tsfn.Release();
    });

    return deferred.Promise();
}

Napi::Value Client::querySubjects(const Napi::CallbackInfo &info) {
    Napi::Env env = info.Env();
    Napi::HandleScope scope(env);

    int length = info.Length();

    size_t argn = 0;

    if (length <= argn || !info[argn].IsString()) {
        Napi::TypeError::New(env, "String expected").ThrowAsJavaScriptException();
        return env.Undefined();
    }

    if (length <= ++argn || !info[argn].IsNumber()) {
        Napi::TypeError::New(env, "Number expected").ThrowAsJavaScriptException();
        return env.Undefined();
    }
    loom::SubjectViewMode viewMode = static_cast<loom::SubjectViewMode>(info[argn].ToNumber().Uint32Value());

    loom::SubjectQueryFlags queryFlags;
    if (length > ++argn) {
        if (!info[argn].IsNumber()) {
            Napi::TypeError::New(env, "Number expected").ThrowAsJavaScriptException();
            return env.Undefined();
        }

        queryFlags = static_cast<loom::SubjectQueryFlags>(info[argn].ToNumber().Uint32Value());
    } else {
        queryFlags = loom::SubjectQueryFlags::Defaults;
    }

    Napi::Promise::Deferred deferred = Napi::Promise::Deferred::New(info.Env());

    auto filter = info[0].ToString().Utf8Value();

    auto tsfn = Napi::ThreadSafeFunction::New(
            env,
            Napi::Function::New(env, [](const Napi::CallbackInfo &cbinfo) { return cbinfo.Env().Undefined(); }),
            "Client.querySubjects()",
            0,
            1);

    client->querySubjects(filter, viewMode, queryFlags, [tsfn, deferred](auto results) mutable {
        tsfn.BlockingCall([deferred, results = std::move(results)](Napi::Env env, Napi::Function) {
            auto ary = Napi::Array::New(env);

            size_t i = 0;
            for (const loom::ConfigMap &result : results) {
                ary.Set(i++, convert<Napi::Object>(result, env));
            }

            deferred.Resolve(ary);
        });
        tsfn.Release();
    }, [tsfn, deferred](const auto &error) mutable {
        std::string reason;

        try {
            std::rethrow_exception(error);
        } catch (const std::exception &exception) {
            reason = exception.what();
        }

        tsfn.BlockingCall([reason = std::move(reason), deferred](Napi::Env env, Napi::Function) {
            deferred.Reject(Napi::String::From(env, reason));
        });
        tsfn.Release();
    });

    return deferred.Promise();
}

Napi::Value Client::announceSubject(const Napi::CallbackInfo &info) {
    Napi::Env env = info.Env();
    Napi::HandleScope scope(env);

    int length = info.Length();

    size_t argn = 0;
    if (length <= argn || !info[argn].IsObject()) {
        Napi::TypeError::New(env, "Object expected").ThrowAsJavaScriptException();
        return env.Undefined();
    }
    auto subjectConfig = convert<loom::ConfigMap>(info[argn].ToObject());

    loom::SubjectAnnouncementFlags flags;
    if (length > ++argn) {
        if (!info[argn].IsNumber()) {
            Napi::TypeError::New(env, "Number expected").ThrowAsJavaScriptException();
            return env.Undefined();
        }

        flags = static_cast<loom::SubjectAnnouncementFlags>(info[argn].ToNumber().Uint32Value());
    } else {
        flags = loom::SubjectAnnouncementFlags::Defaults;
    }

    Napi::Promise::Deferred deferred = Napi::Promise::Deferred::New(info.Env());

    auto tsfn = Napi::ThreadSafeFunction::New(
            env,
            Napi::Function::New(env, [](const Napi::CallbackInfo &cbinfo) { return cbinfo.Env().Undefined(); }),
            "Client.announceSubject()",
            0,
            1);

    client->announceSubject(subjectConfig, flags, [tsfn, deferred]() mutable {
        tsfn.BlockingCall([deferred](Napi::Env env, Napi::Function) {
            deferred.Resolve(env.Undefined());
        });
        tsfn.Release();
    }, [tsfn, deferred](const auto &error) mutable {
        std::string reason;

        try {
            std::rethrow_exception(error);
        } catch (const std::exception &exception) {
            reason = exception.what();
        }

        tsfn.BlockingCall([reason = std::move(reason), deferred](Napi::Env env, Napi::Function) {
            deferred.Reject(Napi::String::From(env, reason));
        });
        tsfn.Release();
    });

    return deferred.Promise();
}

Napi::Value Client::subscribeToSubjects(const Napi::CallbackInfo &info) {
    Napi::Env env = info.Env();
    Napi::HandleScope scope(env);

    int length = info.Length();

    size_t argn = 0;

    if (length <= argn || !info[argn].IsString()) {
        Napi::TypeError::New(env, "String expected").ThrowAsJavaScriptException();
        return env.Undefined();
    }

    if (length <= ++argn || !info[argn].IsFunction()) {
        Napi::TypeError::New(env, "Function expected").ThrowAsJavaScriptException();
        return env.Undefined();
    }
    auto eventCallback = info[argn].As<Napi::Function>();
    auto tsfnEventCallback = Napi::ThreadSafeFunction::New(
            env,
            eventCallback,
            "Client.subscribeToSubjects() event callback",
            0,
            1);
    auto &eventPropagator = dynamic_cast<detail::EventPropagator &>(client->getEventListener());
    eventPropagator.activeListeners.emplace_back(tsfnEventCallback);

    loom::SubjectSubscriptionFlags flags;
    if (length > ++argn) {
        if (!info[argn].IsNumber()) {
            Napi::TypeError::New(env, "Number expected").ThrowAsJavaScriptException();
            return env.Undefined();
        }

        flags = static_cast<loom::SubjectSubscriptionFlags>(info[argn].ToNumber().Uint32Value());
    } else {
        flags = loom::SubjectSubscriptionFlags::Defaults;
    }

    Napi::Promise::Deferred deferred = Napi::Promise::Deferred::New(info.Env());

    auto filter = info[0].ToString().Utf8Value();

    auto tsfn = Napi::ThreadSafeFunction::New(
            env,
            Napi::Function::New(env, [](const Napi::CallbackInfo &cbinfo) { return cbinfo.Env().Undefined(); }),
            "Client.subscribeToSubjects()",
            0,
            1);

    client->subscribeToSubjects(filter, flags, [tsfn, deferred]() mutable {
        tsfn.BlockingCall([deferred](Napi::Env env, Napi::Function) {
            deferred.Resolve(env.Undefined());
        });
        tsfn.Release();
    }, [tsfn, deferred](const auto &error) mutable {
        std::string reason;

        try {
            std::rethrow_exception(error);
        } catch (const std::exception &exception) {
            reason = exception.what();
        }

        tsfn.BlockingCall([reason = std::move(reason), deferred](Napi::Env env, Napi::Function) {
            deferred.Reject(Napi::String::From(env, reason));
        });
        tsfn.Release();
    });

    return deferred.Promise();
}

}