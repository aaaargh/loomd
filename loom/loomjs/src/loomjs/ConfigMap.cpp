//
// Created by void on 06/06/2020.
//

#include <loomjs/ConfigMap.h>

#include <napi.h>

namespace loomjs {

template<>
loom::ConfigMap TypeConverter<loom::ConfigMap, Napi::Object>::operator()(const Napi::Object &map) {
    loom::ConfigMap cm;
    auto propNames = map.GetPropertyNames();

    for (size_t i = 0; i < propNames.Length(); ++i) {
        auto key = propNames.Get(i);
        auto keyStr = key.ToString().Utf8Value();

        auto valueJS = map.Get(key);
        if (valueJS.IsArray()) {
            auto ary = valueJS.As<Napi::Array>();
            for (size_t j = 0; j < ary.Length(); ++j) {
                auto value = convert<loom::ConfigMapValue>(ary.Get(j));
                cm.emplace(std::make_pair(keyStr, std::move(value)));
            }
        } else {
            auto value = convert<loom::ConfigMapValue>(map.Get(key));
            cm.emplace(std::make_pair(keyStr, std::move(value)));
        }
    }

    return cm;
}

template<>
Napi::Object TypeConverter<Napi::Object, loom::ConfigMap>::operator()(Napi::Env &env, const loom::ConfigMap &cm) {
    auto result = Napi::Object::New(env);

    for (auto it = cm.begin(), end = cm.end(); it != end; it = cm.upper_bound(it->first)) {
        auto range = cm.equal_range(it->first);
        auto numElements = std::distance(range.first, range.second);

        if (numElements == 1) {
            auto value = convert<Napi::Value>(it->second, env);
            result.Set(it->first, value);
        } else {
            auto ary = Napi::Array::New(env);

            size_t i = 0;
            while (range.first != range.second) {
                auto value = convert<Napi::Value>(range.first++, env);
                ary.Set(i++, value);
            }

            result.Set(it->first, ary);
        }
    }

    return result;
}

template<>
loom::ConfigMapValue TypeConverter<loom::ConfigMapValue, Napi::Value>::operator()(const Napi::Value &value) {
    if (value.IsString()) {
        return value.ToString().Utf8Value();
    } else if (value.IsNumber()) {
        auto number = value.ToNumber();
        return number.DoubleValue();
    } else {
        throw loom::NotSupportedException("ConfigMap doesn't support this value type");
    }
}

template<>
Napi::Value TypeConverter<Napi::Value, loom::ConfigMapValue>::operator()(Napi::Env &env,
                                                                         const loom::ConfigMapValue &cv) {
    return std::visit([&](auto &&v) -> Napi::Value {
        using T = std::decay_t<decltype(v)>;

        if constexpr(std::is_same_v<std::string, T>) {
            return Napi::String::From(env, v);
        }

        if constexpr(std::is_same_v<double, T>) {
            return Napi::Number::From(env, v);
        }

        if constexpr(std::is_same_v<long, T>) {
            return Napi::Number::New(env, v);
        }

        throw loom::NotImplementedException("Type conversion is not implemented.");
    }, cv);
}

}