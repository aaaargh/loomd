#include <napi.h>

#include <loomjs/Client.h>
#include <loomjs/DataSourceInfo.h>

#include <loomjs/TypeConverter.h>

namespace loomjs {

Napi::Object initCommonObjects(Napi::Env env, Napi::Object exports) {
    auto subjectQueryFlags = Napi::Object::New(env);
    LOOMJS_MAKE_FLAG(loom::SubjectQueryFlags, None, subjectQueryFlags, env);
    LOOMJS_MAKE_FLAG(loom::SubjectQueryFlags, IgnoreOwnSubjects, subjectQueryFlags, env);
    LOOMJS_MAKE_FLAG(loom::SubjectQueryFlags, Defaults, subjectQueryFlags, env);
    exports.Set("SubjectQueryFlags", subjectQueryFlags);

    auto subjectAnnouncementFlags = Napi::Object::New(env);
    LOOMJS_MAKE_FLAG(loom::SubjectAnnouncementFlags, None, subjectAnnouncementFlags, env);
    LOOMJS_MAKE_FLAG(loom::SubjectAnnouncementFlags, ExtendAll, subjectAnnouncementFlags, env);
    LOOMJS_MAKE_FLAG(loom::SubjectAnnouncementFlags, ExtendMissing, subjectAnnouncementFlags, env);
    LOOMJS_MAKE_FLAG(loom::SubjectAnnouncementFlags, ExtendOverride, subjectAnnouncementFlags, env);
    LOOMJS_MAKE_FLAG(loom::SubjectAnnouncementFlags, Defaults, subjectAnnouncementFlags, env);
    exports.Set("SubjectAnnouncementFlags", subjectAnnouncementFlags);

    auto subjectViewMode = Napi::Object::New(env);
    LOOMJS_MAKE_FLAG(loom::SubjectViewMode, RawOptions, subjectViewMode, env);
    LOOMJS_MAKE_FLAG(loom::SubjectViewMode, ResolveFully, subjectViewMode, env);
    LOOMJS_MAKE_FLAG(loom::SubjectViewMode, ResolveRequired, subjectViewMode, env);
    exports.Set("SubjectViewMode", subjectViewMode);

    auto subjectSubscriptionFlags = Napi::Object::New(env);
    LOOMJS_MAKE_FLAG(loom::SubjectSubscriptionFlags, None, subjectSubscriptionFlags, env);
    LOOMJS_MAKE_FLAG(loom::SubjectSubscriptionFlags, EmitExistingSubjects, subjectSubscriptionFlags, env);
    LOOMJS_MAKE_FLAG(loom::SubjectSubscriptionFlags, IgnoreOwnSubjects, subjectSubscriptionFlags, env);
    LOOMJS_MAKE_FLAG(loom::SubjectSubscriptionFlags, Defaults, subjectSubscriptionFlags, env);
    exports.Set("SubjectSubscriptionFlags", subjectSubscriptionFlags);

    return exports;
}

}

Napi::Object InitAll(Napi::Env env, Napi::Object exports) {
    exports = loomjs::Client::Init(env, exports);
    exports = loomjs::initCommonObjects(env, exports);
    exports = loomjs::exportDataSourceInfo(env, exports);

    return exports;
}

NODE_API_MODULE(loomjs, InitAll)
