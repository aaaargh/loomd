//
// Created by void on 08/06/2020.
//

#include <loomjs/DataSourceInfo.h>

namespace loomjs {

Napi::Object exportDataSourceInfo(Napi::Env env, Napi::Object exports) {
    auto dsiAccessType = Napi::Object::New(env);
    LOOMJS_MAKE_FLAG(loom::DataSourceInfo::AccessType, Streamed, dsiAccessType, env);
    LOOMJS_MAKE_FLAG(loom::DataSourceInfo::AccessType, Random, dsiAccessType, env);
    exports.Set("AccessType", dsiAccessType);

    auto dsiSizeHint = Napi::Object::New(env);
    LOOMJS_MAKE_FLAG(loom::DataSourceInfo::SizeHint, Exact, dsiSizeHint, env);
    LOOMJS_MAKE_FLAG(loom::DataSourceInfo::SizeHint, Estimated, dsiSizeHint, env);
    LOOMJS_MAKE_FLAG(loom::DataSourceInfo::SizeHint, AtLeast, dsiSizeHint, env);
    LOOMJS_MAKE_FLAG(loom::DataSourceInfo::SizeHint, AtMost, dsiSizeHint, env);
    exports.Set("SizeHint", dsiSizeHint);

    return exports;
}

namespace detail {
struct JSReader : loom::DataSourceReader, std::enable_shared_from_this<JSReader> {
    ~JSReader() override {
        tsfn.Release();
    }

    void readAsync(loom::MutableBuffer target, size_t offset, const ReadCompleteCallback &callback) override {
        tsfn.NonBlockingCall(
                [callback, target, reader = shared_from_this()](Napi::Env env, Napi::Function jsCallback) mutable {
                    auto buf = Napi::ArrayBuffer::New(env, target.data(), target.size());
                    auto bytesRead = jsCallback.Call({buf}).ToNumber().Uint32Value();

                    callback({
                                     .bytesRead = bytesRead,
                                     .isEOF = bytesRead < target.size()
                             });
                });
    }

    Napi::ThreadSafeFunction tsfn;
};
}

template<>
loom::DataSourceInfo TypeConverter<loom::DataSourceInfo, Napi::Object>::operator()(Napi::Env &env,
                                                                                   const Napi::Object &value) {
    loom::DataSourceInfo dsi{};

    if (value.Has("length")) {
        auto val = value.Get("length");
        if (!val.IsNumber()) {
            throw loom::NotSupportedException("length attribute must be a number");
        }

        dsi.length = val.ToNumber().Uint32Value();
    }

    if (value.Has("accessType")) {
        auto val = value.Get("accessType");
        if (!val.IsNumber()) {
            throw loom::NotSupportedException("accessType attribute must be a number");
        }

        dsi.accessType = static_cast<loom::DataSourceInfo::AccessType>(val.ToNumber().Uint32Value());
    }

    if (value.Has("sizeHint")) {
        auto val = value.Get("sizeHint");
        if (!val.IsNumber()) {
            throw loom::NotSupportedException("sizeHint attribute must be a number");
        }

        dsi.sizeHint = static_cast<loom::DataSourceInfo::SizeHint>(val.ToNumber().Uint32Value());
    }

    if (value.Has("reader")) {
        auto val = value.Get("reader");
        if (!val.IsFunction()) {
            throw loom::NotSupportedException("reader attribute must be an object");
        }

        auto reader = std::make_shared<detail::JSReader>();
        reader->tsfn = Napi::ThreadSafeFunction::New(
                env,
                val.As<Napi::Function>(),
                "DataSourceReader",
                0,
                1);
    }

    return dsi;
}

template<>
Napi::Object TypeConverter<Napi::Object, loom::DataSourceInfo>::operator()(Napi::Env &env,
                                                                           const loom::DataSourceInfo &dsi) {
    auto result = Napi::Object::New(env);

    result["length"] = dsi.length;
    result["accessType"] = static_cast<std::underlying_type_t<decltype(dsi.accessType)> >(dsi.accessType);
    result["sizeHint"] = static_cast<std::underlying_type_t<decltype(dsi.sizeHint)> >(dsi.sizeHint);
    // Copying the reader from C++ side doesn't make any sense, so we initialize the object with null here.
    result["reader"] = env.Null();

    return result;
}

}
