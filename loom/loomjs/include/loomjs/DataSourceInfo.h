//
// Created by void on 08/06/2020.
//

#ifndef LOOMJS_DATASOURCEINFO_H
#define LOOMJS_DATASOURCEINFO_H

#include <loom/DataSource.h>
#include <loomjs/TypeConverter.h>

namespace loomjs {

Napi::Object exportDataSourceInfo(Napi::Env env, Napi::Object exports);

/**
 * Converts a JS object into a data source info.
 * @param value The JS object to convert.
 * @return The converted data source info.
 */
template<>
loom::DataSourceInfo TypeConverter<loom::DataSourceInfo, Napi::Object>::operator()(Napi::Env &env,
                                                                                   const Napi::Object &value);

/**
 * Converts a DataSourceInfo into a JS object.
 * @param dsi The data source info.
 * @return The data source info converted to a JS object.
 */
template<>
Napi::Object TypeConverter<Napi::Object, loom::DataSourceInfo>::operator()(Napi::Env &env,
                                                                           const loom::DataSourceInfo &dsi);

}

#endif //LOOMJS_DATASOURCEINFO_H
