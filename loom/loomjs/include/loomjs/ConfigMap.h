//
// Created by void on 06/06/2020.
//

#ifndef LOOMJS_CONFIGMAP_H
#define LOOMJS_CONFIGMAP_H

#include <loom/ConfigMap.h>

#include <loomjs/TypeConverter.h>

#include <napi.h>

namespace loomjs {

/**
 * Converts a JS object into a config map.
 * @param value The JS object to convert.
 * @return The converted config map.
 */
template<>
loom::ConfigMap TypeConverter<loom::ConfigMap, Napi::Object>::operator()(const Napi::Object &value);

/**
 * Converts a config map into a JS object.
 * @param env The environment to use for creating the results.
 * @param cm The config map to convert.
 * @return The converted config map as JS object.
 */
template<>
Napi::Object TypeConverter<Napi::Object, loom::ConfigMap>::operator()(Napi::Env &env, const loom::ConfigMap &cm);

/**
 * Converts a JS value into a config map value.
 * @param value The JS value to convert.
 * @return The converted config map value.
 */
template<>
loom::ConfigMapValue TypeConverter<loom::ConfigMapValue, Napi::Value>::operator()(const Napi::Value &value);

/**
 * Converts a config map value into a JS value.
 * @param env The environment to use for creating the result.
 * @param cv The config map value to convert.
 * @return The converted config map value as JS value.
 */
template<>
Napi::Value TypeConverter<Napi::Value, loom::ConfigMapValue>::operator()(Napi::Env &env,
                                                                         const loom::ConfigMapValue &cv);

}

#endif //LOOMJS_CONFIGMAP_H
