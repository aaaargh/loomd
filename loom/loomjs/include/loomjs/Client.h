//
// Created by void on 06/06/2020.
//

#ifndef LOOMJS_CLIENT_H
#define LOOMJS_CLIENT_H

#include <loom/client/Client.h>

#include <napi.h>

namespace loomjs {

/**
 * A Napi wrapper for the client that supports async and deferred operations.
 */
class Client : public Napi::ObjectWrap<Client> {
public:
    static Napi::Object Init(Napi::Env env, Napi::Object exports);

    explicit Client(const Napi::CallbackInfo &info);

    void asyncStart(const Napi::CallbackInfo &info);

    void stop(const Napi::CallbackInfo &info);

    Napi::Value authenticate(const Napi::CallbackInfo &info);

    Napi::Value querySubjects(const Napi::CallbackInfo &info);

    Napi::Value announceSubject(const Napi::CallbackInfo &info);

    Napi::Value subscribeToSubjects(const Napi::CallbackInfo &info);

private:
    std::unique_ptr<loom::Client> client;

    static Napi::FunctionReference constructor;
};

}

#endif //LOOMJS_CLIENT_H
