//
// Created by void on 07/06/2020.
//

#ifndef LOOMJS_TYPECONVERTER_H
#define LOOMJS_TYPECONVERTER_H

#include <loom/Exception.h>

#include <napi.h>
#include <sstream>

namespace loomjs {

#define LOOMJS_MAKE_FLAG(FlagType, Flag, obj, env) \
    obj["" # Flag] = Napi::Number::From(env, static_cast<std::underlying_type_t<FlagType> >(FlagType::Flag));

template<typename ToT, typename FromT>
struct TypeConverter {
private:
    [[nodiscard]] ToT throwNotSupportedException() const {
        std::stringstream ss;
        ss << "Type conversion from " << typeid(FromT).name() << " to " << typeid(ToT).name() << " is not supported.";
        throw loom::NotSupportedException(ss.str());
    }

public:
    [[nodiscard]] ToT operator()(const FromT &value) {
        return throwNotSupportedException();
    }

    [[nodiscard]] ToT operator()(Napi::Env &env, const FromT &value) {
        return throwNotSupportedException();
    }
};

template<typename ToT, typename FromT>
ToT convert(const FromT &from) {
    return TypeConverter<ToT, FromT>()(from);
}

template<typename ToT, typename FromT>
ToT convert(const FromT &from, Napi::Env &env) {
    return TypeConverter<ToT, FromT>()(env, from);
}

}

#endif //LOOMJS_TYPECONVERTER_H
