const loom = require(__dirname + "/../../../lib/loom");

const client = new loom.Client("localhost", 6788);

client.asyncStart(async () => {
    await client.authenticate({"uuid": "js client"});

    console.log("Subscribing");
    await client.subscribeToSubjects("(*)", (event) => {
        console.log("Subject event: ", event);
    });

    console.log("Announcing");
    await client.announceSubject({"uuid": "hello", "realm": "default"});

    console.log("Querying");
    const results = await client.querySubjects("(realm=*)", loom.SubjectViewMode.ResolveFully);

    console.log("Results:", results);
    client.stop();
}, () => {
    console.log("Shutdown complete");
});