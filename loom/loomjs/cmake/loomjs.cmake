macro(loomjs_setup_project_defaults)
    # This is for out-of-tree-builds only. When building the module using cmake-js directly, this is not being used at all.
    if (NOT CMAKE_JS_VERSION)
        message(STATUS "Invoking cmake-js to get the correct configuration")

        configure_file(package.json package.json COPYONLY)
        configure_file(package-lock.json package-lock.json COPYONLY)

        find_program(NPM_BIN "npm")

        if (NOT NPM_BIN)
            message(FATAL_ERROR "Unable to find npm")
        endif ()

        if (NOT EXISTS ${CMAKE_CURRENT_BINARY_DIR}/node_modules)
            message(STATUS "node_modules is missing, calling npm install")
            execute_process(COMMAND "${NPM_BIN}" install
                    WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR})
        endif ()

        execute_process(
                COMMAND "${NPM_BIN}" bin
                WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
                OUTPUT_VARIABLE NPM_BIN_PREFIX
                OUTPUT_STRIP_TRAILING_WHITESPACE)

        message(STATUS "NPM bin path is set to ${NPM_BIN_PREFIX}")
        unset(CMAKE_JS_BIN CACHE)
        find_program(CMAKE_JS_BIN "cmake-js" HINTS ${NPM_BIN_PREFIX})

        if (NOT CMAKE_JS_BIN)
            message(FATAL_ERROR "Unable to find cmake-js")
        endif ()

        message(STATUS "cmake-js found at ${CMAKE_JS_BIN}")

        execute_process(
                COMMAND ${CMAKE_JS_BIN} print-configure
                WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
                OUTPUT_VARIABLE CMAKE_JS_CONFIGURE
                OUTPUT_STRIP_TRAILING_WHITESPACE
        )

        message(STATUS "${CMAKE_JS_CONFIGURE}")

        string(REGEX MATCHALL "-D(NODE|CMAKE_JS)([A-Z0-9_]+)=\"[^\"]*\"" CMAKE_JS_DEFINITION_ARGS "${CMAKE_JS_CONFIGURE}")
        string(REPLACE ";" "\\\;" CMAKE_JS_DEFINITION_ARGS "${CMAKE_JS_DEFINITION_ARGS}")
        string(REPLACE "\\\;-D" ";" CMAKE_JS_DEFINITION_ARGS "\\\;${CMAKE_JS_DEFINITION_ARGS}")

        foreach (CMAKE_JS_DEFINITION_ARG ${CMAKE_JS_DEFINITION_ARGS})
            string(REPLACE ";" "\\\;" CMAKE_JS_DEFINITION_LIST ${CMAKE_JS_DEFINITION_ARG})
            string(REPLACE "=" ";" CMAKE_JS_DEFINITION_LIST ${CMAKE_JS_DEFINITION_LIST})
            list(GET CMAKE_JS_DEFINITION_LIST 0 VAR_NAME)
            list(GET CMAKE_JS_DEFINITION_LIST 1 VAR_VALUE)
            string(REPLACE "\"" "" VAR_VALUE "${VAR_VALUE}")
            set(${VAR_NAME} "${VAR_VALUE}")
            message(STATUS "${VAR_NAME} := ${${VAR_NAME}}")
        endforeach ()
    endif ()

    execute_process(COMMAND node -p "require('node-addon-api').include"
            WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
            OUTPUT_VARIABLE NODE_ADDON_API_DIR
            )
    string(REPLACE "\n" "" NODE_ADDON_API_DIR ${NODE_ADDON_API_DIR})
    string(REPLACE "\"" "" NODE_ADDON_API_DIR ${NODE_ADDON_API_DIR})

    message(STATUS "NODE_ADDON_API_DIR := ${NODE_ADDON_API_DIR}")

endmacro()