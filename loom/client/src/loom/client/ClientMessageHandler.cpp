//
// Created by void on 16/04/2020.
//

#include <loom/client/ClientMessageHandler.h>

namespace loom::client {

void ClientMessageHandler::processReceived(BasicClient &client, std::unique_ptr<net::Message> message) {
    // Ignore.
}

void ClientMessageHandler::processSent(BasicClient &client, const net::Message &message) {
    // Ignore.
}

}