//
// Created by void on 19/05/2020.
//

#include <loom/client/Client.h>
#include <loom/Exception.h>

namespace loom {

void Client::setEventListener(std::unique_ptr<ClientEventListener> listener) {
    eventListener = std::move(listener);
}

bool Client::hasEventListener() const {
    return eventListener.operator bool();
}

ClientEventListener &Client::getEventListener() const {
    if (!eventListener) {
        throw NotSupportedException("No event listener registered.");
    }
    return *eventListener;
}

}
