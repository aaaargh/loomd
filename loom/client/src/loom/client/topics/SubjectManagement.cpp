//
// Created by void on 05/05/2020.
//

#include <loom/client/topics/SubjectManagement.h>
#include <loom/client/BasicClient.h>

namespace loom::client::topics {

const net::MessageSerializer &SubjectAnnouncementHandler::getTypedSerializer() const {
    static const net::topics::SubjectAnnouncementSerializer serializer;
    return serializer;
}

const net::MessageSerializer &SubjectDropHandler::getTypedSerializer() const {
    static const net::topics::SubjectDropSerializer serializer;
    return serializer;
}

const net::MessageSerializer &SubjectQueryHandler::getTypedSerializer() const {
    static const net::topics::SubjectQuerySerializer serializer;
    return serializer;
}

const net::MessageSerializer &SubjectQueryResultsHandler::getTypedSerializer() const {
    static const net::topics::SubjectQueryResultsSerializer serializer;
    return serializer;
}

bool DefaultSubjectQueryHandler::canProcess(net::Message::id_type type) const {
    return SubjectQueryResultsHandler::canProcess(type) || SubjectQueryHandler::canProcess(type);
}

bool DefaultSubjectQueryHandler::canSerialize(net::Message::id_type type) const {
    return SubjectQueryHandler::canSerialize(type);
}

bool DefaultSubjectQueryHandler::canDeserialize(net::Message::id_type type) const {
    return SubjectQueryResultsHandler::canDeserialize(type);
}

const net::MessageSerializer &DefaultSubjectQueryHandler::getSerializer(net::Message::id_type type) const {
    if (type == results_message_type::type) {
        return SubjectQueryResultsHandler::getTypedSerializer();
    } else {
        return SubjectQueryHandler::getTypedSerializer();
    }
}

void DefaultSubjectQueryHandler::processReceived(BasicClient &client, std::unique_ptr<net::Message> message) {
    SubjectQueryResultsHandler::processReceived(client, std::move(message));
}

void DefaultSubjectQueryHandler::processSent(BasicClient &client, const net::Message &message) {
    SubjectQueryHandler::processSent(client, message);
}

void DefaultSubjectQueryHandler::processSentTyped(BasicClient &client,
                                                  const request_message_type &msg) {
    pendingQueries.insert(std::make_pair(msg.query, msg.resultsCallback));
}

void DefaultSubjectQueryHandler::processReceivedTyped(BasicClient &client,
                                                      std::unique_ptr<results_message_type> message) {
    auto &msg = *message;
    auto range = pendingQueries.equal_range(msg.query);

    for (auto it = range.first; it != range.second; ++it) {
        it->second(msg.results);
    }

    pendingQueries.erase(range.first, range.second);
}

size_t DefaultSubjectQueryHandler::getPriority() const {
    return 9999;
}

}
