//
// Created by void on 05/05/2020.
//

#include <loom/client/topics/Authentication.h>
#include <loom/client/BasicClient.h>

namespace loom::client::topics {

/**
 * Common stuff.
 */

const net::MessageSerializer &AuthResponseHandler::getTypedSerializer() const {
    const static net::topics::AuthResponseSerializer serializer;
    return serializer;
}

const net::MessageSerializer &AuthRequestHandler::getTypedSerializer() const {
    const static net::topics::AuthRequestSerializer serializer;
    return serializer;
}

void DefaultAuthResponseHandler::processReceivedTyped(BasicClient &client,
                                                      std::unique_ptr<net::topics::AuthResponse> message) {
    if (!client.hasEventListener()) {
        return;
    }

    if (message->error.empty()) {
        // Success
        client.getEventListener().onAuthenticated(client, message->identity);
    } else {
        // Failed
        client.getEventListener().onAuthenticationFailed(client, message->error);
    }
}

size_t DefaultAuthResponseHandler::getPriority() const {
    return 9999;
}
}
