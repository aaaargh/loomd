//
// Created by void on 05/05/2020.
//

#include <loom/client/topics/SubjectEvents.h>
#include <loom/client/BasicClient.h>

#include <loguru.hpp>

namespace loom::client::topics {

/**
 * Daemon -> Client stuff
 */

void SubjectAnnouncedEventHandler::processReceivedTyped(BasicClient &client,
                                                        std::unique_ptr<handler_message_type> message) {
    handleSubjectAnnounced(client, message->subjectId, message->isUpdated);
}

void DummySubjectAnnouncedEventHandler::handleSubjectAnnounced(BasicClient &client, const ConfigMap &subjectId,
                                                               bool isUpdated) {
    if (client.hasEventListener()) {
        client.getEventListener().onSubjectAnnounced(client, subjectId, isUpdated);
        return;
    }

    LOG_S(WARNING) << "Missing SubjectAnnouncedEventHandler or client event listener. Did you forget to register it?";
}

size_t DummySubjectAnnouncedEventHandler::getPriority() const {
    return 9999;
}

void SubjectDroppedEventHandler::processReceivedTyped(BasicClient &client,
                                                      std::unique_ptr<handler_message_type> message) {
    handleSubjectDropped(client, message->subjectId);
}

void DummySubjectDroppedEventHandler::handleSubjectDropped(BasicClient &client, const ConfigMap &subjectId) {
    if (client.hasEventListener()) {
        client.getEventListener().onSubjectDropped(client, subjectId);
        return;
    }

    LOG_S(WARNING) << "Missing SubjectDroppedEventHandler or client event listener. Did you forget to register it?";
}

size_t DummySubjectDroppedEventHandler::getPriority() const {
    return 9999;
}

/**
 * Common stuff
 */

const net::MessageSerializer &SubjectSubscriptionHandler::getTypedSerializer() const {
    static const net::topics::SubjectSubscriptionSerializer serializer;
    return serializer;
}

const net::MessageSerializer &SubjectAnnouncedEventHandler::getTypedSerializer() const {
    static const net::topics::SubjectAnnouncedEventSerializer serializer;
    return serializer;
}

const net::MessageSerializer &SubjectDroppedEventHandler::getTypedSerializer() const {
    static const net::topics::SubjectDroppedEventSerializer serializer;
    return serializer;
}

}

