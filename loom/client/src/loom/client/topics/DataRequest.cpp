//
// Created by void on 29/04/2020.
//

#include <loom/client/topics/DataRequest.h>
#include <loom/client/BasicClient.h>

#include <loguru.hpp>

namespace loom::client::topics {

size_t DefaultDataHandler::getPriority() const {
    return 9999;
}

void DefaultDataHandler::processReceivedTyped(BasicClient &client,
                                              std::unique_ptr<net::topics::DataRequest> message) {
    auto &msg = *message;
    msg.subjectId.normalize({"uuid", "realm"});

    DLOG_S(INFO) << "Client received a data request for " << msg.subjectId << ":" << msg.attribute << ", id := "
                 << msg.requestId;

    // Get the associated data source reader.
    auto reader = client.getDataSourceReader(ConfigMapAttributeKey(msg.subjectId, msg.attribute));

    if (reader) {
        auto chunkReader = std::make_unique<ChunkReader>(client, *this,
                                                         ConfigMapAttributeKey(
                                                                 std::move(msg.subjectId),
                                                                 std::move(msg.attribute)),
                                                         reader,
                                                         MaxSendChunkSize,
                                                         msg.offset, msg.len);
        auto &chunkReaderRef = *chunkReader;
        activeReaders.emplace(std::move(chunkReader));
        chunkReaderRef.start();

        return;
    } else {
        DLOG_S(WARNING) << "No reader defined in data source, handing over to client handler";
    }
}

DefaultDataHandler::ChunkReader::ChunkReader(BasicClient &client,
                                             DefaultDataHandler &handler,
                                             ConfigMapAttributeKey key,
                                             std::shared_ptr<DataSourceReader> reader,
                                             size_t maxChunkSize,
                                             size_t offset, size_t len) :
        loom::ChunkReader(std::move(reader), maxChunkSize, offset, len),
        handler(handler), client(client), key(std::move(key)) {}

void DefaultDataHandler::ChunkReader::handleChunkRead(ImmutableBuffer buffer, size_t offset,
                                                      const ChunkReader::ReadDataResult &result) {
    // Forward the chunk to the daemon.
    auto response = std::make_unique<net::topics::DataResponse>();
    response->subjectId = key.configMap;
    response->attribute = key.attribute;
    response->offset = offset;
    response->data.resize(buffer.size());
    std::memcpy(response->data.data(), buffer.data(), buffer.size());
    response->isEOF = result.isEOF;

    client.sendMessage(std::move(response), {}, [this](const auto &err) { handleError(err); });
}

void DefaultDataHandler::ChunkReader::handleComplete() {
    remove();
}

void DefaultDataHandler::ChunkReader::handleInterrupted() {
    remove();
}

void DefaultDataHandler::ChunkReader::handleError(const std::exception_ptr &error) {
    remove();
}

void DefaultDataHandler::ChunkReader::remove() {
    auto it = std::find_if(handler.activeReaders.begin(), handler.activeReaders.end(), [this](const auto &reader) {
        return reader.get() == this;
    });

    handler.activeReaders.erase(it);
}

void DefaultDataHandler::processReceivedTyped(BasicClient &client,
                                              std::unique_ptr<response_message_type> message) {
    auto &msg = *message;
    msg.subjectId.normalize({"uuid", "realm"});

    DLOG_S(INFO) << "Client received a data response for " << msg.subjectId << ":" << msg.attribute << ", id := "
                 << msg.requestId << " (" << msg.data.size() << " byte(s) @offset " << msg.offset << ", eof := "
                 << msg.isEOF << ")";

    const auto &key = ConfigMapAttributeKey(std::move(msg.subjectId), std::move(msg.attribute));

    size_t affectedBlocks = dataBlockReceiver.pushReceivedData(
            key,
            msg.offset,
            ImmutableBuffer(msg.data.data(), msg.data.size()));

    if (msg.isEOF && !msg.data.empty()) {
        // Push an explicit EOF if the message also contained data.
        affectedBlocks += dataBlockReceiver.pushReceivedData(
                key,
                msg.offset + msg.data.size(),
                NullBuffer
        );
    }

    if (affectedBlocks > 0) {
        return;
    }

    DLOG_S(WARNING) << "No open block writer defined for data source, handing over to client handler";
}

void DefaultDataHandler::processSentTyped(BasicClient &client, const net::topics::DataRequest &msg) {
    if (msg.writer) {
        dataBlockReceiver.openBlock(ConfigMapAttributeKey(msg.subjectId, msg.attribute),
                                    msg.offset, msg.len,
                                    msg.writer);
    } else {
        LOG_S(ERROR) << "No writer has been set for data request.";
    }
}

bool DefaultDataHandler::canProcess(net::Message::id_type type) const {
    return DataRequestHandler::canProcess(type) || DataResponseHandler::canProcess(type);
}

bool DefaultDataHandler::canSerialize(net::Message::id_type type) const {
    return DataRequestHandler::canSerialize(type) || DataResponseHandler::canSerialize(type);
}

bool DefaultDataHandler::canDeserialize(net::Message::id_type type) const {
    return DataRequestHandler::canDeserialize(type) || DataResponseHandler::canDeserialize(type);
}

const net::MessageSerializer &DefaultDataHandler::getSerializer(net::Message::id_type type) const {
    if (type == request_message_type::type) {
        return DataRequestHandler::getSerializer(type);
    } else {
        return DataResponseHandler::getSerializer(type);
    }
}

void DefaultDataHandler::processReceived(BasicClient &client, std::unique_ptr<net::Message> message) {
    if (message->getType() == request_message_type::type) {
        DataRequestHandler::processReceived(client, std::move(message));
    } else {
        DataResponseHandler::processReceived(client, std::move(message));
    }
}

void DefaultDataHandler::processSent(BasicClient &client, const net::Message &message) {
    if (message.getType() == request_message_type::type) {
        DataRequestHandler::processSent(client, message);
    } else {
        DataResponseHandler::processSent(client, message);
    }
}

const net::MessageSerializer &DataRequestHandler::getTypedSerializer() const {
    const static net::topics::DataRequestSerializer serializer;
    return serializer;
}

const net::MessageSerializer &DataResponseHandler::getTypedSerializer() const {
    const static net::topics::DataResponseSerializer serializer;
    return serializer;
}

}
