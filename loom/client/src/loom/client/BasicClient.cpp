//
// Created by void on 16/04/2020.
//

#include <loom/client/BasicClient.h>
#include <loom/client/topics/DataRequest.h>

#include "DefaultMessageHandlers.h"

#include <utility>

#include <loguru.hpp>

namespace loom::client {

BasicClient::BasicClient(bool withDefaultHandlers) {
    if (withDefaultHandlers) {
        DefaultMessageHandlers::registerTo(*this);
    }
}

void BasicClient::querySubjects(const std::string &query,
                                SubjectViewMode viewMode,
                                SubjectQueryFlags flags,
                                const SubjectQueryResultsCallback &callback,
                                const SendMessageErrorCallback &errorCallback) {
    auto msg = std::make_unique<net::topics::SubjectQuery>();
    msg->query = query;
    msg->resultsCallback = callback;
    msg->viewMode = viewMode;
    msg->flags = flags;

    sendMessage(std::move(msg), DefaultSendMessageSuccessCallback, errorCallback);
}

void BasicClient::subscribeToSubjects(const std::string &filter,
                                      SubjectSubscriptionFlags flags,
                                      const SendMessageSuccessCallback &successCallback,
                                      const SendMessageErrorCallback &errorCallback) {
    auto msg = std::make_unique<net::topics::SubjectSubscription>();
    msg->filter = filter;
    msg->flags = flags;
    sendMessage(std::move(msg), successCallback, errorCallback);
}

void BasicClient::announceSubject(const ConfigMap &configMap,
                                  SubjectAnnouncementFlags flags,
                                  const SendMessageSuccessCallback &successCallback,
                                  const SendMessageErrorCallback &errorCallback) {
    auto msg = std::make_unique<net::topics::SubjectAnnouncement>();
    msg->subject = configMap;
    msg->flags = flags;

    auto subjectId = configMap.normalized({"uuid", "realm"});

    for (const auto &entry : configMap) {
        std::visit([&, this](const auto &v) {
            using T = std::decay_t<decltype(v)>;
            if constexpr(std::is_same_v<T, DataSourceInfo>) {
                if (!v.reader) {
                    LOG_S(WARNING) << "No reader has been defined for attribute " << entry.first;
                }

                LOG_S(INFO) << "Registering new data source reader.";

                dataSourceReaders.emplace(std::make_pair(ConfigMapAttributeKey(subjectId, entry.first), v.reader));
            }
        }, entry.second);
    }

    sendMessage(std::move(msg), successCallback, errorCallback);
}

void BasicClient::dropSubject(const ConfigMap &subjectId,
                              const SendMessageSuccessCallback &successCallback,
                              const SendMessageErrorCallback &errorCallback) {
    auto msg = std::make_unique<net::topics::SubjectDrop>();
    msg->subjectId = subjectId;

    for (auto it = dataSourceReaders.begin(); it != dataSourceReaders.end(); ++it) {
        if (subjectId ^= it->first.configMap) {
            continue;
        }

        LOG_S(INFO) << "Dropping associated data source reader.";
        dataSourceReaders.erase(it);
        break;
    }

    sendMessage(std::move(msg), successCallback, errorCallback);
}

void BasicClient::authenticate(const ConfigMap &identity,
                               const SendMessageSuccessCallback &successCallback,
                               const SendMessageErrorCallback &errorCallback) {
    auto msg = std::make_unique<net::topics::AuthRequest>();
    msg->identity = identity;
    sendMessage(std::move(msg), successCallback, errorCallback);
}

void BasicClient::requestData(const ConfigMap &subjectId,
                              const ConfigMap::key_type &attribute,
                              size_t offset,
                              size_t len,
                              std::shared_ptr<DataSourceWriter> writer,
                              const SendMessageSuccessCallback &successCallback,
                              const SendMessageErrorCallback &errorCallback) {
    auto msg = std::make_unique<net::topics::DataRequest>();
    msg->requestId = nextDataRequestId++;
    msg->subjectId = subjectId.normalized({"uuid", "realm"});
    msg->attribute = attribute;
    msg->offset = offset;
    msg->len = len;

    msg->writer = writer;

    sendMessage(std::move(msg), successCallback, errorCallback);
}

void BasicClient::sendMessage(std::unique_ptr<net::Message> message,
                              const SendMessageSuccessCallback &successCallback,
                              const SendMessageErrorCallback &errorCallback) {
    getProcessingHandler(message->getType()).processSent(*this, *message);
    getProtocolHandler().sendMessage(std::move(message), successCallback, errorCallback);
}

void BasicClient::handleMessageReceived(std::unique_ptr<net::Message> message) {
    auto &processingHandler = getProcessingHandler(message->getType());
    processingHandler.processReceived(*this, std::move(message));
}

void BasicClient::handleDisconnect(const std::exception_ptr &error) {
    if (hasEventListener()) {
        getEventListener().onConnectionLost(*this, error);
    }
}

const net::MessageHandlerInvoker &BasicClient::getMessageHandlerInvoker() const {
    return *this;
}

std::shared_ptr<DataSourceReader> BasicClient::getDataSourceReader(const ConfigMapAttributeKey &key) const {
    auto it = dataSourceReaders.find(key);
    if (it != dataSourceReaders.end()) {
        return it->second;
    }

    throw NotFoundException("No reader found for given data source");
}
}
