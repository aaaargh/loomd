//
// Created by void on 16/04/2020.
//

#include <loom/client/ClientTCP.h>

#include <boost/asio/connect.hpp>
#include <boost/asio/write.hpp>

#include <utility>
#include <loguru.hpp>

using namespace boost::asio::ip;

namespace loom::client {

ClientTCP::ClientTCP(boost::asio::io_context &ioContext,
                     const std::string &daemonAddress, unsigned short daemonPort,
                     bool withDefaultHandlers) :
        BasicClient(withDefaultHandlers),
        ioContext(ioContext),
        query(daemonAddress, std::to_string(daemonPort), tcp::resolver::query::canonical_name) {}

ClientTCP::~ClientTCP() {
    if (protocolHandler) {
        protocolHandler->notifyClosed();
    }
}

void ClientTCP::connect() {
    protocolHandler = std::make_shared<protocol_handler_type>(*this, ioContext);

    tcp::resolver resolver(ioContext);
    auto const results = resolver.resolve(query);
    boost::asio::connect(protocolHandler->getSocket(), results.begin(), results.end());

    protocolHandler->notifyConnected();
}

net::ProtocolHandler &ClientTCP::getProtocolHandler() const {
    return *protocolHandler;
}

}
