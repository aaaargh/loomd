//
// Created by void on 16/04/2020.
//

#ifndef LOOM_NET_CLIENT_DEFAULTMESSAGEHANDLERS_H
#define LOOM_NET_CLIENT_DEFAULTMESSAGEHANDLERS_H

#include <loom/client/BasicClient.h>

#include <loom/client/topics/Authentication.h>
#include <loom/client/topics/DataRequest.h>
#include <loom/client/topics/SubjectEvents.h>
#include <loom/client/topics/SubjectManagement.h>

namespace loom::client {

/**
 * A static class that registers the default message handlers for a client.
 */
class DefaultMessageHandlers {
public:
    DefaultMessageHandlers() = delete;

public:
    /**
     * Registers the default message handlers to the given client.
     * @param daemon The client instance to register the handlers to.
     */
    static void registerTo(BasicClient &client) {
        client.registerMessageHandler<topics::AuthRequestHandler>();
        client.registerMessageHandler<topics::DefaultAuthResponseHandler>();

        client.registerMessageHandler<topics::DefaultDataHandler>();

        client.registerMessageHandler<topics::SubjectSubscriptionHandler>();
        client.registerMessageHandler<topics::DummySubjectAnnouncedEventHandler>();
        client.registerMessageHandler<topics::DummySubjectDroppedEventHandler>();

        client.registerMessageHandler<topics::SubjectAnnouncementHandler>();
        client.registerMessageHandler<topics::SubjectDropHandler>();

        client.registerMessageHandler<topics::DefaultSubjectQueryHandler>();
    }
};

}

#endif //LOOM_NET_CLIENT_DEFAULTMESSAGEHANDLERS_H
