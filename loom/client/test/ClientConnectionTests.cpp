//
// Created by void on 06/06/2020.
//

#include <loom/testing.h>

#include <loom/daemon/DaemonTCP.h>
#include <loom/client/ClientTCP.h>

#include "TestCommon.h"

using namespace loom::testing;

namespace loom::net::testing {
BOOST_AUTO_TEST_SUITE(ClientConnectionTests)

BOOST_AUTO_TEST_CASE(can_detect_disconnection) {
    boost::asio::io_context ctx;
    engine::Engine engine(ctx);

    struct : daemon::DaemonTCP {
        using DaemonTCP::DaemonTCP;
        std::shared_ptr<daemon::Peer> connectedPeer;

        void handlePeerConnected(const std::shared_ptr<daemon::Peer> &peer) override {
            connectedPeer = peer;
            DaemonTCP::handlePeerConnected(peer);
        }

        void closeConnection() {
            connectedPeer->disconnect();
            connectedPeer.reset();
        }
    } daemon(engine, LOOM_TEST_DAEMON_PORT);

    auto client = std::make_unique<loom::client::ClientTCP>(ctx, "localhost", LOOM_TEST_DAEMON_PORT);

    struct Listener : ClientEventListener {
        bool disconnected = false;

        void onConnectionLost(Client &client, const std::exception_ptr &error) override {
            disconnected = true;
        }
    };

    auto listener = std::make_unique<Listener>();
    auto &listenerRef = *listener;
    client->setEventListener(std::move(listener));

    client->connect();

    ctx.poll();

    BOOST_REQUIRE(daemon.connectedPeer);

    daemon.closeConnection();

    ctx.poll();

    BOOST_REQUIRE(listenerRef.disconnected);
}

BOOST_AUTO_TEST_SUITE_END()
}