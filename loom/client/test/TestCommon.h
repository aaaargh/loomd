//
// Created by void on 21/04/2020.
//

#ifndef LOOM_NET_TESTCOMMON_H

#include <loom/testing.h>

#include <loom/daemon/DaemonTCP.h>

#ifndef LOOM_TEST_DAEMON_PORT
#define LOOM_TEST_DAEMON_PORT loom::daemon::DaemonTCP::DefaultPort
#endif

#endif //LOOM_NET_TESTCOMMON_H
