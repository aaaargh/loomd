//
// Created by void on 16/04/2020.
//

#include <loom/testing.h>

#include <loom/daemon/DaemonTCP.h>
#include <loom/client/ClientTCP.h>

#include "TestCommon.h"

#include <loguru.hpp>

using namespace loom::testing;

namespace loom::net::testing {
BOOST_AUTO_TEST_SUITE(TCPCommunicationTests)

class TestMessage : public TypedMessage<0x1234> {
public:
    int payloadInt = 42;
};

class TestSerializer : public StaticMessageSerializer<
        message::MemberPart<&TestMessage::payloadInt>
> {
};

class DaemonTestMessageHandler : public daemon::TypedDaemonMessageHandler<TestMessage> {
public:
    mutable std::vector<std::unique_ptr<TestMessage> > received;

public:
    [[nodiscard]] const MessageSerializer &getTypedSerializer() const override {
        const static TestSerializer serializer;
        return serializer;
    }

    void processTypedMessage(const std::shared_ptr<daemon::Peer> &peer,
                             std::unique_ptr<TestMessage> message) const override {
        received.emplace_back(std::move(message));
    }
};

class ClientTestMessageHandler : public client::TypedClientMessageHandler<TestMessage> {
public:
    [[nodiscard]] const MessageSerializer &getTypedSerializer() const override {
        const static TestSerializer serializer;
        return serializer;
    }
};

BOOST_AUTO_TEST_CASE(client_can_connect) {
        // Fire up the daemon.
        boost::asio::io_context ctx;
        engine::Engine engine(ctx);
        struct : loom::daemon::DaemonTCP {
            using DaemonTCP::DaemonTCP;

            bool peerConnected = false;

            void handlePeerConnected(const std::shared_ptr<loom::daemon::Peer> &peer)
            override{
                    DaemonTCP::handlePeerConnected(peer);
                    peerConnected = true;
            }
        } daemon(engine, LOOM_TEST_DAEMON_PORT);

        // Create the client.
        std::stringstream remoteAddr;
        auto client = std::make_unique<loom::client::ClientTCP>(ctx, "localhost", LOOM_TEST_DAEMON_PORT);

        client->connect();

        pollAndAwait(daemon.getEngine().getIOContext(), daemon.peerConnected);

        BOOST_REQUIRE(daemon.peerConnected);
}

BOOST_AUTO_TEST_CASE(client_can_disconnect) {
        // Fire up the daemon.
        boost::asio::io_context ctx;
        engine::Engine engine(ctx);
        struct : loom::daemon::DaemonTCP {
            using DaemonTCP::DaemonTCP;

            bool peerDisconnected = false;

            void handlePeerDisconnected(loom::daemon::Peer &peer)
            override{
                    peerDisconnected = true;
            }
        } daemon(engine, LOOM_TEST_DAEMON_PORT);

        // Create the client.
        std::stringstream remoteAddr;
        auto client = std::make_unique<loom::client::ClientTCP>(ctx, "localhost", LOOM_TEST_DAEMON_PORT);

        client->connect();

        client.reset();

        loom::testing::pollAndAwait(daemon.getEngine().getIOContext(), daemon.peerDisconnected);

        BOOST_REQUIRE(daemon.peerDisconnected);
}

BOOST_AUTO_TEST_CASE(client_can_send_message) {
        // Fire up the daemon.
        boost::asio::io_context ctx;
        engine::Engine engine(ctx);
        daemon::DaemonTCP daemon(engine, LOOM_TEST_DAEMON_PORT);

        daemon.registerMessageHandler<DaemonTestMessageHandler>();

        bool sendMessageComplete = false;

        // Create the client.
        std::stringstream remoteAddr;
        auto client = std::make_unique<loom::client::ClientTCP>(ctx, "localhost", LOOM_TEST_DAEMON_PORT, false);
        client->registerMessageHandler<ClientTestMessageHandler>();

        client->connect();

        client->sendMessage(std::make_unique<TestMessage>(),[&] {
            sendMessageComplete = true;
        },[](const auto &err) {
            std::rethrow_exception(err);
        });

        ctx.poll();

        loom::testing::pollAndAwait(daemon.getEngine().getIOContext(), sendMessageComplete);

        BOOST_REQUIRE(sendMessageComplete);
}

BOOST_AUTO_TEST_CASE(client_can_send_multiple_messages_at_once) {
        // Fire up the daemon.
        boost::asio::io_context ctx;
        engine::Engine engine(ctx);
        daemon::DaemonTCP daemon(engine, LOOM_TEST_DAEMON_PORT);

        auto &daemonHandler = daemon.registerMessageHandler<DaemonTestMessageHandler>();
        auto &ioContext = daemon.getEngine().getIOContext();

        const size_t toSend = 32;
        size_t sent = 0;

        // Create the client.
        std::stringstream remoteAddr;
        auto client = std::make_unique<loom::client::ClientTCP>(ctx, "localhost", LOOM_TEST_DAEMON_PORT, false);
        client->registerMessageHandler<ClientTestMessageHandler>();

        client->connect();

        for (size_t i = 0; i < toSend; ++i) {
            client->sendMessage(std::make_unique<TestMessage>(), [&] {
                sent++;
            }, [](const auto &err) {
                std::rethrow_exception(err);
            });
            ctx.poll();
        }

        loom::testing::await([&] {
            ioContext.poll();
            return daemonHandler.received.size() == toSend;
        });

        BOOST_REQUIRE_EQUAL(toSend, sent);
        BOOST_REQUIRE_EQUAL(toSend, daemonHandler.received.size());
}

BOOST_AUTO_TEST_SUITE_END()
}