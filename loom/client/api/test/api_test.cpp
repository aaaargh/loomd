//
// Created by void on 19/05/2020.
//

#include <loom/System.h>
#include <iostream>

int main() {
    auto client = loom::System::get().createTCPClient("localhost", 6788);

    client->authenticate({{"id", "client"}}, {}, [](const auto &error) {
        std::rethrow_exception(error);
    });

    client->querySubjects(
            "(*)",
            loom::SubjectViewMode::ResolveFully,
            loom::SubjectQueryFlags::Defaults,
            [](const auto &results) {
                std::cout << "Found " << results.size() << " result(s)." << std::endl;
            }, [](const auto &error) {
                std::rethrow_exception(error);
            });

    loom::System::get().messagePump();
    return 0;
}