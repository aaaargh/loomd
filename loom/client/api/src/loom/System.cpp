//
// Created by void on 19/05/2020.
//

#include <loom/System.h>

#include <loom/client/ClientTCP.h>

#include <boost/asio/signal_set.hpp>

#include <loguru.hpp>

namespace loom {

std::unique_ptr<System> System::singleton{};

struct System::Context {
    boost::asio::io_context ioContext{};
};

System::System() : context(std::make_unique<Context>()) {}

std::unique_ptr<Client> loom::System::createTCPClient(const std::string &daemonAddress,
                                                      unsigned short daemonPort) {
    auto client = std::make_unique<client::ClientTCP>(context->ioContext, daemonAddress, daemonPort);
    client->connect();
    return std::move(client);
}

void System::messagePump() {
    boost::asio::signal_set signals(context->ioContext, SIGINT, SIGTERM);
    signals.async_wait([&](const auto &ec, int sig) {
        LOG_S(INFO) << "Received signal " << sig << ", gracefully shutting down system.";
        context->ioContext.stop();
    });

    LOG_S(INFO) << "Entering system event loop.";

    context->ioContext.run();

    LOG_S(INFO) << "System shutdown complete.";
}

System &System::get() {
    if (!singleton) {
        singleton = std::make_unique<System>();
    }

    return *singleton;
}

void System::stop() {
    if (!context->ioContext.stopped()) {
        LOG_S(INFO) << "Interrupting system event loop.";
        context->ioContext.stop();
    }
}

}