# Shared embeddable client API
include(GNUInstallDirs)

project(loom-client-api)

if (LOOM_API_BUILD_SHARED OR LOOM_API_BUILD_STATIC)

    set(LOOM_CLIENT_VERSION_MAJOR 1 CACHE STRING "loom client major version" FORCE)
    set(LOOM_CLIENT_VERSION_MINOR 0 CACHE STRING "loom client minor version" FORCE)
    set(LOOM_CLIENT_VERSION ${LOOM_CLIENT_VERSION_MAJOR}.${LOOM_CLIENT_VERSION_MINOR} CACHE STRING "loom client version" FORCE)

    # libLoomClient objects configuration
    add_library(LoomClient-obj
            OBJECT
            src/loom/System.cpp
            include/loom/System.h
            )
    set_property(TARGET LoomClient-obj PROPERTY POSITION_INDEPENDENT_CODE ON)

    target_link_libraries(LoomClient-obj PUBLIC loom-client)

    set_target_properties(LoomClient-obj
            PROPERTIES
            CXX_STANDARD 17
            CXX_STANDARD_REQUIRED YES
            CXX_EXTENSIONS NO
            POSITION_INDEPENDENT_CODE ON
            )

    target_include_directories(LoomClient-obj
            PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/include)

    # libLoomClient shared target
    if (LOOM_API_BUILD_SHARED)
        add_library(Client
                SHARED
                $<TARGET_OBJECTS:LoomClient-obj>
                )

        set_target_properties(Client PROPERTIES OUTPUT_NAME "LoomClient")

        target_link_libraries(Client PUBLIC
                $<BUILD_INTERFACE:loom-client>
                )

        target_include_directories(Client PUBLIC
                $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
                $<INSTALL_INTERFACE:include>
                )

        add_library(Loom::Client ALIAS Client)
        list(APPEND TARGETS_TO_EXPORT Client)
    endif ()

    # libLoomClient static target
    if (LOOM_API_BUILD_STATIC)
        add_library(Client-static
                STATIC
                $<TARGET_OBJECTS:LoomClient-obj>
                )

        set_target_properties(Client-static PROPERTIES OUTPUT_NAME "LoomClient-static")

        target_link_libraries(Client-static PUBLIC
                $<BUILD_INTERFACE:loom-client>
                )

        target_include_directories(Client-static PUBLIC
                $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
                $<INSTALL_INTERFACE:include>
                )

        add_library(Loom::Client-static ALIAS Client-static)
        list(APPEND TARGETS_TO_EXPORT Client-static)
    endif ()

    install(TARGETS ${TARGETS_TO_EXPORT}
            EXPORT LoomClientTargets
            ARCHIVE DESTINATION ${CMAKE_INSTALL_LIBDIR}
            )

    # Install the targets
    install(EXPORT LoomClientTargets
            DESTINATION ${CMAKE_INSTALL_LIBDIR}/cmake/LoomClient
            NAMESPACE Loom::
            )

    include(CMakePackageConfigHelpers)
    configure_package_config_file(
            LoomClientConfig.cmake.in
            LoomClientConfig.cmake
            INSTALL_DESTINATION cmake
    )

    write_basic_package_version_file(
            ${CMAKE_CURRENT_BINARY_DIR}/LoomClientVersion.cmake
            VERSION ${LOOM_CLIENT_VERSION}
            COMPATIBILITY SameMajorVersion
    )

    install(
            FILES
            ${CMAKE_CURRENT_BINARY_DIR}/LoomClientConfig.cmake
            ${CMAKE_CURRENT_BINARY_DIR}/LoomClientVersion.cmake
            DESTINATION ${CMAKE_INSTALL_LIBDIR}/cmake/LoomClient
    )

    install(FILES
            include/loom/System.h
            DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/loom
            )

    # Tests
    #add_executable(LoomClientAPITest test/api_test.cpp)
    #add_dependencies(LoomClientAPITest LoomClient-shared)

    # Link the API lib manually to make sure everything is correct.
    #target_include_directories(LoomClientAPITest PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/include)
    #target_link_libraries(LoomClientAPITest PRIVATE $<TARGET_FILE:LoomClient-shared>)

    # Since this test requires an active daemon, we do not want to run this automatically within CTest.
    # add_test(NAME LoomClientAPITest COMMAND LoomClientAPITest)

else ()
    message(WARNING "Neither LOOM_API_BUILD_STATIC nor LOOM_API_BUILD_SHARED have been enabled, won't build the client API")
endif ()