//
// Created by void on 19/05/2020.
//

#ifndef LOOM_SYSTEM_H
#define LOOM_SYSTEM_H

#include <memory>

#include <loom/client/Client.h>

namespace loom {

/**
 * This is loom's main system class that is used to create clients for an application.
 * Usually you do not want to instantiate this class, instead you should use the provided singleton getter.
 */
class System {
public:

    /**
     * @return A global singleton instance.
     */
    static System &get();

public:
    /**
     * Creates a new instance of the system class. Useful only if you don't want to use singletons in your application.
     */
    System();

public:
    /**
     * Performs an IO message pump for all created clients.
     */
    void messagePump();

    /**
     * Interrupts the message pump loop.
     */
    void stop();

public:
    /**
     * Creates a new TCP client.
     * @param daemonAddress The address of the daemon endpoint (local or remote)
     * @param daemonPort The daemon port (6788 by default).
     * @return The newly created client.
     */
    std::unique_ptr<Client> createTCPClient(const std::string &daemonAddress, unsigned short daemonPort);

private:
    struct Context;
    std::unique_ptr<Context> context;
    static std::unique_ptr<System> singleton;
};

}

#endif //LOOM_SYSTEM_H
