//
// Created by void on 16/04/2020.
//

#ifndef LOOM_CLIENT_CLIENTTCP_H
#define LOOM_CLIENT_CLIENTTCP_H

#include <loom/loom.h>

#include <loom/client/BasicClient.h>
#include <loom/net/SocketProtocolHandler.h>

#include <boost/asio/io_context.hpp>
#include <boost/asio/ip/tcp.hpp>

namespace loom::client {

/**
 * TCP implementation for a client.
 */
class ClientTCP : public BasicClient {
private:
    using protocol_handler_type = net::TCPSocketProtocolHandler;

public:
    ClientTCP(boost::asio::io_context &ioContext,
              const std::string &daemonAddress,
              unsigned short port,
              bool withDefaultHandlers = true);

public:
    ~ClientTCP() override;

public:
    void connect() override;

protected:
    [[nodiscard]] net::ProtocolHandler &getProtocolHandler() const override;

protected:

private:
    boost::asio::io_context &ioContext;
    std::shared_ptr<protocol_handler_type> protocolHandler;
    boost::asio::ip::tcp::resolver::query query;
};

}

#endif //LOOM_CLIENT_CLIENTTCP_H
