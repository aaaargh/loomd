//
// Created by void on 16/04/2020.
//

#ifndef LOOM_CLIENT_CLIENTMESSAGEHANDLER_H
#define LOOM_CLIENT_CLIENTMESSAGEHANDLER_H

#include <loom/loom.h>

#include <loom/net/MessageHandler.h>

namespace loom::client {

class BasicClient;

class ClientMessageHandler : public net::MessageHandler {
public:
    /**
     * Handles the message.
     *
     * @param message The message received.
     */
    virtual void processReceived(BasicClient &client, std::unique_ptr<net::Message> message);

    virtual void processSent(BasicClient &client, const net::Message &message);
};

template<typename MessageT>
class TypedClientMessageHandler
        : public virtual net::TypedMessageHandler<MessageT, ClientMessageHandler> {

private:
    static std::unique_ptr<MessageT> castPointer(std::unique_ptr<net::Message> ptr) {
        return std::unique_ptr<MessageT>(dynamic_cast<MessageT *>(ptr.release()));
    }

public:
    using handler_message_type = MessageT;

    virtual void processReceivedTyped(BasicClient &client, std::unique_ptr<handler_message_type> message) {
        // Ignore
    }

    virtual void processSentTyped(BasicClient &client, const handler_message_type &message) {
        // Ignore
    }

    void processReceived(BasicClient &client, std::unique_ptr<net::Message> message) override {
        processReceivedTyped(client, castPointer(std::move(message)));
    }

    void processSent(BasicClient &client, const net::Message &message) override {
        processSentTyped(client, static_cast<const handler_message_type &>(message));
    }
};

}

#endif //LOOM_CLIENT_CLIENTMESSAGEHANDLER_H