//
// Created by void on 16/04/2020.
//

#ifndef LOOM_CLIENT_BASICCLIENT_H
#define LOOM_CLIENT_BASICCLIENT_H

#include <loom/loom.h>

#include <loom/client/Client.h>
#include <loom/client/ClientMessageHandler.h>

#include <loom/net/MessageHandlerRegistry.h>
#include <loom/net/ProtocolHandler.h>

#include <loom/ChunkReader.h>
#include <loom/DataBlockReceiver.h>

#include <loom/ConfigMap.h>

#include <map>
#include <mutex>

namespace loom::client {

class BasicClient :
        public Client,
        public net::MessageHandlerRegistry<ClientMessageHandler>,
        public net::ProtocolInvoker {
public:
    explicit BasicClient(bool withDefaultHandlers = true);

    virtual void connect() = 0;

protected:
    [[nodiscard]] virtual net::ProtocolHandler &getProtocolHandler() const = 0;

public:
    using SendMessageSuccessCallback = Client::SendMessageSuccessCallback;
    using SendMessageErrorCallback = Client::SendMessageErrorCallback;

    void querySubjects(const std::string &query,
                       SubjectViewMode viewMode,
                       SubjectQueryFlags flags,
                       const SubjectQueryResultsCallback &callback,
                       const SendMessageErrorCallback &errorCallback) override;

    void subscribeToSubjects(const std::string &filter,
                             SubjectSubscriptionFlags flags,
                             const SendMessageSuccessCallback &successCallback,
                             const SendMessageErrorCallback &errorCallback) override;

    void announceSubject(const ConfigMap &configMap,
                         SubjectAnnouncementFlags flags,
                         const SendMessageSuccessCallback &successCallback,
                         const SendMessageErrorCallback &errorCallback) override;

    void dropSubject(const ConfigMap &subjectId,
                     const SendMessageSuccessCallback &successCallback,
                     const SendMessageErrorCallback &errorCallback) override;

    void authenticate(const ConfigMap &identity,
                      const SendMessageSuccessCallback &successCallback,
                      const SendMessageErrorCallback &errorCallback) override;

    void requestData(const ConfigMap &subjectId,
                     const ConfigMap::key_type &attribute,
                     size_t offset,
                     size_t len,
                     std::shared_ptr<DataSourceWriter> writer,
                     const SendMessageSuccessCallback &successCallback,
                     const SendMessageErrorCallback &errorCallback) override;

public:
    void sendMessage(std::unique_ptr<net::Message> message,
                     const SendMessageSuccessCallback &successCallback,
                     const SendMessageErrorCallback &errorCallback);

public:
    std::shared_ptr<DataSourceReader> getDataSourceReader(const ConfigMapAttributeKey &) const;

protected:
    void handleMessageReceived(std::unique_ptr<net::Message> msg) override;

    void handleDisconnect(const std::exception_ptr &error) override;

    [[nodiscard]] const MessageHandlerInvoker &getMessageHandlerInvoker() const override;

private:
    std::map<ConfigMapAttributeKey, std::shared_ptr<DataSourceReader> > dataSourceReaders;

    std::atomic_short nextDataRequestId = 0;
};

}

#endif //LOOM_CLIENT_BASICCLIENT_H
