//
// Created by void on 19/05/2020.
//

#ifndef LOOM_CLIENT_H
#define LOOM_CLIENT_H

#include <loom/loom.h>
#include <loom/Buffer.h>
#include <loom/ConfigMap.h>
#include <loom/SubjectViewMode.h>

#include <loom/SubjectAnnouncementFlags.h>
#include <loom/SubjectQueryFlags.h>
#include <loom/SubjectSubscriptionFlags.h>

#include <vector>
#include <functional>
#include <memory>

namespace loom {

class Client;

class LOOMAPI ClientEventListener {
public:
    virtual ~ClientEventListener() = default;

public:
    virtual void onSubjectAnnounced(Client &client, const ConfigMap &subject, bool isUpdate) {}

    virtual void onSubjectDropped(Client &client, const ConfigMap &subject) {}

    virtual void onAuthenticationFailed(Client &client, const std::string &reason) {}

    virtual void onAuthenticated(Client &client, const ConfigMap &identity) {}

    virtual void onConnected(Client &client) {}

    virtual void onConnectionLost(Client &client, const std::exception_ptr &error) {}
};

class LOOMAPI Client {
public:
    virtual ~Client() = default;

public:
    using SubjectQueryResultsCallback = std::function<void(const std::vector<ConfigMap> &results)>;
    using SendMessageSuccessCallback = std::function<void()>;
    using SendMessageErrorCallback = std::function<void(const std::exception_ptr &error)>;

    virtual void querySubjects(const std::string &query,
                               SubjectViewMode viewMode,
                               SubjectQueryFlags flags,
                               const SubjectQueryResultsCallback &callback,
                               const SendMessageErrorCallback &errorCallback) = 0;

    virtual void subscribeToSubjects(const std::string &filter,
                                     SubjectSubscriptionFlags flags,
                                     const SendMessageSuccessCallback &successCallback,
                                     const SendMessageErrorCallback &errorCallback) = 0;

    virtual void announceSubject(const ConfigMap &configMap,
                                 SubjectAnnouncementFlags flags,
                                 const SendMessageSuccessCallback &successCallback,
                                 const SendMessageErrorCallback &errorCallback) = 0;

    virtual void dropSubject(const ConfigMap &subjectId,
                             const SendMessageSuccessCallback &successCallback,
                             const SendMessageErrorCallback &errorCallback) = 0;

    virtual void authenticate(const ConfigMap &identity,
                              const SendMessageSuccessCallback &successCallback,
                              const SendMessageErrorCallback &errorCallback) = 0;

    virtual void requestData(const ConfigMap &subjectId,
                             const ConfigMap::key_type &attribute,
                             size_t offset,
                             size_t len,
                             std::shared_ptr<DataSourceWriter> writer,
                             const SendMessageSuccessCallback &successCallback,
                             const SendMessageErrorCallback &errorCallback) = 0;

    virtual void setEventListener(std::unique_ptr<ClientEventListener> listener);

    [[nodiscard]] virtual bool hasEventListener() const;

    [[nodiscard]] virtual ClientEventListener &getEventListener() const;

protected:
    std::unique_ptr<ClientEventListener> eventListener;
};

}

#endif //LOOM_CLIENT_H
