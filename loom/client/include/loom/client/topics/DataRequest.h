//
// Created by void on 29/04/2020.
//

#ifndef LOOM_CLIENT_TOPICS_DATAREQUEST_H
#define LOOM_CLIENT_TOPICS_DATAREQUEST_H

#include <loom/loom.h>
#include <loom/ChunkReader.h>
#include <loom/DataBlockReceiver.h>

#include <loom/net/topics/DataRequest.h>
#include <loom/client/ClientMessageHandler.h>

#include <set>
#include <memory>

namespace loom::client::topics {

class DataRequestHandler : public TypedClientMessageHandler<net::topics::DataRequest> {
public:
    [[nodiscard]] const net::MessageSerializer &getTypedSerializer() const override;
};

class DataResponseHandler : public TypedClientMessageHandler<net::topics::DataResponse> {
public:
    [[nodiscard]] const net::MessageSerializer &getTypedSerializer() const override;
};

class DefaultDataHandler : public virtual DataRequestHandler, public virtual DataResponseHandler {
public:
    bool canProcess(net::Message::id_type type) const override;

    bool canSerialize(net::Message::id_type type) const override;

    bool canDeserialize(net::Message::id_type type) const override;

    const net::MessageSerializer &getSerializer(net::Message::id_type type) const override;

    void processReceived(BasicClient &client, std::unique_ptr<net::Message> message) override;

    void processSent(BasicClient &client, const net::Message &message) override;

public:
    using request_message_type = DataRequestHandler::handler_message_type;
    using response_message_type = DataResponseHandler::handler_message_type;

    [[nodiscard]] size_t getPriority() const override;

    void processReceivedTyped(BasicClient &client, std::unique_ptr<request_message_type> message) override;

    void processSentTyped(BasicClient &client, const net::topics::DataRequest &message) override;

private:
    struct ChunkReader : public loom::ChunkReader {
        explicit ChunkReader(BasicClient &client,
                             DefaultDataHandler &handler,
                             ConfigMapAttributeKey key,
                             std::shared_ptr<DataSourceReader> reader,
                             size_t maxChunkSize,
                             size_t offset, size_t len);

    protected:
        void handleChunkRead(ImmutableBuffer buffer, size_t offset, const ReadDataResult &result) override;

        void handleComplete() override;

        void handleInterrupted() override;

        void handleError(const std::exception_ptr &error) override;

    private:
        void remove();

    private:
        ConfigMapAttributeKey key;
        BasicClient &client;
        DefaultDataHandler &handler;
    };

    static constexpr size_t MaxSendChunkSize = 524280;

    std::set<std::unique_ptr<ChunkReader> > activeReaders;

public:
    void processReceivedTyped(BasicClient &client, std::unique_ptr<response_message_type> message) override;

private:
    DataBlockReceiver dataBlockReceiver;
};

}

#endif //LOOM_CLIENT_TOPICS_DATAREQUEST_H
