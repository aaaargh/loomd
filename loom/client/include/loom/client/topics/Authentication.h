//
// Created by void on 05/05/2020.
//

#ifndef LOOM_CLIENT_TOPICS_AUTHENTICATION_H
#define LOOM_CLIENT_TOPICS_AUTHENTICATION_H

#include <loom/loom.h>

#include <loom/net/topics/Authentication.h>
#include <loom/client/ClientMessageHandler.h>

namespace loom::client::topics {

class AuthRequestHandler : public TypedClientMessageHandler<net::topics::AuthRequest> {
public:
    [[nodiscard]] const net::MessageSerializer &getTypedSerializer() const override;
};

class AuthResponseHandler : public TypedClientMessageHandler<net::topics::AuthResponse> {
public:
    [[nodiscard]] const net::MessageSerializer &getTypedSerializer() const override;


};

class DefaultAuthResponseHandler : public AuthResponseHandler {
public:
    void processReceivedTyped(BasicClient &client, std::unique_ptr<handler_message_type> message) override;

    size_t getPriority() const override;
};

}

#endif //LOOM_CLIENT_TOPICS_AUTHENTICATION_H
