//
// Created by void on 05/05/2020.
//

#ifndef LOOM_CLIENT_TOPICS_SUBJECTMANAGEMENT_H
#define LOOM_CLIENT_TOPICS_SUBJECTMANAGEMENT_H

#include <loom/loom.h>

#include <loom/net/topics/SubjectManagement.h>
#include <loom/client/ClientMessageHandler.h>

namespace loom::client::topics {

class SubjectAnnouncementHandler : public TypedClientMessageHandler<net::topics::SubjectAnnouncement> {
public:
    [[nodiscard]] const net::MessageSerializer &getTypedSerializer() const override;
};

class SubjectDropHandler : public TypedClientMessageHandler<net::topics::SubjectDrop> {
    [[nodiscard]] const net::MessageSerializer &getTypedSerializer() const override;
};

class SubjectQueryHandler : virtual public TypedClientMessageHandler<net::topics::SubjectQuery> {
public:
    [[nodiscard]] const net::MessageSerializer &getTypedSerializer() const override;
};

class SubjectQueryResultsHandler : virtual public TypedClientMessageHandler<net::topics::SubjectQueryResults> {
public:
    const net::MessageSerializer &getTypedSerializer() const override;
};

class DefaultSubjectQueryHandler : virtual public SubjectQueryHandler, virtual public SubjectQueryResultsHandler {
public:
    virtual ~DefaultSubjectQueryHandler() = default;

    using request_message_type = SubjectQueryHandler::handler_message_type;
    using results_message_type = SubjectQueryResultsHandler::handler_message_type;

    bool canProcess(net::Message::id_type type) const override;

    bool canSerialize(net::Message::id_type type) const override;

    bool canDeserialize(net::Message::id_type type) const override;

    const net::MessageSerializer &getSerializer(net::Message::id_type type) const override;

    void processReceived(BasicClient &client, std::unique_ptr<net::Message> message) override;

    void processSent(BasicClient &client, const net::Message &message) override;

    virtual void processSentTyped(BasicClient &client, const request_message_type &message) override;

    void processReceivedTyped(BasicClient &client, std::unique_ptr<results_message_type> message) override;

    size_t getPriority() const override;

private:
    std::multimap<std::string, request_message_type::ResultsCallback> pendingQueries;
};

}

#endif //LOOM_CLIENT_TOPICS_SUBJECTMANAGEMENT_H
