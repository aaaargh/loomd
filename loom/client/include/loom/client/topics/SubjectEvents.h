//
// Created by void on 05/05/2020.
//

#ifndef LOOM_CLIENT_TOPICS_SUBJECTEVENTS_H
#define LOOM_CLIENT_TOPICS_SUBJECTEVENTS_H

#include <loom/loom.h>

#include <loom/net/topics/SubjectEvents.h>
#include <loom/client/ClientMessageHandler.h>

namespace loom::client::topics {

class SubjectSubscriptionHandler : public TypedClientMessageHandler<net::topics::SubjectSubscription> {
public:
    [[nodiscard]] const net::MessageSerializer &getTypedSerializer() const override;
};

class SubjectAnnouncedEventHandler : public TypedClientMessageHandler<net::topics::SubjectAnnouncedEvent> {
public:
    [[nodiscard]] const net::MessageSerializer &getTypedSerializer() const override;

    void processReceivedTyped(BasicClient &client, std::unique_ptr<handler_message_type> message) override;

protected:
    virtual void handleSubjectAnnounced(BasicClient &client, const ConfigMap &subjectId, bool isUpdate) = 0;
};

class DummySubjectAnnouncedEventHandler : public SubjectAnnouncedEventHandler {
protected:
    void handleSubjectAnnounced(BasicClient &client, const ConfigMap &subjectId, bool isUpdated) override;

public:
    [[nodiscard]] size_t getPriority() const override;
};

class SubjectDroppedEventHandler : public TypedClientMessageHandler<net::topics::SubjectDroppedEvent> {
public:
    [[nodiscard]] const net::MessageSerializer &getTypedSerializer() const override;

    void processReceivedTyped(BasicClient &client, std::unique_ptr<handler_message_type> message) override;

protected:
    virtual void handleSubjectDropped(BasicClient &client, const ConfigMap &subjectId) = 0;
};

class DummySubjectDroppedEventHandler : public SubjectDroppedEventHandler {
protected:
    void handleSubjectDropped(BasicClient &client, const ConfigMap &subjectId) override;

public:
    [[nodiscard]] size_t getPriority() const override;
};

}

#endif //LOOM_CLIENT_TOPICS_SUBJECTEVENTS_H
