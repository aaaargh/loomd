cmake_minimum_required(VERSION 3.13.2)

include(loom-build)

project(loom-client)
set(SOURCES
        src/loom/client/Client.cpp
        include/loom/client/Client.h

        src/loom/client/BasicClient.cpp
        include/loom/client/BasicClient.h

        src/loom/client/ClientMessageHandler.cpp
        include/loom/client/ClientMessageHandler.h

        src/loom/client/ClientTCP.cpp
        include/loom/client/ClientTCP.h

        src/loom/client/DefaultMessageHandlers.h

        src/loom/client/topics/Authentication.cpp
        include/loom/client/topics/Authentication.h

        src/loom/client/topics/DataRequest.cpp
        include/loom/client/topics/DataRequest.h

        src/loom/client/topics/SubjectEvents.cpp
        include/loom/client/topics/SubjectEvents.h

        src/loom/client/topics/SubjectManagement.cpp
        include/loom/client/topics/SubjectManagement.h
        )

add_library(
        loom-client
        STATIC
        ${SOURCES}
)

target_link_libraries(loom-client PUBLIC loom-net)

find_package(Loguru REQUIRED)
target_link_libraries(loom-client PUBLIC Loguru::Loguru)

loom_set_target_defaults(loom-client)

loom_setup_tests(
        test/TCPCommunicationTests.cpp
        test/ClientConnectionTests.cpp
        LIBS loom-client loom-daemon
)

include(GNUInstallDirs)

install(FILES
        include/loom/client/Client.h
        DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/loom/client
        )

add_subdirectory(api)
