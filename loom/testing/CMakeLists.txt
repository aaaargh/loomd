cmake_minimum_required(VERSION 3.13.2)

include(loom-build)

project(loom-testing)

add_library(loom-testing STATIC
        src/loom/testing/await.cpp
        include/loom/testing/await.h
        include/loom/testing/collections.h
        )

loom_set_target_defaults(loom-testing)

find_package(Boost REQUIRED COMPONENTS unit_test_framework)
target_include_directories(loom-testing PUBLIC Boost::unit_test_framework) # Don't link the boost testing library here!
target_link_libraries(loom-testing PRIVATE Boost::boost) # This is for boost asio use in await

find_package(Loguru REQUIRED)
target_link_libraries(loom-testing PUBLIC Loguru::Loguru)

target_include_directories(loom-testing PUBLIC include)

add_library(loom-testing-main STATIC
        src/loom/testing/unit_test_main.cpp
        )
target_link_libraries(loom-testing-main PUBLIC loom-testing)
loom_set_target_defaults(loom-testing-main)

find_package(Boost REQUIRED COMPONENTS unit_test_framework)
target_link_libraries(loom-testing-main PUBLIC Boost::unit_test_framework)

if (LOOM_AGGREGATE_TESTS)
    project(loom-tests)
    add_executable(loom-tests ${LOOM_TEST_SOURCES} src/loom/testing/unit_test_main.cpp)
    target_include_directories(loom-tests PRIVATE ${LOOM_TEST_INCLUDE_DIRS})
    target_link_libraries(loom-tests PRIVATE ${LOOM_TEST_LIBRARIES})
    loom_set_target_defaults(loom-tests)
endif ()
