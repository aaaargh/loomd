//
// Created by void on 16/04/2020.
//

#ifndef LOOM_TEST_AWAIT_H
#define LOOM_TEST_AWAIT_H

#include <optional>
#include <functional>
#include <mutex>
#include <condition_variable>
#include <boost/asio/io_context.hpp>

namespace loom::testing {

/**
 * Waits until the given condition returns true.
 * @param fun A function that checks the condition.
 */
void await(const std::function<bool()> &fun);

/**
 * Waits for an optional to be filled.
 * @tparam ResultT The optional type.
 * @param value The optional value.
 */
template<typename ResultT>
void await(std::optional<ResultT> &value) {
    await([&] { return value.has_value(); });
}

template<typename ValueT>
void await(ValueT &value) {
    await([&] { return (bool) value; });
}

/**
 * A little helper function that awaits an asynchronous callback result and blocks execution until it has a result.
 * @tparam ResultT The result type.
 * @param result An optional containing the result.
 * @param fun The lambda function to execute.
 */
template<typename ResultT>
void awaitCallback(std::optional<ResultT> &result, const std::function<void()> &fun) {
    fun();
    await(result);
}

template<typename ValueT>
void awaitCallback(ValueT &value, const std::function<void()> &fun) {
    fun();
    await(value);
}

template<typename ResultT>
void pollAndAwaitCallback(boost::asio::io_context &ioContext, std::optional<ResultT> &result,
                          const std::function<void()> &fun) {
    fun();
    pollAndAwait(ioContext, result);
}

void pollAndAwait(boost::asio::io_context &ioContext, const std::function<bool()> &fun);

template<typename ResultT>
void pollAndAwait(boost::asio::io_context &ioContext, std::optional<ResultT> &value) {
    await([&] {
        ioContext.poll();
        return value.has_value();
    });
}

template<typename ValueT>
void pollAndAwait(boost::asio::io_context &ioContext, ValueT &value) {
    await([&] {
        ioContext.poll();
        return (bool) value;
    });
}

}

#endif //LOOM_TEST_AWAIT_H
