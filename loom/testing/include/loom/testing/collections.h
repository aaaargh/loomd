//
// Created by void on 25/05/2020.
//

#ifndef LOOM_TESTING_COLLECTIONS_H
#define LOOM_TESTING_COLLECTIONS_H

#include <boost/test/test_tools.hpp>
#include <boost/test/tools/assertion_result.hpp>

namespace boost::test_tools::tt_detail {

struct collections_equal_any_order {

    template<typename AIt, typename BIt>
    boost::test_tools::assertion_result operator()(AIt aBeg, AIt aEnd, BIt bBeg, BIt bEnd) {
        boost::test_tools::assertion_result pr(true);

        if (std::distance(aBeg, aEnd) != std::distance(bBeg, bEnd)) {
            pr = false;
            pr.message() << "Container sizes are different.";
        }

        for (auto aIt = aBeg; aIt != aEnd; ++aIt) {
            bool found = false;
            for (auto bIt = bBeg; bIt != bEnd; ++bIt) {
                if (*bIt == *aIt) {
                    found = true;
                    break;
                }
            }

            if (!found) {
                pr = false;
                pr.message() << "Element missing in A: " << boost::test_tools::tt_detail::print_helper(*aIt);
                break;
            }
        }

        for (auto bIt = bBeg; bIt != bEnd; ++bIt) {
            bool found = false;
            for (auto aIt = aBeg; aIt != aEnd; ++aIt) {
                if (*aIt == *bIt) {
                    found = true;
                    break;
                }
            }

            if (!found) {
                pr = false;
                pr.message() << "Element missing in B: " << boost::test_tools::tt_detail::print_helper(*bIt);
                break;
            }
        }

        return pr;
    }
};

}

#define LOOM_REQUIRE_EQUAL_COLLECTIONS_ANY_ORDER(L_begin, L_end, R_begin, R_end) \
    BOOST_TEST_TOOL_IMPL( 1, ::boost::test_tools::tt_detail::collections_equal_any_order(),  \
        "", REQUIRE, CHECK_EQUAL_COLL, (L_begin)(L_end)(R_begin)(R_end) ) \

#endif //LOOM_TESTING_COLLECTIONS_H
