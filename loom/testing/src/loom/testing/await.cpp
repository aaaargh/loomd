//
// Created by void on 22/04/2020.
//

#include <loom/testing/await.h>

namespace loom::testing {

namespace detail {
constexpr auto await_polling_interval = std::chrono::milliseconds(100);
}

void await(const std::function<bool()> &fun) {
    std::mutex mutex;
    std::condition_variable cv;

    std::unique_lock lk(mutex);

    while (!fun()) {
        cv.wait_for(lk, detail::await_polling_interval, fun);
    }
}

void pollAndAwait(boost::asio::io_context &ioContext, const std::function<bool()> &fun) {
    await([&] {
        ioContext.poll();
        return fun();
    });
}
}