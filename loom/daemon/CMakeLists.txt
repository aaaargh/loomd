cmake_minimum_required(VERSION 3.13.2)

include(loom-build)

project(loom-daemon)
set(SOURCES
        src/loom/daemon/Daemon.cpp
        include/loom/daemon/Daemon.h

        src/loom/daemon/DaemonMessageHandler.cpp
        include/loom/daemon/DaemonMessageHandler.h

        src/loom/daemon/Peer.cpp
        include/loom/daemon/Peer.h

        src/loom/daemon/DaemonTCP.cpp
        include/loom/daemon/DaemonTCP.h

        src/loom/daemon/PeerTCP.cpp
        include/loom/daemon/PeerTCP.h

        src/loom/daemon/PeerMemory.cpp
        include/loom/daemon/PeerMemory.h

        # Topics
        src/loom/daemon/topics/Authentication.cpp
        include/loom/daemon/topics/Authentication.h

        src/loom/daemon/topics/DataRequest.cpp
        include/loom/daemon/topics/DataRequest.h

        src/loom/daemon/topics/SubjectEvents.cpp
        include/loom/daemon/topics/SubjectEvents.h

        src/loom/daemon/topics/SubjectManagement.cpp
        include/loom/daemon/topics/SubjectManagement.h
        )

add_library(
        loom-daemon
        STATIC
        ${SOURCES}
)

target_link_libraries(loom-daemon PUBLIC loom-engine loom-net)

find_package(Loguru REQUIRED)
target_link_libraries(loom-daemon PUBLIC Loguru::Loguru)

if (WIN32)
    target_link_libraries(loom-daemon PUBLIC ws2_32 wsock32)
endif ()

loom_set_target_defaults(loom-daemon)

add_subdirectory(test)