//
// Created by void on 15/03/2020.
//

#ifndef LOOM_DAEMON_PEERTCP_H
#define LOOM_DAEMON_PEERTCP_H

#include <loom/daemon/Peer.h>
#include <loom/net/SocketProtocolHandler.h>

#include <boost/asio/io_context.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/streambuf.hpp>

namespace loom::daemon {

class DaemonTCP;

/**
 * The TCP implementation of a peer.
 */
class PeerTCP : public Peer {
private:
    using protocol_handler_type = net::TCPSocketProtocolHandler;

public:
    explicit PeerTCP(DaemonTCP &daemon);
    ~PeerTCP() override;

    void start() override;

    using socket_type = protocol_handler_type::socket_type;

    socket_type &getSocket() const;

protected:
    net::ProtocolHandler &getProtocolHandler() const override;

private:
    std::shared_ptr<protocol_handler_type> protocolHandler;
};

}

#endif //LOOM_DAEMON_PEERTCP_H
