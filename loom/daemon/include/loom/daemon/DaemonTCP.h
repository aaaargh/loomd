//
// Created by void on 15/03/2020.
//

#ifndef LOOM_DAEMON_DAEMONTCP_H
#define LOOM_DAEMON_DAEMONTCP_H

#include <loom/daemon/Daemon.h>
#include <loom/daemon/PeerTCP.h>

#include <boost/asio/io_context.hpp>
#include <boost/asio/ip/address.hpp>
#include <boost/asio/ip/tcp.hpp>

#include <list>

namespace loom::daemon {

/**
 * A TCP streaming implementation for the Loom Daemon.
 */
class DaemonTCP : public Daemon {
public:
    static const unsigned short DefaultPort;

public:
    explicit DaemonTCP(engine::Engine &engine, unsigned short portNum = DefaultPort);

    DaemonTCP(engine::Engine &engine, const boost::asio::ip::address &address, unsigned short portNum = DefaultPort);

private:
    void startAccept();

    void handleAccept(const std::shared_ptr<PeerTCP> &peer, const boost::system::error_code &ec);
    
private:
    boost::asio::ip::tcp::acceptor acceptor;
};

}

#endif //LOOM_DAEMON_DAEMONTCP_H
