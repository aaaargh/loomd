//
// Created by void on 16/04/2020.
//

#ifndef LOOM_DAEMON_DAEMONMESSAGEHANDLER_H
#define LOOM_DAEMON_DAEMONMESSAGEHANDLER_H

#include <loom/net/MessageHandler.h>
#include <loom/daemon/Peer.h>

namespace loom::daemon {

/**
 * A message handler extension for the daemon.
 */
class DaemonMessageHandler : public net::MessageHandler {
public:
    /**
     * Handles the message.
     *
     * @param peer The sender.
     * @param message The message received.
     */
    virtual void processMessage(const std::shared_ptr<Peer> &peer, std::unique_ptr<net::Message> message) const;
};

template<typename MessageT>
class TypedDaemonMessageHandler
        : public net::TypedMessageHandler<MessageT, DaemonMessageHandler> {
public:
    using message_type = MessageT;

private:
    static std::unique_ptr<MessageT> castPointer(std::unique_ptr<net::Message> ptr) {
        return std::unique_ptr<MessageT>(dynamic_cast<MessageT *>(ptr.release()));
    }

public:
    virtual void processTypedMessage(const std::shared_ptr<Peer> &peer,
                                     std::unique_ptr<message_type> message) const {
        throw std::logic_error("Unable to process this message");
    }

    void processMessage(const std::shared_ptr<Peer> &peer, std::unique_ptr<net::Message> message) const {
        processTypedMessage(peer, castPointer(std::move(message)));
    }

};

}

#endif //LOOM_DAEMON_DAEMONMESSAGEHANDLER_H
