//
// Created by void on 29/04/2020.
//

#ifndef LOOM_DAEMON_TOPICS_CLIENTDATA_H
#define LOOM_DAEMON_TOPICS_CLIENTDATA_H

#include <loom/net/topics/DataRequest.h>
#include <loom/net/MessageHandler.h>

#include <loom/daemon/DaemonMessageHandler.h>

namespace loom::daemon::topics {

class DataRequestHandler : public TypedDaemonMessageHandler<net::topics::DataRequest> {
public:
    [[nodiscard]] const net::MessageSerializer &getTypedSerializer() const override;

    void processTypedMessage(const std::shared_ptr<Peer> &peer, std::unique_ptr<message_type> message) const override;
};

class DataResponseHandler : public TypedDaemonMessageHandler<net::topics::DataResponse> {
public:
    [[nodiscard]] const net::MessageSerializer &getTypedSerializer() const override;

    void processTypedMessage(const std::shared_ptr<Peer> &peer, std::unique_ptr<message_type> message) const override;
};

}

#endif //LOOM_DAEMON_TOPICS_CLIENTDATA_H
