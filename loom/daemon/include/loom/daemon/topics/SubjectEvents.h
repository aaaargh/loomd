//
// Created by void on 05/05/2020.
//

#ifndef LOOM_DAEMON_TOPICS_SUBJECTEVENTS_H
#define LOOM_DAEMON_TOPICS_SUBJECTEVENTS_H

#include <loom/net/topics/SubjectEvents.h>
#include <loom/net/MessageHandler.h>

#include <loom/daemon/DaemonMessageHandler.h>

namespace loom::daemon::topics {

class SubjectSubscriptionHandler : public TypedDaemonMessageHandler<net::topics::SubjectSubscription> {
public:
    [[nodiscard]] const net::MessageSerializer &getTypedSerializer() const override;

    void processTypedMessage(const std::shared_ptr<Peer> &peer, std::unique_ptr<message_type> message) const override;
};

class SubjectAnnouncedEventHandler : public TypedDaemonMessageHandler<net::topics::SubjectAnnouncedEvent> {
public:
    [[nodiscard]] const net::MessageSerializer &getTypedSerializer() const override;

    void processTypedMessage(const std::shared_ptr<Peer> &peer, std::unique_ptr<message_type> message) const override;
};

class SubjectDroppedEventHandler : public TypedDaemonMessageHandler<net::topics::SubjectDroppedEvent> {
public:
    [[nodiscard]] const net::MessageSerializer &getTypedSerializer() const override;

    void processTypedMessage(const std::shared_ptr<Peer> &peer, std::unique_ptr<message_type> message) const override;
};

}

#endif //LOOM_DAEMON_TOPICS_SUBJECTEVENTS_H
