//
// Created by void on 05/05/2020.
//

#ifndef LOOM_DAEMON_TOPICS_AUTHENTICATION_H
#define LOOM_DAEMON_TOPICS_AUTHENTICATION_H

#include <loom/net/topics/Authentication.h>
#include <loom/net/MessageHandler.h>

#include <loom/daemon/DaemonMessageHandler.h>

namespace loom::daemon::topics {

class AuthRequestHandler : public TypedDaemonMessageHandler<net::topics::AuthRequest> {
public:
    [[nodiscard]] const net::MessageSerializer &getTypedSerializer() const override;

    void processTypedMessage(const std::shared_ptr<Peer> &peer, std::unique_ptr<message_type> message) const override;
};

class AuthResponseHandler : public TypedDaemonMessageHandler<net::topics::AuthResponse> {
public:
    const net::MessageSerializer &getTypedSerializer() const override;
};

}

#endif //LOOM_DAEMON_TOPICS_AUTHENTICATION_H
