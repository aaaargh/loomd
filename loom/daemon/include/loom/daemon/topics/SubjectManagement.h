//
// Created by void on 05/05/2020.
//

#ifndef LOOM_DAEMON_TOPICS_SUBJECTMANAGEMENT_H
#define LOOM_DAEMON_TOPICS_SUBJECTMANAGEMENT_H

#include <loom/net/topics/SubjectManagement.h>
#include <loom/daemon/DaemonMessageHandler.h>

namespace loom::daemon::topics {

class SubjectAnnouncementHandler : public TypedDaemonMessageHandler<net::topics::SubjectAnnouncement> {
public:
    [[nodiscard]] const net::MessageSerializer &getTypedSerializer() const override;

    void processTypedMessage(const std::shared_ptr<Peer> &peer, std::unique_ptr<message_type> message) const override;
};

class SubjectDropHandler : public TypedDaemonMessageHandler<net::topics::SubjectDrop> {
public:
    [[nodiscard]] const net::MessageSerializer &getTypedSerializer() const override;

    void processTypedMessage(const std::shared_ptr<Peer> &peer, std::unique_ptr<message_type> message) const override;
};

class SubjectQueryHandler : public TypedDaemonMessageHandler<net::topics::SubjectQuery> {
public:
    [[nodiscard]] const net::MessageSerializer &getTypedSerializer() const override;

    void processTypedMessage(const std::shared_ptr<Peer> &peer, std::unique_ptr<message_type> message) const override;
};

class SubjectQueryResultsHandler : public TypedDaemonMessageHandler<net::topics::SubjectQueryResults> {
public:
    [[nodiscard]] const net::MessageSerializer &getTypedSerializer() const override;
};

}

#endif //LOOM_DAEMON_TOPICS_SUBJECTMANAGEMENT_H
