//
// Created by void on 16/04/2020.
//

#ifndef LOOM_DAEMON_PEERMEMORY_H
#define LOOM_DAEMON_PEERMEMORY_H

#include <loom/engine/Subject.h>
#include <loom/engine/Subscription.h>

#include <map>
#include <set>
#include <memory>

namespace loom::daemon {

struct PeerMemory {
    void clear();

    std::map<ConfigMap, engine::SubjectHandle> ownedSubjects{};
    std::set<std::shared_ptr<engine::Subscription> > subscriptions{};
};

}

#endif //LOOM_DAEMON_PEERMEMORY_H
