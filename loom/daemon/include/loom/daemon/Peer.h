//
// Created by void on 12/03/2020.
//

#ifndef LOOM_DAEMON_PEER_H
#define LOOM_DAEMON_PEER_H

#include <loom/net/Message.h>
#include <loom/net/ProtocolHandler.h>
#include <loom/daemon/PeerMemory.h>
#include <loom/engine/Actor.h>

#include <functional>
#include <memory>

#include <boost/system/error_code.hpp>

namespace loom::daemon {

class Daemon;

/**
 * The basic implementation of a peer that connected to a loom daemon.
 */
class Peer :
        public std::enable_shared_from_this<Peer>,
        public engine::ActorBrain,
        public net::ProtocolInvoker {
public:
    /**
     * Creates a new peer.
     * @param daemon The daemon that the peer connected to.
     */
    explicit Peer(Daemon &daemon);

    virtual ~Peer();

public:
    Daemon &getDaemon();

    PeerMemory &getMemory();

    const engine::ActorHandle &getIdentity() const;

    void setIdentity(engine::ActorHandle identity);

    virtual void start() = 0;

    void disconnect(const std::exception_ptr &error = std::exception_ptr());

public:
    void sendMessage(std::unique_ptr<net::Message> message,
                     const SendMessageSuccessCallback &successCallback = DefaultSendMessageSuccessCallback,
                     const SendMessageErrorCallback &errorCallback = DefaultSendMessageErrorCallback);

protected:
    [[nodiscard]] virtual net::ProtocolHandler &getProtocolHandler() const = 0;

protected:

    void handleDisconnect(const std::exception_ptr &error) override;

    void handleMessageReceived(std::unique_ptr<net::Message> msg) override;

    void handleDataRequest(ConfigMap subjectId,
                           ConfigMap::key_type attribute,
                           size_t offset, size_t len) override;

public:
    void handleSubjectOwned(engine::SubjectHandle subject) override;

    void handleSubjectOwnershipLost(engine::SubjectHandle subject) override;

public:
    const net::MessageHandlerInvoker &getMessageHandlerInvoker() const override;

protected:
    engine::ActorHandle identity{};
    Daemon &daemon;
    PeerMemory memory;

private:
    std::atomic_short nextDataRequestId = 0;
};

}

#endif //LOOM_DAEMON_PEER_H
