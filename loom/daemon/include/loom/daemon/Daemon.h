//
// Created by void on 12/03/2020.
//

#ifndef LOOM_DAEMON_DAEMON_H
#define LOOM_DAEMON_DAEMON_H

#include <set>
#include <memory>

#include <loom/net/MessageHandlerRegistry.h>
#include <loom/daemon/Peer.h>
#include <loom/daemon/DaemonMessageHandler.h>
#include <loom/engine/Engine.h>

#include <boost/iterator/indirect_iterator.hpp>
#include <boost/range/iterator_range.hpp>

namespace loom::daemon {

/**
 * The basic implementation of the LOOM daemon.
 */
class Daemon : public net::MessageHandlerRegistry<DaemonMessageHandler> {
public:
    /**
     * Creates a new daemon for the given engine.
     * @param engine The engine to use with the daemon.
     */
    explicit Daemon(engine::Engine &engine);

    ~Daemon() override = default;

public:
    engine::Engine &getEngine();

    [[nodiscard]] boost::asio::io_context &getIOContext() const;

protected:
    friend class Peer;

    /**
     * Called when a new peer has connected.
     * @param peer The peer that connected.
     */
    virtual void handlePeerConnected(const std::shared_ptr<Peer> &peer);

    /**
     * Called when a peer has been disconnected.
     * @param peer The peer that disconnected.
     */
    virtual void handlePeerDisconnected(Peer &peer);

    /**
     * Called when a message was received from a peer.
     * @param peer The peer that sent the message.
     * @param message The message received.
     */
    void handlePeerMessageReceived(const std::shared_ptr<Peer> &peer, std::unique_ptr<net::Message> message);

private:
    std::set<std::shared_ptr<Peer> > peers;
    engine::Engine &engine;
};

}

#endif //LOOM_DAEMON_DAEMON_H
