//
// Created by void on 15/03/2020.
//

#include <loom/daemon/PeerTCP.h>
#include <loom/daemon/DaemonTCP.h>

#include <loguru.hpp>

namespace loom::daemon {

using namespace boost::asio;
using namespace boost::asio::ip;

PeerTCP::PeerTCP(DaemonTCP &daemon) :
        Peer(daemon),
        protocolHandler(
                std::make_shared<net::SocketProtocolHandler<boost::asio::ip::tcp::socket> >(*this,
                                                                                            daemon.getIOContext())) {}

void PeerTCP::start() {
    protocolHandler->notifyConnected();
}

PeerTCP::~PeerTCP() {
    protocolHandler->notifyClosed();
}

net::ProtocolHandler &PeerTCP::getProtocolHandler() const {
    return *protocolHandler;
}

PeerTCP::socket_type &PeerTCP::getSocket() const {
    return protocolHandler->getSocket();
}

}