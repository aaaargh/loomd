//
// Created by void on 02/04/2020.
//

#ifndef LOOM_DAEMON_DEFAULTMESSAGEHANDLERS_H
#define LOOM_DAEMON_DEFAULTMESSAGEHANDLERS_H

#include <loom/daemon/Daemon.h>

#include <loom/daemon/topics/Authentication.h>
#include <loom/daemon/topics/DataRequest.h>
#include <loom/daemon/topics/SubjectEvents.h>
#include <loom/daemon/topics/SubjectManagement.h>

namespace loom::daemon {

/**
 * A static class that registers the default message handlers to a daemon.
 */
class DefaultMessageHandlers {
public:
    DefaultMessageHandlers() = delete;

public:
    /**
     * Registers the default message handlers to the given daemon.
     * @param daemon The daemon instance to register the handlers to.
     */
    static void registerTo(Daemon &daemon) {
        // New stuff
        daemon.registerMessageHandler<topics::AuthRequestHandler>();
        daemon.registerMessageHandler<topics::AuthResponseHandler>();

        daemon.registerMessageHandler<topics::DataRequestHandler>();
        daemon.registerMessageHandler<topics::DataResponseHandler>();

        daemon.registerMessageHandler<topics::SubjectSubscriptionHandler>();
        daemon.registerMessageHandler<topics::SubjectAnnouncedEventHandler>();
        daemon.registerMessageHandler<topics::SubjectDroppedEventHandler>();

        daemon.registerMessageHandler<topics::SubjectAnnouncementHandler>();
        daemon.registerMessageHandler<topics::SubjectDropHandler>();

        daemon.registerMessageHandler<topics::SubjectQueryHandler>();
        daemon.registerMessageHandler<topics::SubjectQueryResultsHandler>();
    }
};

}

#endif //LOOM_DAEMON_DEFAULTMESSAGEHANDLERS_H
