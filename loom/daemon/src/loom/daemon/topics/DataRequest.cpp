//
// Created by void on 29/04/2020.
//

#include <loom/daemon/topics/DataRequest.h>
#include <loom/daemon/Daemon.h>
#include <loom/engine/Actor.h>
#include <loom/engine/Engine.h>

#include <utility>
#include <loguru.hpp>

namespace loom::daemon::topics {

constexpr size_t MaxChunkSize = 262140;

/**
 * Engine -> Client request implementation.
 */
void DataResponseHandler::processTypedMessage(const std::shared_ptr<Peer> &peer,
                                              std::unique_ptr<net::topics::DataResponse> message) const {
    // The client has sent a chunk of data.
    // LOG_S(INFO) << "Received " << message->data.size() << " byte(s) of data for request " << message->requestId;

    auto len = message->data.size();

    ConfigMapAttributeKey key(std::move(message->subjectId), std::move(message->attribute));

    auto &engine = peer->getDaemon().getEngine();
    engine.putData(peer->getIdentity(),
                   key,
                   message->offset, std::move(message->data),
                   {}, [peer](const auto &err) {
                peer->disconnect(err);
            });

    if (message->isEOF) {
        engine.putData(peer->getIdentity(),
                       key,
                       message->offset + len,
                       engine::dataEOF,
                       {}, [peer](const auto &err) {
                    peer->disconnect(err);
                });
    }


}

/**
 * Client -> Engine request implementation.
 */

namespace detail {
class PeerDataTransactionHandler : public engine::DataTransactionHandler {
public:
    PeerDataTransactionHandler(std::shared_ptr<Peer> peer,
                               short requestId,
                               ConfigMap subjectId,
                               ConfigMap::key_type attribute,
                               size_t offset,
                               size_t len
    ) : peer(std::move(peer)), requestId(requestId), subjectId(std::move(subjectId)),
        attribute(std::move(attribute)), offset(offset), len(len) {}

public:
    void handleError(const std::exception_ptr &error) override {
        try {
            std::rethrow_exception(error);
        } catch (const std::exception &ex) {
            DLOG_S(ERROR) << "Error in transaction handler: " << ex.what();
        }
    }

    void handleTransactionComplete(CompleteReason reason) override {
        DLOG_S(INFO) << "Data block transaction is complete.";
        bool isEOF = reason == CompleteReason::ReachedEOF;

        sendChunk(peer, requestId, subjectId, attribute, nextChunkOffset, len, std::move(nextChunk), 0, isEOF);
    }

    void handleDataReceived(engine::data_buffer_type dataChunk, size_t chunkOffset) override {
        nextChunk = std::move(dataChunk);
        nextChunkOffset = chunkOffset;
    }

    void handleTransactionContinue() override {
        // Chunk transaction is still ongoing.
        DLOG_S(INFO) << "Resuming data block transaction";
        sendChunk(peer, requestId, subjectId, attribute, nextChunkOffset, len, std::move(nextChunk), 0, false);
    }

    static void sendChunk(const std::shared_ptr<Peer> &peer, short requestId, ConfigMap subjectId,
                          ConfigMap::key_type attribute, size_t offset, size_t len,
                          engine::data_buffer_type dataBlock, size_t bufferOffset, bool isEOF) {

        auto thisChunkSize = std::min(MaxChunkSize, dataBlock.size() - bufferOffset);

        if (thisChunkSize == 0 && !isEOF) {
            // Nothing to send.
            return;
        }

        auto responseMessage = std::make_unique<net::topics::DataResponse>();

        bool isLastChunkBeforeEOF = isEOF && (bufferOffset + thisChunkSize >= dataBlock.size());

        responseMessage->requestId = requestId;
        responseMessage->subjectId = subjectId;
        responseMessage->attribute = attribute;
        responseMessage->offset = offset + bufferOffset;
        responseMessage->data.resize(thisChunkSize);
        responseMessage->isEOF = isLastChunkBeforeEOF;
        std::memcpy(responseMessage->data.data(), dataBlock.data() + bufferOffset, thisChunkSize);

        DLOG_S(INFO) << "Peer is sending a data chunk of "
                     << responseMessage->data.size() << "/" << len << " byte(s), "
                     << "offset := " << (offset + bufferOffset) << " id := " << responseMessage->requestId
                     << ", eof := " << isLastChunkBeforeEOF;

        bufferOffset += thisChunkSize;

        peer->sendMessage(
                std::move(responseMessage),
                [
                        peer,
                        requestId,
                        subjectId = std::move(subjectId),
                        attribute = std::move(attribute),
                        offset,
                        len,
                        dataBlock = std::move(dataBlock),
                        bufferOffset,
                        isEOF,
                        isLastChunkBeforeEOF] {
                    if (!isLastChunkBeforeEOF) {
                        sendChunk(peer, requestId, subjectId, attribute, offset, len, dataBlock, bufferOffset, isEOF);
                    }
                });
    }

private:
    engine::data_buffer_type nextChunk{};
    size_t nextChunkOffset = 0;
    std::shared_ptr<Peer> peer;
    short requestId;
    ConfigMap subjectId;
    ConfigMap::key_type attribute;
    size_t offset;
    size_t len;
};
}


void DataRequestHandler::processTypedMessage(const std::shared_ptr<Peer> &peer,
                                             std::unique_ptr<net::topics::DataRequest> message) const {
    auto &engine = peer->getDaemon().getEngine();

    auto handler = std::make_shared<detail::PeerDataTransactionHandler>(
            peer,
            message->requestId,
            message->subjectId,
            message->attribute,
            message->offset,
            message->len
    );

    engine.requestData(peer->getIdentity(),
                       ConfigMapAttributeKey(std::move(message->subjectId), std::move(message->attribute)),
                       message->offset,
                       message->len,
                       handler
    );
}

/**
 * Common stuff.
 */

const net::MessageSerializer &DataResponseHandler::getTypedSerializer() const {
    const static net::topics::DataResponseSerializer serializer;
    return serializer;
}

const net::MessageSerializer &DataRequestHandler::getTypedSerializer() const {
    const static net::topics::DataRequestSerializer serializer;
    return serializer;
}


}
