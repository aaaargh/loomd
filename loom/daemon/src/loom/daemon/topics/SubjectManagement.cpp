//
// Created by void on 05/05/2020.
//

#include <loom/daemon/topics/SubjectManagement.h>
#include <loom/daemon/Daemon.h>
#include <loom/engine/Engine.h>

namespace loom::daemon::topics {

void SubjectAnnouncementHandler::processTypedMessage(const std::shared_ptr<Peer> &peer,
                                                     std::unique_ptr<message_type> message) const {
    auto &daemon = peer->getDaemon();
    auto &engine = daemon.getEngine();

    engine.announceSubject(
            peer->getIdentity(),
            std::move(message->subject),
            message->flags,
            {}, [peer](const auto &err) {
                peer->disconnect(err);
            });
}

const net::MessageSerializer &SubjectAnnouncementHandler::getTypedSerializer() const {
    const static net::topics::SubjectAnnouncementSerializer serializer;
    return serializer;
}

void SubjectDropHandler::processTypedMessage(const std::shared_ptr<Peer> &peer,
                                             std::unique_ptr<message_type> message) const {
    // Nothing to do here.
}

const net::MessageSerializer &SubjectDropHandler::getTypedSerializer() const {
    const static net::topics::SubjectDropSerializer serializer;
    return serializer;
}

void SubjectQueryHandler::processTypedMessage(const std::shared_ptr<Peer> &peer,
                                              std::unique_ptr<net::topics::SubjectQuery> message) const {
    auto &daemon = peer->getDaemon();
    auto &engine = daemon.getEngine();

    engine.querySubjects(
            peer->getIdentity(),
            message->query,
            message->flags,
            message->viewMode,
            [peer, query = message->query](std::vector<ConfigMap> results) {
                auto response = std::make_unique<net::topics::SubjectQueryResults>();
                response->query = query;
                response->results = std::move(results);
                peer->sendMessage(std::move(response));
            }, [peer](const auto &error) {
                try {
                    std::rethrow_exception(error);
                } catch (const std::exception &ex) {
                    auto responseMsg = std::make_unique<net::topics::SubjectQueryResults>();
                    responseMsg->error = ex.what();
                    peer->sendMessage(std::move(responseMsg));
                }
            });
}

const net::MessageSerializer &SubjectQueryHandler::getTypedSerializer() const {
    const static net::topics::SubjectQuerySerializer serializer;
    return serializer;
}

const net::MessageSerializer &SubjectQueryResultsHandler::getTypedSerializer() const {
    const static net::topics::SubjectQueryResultsSerializer serializer;
    return serializer;
}

}
