//
// Created by void on 05/05/2020.
//

#include <loom/daemon/topics/SubjectEvents.h>
#include <loom/daemon/Daemon.h>
#include <loom/engine/Engine.h>

namespace loom::daemon::topics {

/**
 * Client -> Daemon stuff
 */

void SubjectSubscriptionHandler::processTypedMessage(const std::shared_ptr<Peer> &peer,
                                                     std::unique_ptr<message_type> message) const {
    auto &daemon = peer->getDaemon();
    auto &engine = daemon.getEngine();

    engine.subscribe(
            peer->getIdentity(),
            message->filter,
            message->flags,
            [peer](auto subscription) {
                engine::Subscription &s = *subscription;

                s.subjectAnnounced += [peer](const auto &subjectHandle, bool isUpdated) {
                    auto msg = std::make_unique<net::topics::SubjectAnnouncedEvent>();
                    msg->subjectId = subjectHandle->getUniqueId();
                    msg->isUpdated = isUpdated;
                    peer->sendMessage(std::move(msg));
                };

                s.subjectDropped += [peer](const auto &subjectConfig) {
                    auto msg = std::make_unique<net::topics::SubjectDroppedEvent>();
                    msg->subjectId = subjectConfig;
                    peer->sendMessage(std::move(msg));
                };

                peer->getMemory().subscriptions.emplace(std::move(subscription));
            }, [peer](const auto &err) {
                peer->disconnect(err);
            });
}

/**
 * Daemon -> client stuff
 */

void SubjectAnnouncedEventHandler::processTypedMessage(const std::shared_ptr<Peer> &peer,
                                                       std::unique_ptr<message_type> message) const {

}

void SubjectDroppedEventHandler::processTypedMessage(const std::shared_ptr<Peer> &peer,
                                                     std::unique_ptr<message_type> message) const {

}

/**
 * Common stuff
 */

const net::MessageSerializer &SubjectSubscriptionHandler::getTypedSerializer() const {
    static const net::topics::SubjectSubscriptionSerializer serializer;
    return serializer;
}

const net::MessageSerializer &SubjectAnnouncedEventHandler::getTypedSerializer() const {
    static const net::topics::SubjectAnnouncedEventSerializer serializer;
    return serializer;
}

const net::MessageSerializer &SubjectDroppedEventHandler::getTypedSerializer() const {
    static const net::topics::SubjectDroppedEventSerializer serializer;
    return serializer;
}

}
