//
// Created by void on 05/05/2020.
//

#include <loom/daemon/topics/Authentication.h>
#include <loom/daemon/Daemon.h>
#include <loom/engine/Engine.h>

namespace loom::daemon::topics {

void AuthRequestHandler::processTypedMessage(const std::shared_ptr<Peer> &peer,
                                             std::unique_ptr<message_type> message) const {
    auto &daemon = peer->getDaemon();
    auto &engine = daemon.getEngine();

    engine.registerActor(message->identity, peer, [peer](engine::ActorHandle handle) {
        auto responseMsg = std::make_unique<net::topics::AuthResponse>();
        responseMsg->identity = handle->getIdentitySubject()->getUniqueId();
        peer->setIdentity(std::move(handle));

        peer->sendMessage(std::move(responseMsg));
    }, [peer](const auto &error) {
        try {
            std::rethrow_exception(error);
        } catch (const std::exception &ex) {
            auto responseMsg = std::make_unique<net::topics::AuthResponse>();
            responseMsg->error = ex.what();
            peer->sendMessage(std::move(responseMsg));
        }
    });
}

const net::MessageSerializer &AuthRequestHandler::getTypedSerializer() const {
    const static net::topics::AuthRequestSerializer serializer;
    return serializer;
}

const net::MessageSerializer &AuthResponseHandler::getTypedSerializer() const {
    const static net::topics::AuthResponseSerializer serializer;
    return serializer;
}
}

