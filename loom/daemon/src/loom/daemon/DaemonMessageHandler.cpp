//
// Created by void on 16/04/2020.
//

#include <loom/daemon/DaemonMessageHandler.h>

namespace loom::daemon {

void DaemonMessageHandler::processMessage(const std::shared_ptr<Peer> &peer,
                                          std::unique_ptr<net::Message> message) const {
    throw std::logic_error("Cannot handle this message");
}
}
