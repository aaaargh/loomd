//
// Created by void on 15/03/2020.
//

#include <loom/daemon/DaemonTCP.h>
#include <loom/daemon/PeerTCP.h>

#include <loguru.hpp>

namespace loom::daemon {

using namespace boost::system;
using namespace boost::asio;
using namespace boost::asio::ip;

const unsigned short DaemonTCP::DefaultPort = 6788;

DaemonTCP::DaemonTCP(engine::Engine &engine, unsigned short portNum) : DaemonTCP(engine,
                                                                                 address_v4::any(),
                                                                                 portNum) {}

DaemonTCP::DaemonTCP(engine::Engine &engine, const address &address, unsigned short portNum) :
        Daemon(engine),
        acceptor(engine.getIOContext(), tcp::endpoint(address, portNum)) {
    LOG_S(INFO) << "Daemon is listening on " << address << ":" << portNum;
    startAccept();
}

void DaemonTCP::startAccept() {
    auto newPeer = std::make_shared<PeerTCP>(*this);

    acceptor.async_accept(newPeer->getSocket(),
                          [this, newPeer](const auto &ec) { handleAccept(newPeer, ec); });
}

void DaemonTCP::handleAccept(const std::shared_ptr<PeerTCP> &peer, const error_code &ec) {
    if (!ec) {
        handlePeerConnected(peer);
        peer->start();
    } else {
        if (ec == boost::asio::error::operation_aborted) {
            LOG_S(WARNING) << "Operation was aborted, did the socket close?";
            return;
        }

        LOG_S(ERROR) << "Error accepting peer: " << ec.message();
    }

    startAccept();
}

}
