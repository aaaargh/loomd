//
// Created by void on 12/03/2020.
//

#include <loom/daemon/Daemon.h>

#include <loguru.hpp>

#include "DefaultMessageHandlers.h"

namespace loom::daemon {

Daemon::Daemon(engine::Engine &engine) :
        engine(engine) {
    DefaultMessageHandlers::registerTo(*this);
}

void Daemon::handlePeerConnected(const std::shared_ptr<Peer> &peer) {
    LOG_S(INFO) << "New peer connected to daemon.";
    peers.insert(peer);
}

void Daemon::handlePeerDisconnected(Peer &peer) {
    // Because the subscriptions may contain a self-reference, we need to clear the memory here before deleting the peer.
    peer.getMemory().clear();

    auto it = std::find_if(peers.begin(), peers.end(), [&](const auto &ptr) { return ptr.get() == &peer; });
    if (it != peers.end()) {
        LOG_S(INFO) << "Peer disconnected.";
        peers.erase(it);
    }
}

void Daemon::handlePeerMessageReceived(const std::shared_ptr<Peer> &peer, std::unique_ptr<net::Message> message) {
    auto &handler = getProcessingHandler(message->getType());
    handler.processMessage(peer, std::move(message));
}

engine::Engine &Daemon::getEngine() {
    return engine;
}

boost::asio::io_context &Daemon::getIOContext() const {
    return engine.getIOContext();
}

}
