//
// Created by void on 12/03/2020.
//

#include <loom/daemon/Peer.h>
#include <loom/daemon/Daemon.h>

#include <loom/net/topics/DataRequest.h>

#include <loguru.hpp>

namespace loom::daemon {

Peer::Peer(Daemon &daemon) : daemon(daemon) {}

void Peer::handleMessageReceived(std::unique_ptr<net::Message> msg) {
    //DLOG_S(INFO) << "Peer received a message (0x" << std::hex << msg->getType() << ")";
    daemon.handlePeerMessageReceived(this->shared_from_this(), std::move(msg));
}

void Peer::handleDisconnect(const std::exception_ptr &error) {
    if (identity) {
        DLOG_S(INFO) << "Resetting peer identity " <<
                     identity->getIdentitySubject()->getUniqueId()
                     << " during disconnection of peer.";
#ifdef LOOM_MANAGEDINSTANCE_DEBUG_TRACE
        identity.debugDumpInstance();
#endif
    } else {
        DLOG_S(INFO) << "Disconnected unidentified peer.";
    }

    auto temp = identity;
    identity.reset();
    daemon.handlePeerDisconnected(*this);

    if (error) {
        try {
            std::rethrow_exception(error);
        } catch (const boost::system::error_code &ex) {
            DLOG_S(INFO) << "Peer got disconnected with error: " << ex.message();
        } catch (const std::exception &ex) {
            DLOG_S(INFO) << "Peer got disconnected with error: " << ex.what();
        }
    } else {
        DLOG_S(INFO) << "Peer got disconnected without error.";
    }
}

Daemon &Peer::getDaemon() {
    return daemon;
}

PeerMemory &Peer::getMemory() {
    return memory;
}

const engine::ActorHandle &Peer::getIdentity() const {
    return identity;
}

void Peer::setIdentity(engine::ActorHandle newIdentity) {
    DLOG_S(INFO) << "Updated peer identity.";
    identity = std::move(newIdentity);
}

Peer::~Peer() {
    DLOG_S(INFO) << "Peer destroyed.";
}

void Peer::handleDataRequest(ConfigMap subjectId,
                             ConfigMap::key_type attribute,
                             size_t offset, size_t len) {

    auto msg = std::make_unique<net::topics::DataRequest>();

    msg->requestId = nextDataRequestId++;
    msg->subjectId = std::move(subjectId);
    msg->attribute = std::move(attribute);
    msg->offset = offset;
    msg->len = len;

    LOG_S(INFO)
    << "A data request for subject " << msg->subjectId << " has been forwarded to the providing peer, id := "
    << msg->requestId;
    sendMessage(std::move(msg));
}

void Peer::sendMessage(std::unique_ptr<net::Message> message,
                       const SendMessageSuccessCallback &successCallback,
                       const SendMessageErrorCallback &errorCallback) {
    getProtocolHandler().sendMessage(std::move(message), successCallback, errorCallback);
}

const net::MessageHandlerInvoker &Peer::getMessageHandlerInvoker() const {
    return daemon;
}

void Peer::handleSubjectOwned(engine::SubjectHandle subject) {
    DLOG_S(INFO) << "Peer took ownership over " << subject->getUniqueId();
    memory.ownedSubjects.insert(std::make_pair(subject->getUniqueId(), subject));
}

void Peer::handleSubjectOwnershipLost(engine::SubjectHandle subject) {
    DLOG_S(INFO) << "Peer lost ownership over " << subject->getUniqueId();
    assert(memory.ownedSubjects.erase(subject->getUniqueId()) == 1);
}

void Peer::disconnect(const std::exception_ptr &error) {
    handleDisconnect(error);
}

}
