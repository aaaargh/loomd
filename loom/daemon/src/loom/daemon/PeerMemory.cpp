//
// Created by void on 22/04/2020.
//

#include <loom/daemon/PeerMemory.h>

namespace loom::daemon {

void daemon::PeerMemory::clear() {
    ownedSubjects.clear();
    subscriptions.clear();
}

}
