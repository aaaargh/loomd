//
// Created by void on 12/03/2020.
//

#include <loom/testing.h>

#include <loom/testing/TestPeer.h>
#include <loom/testing/TestDaemon.h>
#include <loom/testing/TestMessage.h>

using namespace loom::testing;

namespace loom::daemon::testing {

BOOST_AUTO_TEST_SUITE(PeerTests)

BOOST_AUTO_TEST_CASE(message_handler_is_invoked) {
    TestDaemon daemon;
    daemon.registerMessageHandler<TestMessageHandler>();

    struct : TestPeer {
        using TestPeer::TestPeer;
        net::Message::id_type sentId = 0;

        void handleMessageReceived(std::unique_ptr<net::Message> msg) override {
            sentId = msg->getType();
        }
    } peer(daemon);

    peer.simulateReceivedMessage(std::make_unique<TestMessage>());

    daemon.getEngine().getIOContext().run_one();

    BOOST_CHECK_EQUAL(TestMessage::type, peer.sentId);
}

BOOST_AUTO_TEST_CASE(peer_can_receive_valid_messages) {
    TestDaemon daemon;
    daemon.registerMessageHandler<TestMessageHandler>();

    auto peer = std::make_shared<TestPeer>(daemon);

    BOOST_CHECK_NO_THROW(
            peer->simulateReceivedMessage(std::make_unique<TestMessage>())
    );
}

BOOST_AUTO_TEST_SUITE_END()

}