//
// Created by void on 16/04/2020.
//

#include <loom/testing.h>

#include <loom/testing/TestDaemon.h>
#include <loom/testing/TestMessage.h>
#include <loom/testing/await.h>

#include <loom/daemon/topics/SubjectManagement.h>
#include <loom/lql/SimpleMatcher.h>

#include <optional>

using namespace loom::testing;

namespace loom::daemon::testing {

BOOST_AUTO_TEST_SUITE(DaemonSubjectTests)

BOOST_AUTO_TEST_CASE(daemon_forwards_announced_subject_to_engine) {
    TestDaemon daemon;
    auto &engine = daemon.getEngine();
    daemon.registerMessageHandler<TestMessageHandler>();

    engine::ActorHandle actor;

    awaitCallback(actor, [&] {
        engine.registerActor({{"uuid", "test-actor"}}, std::shared_ptr<engine::ActorBrain>(), [&](auto handle) {
            actor = handle;
        }, [](const auto &err) { std::rethrow_exception(err); });
    });

    std::optional<std::shared_ptr<engine::Subscription> > result;
    awaitCallback(result, [&] {
        engine.subscribe(
                actor,
                lql::AnyMatch,
                SubjectSubscriptionFlags::Defaults,
                [&](const std::shared_ptr<engine::Subscription> &subscription) {
                    result = subscription;
                }, [&](const auto &ex) {
                    result = std::shared_ptr<engine::Subscription>();
                });
    });

    BOOST_REQUIRE(result.value());

    engine::SubjectHandle createdHandle;

    auto subscription = result->get();
    subscription->subjectAnnounced += [&](const engine::SubjectHandle &handle, bool isUpdated) {
        createdHandle = handle;
    };

    auto peer = daemon.simulatePeerConnected("test-actor-2");

    auto msg = std::make_unique<net::topics::SubjectAnnouncement>();
    msg->subject = {
            {"uuid",  "1"},
            {"realm", "default"}
    };

    peer->simulateReceivedMessage(std::move(msg));

    awaitCallback(createdHandle, [] {});

    BOOST_REQUIRE(createdHandle);
}

BOOST_AUTO_TEST_CASE(announced_subjects_are_dropped_if_peer_disconnects) {
    TestDaemon daemon;
    auto &engine = daemon.getEngine();

    engine::ActorHandle actor;

    awaitCallback(actor, [&] {
        engine.registerActor({{"uuid", "test-actor"}}, std::shared_ptr<engine::ActorBrain>(), [&](auto handle) {
            actor = handle;
        }, [](const auto &err) { std::rethrow_exception(err); });
    });

    std::optional<std::shared_ptr<engine::Subscription> > result;
    awaitCallback(result, [&] {
        engine.subscribe(
                actor,
                "(realm=default)",
                SubjectSubscriptionFlags::Defaults,
                [&](const std::shared_ptr<engine::Subscription> &subscription) {
                    result = subscription;
                }, [](const auto &err) { std::rethrow_exception(err); });
    });

    std::optional<engine::SubjectHandle> engineHandle;

    auto subscription = result->get();
    subscription->subjectAnnounced += [&](const engine::SubjectHandle &handle, bool isUpdated) {
        engineHandle = handle;
    };

    std::optional<ConfigMap> droppedSubjectId;

    subscription->subjectDropped += [&](const ConfigMap &subjectId) {
        droppedSubjectId = subjectId;
    };

    auto peer = daemon.simulatePeerConnected("test-actor-2");

    auto msg = std::make_unique<net::topics::SubjectAnnouncement>();
    msg->subject = {
            {"uuid",  "1"},
            {"realm", "default"}
    };

    peer->simulateReceivedMessage(std::move(msg));

    await(engineHandle);

    auto engineSubjectId = (**engineHandle).getUniqueId();
    engineHandle.reset();

    peer->simulateDisconnect();
    peer.reset();

    engine.getIOContext().poll();

    BOOST_REQUIRE(droppedSubjectId == engineSubjectId);
}

BOOST_AUTO_TEST_SUITE_END()

}
