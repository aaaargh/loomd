//
// Created by void on 23/04/2020.
//

#ifndef LOOM_TESTING_TESTMESSAGE_H
#define LOOM_TESTING_TESTMESSAGE_H

#include <loom/net/Message.h>
#include <loom/daemon/DaemonMessageHandler.h>

namespace loom::testing {

class TestMessage : public net::TypedMessage<0x1234> {
public:
    explicit TestMessage(id_type type = TestMessage::type);

public:
    [[nodiscard]] id_type getType() const override {
        return myType;
    }

public:
    std::string value = "This is a test message.";

private:
    id_type myType;
};

class TestMessageSerializer : public net::MemberSerializer<
        &TestMessage::value> {
};

class TestMessageHandler : public daemon::TypedDaemonMessageHandler<TestMessage> {
public:
    const net::MessageSerializer &getTypedSerializer() const override;

    void processTypedMessage(const std::shared_ptr<daemon::Peer> &peer,
                             std::unique_ptr<message_type> message) const override;

public:
    mutable std::unique_ptr<TestMessage> messageReceived;
};

class TestMessageRespondingHandler : public TestMessageHandler {
public:
    void processTypedMessage(const std::shared_ptr<daemon::Peer> &peer,
                             std::unique_ptr<message_type> message) const override;

public:
    mutable bool wasSuccessful = false;
    mutable bool hadError = false;
};

}

#endif //LOOM_TESTING_TESTMESSAGE_H
