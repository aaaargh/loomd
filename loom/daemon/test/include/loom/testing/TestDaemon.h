//
// Created by void on 23/04/2020.
//

#ifndef LOOM_TESTING_TESTDAEMON_H
#define LOOM_TESTING_TESTDAEMON_H

#include <loom/daemon/Daemon.h>

#include <loom/testing/TestEngine.h>
#include <loom/testing/TestPeer.h>

namespace loom::testing {

class TestDaemon : public daemon::Daemon {
public:
    using Daemon::Daemon;

    explicit TestDaemon(
            std::unique_ptr<engine::Engine> engine = std::make_unique<TestEngine>()
    );

public:
    [[nodiscard]] std::shared_ptr<TestPeer> simulatePeerConnected(const std::string &actorId = "test-actor");

private:
    std::unique_ptr<engine::Engine> selfContainedEngine;
};

}

#endif //LOOM_TESTING_TESTDAEMON_H
