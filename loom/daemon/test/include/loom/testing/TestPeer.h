//
// Created by void on 23/04/2020.
//

#ifndef LOOM_TESTING_TESTPEER_H
#define LOOM_TESTING_TESTPEER_H

#include <loom/daemon/Peer.h>

namespace loom::testing {

class TestPeer : public daemon::Peer {
public:
    explicit TestPeer(daemon::Daemon &daemon);

public:
    void simulateReceivedMessage(std::unique_ptr<net::Message> msg);

    void simulateDisconnect();

protected:
public:
    void start() override;

protected:
    net::ProtocolHandler &getProtocolHandler() const override;

private:
    std::shared_ptr<net::ProtocolHandler> protocolHandler;
};

}

#endif //LOOM_TESTING_TESTPEER_H
