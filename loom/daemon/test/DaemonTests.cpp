//
// Created by void on 15/03/2020.
//

#include <loom/testing.h>

#include <loom/testing/TestDaemon.h>
#include <loom/testing/TestMessage.h>

using namespace loom::testing;

namespace loom::daemon::testing {

BOOST_AUTO_TEST_SUITE(DaemonTests)

BOOST_AUTO_TEST_CASE(daemon_throws_exception_for_invalid_messages) {
    TestDaemon daemon;

    auto peer = daemon.simulatePeerConnected();

    BOOST_CHECK_THROW(
            peer->simulateReceivedMessage(std::make_unique<TestMessage>(7777)),
            std::domain_error
    );
}

BOOST_AUTO_TEST_CASE(daemon_invokes_message_handlers) {
    TestDaemon daemon;

    bool sentFlag = false;

    class Handler :
            public DaemonMessageHandler {
    public:
        explicit Handler(bool &sentFlag) : sentFlag(sentFlag) {}

        [[nodiscard]] bool canProcess(net::Message::id_type type) const
        override {
            return type == TestMessage::type;
        }

        void processMessage(const std::shared_ptr<Peer> &peer, std::unique_ptr<net::Message> message) const
        override {
            sentFlag = true;
        }

    private:
        bool &sentFlag;
    };

    daemon.registerMessageHandler<Handler>(sentFlag);

    auto peer = daemon.simulatePeerConnected();

    peer->simulateReceivedMessage(std::make_unique<TestMessage>());

    BOOST_CHECK(sentFlag);
}

BOOST_AUTO_TEST_SUITE_END()

}