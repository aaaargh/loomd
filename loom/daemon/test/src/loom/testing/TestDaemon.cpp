//
// Created by void on 23/04/2020.
//

#include <loom/testing/TestDaemon.h>
#include <loom/testing/TestEngine.h>

namespace loom::testing {

TestDaemon::TestDaemon(std::unique_ptr<engine::Engine> engine) :
        Daemon(*engine),
        selfContainedEngine(std::move(engine)) {}

std::shared_ptr<TestPeer> TestDaemon::simulatePeerConnected(const std::string &actorId) {
    auto peer = std::make_shared<TestPeer>(*this);
    Daemon::handlePeerConnected(peer);
    peer->setIdentity(dynamic_cast<TestEngine &>(getEngine()).makeTestActor(actorId, peer->shared_from_this()));
    return peer;
}

}