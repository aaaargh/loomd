//
// Created by void on 23/04/2020.
//

#include <loom/testing/TestPeer.h>

namespace loom::testing {

struct NullProtocolHandler : net::ProtocolHandler {
    using ProtocolHandler::ProtocolHandler;

    void notifyClosed() override {

    }

    void notifyConnected() override {

    }

    void sendMessage(std::unique_ptr<net::Message> message, const SendMessageSuccessCallback &successCallback,
                     const SendMessageErrorCallback &errorCallback) override {
        successCallback();
    }
};

TestPeer::TestPeer(daemon::Daemon &daemon) : Peer(daemon) {
    protocolHandler = std::make_shared<NullProtocolHandler>(*this);
}

void TestPeer::simulateReceivedMessage(std::unique_ptr<net::Message> msg) {
    handleMessageReceived(std::move(msg));
}

void TestPeer::simulateDisconnect() {
    protocolHandler->notifyClosed();
    handleDisconnect(std::exception_ptr());
}

void TestPeer::start() {}

net::ProtocolHandler &TestPeer::getProtocolHandler() const {
    return *protocolHandler;
}

}
