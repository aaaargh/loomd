//
// Created by void on 23/04/2020.
//

#include <loom/testing/TestMessage.h>

namespace loom::testing {

TestMessage::TestMessage(net::Message::id_type myType) : myType(myType) {}

void TestMessageHandler::processTypedMessage(const std::shared_ptr<daemon::Peer> &peer,
                                             std::unique_ptr<message_type> message) const {
    messageReceived = std::move(message);
}

void TestMessageRespondingHandler::processTypedMessage(const std::shared_ptr<daemon::Peer> &peer,
                                                       std::unique_ptr<message_type> message) const {
    wasSuccessful = hadError = false;

    peer->sendMessage(std::move(message), [this] {
        wasSuccessful = true;
    }, [this](const std::exception_ptr &ex) {
        hadError = true;
    });
}

const net::MessageSerializer &TestMessageHandler::getTypedSerializer() const {
    const static TestMessageSerializer serializer;
    return serializer;
}
}