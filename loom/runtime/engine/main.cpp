//
// Created by void on 20/04/2020.
//

#include <boost/asio/signal_set.hpp>

#include <loom/daemon/DaemonTCP.h>
#include <loom/engine/Engine.h>
#include <iostream>

#include <argh.h>
#include <boost/lexical_cast.hpp>

int main(int, const char *argv[]) {
    argh::parser cmdl(argv);

    std::string daemonAddress = "0.0.0.0";
    unsigned short daemonPort = 6788;

    if (cmdl[{"-a", "--address"}]) {
        daemonAddress = cmdl("host").str();
    }

    if (cmdl[{"-p", "--port"}]) {
        daemonPort = boost::lexical_cast<unsigned short>(cmdl("port").str());
    }

    boost::asio::io_context ioContext;

    std::cout << "Creating engine ...\n";
    loom::engine::Engine engine(ioContext);

    boost::asio::signal_set signals(engine.getIOContext(), SIGINT, SIGTERM);
    
    loom::daemon::DaemonTCP daemon(engine, boost::asio::ip::make_address(daemonAddress), daemonPort);

    signals.async_wait(
            [&](boost::system::error_code, int) {
                engine.getIOContext().stop();
            });

    ioContext.run();

    std::cout << "Shutting down engine..." << std::endl;

    return 0;
}
