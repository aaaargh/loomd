//
// Created by void on 26/04/2020.
//

#include <loom/testing.h>

#include <loom/testing/IntegrationTestEnvironment.h>

#include <loom/client/topics/DataRequest.h>
#include <loom/net/topics/Authentication.h>

namespace loom::testing {

BOOST_AUTO_TEST_SUITE(SubjectDataTests)

struct SimpleStringReader : DataSourceReader {
    const std::string data = "Hello my dear World, this is a lovely test string. Do you like it?";

    void readAsync(MutableBuffer target, size_t offset, const ReadCompleteCallback &callback) override {
        auto toRead = std::min(data.size() - offset, target.size());

        std::memcpy(target.data(), data.data() + offset, toRead);

        callback({
                         .bytesRead = toRead,
                         .isEOF = toRead < target.size()
                 });
    }
};

struct SimpleStringWriter : DataSourceWriter {
    bool beginTransactionCalled = false;
    bool endTransactionCalled = false;
    std::string written;
    bool eofReceived = false;

    void beginTransaction() override {
        beginTransactionCalled = true;
    }

    void write(const ImmutableBuffer &buffer, size_t offset) override {
        auto len = std::max(buffer.size() + offset, written.size());
        written.resize(len);

        std::memcpy(written.data() + offset, buffer.data(), buffer.size());
    }

    void setEOF(size_t offset) override {
        eofReceived = true;
        written.resize(offset);
    }

    void endTransaction() override {
        endTransactionCalled = true;
    }
};

BOOST_AUTO_TEST_CASE(can_request_data_using_builtin_reader_writer) {
    struct : IntegrationTestEnvironment {

        void handleTestPhase(daemon::Daemon &daemon,
                             std::vector<std::unique_ptr<client::BasicClient> > &clients) override {
            // Identify
            ConfigMap identity = {
                    {"uuid", "i am client"}
            };

            clients[0]->authenticate(identity, {}, [](const auto &err) {
                std::rethrow_exception(err);
            });

            awaitClientMessageReceived(clients[0], net::topics::MT_AuthResponse);

            // Subscribe
            clients[0]->subscribeToSubjects(
                    "(realm=*)",
                    SubjectSubscriptionFlags::Defaults,
                    {},
                    [](const auto &err) {
                        std::rethrow_exception(err);
                    });

            // Announce
            auto reader = std::make_shared<SimpleStringReader>();

            ConfigMap subject = {
                    {"uuid",  "some file"},
                    {"realm", "default"},
                    {"file",  DataSourceInfo{
                            .reader = reader
                    }}
            };

            clients[0]->announceSubject(
                    subject,
                    SubjectAnnouncementFlags::Defaults,
                    {}, [](const auto &err) {
                        std::rethrow_exception(err);
                    });

            auto writer = std::make_shared<SimpleStringWriter>();

            clients[0]->requestData(subject, "file", 0, 120, writer, {},
                                    [](const auto &err) {
                                        std::rethrow_exception(err);
                                    });

            awaitClientMessageReceived(clients[0], net::topics::MT_AuthResponse);

            BOOST_REQUIRE(writer->eofReceived);
            BOOST_REQUIRE(writer->beginTransactionCalled);
            BOOST_REQUIRE(writer->endTransactionCalled);
            BOOST_REQUIRE_EQUAL(reader->data, writer->written);
        }
    } env;
    env
            .with<daemon::DaemonTCP>()
            .with<client::ClientTCP>()
            .go();
}

BOOST_AUTO_TEST_SUITE_END()

}
