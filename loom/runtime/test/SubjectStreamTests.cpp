//
// Created by void on 20/05/2020.
//

#include <loom/testing.h>

#include <loom/testing/IntegrationTestEnvironment.h>

#include <loom/client/topics/DataRequest.h>
#include <loom/net/topics/Authentication.h>

namespace loom::testing {

BOOST_AUTO_TEST_SUITE(SubjectStreamTests)

struct SimpleStringReader : DataSourceReader {
    size_t eofPosition = 60000;
    size_t bytesRead = 0;

    void readAsync(MutableBuffer target, size_t offset, const ReadCompleteCallback &callback) override {
        auto toRead = bytesRead > eofPosition ? 0 : std::min(target.size(), eofPosition - bytesRead);

        bytesRead += toRead;

        callback({
                         .bytesRead = toRead,
                         .isEOF = bytesRead + target.size() > eofPosition
                 });
    }
};

struct SimpleStringWriter : DataSourceWriter {
    bool beginTransactionCalled = false;
    bool endTransactionCalled = false;
    size_t bytesWritten;
    size_t eofPosition = 0;
    bool eofReceived = false;

    void beginTransaction() override {
        beginTransactionCalled = true;
    }

    void write(const ImmutableBuffer &buffer, size_t offset) override {
        bytesWritten += buffer.size();
    }

    void setEOF(size_t offset) override {
        eofReceived = true;
        eofPosition = offset;
    }

    void endTransaction() override {
        endTransactionCalled = true;
    }
};

BOOST_AUTO_TEST_CASE(can_stream_data_using_builtin_reader_writer) {
    struct : IntegrationTestEnvironment {

        void handleTestPhase(daemon::Daemon &daemon,
                             std::vector<std::unique_ptr<client::BasicClient> > &clients) override {
            // Identify
            ConfigMap identity = {
                    {"uuid", "i am client"}
            };

            clients[0]->authenticate(identity, {}, [](const auto &err) {
                std::rethrow_exception(err);
            });

            awaitClientMessageReceived(clients[0], net::topics::MT_AuthResponse);

            // Subscribe
            clients[0]->subscribeToSubjects(
                    "(realm=*)",
                    SubjectSubscriptionFlags::Defaults,
                    {}, [](const auto &err) {
                        std::rethrow_exception(err);
                    });

            // Announce
            auto reader = std::make_shared<SimpleStringReader>();

            ConfigMap subject = {
                    {"uuid",  "some file"},
                    {"realm", "default"},
                    {"file",  DataSourceInfo{
                            .accessType = DataSourceInfo::AccessType::Streamed,
                            .reader = reader
                    }}
            };

            clients[0]->announceSubject(
                    subject,
                    SubjectAnnouncementFlags::Defaults,
                    {}, [](const auto &err) {
                        std::rethrow_exception(err);
                    });

            auto writer = std::make_shared<SimpleStringWriter>();

            clients[0]->requestData(subject, "file", 0, 120, writer, {},
                                    [](const auto &err) {
                                        std::rethrow_exception(err);
                                    });

            awaitClientMessageReceived(clients[0], net::topics::MT_DataResponse);

            BOOST_REQUIRE(!writer->eofReceived);
            BOOST_REQUIRE(writer->beginTransactionCalled);
            BOOST_REQUIRE(writer->endTransactionCalled);
            BOOST_REQUIRE_LT(writer->bytesWritten, reader->bytesRead);
        }
    } env;
    env
            .with<daemon::DaemonTCP>()
            .with<client::ClientTCP>()
            .go();
}

BOOST_AUTO_TEST_CASE(can_stream_data_until_eof) {
    struct : IntegrationTestEnvironment {

        void handleTestPhase(daemon::Daemon &daemon,
                             std::vector<std::unique_ptr<client::BasicClient> > &clients) override {
            // Identify
            ConfigMap identity = {
                    {"uuid", "i am client"}
            };

            clients[0]->authenticate(identity, {}, [](const auto &err) {
                std::rethrow_exception(err);
            });

            messagePump();

            // Subscribe
            clients[0]->subscribeToSubjects(
                    "(realm=*)",
                    SubjectSubscriptionFlags::Defaults,
                    {}, [](const auto &err) {
                        std::rethrow_exception(err);
                    });

            messagePump();

            // Announce
            auto reader = std::make_shared<SimpleStringReader>();

            ConfigMap subject = {
                    {"uuid",  "some file"},
                    {"realm", "default"},
                    {"file",  DataSourceInfo{
                            .accessType = DataSourceInfo::AccessType::Streamed,
                            .reader = reader
                    }}
            };

            clients[0]->announceSubject(
                    subject,
                    SubjectAnnouncementFlags::Defaults,
                    {}, [](const auto &err) {
                        std::rethrow_exception(err);
                    });

            messagePump();

            auto writer = std::make_shared<SimpleStringWriter>();

            clients[0]->requestData(subject, "file", 0, 0, writer, {},
                                    [](const auto &err) {
                                        std::rethrow_exception(err);
                                    });

            messagePump();

            BOOST_REQUIRE(writer->eofReceived);
            BOOST_REQUIRE(writer->beginTransactionCalled);
            BOOST_REQUIRE(!writer->endTransactionCalled);
            BOOST_REQUIRE_EQUAL(writer->bytesWritten, reader->bytesRead);
        }
    } env;
    env
            .with<daemon::DaemonTCP>()
            .with<client::ClientTCP>()
            .go();
}


BOOST_AUTO_TEST_SUITE_END()

}
