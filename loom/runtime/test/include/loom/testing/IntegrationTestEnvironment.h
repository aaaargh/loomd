//
// Created by void on 23/04/2020.
//

#ifndef LOOM_TESTING_INTEGRATIONTESTENVIRONMENT_H
#define LOOM_TESTING_INTEGRATIONTESTENVIRONMENT_H

#include <memory>
#include <loom/daemon/Daemon.h>
#include <loom/client/BasicClient.h>
#include <loom/daemon/DaemonTCP.h>
#include <loom/client/ClientTCP.h>

namespace loom::testing {

class IntegrationTestEnvironment {
public:
    template<typename T>
    IntegrationTestEnvironment &with(std::unique_ptr<T> object) {
        return _with(std::move(object));
    }

    template<typename T, typename ... Args>
    IntegrationTestEnvironment &with(Args &&... args) {
        return with(make<T>(std::forward(args)...));
    }

protected:
    template<typename T, typename... Args>
    std::unique_ptr<T> make(Args &&... args) {
        return std::make_unique<T>(std::forward(args)...);
    }

public:
    virtual void handleDaemonMessageReceived(net::Message::id_type type);

    virtual void handleDaemonMessageSent(net::Message::id_type type);

    virtual void handleClientMessageReceived(const client::BasicClient &client, net::Message::id_type type);

    virtual void handleClientMessageSent(const client::BasicClient &client, net::Message::id_type type);


    void awaitDaemonMessageReceived(net::Message::id_type type,
                                    std::function<bool(size_t)> callback = [](size_t) { return true; });

    void awaitClientMessageReceived(const std::unique_ptr<client::BasicClient> &client,
                                    net::Message::id_type type,
                                    std::function<bool(size_t)> callback = [](size_t) { return true; });

private:
    IntegrationTestEnvironment &_with(std::unique_ptr<engine::Engine> newEngine) {
        if (engine) {
            throw std::logic_error("Engine already set.");
        }

        engine = std::move(newEngine);
        return *this;
    }

    IntegrationTestEnvironment &_with(std::unique_ptr<daemon::Daemon> newDaemon) {
        daemon = std::move(newDaemon);
        return *this;
    }

    IntegrationTestEnvironment &_with(std::unique_ptr<client::BasicClient> client) {
        if (!daemon) {
            throw std::logic_error("Daemon not set.");
        }

        clients.emplace_back(std::move(client));
        return *this;
    }

public:
    void go();

    void messagePump();

public:
    virtual void handleTestPhase(daemon::Daemon &daemon,
                                 std::vector<std::unique_ptr<client::BasicClient> > &clients) = 0;

protected:
    boost::asio::io_context ioContext{};

    std::unique_ptr<engine::Engine> engine;
    std::unique_ptr<daemon::Daemon> daemon;
    std::vector<std::unique_ptr<client::BasicClient> > clients;

    std::mutex messageCVMutex;

    struct CountedCV {
        size_t numTriggered = 0;
        std::condition_variable cv;
    };

    std::map<net::Message::id_type, CountedCV> daemonMessageReceivedCVs;
    std::map<std::pair<const client::BasicClient *, net::Message::id_type>, CountedCV>
            clientMessageReceivedCVs;
};

template<>
std::unique_ptr<engine::Engine> IntegrationTestEnvironment::make<engine::Engine>();

template<>
std::unique_ptr<daemon::DaemonTCP> IntegrationTestEnvironment::make<daemon::DaemonTCP>();

template<>
std::unique_ptr<client::ClientTCP> IntegrationTestEnvironment::make<client::ClientTCP>();

}

#endif //LOOM_TESTING_INTEGRATIONTESTENVIRONMENT_H
