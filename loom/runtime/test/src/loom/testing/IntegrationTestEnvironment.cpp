//
// Created by void on 23/04/2020.
//

#include <loom/testing/IntegrationTestEnvironment.h>
#include <loom/daemon/Daemon.h>

#include <loguru.hpp>
#include <future>

namespace loom::testing {

namespace detail {
struct DaemonMessageHandlerMonitor : public daemon::DaemonMessageHandler {
    IntegrationTestEnvironment &env;

    explicit DaemonMessageHandlerMonitor(IntegrationTestEnvironment
                                         &env) : env(env) {}

    bool canProcess(net::Message::id_type type) const override {
        return false;
    }

    bool canSerialize(net::Message::id_type type) const override {
        env.handleDaemonMessageSent(type);
        return false;
    }

    bool canDeserialize(net::Message::id_type type) const override {
        env.handleDaemonMessageReceived(type);
        return false;
    }

    size_t getPriority() const override {
        return 1;
    }
};

struct ClientMessageHandlerMonitor : public client::ClientMessageHandler {
    IntegrationTestEnvironment &env;
    client::BasicClient &client;

    explicit ClientMessageHandlerMonitor(IntegrationTestEnvironment
                                         &env, client::BasicClient &client) : env(env), client(client) {}

    bool canProcess(net::Message::id_type type) const override {
        return false;
    }

    bool canSerialize(net::Message::id_type type) const override {
        env.handleClientMessageSent(client, type);
        return false;
    }

    bool canDeserialize(net::Message::id_type type) const override {
        env.handleClientMessageReceived(client, type);
        return false;
    }

    size_t getPriority() const override {
        return 1;
    }
};
}

void IntegrationTestEnvironment::awaitDaemonMessageReceived(net::Message::id_type type,
                                                            std::function<bool(size_t)> callback) {
    std::unique_lock lk(messageCVMutex);
    CountedCV &cv = daemonMessageReceivedCVs[type];

    auto initialTrigger = cv.numTriggered;

    auto x = std::async(std::launch::async, [this] {
        messagePump();
    });

    cv.cv.wait(lk, [initialTrigger, &cv, callback] {
        auto n = cv.numTriggered - initialTrigger;
        return n > 0 && callback(n);
    });
}

void IntegrationTestEnvironment::awaitClientMessageReceived(const std::unique_ptr<client::BasicClient> &client,
                                                            net::Message::id_type type,
                                                            std::function<bool(size_t)> callback) {
    std::unique_lock lk(messageCVMutex);
    CountedCV &cv = clientMessageReceivedCVs[std::make_pair(client.get(), type)];

    auto initialTrigger = cv.numTriggered;

    auto x = std::async(std::launch::async, [this] {
        messagePump();
    });

    cv.cv.wait(lk, [initialTrigger, &cv, callback] {
        auto n = cv.numTriggered - initialTrigger;
        return n > 0 && callback(n);
    });
}

void IntegrationTestEnvironment::handleDaemonMessageReceived(net::Message::id_type type) {
    CountedCV &cv = daemonMessageReceivedCVs[type];
    cv.numTriggered++;
    cv.cv.notify_all();
}

void IntegrationTestEnvironment::handleDaemonMessageSent(net::Message::id_type type) {

}

void IntegrationTestEnvironment::handleClientMessageReceived(const client::BasicClient &client,
                                                             net::Message::id_type type) {
    CountedCV &cv = clientMessageReceivedCVs[std::make_pair(&client, type)];
    cv.numTriggered++;
    cv.cv.notify_all();
}

void IntegrationTestEnvironment::handleClientMessageSent(const client::BasicClient &client,
                                                         net::Message::id_type type) {

}

void IntegrationTestEnvironment::go() {
    if (!engine) {
        engine = make<engine::Engine>();
    }

    if (!daemon) {
        throw std::logic_error("Daemon not set.");
    }

    daemon->registerMessageHandler<detail::DaemonMessageHandlerMonitor>(*this);

    for (auto &client : clients) {
        client->registerMessageHandler<detail::ClientMessageHandlerMonitor>(*this, *client);
    }

    messagePump();

    // Connection phase.
    for (auto &client : clients) {
        client->connect();
    }

    // Test phase
    LOG_S(INFO) << "Entering test phase.";
    handleTestPhase(*daemon, clients);
    LOG_S(INFO) << "Exiting test phase.";

    // Shutdown phase
    for (auto &client : clients) {
        messagePump();
        LOG_S(INFO) << "Shutting down client.";
        client.reset();
        messagePump();
    }

    messagePump();
    LOG_S(INFO) << "Shutting down daemon.";
    daemon.reset();
    ioContext.run();
    LOG_S(INFO) << "Test complete.";


}

void IntegrationTestEnvironment::messagePump() {
    ioContext.poll();
}

template<>
std::unique_ptr<engine::Engine> IntegrationTestEnvironment::make<engine::Engine>() {
    return std::make_unique<engine::Engine>(ioContext);
}

template<>
std::unique_ptr<daemon::DaemonTCP> IntegrationTestEnvironment::make<daemon::DaemonTCP>() {
    if (!engine) {
        with<engine::Engine>();
    }

    return std::make_unique<daemon::DaemonTCP>(*engine);
}

template<>
std::unique_ptr<client::ClientTCP> IntegrationTestEnvironment::make<client::ClientTCP>() {
    return std::make_unique<client::ClientTCP>(ioContext, "localhost", daemon::DaemonTCP::DefaultPort);
}

}
