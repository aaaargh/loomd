//
// Created by void on 23/04/2020.
//

#include <loom/testing.h>

#include <loom/testing/IntegrationTestEnvironment.h>

namespace loom::testing {

BOOST_AUTO_TEST_SUITE(BasicIntegrationTests)

BOOST_AUTO_TEST_CASE(setup_with_single_client_is_working) {
    struct : IntegrationTestEnvironment {
        bool testPhaseEntered = false;

        void handleTestPhase(daemon::Daemon &daemon,
                             std::vector<std::unique_ptr<client::BasicClient> > &clients) override {
            testPhaseEntered = true;
        }
    } env;

    env
            .with<daemon::DaemonTCP>()
            .with<client::ClientTCP>()
            .go();

    BOOST_REQUIRE(env.testPhaseEntered);
}

BOOST_AUTO_TEST_CASE(can_announce_subject) {
    class MyTestEngine : public engine::Engine {

    };

    struct : IntegrationTestEnvironment {
        void handleTestPhase(daemon::Daemon &daemon,
                             std::vector<std::unique_ptr<client::BasicClient> > &clients) override {
            std::optional<bool> success;

            ConfigMap configMap{
                    {"uuid",  "something"},
                    {"realm", "default"}
            };

            clients[0]->announceSubject(configMap,
                                        SubjectAnnouncementFlags::Defaults,
                                        [&] { success = true; },
                                        [&](const auto &error) { success = false; }
            );

            messagePump();

            await(success);
            BOOST_REQUIRE(success);
        }
    } env;

    env
            .with<daemon::DaemonTCP>()

            .with<client::ClientTCP>()

            .go();
}

BOOST_AUTO_TEST_CASE(can_subscribe_to_other_clients_subjects) {
    struct : IntegrationTestEnvironment {
        void handleTestPhase(daemon::Daemon &daemon,
                             std::vector<std::unique_ptr<client::BasicClient> > &clients) override {
            std::optional<bool> success;

            clients[0]->subscribeToSubjects("(realm=*)",
                                            SubjectSubscriptionFlags::Defaults,
                                            [&] { success = true; },
                                            [&](const auto &error) { success = false; }
            );

            messagePump();

            await(success);
            BOOST_REQUIRE(success);

            success.reset();

            ConfigMap configMap{
                    {"uuid",  "something"},
                    {"realm", "default"}
            };

            clients[1]->announceSubject(configMap,
                                        SubjectAnnouncementFlags::Defaults,
                                        [&] { success = true; },
                                        [&](const auto &error) { success = false; }
            );

            messagePump();

            await(success);
            BOOST_REQUIRE(success);
        }
    } env;

    env
            .with<daemon::DaemonTCP>()

            .with<client::ClientTCP>()

            .with<client::ClientTCP>()

            .go();
}


BOOST_AUTO_TEST_SUITE_END()

}
