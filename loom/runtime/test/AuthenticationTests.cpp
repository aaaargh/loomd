//
// Created by void on 26/04/2020.
//

#include <loom/testing.h>

#include <loom/testing/IntegrationTestEnvironment.h>

namespace loom::testing {

BOOST_AUTO_TEST_SUITE(AuthenticationTests)

BOOST_AUTO_TEST_CASE(authentication_fails_when_uuid_is_missing) {
    struct : IntegrationTestEnvironment {

        bool authenticated = false;

        void handleTestPhase(daemon::Daemon &daemon,
                             std::vector<std::unique_ptr<client::BasicClient> > &clients) override {
            ConfigMap identity = {
                    {"not-an-uuid", "i am client"}
            };

            struct EventListener : ClientEventListener {
                std::optional<bool> authSuccess;

                void onAuthenticationFailed(Client &client, const std::string &reason) override {
                    authSuccess = false;
                }

                void onAuthenticated(Client &client, const ConfigMap &identity) override {
                    authSuccess = true;
                }
            };

            clients[0]->setEventListener(std::make_unique<EventListener>());
            auto &authSuccess = static_cast<EventListener &>(clients[0]->getEventListener()).authSuccess;

            clients[0]->authenticate(identity, {}, [](const auto &err) {
                std::rethrow_exception(err);
            });

            await([&] {
                messagePump();
                return authSuccess.has_value();
            });

            bool success = authSuccess.value();

            BOOST_REQUIRE(!success);
        }
    } env;
    env
            .with<daemon::DaemonTCP>()
            .with<client::ClientTCP>()
            .go();
}

BOOST_AUTO_TEST_CASE(client_can_authenticate) {
    struct : IntegrationTestEnvironment {

        bool authenticated = false;

        void handleTestPhase(daemon::Daemon &daemon,
                             std::vector<std::unique_ptr<client::BasicClient> > &clients) override {
            ConfigMap identity = {
                    {"uuid", "i am client"}
            };

            struct EventListener : ClientEventListener {
                std::optional<bool> authSuccess;

                void onAuthenticationFailed(Client &client, const std::string &reason) override {
                    authSuccess = false;
                }

                void onAuthenticated(Client &client, const ConfigMap &identity) override {
                    authSuccess = true;
                }
            };

            clients[0]->setEventListener(std::make_unique<EventListener>());
            auto &authSuccess = dynamic_cast<EventListener &>(clients[0]->getEventListener()).authSuccess;

            clients[0]->authenticate(identity, {}, [](const auto &err) {
                std::rethrow_exception(err);
            });

            await([&] {
                messagePump();
                return authSuccess.has_value();
            });

            bool success = authSuccess.value();

            BOOST_REQUIRE(success);
        }
    } env;
    env
            .with<daemon::DaemonTCP>()
            .with<client::ClientTCP>()
            .go();
}

BOOST_AUTO_TEST_SUITE_END()

}