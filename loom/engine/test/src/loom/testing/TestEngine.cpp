//
// Created by void on 23/04/2020.
//

#include <loom/testing/TestEngine.h>
#include <loom/testing/TestBrain.h>
#include <loom/testing/await.h>
#include <loom/Exception.h>

namespace loom::testing {

namespace detail {

}

TestEngine::TestEngine(boost::asio::io_context &ioContext) :
        Engine(ioContext) {}

TestEngine::TestEngine(std::unique_ptr<boost::asio::io_context> ioContext) :
        Engine(*ioContext),
        selfContainedIOContext(std::move(ioContext)) {}

void TestEngine::simulateTick() {
    handleEngineTick({});
}

engine::ActorHandle TestEngine::makeTestActor(const std::string &id, std::shared_ptr<engine::ActorBrain> brain) {
    ConfigMap cm{
            {"uuid",  id},
            {"realm", "default"}
    };

    std::optional<engine::ActorHandle> actor;

    registerActor(std::move(cm), std::move(brain), [&](const engine::ActorHandle &handle) {
        actor = handle;
    }, [](const auto &err) { std::rethrow_exception(err); });

    await(actor);

    return actor.value();
}

engine::ActorHandle TestEngine::makeTestActor(const std::string &id,
                                              std::shared_ptr<engine::DataTransactionHandler> transactionHandler) {
    return makeTestActor(id, std::make_shared<TestBrain>(transactionHandler));
}

}
