//
// Created by void on 01/06/2020.
//

#include <loom/testing/TestActor.h>

namespace loom::testing {

TestEngine &TestActor::getTestEngine() {
    static TestEngine engine{};
    return engine;
}

TestActor::TestActor() : Actor(
        getTestEngine(),
        std::make_shared<NoBrain>(),
        {}) {}

}
