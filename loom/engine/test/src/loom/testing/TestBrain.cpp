//
// Created by void on 28/04/2020.
//

#include <loom/testing/TestBrain.h>
#include <loom/Exception.h>

#include <cstring>
#include <utility>

namespace loom::testing {

TestBrain::TestBrain(std::shared_ptr<engine::DataTransactionHandler> handler, engine::data_buffer_type data)
        : data(std::move(data)), handler(std::move(handler)) {

}

TestBrain::TestBrain(std::shared_ptr<engine::DataTransactionHandler> handler, size_t numBytes)
        : data{numBytes}, handler(std::move(handler)) {
    for (size_t i = 0; i < numBytes; ++i) {
        data[i] = std::byte(i);
    }
}

void TestBrain::handleDataRequest(ConfigMap subjectId, ConfigMap::key_type attribute, size_t offset, size_t len) {
    engine::data_buffer_type result{std::min(len, data.size() - offset)};
    std::memcpy(result.data(), data.data() + offset, result.size());

    auto reason = result.size() < len ? engine::DataTransactionHandler::CompleteReason::ReachedEOF
                                      : engine::DataTransactionHandler::CompleteReason::RequestComplete;

    handler->handleDataReceived(std::move(result), offset);
    handler->handleTransactionComplete(reason);
}

void TestBrain::handleSubjectOwned(engine::SubjectHandle subject) {

}

void TestBrain::handleSubjectOwnershipLost(engine::SubjectHandle subject) {

}

}