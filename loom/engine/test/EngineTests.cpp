//
// Created by void on 13/04/2020.
//

#include <loom/testing.h>

#include <loom/testing/TestEngine.h>
#include <loom/testing/await.h>

#include <loom/lql/SimpleMatcher.h>
#include <loom/lql/Parser.h>

#include <condition_variable>
#include <utility>

using namespace loom::testing;

namespace loom::engine::testing {

BOOST_AUTO_TEST_SUITE(EngineTests)

BOOST_AUTO_TEST_CASE(can_announce_subject_with_valid_config) {
    boost::asio::io_context ctx;
    TestEngine engine(ctx);

    ConfigMap subjectConfig{
            {"uuid",  "1"},
            {"realm", "default"}
    };

    std::optional<bool> result;

    auto actor = engine.makeTestActor("test-actor");

    awaitCallback(result, [&] {
        engine.announceSubject(
                actor,
                std::move(subjectConfig),
                SubjectAnnouncementFlags::Defaults,
                [&](const SubjectHandle &handle) {
                    result = true;
                }, [&](const std::exception_ptr &err) {
                    result = false;
                });
    });

    BOOST_REQUIRE(result.value());
}

BOOST_AUTO_TEST_CASE(subscription_is_working) {
    boost::asio::io_context ctx;
    TestEngine engine(ctx);

    std::optional<std::shared_ptr<Subscription> > result;

    auto actor = engine.makeTestActor("test-actor");

    awaitCallback(result, [&] {
        engine.subscribe(actor, lql::AnyMatch, SubjectSubscriptionFlags::Defaults, [&](auto s) {
                             result = s;
                         }, [&](auto error) {
                             result = std::shared_ptr<Subscription>();
                         }
        );
    });

    BOOST_REQUIRE(result.value());
}

BOOST_AUTO_TEST_CASE(subscription_receives_subject_announcement_event) {
    boost::asio::io_context ctx;
    TestEngine engine(ctx);

    auto actor = engine.makeTestActor("test-actor");

    std::optional<std::shared_ptr<Subscription> > result;

    awaitCallback(result, [&] {
        engine.subscribe(actor, lql::AnyMatch, SubjectSubscriptionFlags::Defaults, [&](auto s) {
                             result = s;
                         }, [&](auto error) {
                             result = std::shared_ptr<Subscription>();
                         }
        );
    });

    BOOST_REQUIRE(result.value());
    auto &subscription = *result;

    bool receivedAnnouncement = false;

    subscription->subjectAnnounced += [&](auto subjectHandle, bool isUpdated) {
        receivedAnnouncement = true;
    };

    std::optional<bool> subjectAnnounced;

    awaitCallback(subjectAnnounced, [&] {
        ConfigMap subjectConfig{
                {"uuid",  "1"},
                {"realm", "default"}
        };

        engine.announceSubject(
                actor,
                std::move(subjectConfig),
                SubjectAnnouncementFlags::Defaults,
                [&](SubjectHandle handle) {
                    subjectAnnounced = true;
                }, [](const auto &err) { std::rethrow_exception(err); });
    });

    BOOST_REQUIRE(receivedAnnouncement);
}

BOOST_AUTO_TEST_CASE(subscription_can_be_cancelled) {
    boost::asio::io_context ctx;
    TestEngine engine(ctx);

    auto actor = engine.makeTestActor("test-actor");

    std::optional<std::shared_ptr<Subscription> > result;

    awaitCallback(result, [&] {
        engine.subscribe(actor, lql::AnyMatch, SubjectSubscriptionFlags::Defaults, [&](auto s) {
                             result = s;
                         }, [&](auto error) {
                             result = std::shared_ptr<Subscription>();
                         }
        );
    });

    BOOST_REQUIRE(result.value());
    auto subscription = *result;
    result.reset();

    bool receivedAnnouncement = false;

    subscription->subjectAnnounced += [&](auto subjectHandle, bool isUpdated) {
        receivedAnnouncement = true;
    };

    subscription.reset();

    std::optional<bool> subjectAnnounced;

    awaitCallback(subjectAnnounced, [&] {
        ConfigMap subjectConfig{
                {"uuid",  "1"},
                {"realm", "default"}
        };

        engine.announceSubject(
                actor,
                std::move(subjectConfig),
                SubjectAnnouncementFlags::Defaults,
                [&](SubjectHandle handle) {
                    subjectAnnounced = true;
                }, [](const auto &err) { std::rethrow_exception(err); });
    });

    BOOST_REQUIRE(!receivedAnnouncement);
}

BOOST_AUTO_TEST_CASE(subscription_receives_subject_announcement_event_when_filtering) {
    boost::asio::io_context ctx;
    TestEngine engine(ctx);

    auto actor = engine.makeTestActor("test-actor");

    std::optional<std::shared_ptr<Subscription> > result;

    awaitCallback(result, [&] {
        engine.subscribe(
                actor,
                lql::Parser::parse("(|(quz='on')(foo!='baz'))"),
                SubjectSubscriptionFlags::Defaults & ~SubjectSubscriptionFlags::IgnoreOwnSubjects,
                [&](auto s) {
                    result = s;
                }, [&](auto error) {
                    result = std::shared_ptr<Subscription>();
                }
        );
    });

    BOOST_REQUIRE(result.value());
    auto &subscription = *result;

    bool receivedAnnouncement = false;

    subscription->subjectAnnounced += [&](auto subjectHandle, bool isUpdated) {
        receivedAnnouncement = true;
    };

    std::optional<bool> subjectAnnounced;

    awaitCallback(subjectAnnounced, [&] {
        ConfigMap subjectConfig{
                {"uuid",  "1"},
                {"realm", "default"},
                {"foo",   "bar"},
                {"foo",   "baz"}
        };

        engine.announceSubject(
                actor,
                std::move(subjectConfig),
                SubjectAnnouncementFlags::Defaults,
                [&](const SubjectHandle &handle) {
                    subjectAnnounced = true;
                }, [](const auto &err) { std::rethrow_exception(err); });
    });

    BOOST_REQUIRE(receivedAnnouncement);
}

BOOST_AUTO_TEST_CASE(engine_tick_is_working) {
    boost::asio::io_context ctx;

    class : public TestEngine {
    public:
        using TestEngine::TestEngine;
        size_t numTicks = 0;

    protected:
        void handleEngineTick(const boost::system::error_code &ec) override {
            numTicks++;
        }
    } engine(ctx);

    engine.simulateTick();
    BOOST_REQUIRE_EQUAL(1, engine.numTicks);

    engine.simulateTick();
    BOOST_REQUIRE_EQUAL(2, engine.numTicks);

    engine.simulateTick();
    BOOST_REQUIRE_EQUAL(3, engine.numTicks);
}

BOOST_AUTO_TEST_CASE(variant_selection_is_forwarded_to_readers) {
    boost::asio::io_context ctx;
    TestEngine engine(ctx);

    struct Reader : DataSourceReader {

    };

    auto reader = std::make_shared<Reader>();

    struct Handler : DataTransactionHandler {
        std::optional<bool> success;

        void handleError(const std::exception_ptr &error) override {
            success = false;
        }

        void handleTransactionComplete(CompleteReason reason) override {
            success = true;
        }

        void handleDataReceived(data_buffer_type dataChunk, size_t chunkOffset) override {

        }
    };

    auto handler = std::make_shared<Handler>();
    auto actor = engine.makeTestActor("test-actor", handler);

    ConfigMap subjectConfig{
            {"uuid",  "1"},
            {"realm", "default"},
            {"color", Variant("red", "blue")},
            {"data",  DataSourceInfo{.reader=reader}}
    };

    SubjectHandle subjectHandle;

    awaitCallback(handler->success, [&] {
        engine.announceSubject(
                actor,
                std::move(subjectConfig),
                SubjectAnnouncementFlags::Defaults,
                [&](SubjectHandle handle) {
                    handler->success = true;
                    subjectHandle = std::move(handle);
                }, [](const auto &err) { std::rethrow_exception(err); });
    });

    handler->success.reset();

    ConfigMap variantConfig{
            {"uuid",  "1"},
            {"realm", "default"},
            {"color", "blue"}
    };

    engine.requestData(actor, ConfigMapAttributeKey(variantConfig, "data"), 0, 0, handler);

    await(handler->success);
}

BOOST_AUTO_TEST_SUITE_END()
}
