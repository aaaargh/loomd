//
// Created by void on 16/04/2020.
//

#include <loom/testing.h>
#include <loom/testing/TestEngine.h>

#include <loom/engine/SubjectStore.h>
#include <loom/engine/InMemorySubjectStore.h>

using namespace loom::testing;

namespace loom::engine::testing {

BOOST_AUTO_TEST_SUITE(InMemorySubjectStoreTests)

BOOST_AUTO_TEST_CASE(can_put_subject) {
    InMemorySubjectStore store;

    ConfigMap subjectConfig{
            {"uuid",    "1"},
            {"realm",   "default"},
            {"is_test", "true"}
    };

    BOOST_REQUIRE_NO_THROW(store.put(subjectConfig, SubjectAnnouncementFlags::Defaults, engine::ActorHandle{}));
}

BOOST_AUTO_TEST_CASE(can_retrieve_subject) {
    InMemorySubjectStore store;

    ConfigMap subjectConfig{
            {"uuid",    "1"},
            {"realm",   "default"},
            {"is_test", "true"}
    };

    store.put(subjectConfig, SubjectAnnouncementFlags::Defaults, engine::ActorHandle{});

    auto results = store.find("(*)", SubjectViewMode::ResolveFully);

    BOOST_REQUIRE_EQUAL(1, results.size());

    ConfigMap resultSubjectConfig{};
    results.front().fetchAllAttributes(resultSubjectConfig);

    BOOST_REQUIRE(subjectConfig == resultSubjectConfig);
}

BOOST_AUTO_TEST_CASE(can_retrieve_filtered_subjects_subset) {
    InMemorySubjectStore store;

    ConfigMap subjectConfig{
            {"uuid",     "1"},
            {"realm",    "default"},
            {"included", "false"}
    };

    store.put(subjectConfig, SubjectAnnouncementFlags::Defaults, engine::ActorHandle{});

    subjectConfig = {
            {"uuid",     "2"},
            {"realm",    "default"},
            {"included", "true"}
    };

    store.put(subjectConfig, SubjectAnnouncementFlags::Defaults, engine::ActorHandle{});

    auto results = store.find("(included=true)", SubjectViewMode::ResolveFully);

    BOOST_REQUIRE_EQUAL(1, results.size());

    ConfigMap resultSubjectConfig{};
    results.front().fetchAllAttributes(resultSubjectConfig);

    BOOST_REQUIRE(subjectConfig == resultSubjectConfig);
}

BOOST_AUTO_TEST_CASE(can_remove_subject) {
    InMemorySubjectStore store;

    ConfigMap subjectConfig{
            {"uuid",  "1"},
            {"realm", "default"},
    };

    store.put(subjectConfig, SubjectAnnouncementFlags::Defaults, engine::ActorHandle{});

    store.remove("(*)");

    auto results = store.find("(*)", SubjectViewMode::ResolveFully);

    BOOST_REQUIRE(results.empty());
}

BOOST_AUTO_TEST_CASE(can_remove_subject_using_config_map) {
    InMemorySubjectStore store;

    ConfigMap subjectConfig{
            {"uuid",  "1"},
            {"realm", "default"},
    };

    store.put(subjectConfig, SubjectAnnouncementFlags::Defaults, engine::ActorHandle{});

    store.remove(subjectConfig);

    auto results = store.find("(*)", SubjectViewMode::ResolveFully);

    BOOST_REQUIRE(results.empty());
}

BOOST_AUTO_TEST_CASE(can_remove_subject_using_pointer) {
    InMemorySubjectStore store;

    ConfigMap subjectConfig{
            {"uuid",  "1"},
            {"realm", "default"},
    };

    store.put(subjectConfig, SubjectAnnouncementFlags::Defaults, engine::ActorHandle{});

    auto results = store.find("(*)", SubjectViewMode::ResolveFully);

    store.remove(results.front().fetchUniqueId());

    results = store.find("(*)", SubjectViewMode::ResolveFully);

    BOOST_REQUIRE(results.empty());
}

BOOST_AUTO_TEST_CASE(can_remove_subject_using_filter) {
    InMemorySubjectStore store;

    ConfigMap subjectConfig{
            {"uuid",      "1"},
            {"realm",     "default"},
            {"remove_me", "true"}
    };

    store.put(subjectConfig, SubjectAnnouncementFlags::Defaults, engine::ActorHandle{});

    subjectConfig = {
            {"uuid",      "2"},
            {"realm",     "default"},
            {"remove_me", "false"}
    };

    store.put(subjectConfig, SubjectAnnouncementFlags::Defaults, engine::ActorHandle{});

    store.remove("(remove_me=true)");

    auto results = store.find("(*)", SubjectViewMode::ResolveFully);

    BOOST_REQUIRE_EQUAL(1, results.size());

    ConfigMap resultSubjectConfig{};
    results.front().fetchAllAttributes(resultSubjectConfig);

    BOOST_TEST(subjectConfig == resultSubjectConfig);
}

BOOST_AUTO_TEST_CASE(can_store_config_with_datasources) {
    InMemorySubjectStore store;

    ConfigMap subjectConfig{
            {"uuid",  "1"},
            {"realm", "default"},
            {"file",  DataSourceInfo{.length=200, .accessType = DataSourceInfo::AccessType::Random}}
    };

    ConfigMap fetchedConfig;

    auto subject = store.put(subjectConfig, SubjectAnnouncementFlags::Defaults, engine::ActorHandle{});
    BOOST_REQUIRE_NO_THROW(subject.fetchAttribute(fetchedConfig, "file"));

    auto &file = fetchedConfig.find("file")->second;
    BOOST_REQUIRE(std::holds_alternative<DataSourceInfo>(file));

    auto dsi = std::get<DataSourceInfo>(file);
    BOOST_REQUIRE(DataSourceInfo::AccessType::Random == dsi.accessType);
    BOOST_REQUIRE_EQUAL(200, dsi.length);
}

BOOST_AUTO_TEST_CASE(can_retrieve_fully_resolved_variants_implicit) {
    InMemorySubjectStore store;

    ConfigMap subjectConfig{
            {"uuid",  "1"},
            {"realm", "default"},
            {"color", Variant("red" /* 'red' is the default value */, "blue", "green")}
    };

    auto subject = store.put(subjectConfig, SubjectAnnouncementFlags::Defaults, engine::ActorHandle{});
    auto results = store.find("(*)", SubjectViewMode::ResolveFully);

    BOOST_REQUIRE_EQUAL(1, results.size());

    ConfigMap fetchedConfig;
    results[0].fetchAttribute(fetchedConfig, "color");
    BOOST_REQUIRE(fetchedConfig.find("color") != fetchedConfig.end());

    auto &color = fetchedConfig.find("color")->second;
    BOOST_REQUIRE(std::holds_alternative<std::string>(color));
    BOOST_REQUIRE_EQUAL("red", std::get<std::string>(color));
}

BOOST_AUTO_TEST_CASE(can_retrieve_fully_resolved_variants_explicit) {
    InMemorySubjectStore store;

    ConfigMap subjectConfig{
            {"uuid",  "1"},
            {"realm", "default"},
            {"color", Variant("red", "blue", "green")}
    };

    auto subject = store.put(subjectConfig, SubjectAnnouncementFlags::Defaults, engine::ActorHandle{});
    auto results = store.find("(color=green)", SubjectViewMode::ResolveFully);

    BOOST_REQUIRE_EQUAL(1, results.size());

    ConfigMap fetchedConfig;
    results[0].fetchAttribute(fetchedConfig, "color");
    BOOST_REQUIRE(fetchedConfig.find("color") != fetchedConfig.end());

    auto &color = fetchedConfig.find("color")->second;
    BOOST_REQUIRE(std::holds_alternative<std::string>(color));
    BOOST_REQUIRE_EQUAL("green", std::get<std::string>(color));
}

BOOST_AUTO_TEST_CASE(multiple_explicit_variant_expressions_return_first_result) {
    InMemorySubjectStore store;

    ConfigMap subjectConfig{
            {"uuid",  "1"},
            {"realm", "default"},
            {"color", Variant("red", "blue", "green")}
    };

    auto subject = store.put(subjectConfig, SubjectAnnouncementFlags::Defaults, engine::ActorHandle{});
    auto results = store.find("(&(color=blue)(color=green))", SubjectViewMode::ResolveFully);

    BOOST_REQUIRE_EQUAL(1, results.size());

    ConfigMap fetchedConfig;
    results[0].fetchAttribute(fetchedConfig, "color");
    BOOST_REQUIRE(fetchedConfig.find("color") != fetchedConfig.end());

    auto &color = fetchedConfig.find("color")->second;
    BOOST_REQUIRE(std::holds_alternative<std::string>(color));
    BOOST_REQUIRE_EQUAL("blue", std::get<std::string>(color));
}

BOOST_AUTO_TEST_CASE(can_resolve_any_variant) {
    InMemorySubjectStore store;

    ConfigMap subjectConfig{
            {"uuid",  "1"},
            {"realm", "default"},
            {"size",  Variant("*")}
    };

    auto subject = store.put(subjectConfig, SubjectAnnouncementFlags::Defaults, engine::ActorHandle{});
    auto results = store.find("(size=100)", SubjectViewMode::ResolveFully);

    BOOST_REQUIRE_EQUAL(1, results.size());

    ConfigMap fetchedConfig;
    results[0].fetchAttribute(fetchedConfig, "size");
    BOOST_REQUIRE(fetchedConfig.find("size") != fetchedConfig.end());

    auto &size = fetchedConfig.find("size")->second;
    BOOST_REQUIRE(std::holds_alternative<std::string>(size));
    BOOST_REQUIRE_EQUAL("100", std::get<std::string>(size));
}

BOOST_AUTO_TEST_CASE(can_resolve_partially) {
    InMemorySubjectStore store;

    ConfigMap subjectConfig{
            {"uuid",     "1"},
            {"realm",    "default"},
            {"color",    Variant("red", "blue", "green")},
            {"material", Variant("wood", "stone", "iron")},
    };

    auto subject = store.put(subjectConfig, SubjectAnnouncementFlags::Defaults, engine::ActorHandle{});
    auto results = store.find("(color=blue)", SubjectViewMode::ResolveRequired);

    BOOST_REQUIRE_EQUAL(1, results.size());

    ConfigMap fetchedConfig;
    results[0].fetchAttributes(fetchedConfig, {"color", "material"});
    BOOST_REQUIRE(fetchedConfig.find("color") != fetchedConfig.end());
    BOOST_REQUIRE(fetchedConfig.find("material") != fetchedConfig.end());

    BOOST_REQUIRE(std::holds_alternative<std::string>(fetchedConfig.find("color")->second));
    BOOST_REQUIRE(std::holds_alternative<Variant>(fetchedConfig.find("material")->second));
}

BOOST_AUTO_TEST_CASE(can_resolve_fully) {
    InMemorySubjectStore store;

    ConfigMap subjectConfig{
            {"uuid",     "1"},
            {"realm",    "default"},
            {"color",    Variant("red", "blue", "green")},
            {"material", Variant("wood", "stone", "iron")},
    };

    auto subject = store.put(subjectConfig, SubjectAnnouncementFlags::Defaults, engine::ActorHandle{});
    auto results = store.find("(*)", SubjectViewMode::ResolveFully);

    BOOST_REQUIRE_EQUAL(1, results.size());

    ConfigMap fetchedConfig;
    results[0].fetchAttributes(fetchedConfig, {"color", "material"});
    BOOST_REQUIRE(fetchedConfig.find("color") != fetchedConfig.end());
    BOOST_REQUIRE(fetchedConfig.find("material") != fetchedConfig.end());

    BOOST_REQUIRE(std::holds_alternative<std::string>(fetchedConfig.find("color")->second));
    BOOST_REQUIRE(std::holds_alternative<std::string>(fetchedConfig.find("material")->second));
}

BOOST_AUTO_TEST_CASE(can_resolve_raw_options) {
    InMemorySubjectStore store;

    ConfigMap subjectConfig{
            {"uuid",     "1"},
            {"realm",    "default"},
            {"color",    Variant("red", "blue", "green")},
            {"material", Variant("wood", "stone", "iron")},
    };

    auto subject = store.put(subjectConfig, SubjectAnnouncementFlags::Defaults, engine::ActorHandle{});
    auto results = store.find("(&(color=red)(material=iron))", SubjectViewMode::RawOptions);

    BOOST_REQUIRE_EQUAL(1, results.size());

    ConfigMap fetchedConfig;
    results[0].fetchAttributes(fetchedConfig, {"color", "material"});
    BOOST_REQUIRE(fetchedConfig.find("color") != fetchedConfig.end());
    BOOST_REQUIRE(fetchedConfig.find("material") != fetchedConfig.end());

    BOOST_REQUIRE(std::holds_alternative<Variant>(fetchedConfig.find("color")->second));
    BOOST_REQUIRE(std::holds_alternative<Variant>(fetchedConfig.find("material")->second));
}

BOOST_AUTO_TEST_CASE(is_extending_stored_subject) {
    InMemorySubjectStore store;

    ConfigMap subjectConfig1{
            {"uuid",  "1"},
            {"realm", "default"},
            {"alpha", "set by a"}
    };

    store.put(subjectConfig1, SubjectAnnouncementFlags::ExtendAll, engine::ActorHandle{});

    ConfigMap subjectConfig2{
            {"uuid",  "1"},
            {"realm", "default"},
            {"alpha", "set by b"},
            {"beta",  "set by b"}
    };

    auto subject = store.put(subjectConfig2, SubjectAnnouncementFlags::ExtendAll, engine::ActorHandle{});
    ConfigMap putResult;
    subject.fetchAllAttributes(putResult);
    auto results = store.find("(uuid=1)", SubjectViewMode::ResolveFully);

    BOOST_REQUIRE_EQUAL(1, results.size());

    ConfigMap expected{
            {"uuid",  "1"},
            {"realm", "default"},
            {"alpha", "set by a"},
            {"alpha", "set by b"},
            {"beta",  "set by b"}
    };

    ConfigMap fetchedConfig;
    results[0].fetchAllAttributes(fetchedConfig);

    BOOST_REQUIRE_EQUAL(expected, fetchedConfig);
}

BOOST_AUTO_TEST_CASE(is_using_proper_readers_for_requesting_data_of_extended_configs) {
    InMemorySubjectStore store;
    TestEngine engine;

    auto actor1 = engine.makeTestActor("test-actor");
    struct Reader1 : DataSourceReader {

    };
    ConfigMap subjectConfig1{
            {"uuid",  "1"},
            {"realm", "default"},
            {"ds1",   DataSourceInfo{.reader = std::make_shared<Reader1>()}}
    };

    auto actor2 = engine.makeTestActor("test-actor2");
    struct Reader2 : DataSourceReader {

    };
    ConfigMap subjectConfig2{
            {"uuid",  "1"},
            {"realm", "default"},
            {"ds2",   DataSourceInfo{.reader = std::make_shared<Reader2>()}}
    };

    store.put(subjectConfig1, SubjectAnnouncementFlags::Defaults, actor1);
    store.put(subjectConfig2, SubjectAnnouncementFlags::ExtendAll, actor2);

    auto results = store.find("(uuid=1)", SubjectViewMode::ResolveFully);

    BOOST_REQUIRE_EQUAL(1, results.size());

    ConfigMap fetchedConfig;
    results[0].fetchAllAttributes(fetchedConfig);

    BOOST_REQUIRE_EQUAL(std::get<DataSourceInfo>(subjectConfig1.find("ds1")->second).reader,
                        std::get<DataSourceInfo>(fetchedConfig.find("ds1")->second).reader);
    BOOST_REQUIRE_EQUAL(std::get<DataSourceInfo>(subjectConfig2.find("ds2")->second).reader,
                        std::get<DataSourceInfo>(fetchedConfig.find("ds2")->second).reader);
}

BOOST_AUTO_TEST_CASE(extend_missing_is_working) {
    InMemorySubjectStore store;

    ConfigMap subjectConfig1{
            {"uuid",  "1"},
            {"realm", "default"},
            {"value", "base"}
    };

    ConfigMap subjectConfig2{
            {"uuid",     "1"},
            {"realm",    "default"},
            {"value",    "extended"},
            {"extended", "extended"}
    };

    store.put(subjectConfig1, SubjectAnnouncementFlags::Defaults, {});
    store.put(subjectConfig2, SubjectAnnouncementFlags::ExtendMissing, {});

    ConfigMap expectedConfig{
            {"uuid",     "1"},
            {"realm",    "default"},
            {"value",    "base"},
            {"extended", "extended"}
    };

    auto results = store.find("(uuid=1)", SubjectViewMode::ResolveFully);

    BOOST_REQUIRE_EQUAL(1, results.size());

    ConfigMap fetchedConfig;
    results[0].fetchAllAttributes(fetchedConfig);

    BOOST_REQUIRE_EQUAL(expectedConfig, fetchedConfig);
}

BOOST_AUTO_TEST_CASE(extend_override_is_working) {
    InMemorySubjectStore store;

    ConfigMap subjectConfig1{
            {"uuid",  "1"},
            {"realm", "default"},
            {"value", "base"}
    };

    ConfigMap subjectConfig2{
            {"uuid",  "1"},
            {"realm", "default"},
            {"value", "extended"}
    };

    store.put(subjectConfig1, SubjectAnnouncementFlags::Defaults, {});
    store.put(subjectConfig2, SubjectAnnouncementFlags::ExtendOverride, {});

    ConfigMap expectedConfig{
            {"uuid",  "1"},
            {"realm", "default"},
            {"value", "extended"}
    };

    auto results = store.find("(uuid=1)", SubjectViewMode::ResolveFully);

    BOOST_REQUIRE_EQUAL(1, results.size());

    ConfigMap fetchedConfig;
    results[0].fetchAllAttributes(fetchedConfig);

    BOOST_REQUIRE_EQUAL(expectedConfig, fetchedConfig);
}

BOOST_AUTO_TEST_SUITE_END()

}
