//
// Created by void on 27/04/2020.
//

#include <loom/testing.h>
#include <loom/testing/TestBrain.h>

#include <loom/engine/PassthroughDataStore.h>
#include <loom/engine/ActorBrain.h>

using namespace loom::testing;

namespace loom::engine::testing {

BOOST_AUTO_TEST_SUITE(DataSourceTests)

class TestDataTransactionHandler : public DataTransactionHandler {
public:
    std::exception_ptr error{};
    bool isComplete = false;
    data_buffer_type dataReceived;

public:
    void handleError(const std::exception_ptr &e) override {
        error = e;
    }

    void handleTransactionComplete(CompleteReason reason) override {
        isComplete = true;
    }

    void handleDataReceived(data_buffer_type dataChunk, size_t chunkOffset) override {
        dataReceived.resize(std::max(dataReceived.size(), chunkOffset + dataChunk.size()));
        std::memcpy(dataReceived.data() + chunkOffset, dataChunk.data(), dataChunk.size());
    }
};

BOOST_AUTO_TEST_CASE(can_request_data) {

    ConfigMap subjectId{{"uuid",  "1"},
                        {"realm", "default"}};

    auto handler = std::make_shared<TestDataTransactionHandler>();
    TestBrain brain(handler);

    brain.handleDataRequest(
            subjectId, "test",
            20, 30);

    BOOST_REQUIRE(!handler->error);
    BOOST_REQUIRE(handler->isComplete);
    BOOST_REQUIRE_EQUAL(50, handler->dataReceived.size());
}

BOOST_AUTO_TEST_CASE(read_handler_is_invoked) {

    ConfigMap subjectId{{"uuid",  "1"},
                        {"realm", "default"}};

    auto handler = std::make_shared<TestDataTransactionHandler>();
    TestBrain brain(handler);

    PassthroughDataStore store(subjectId, "test", brain);

    store.requestData(0, 30, handler);

    BOOST_REQUIRE(!handler->error);
    BOOST_REQUIRE(handler->isComplete);
    BOOST_REQUIRE_EQUAL(30, handler->dataReceived.size());
}

BOOST_AUTO_TEST_SUITE_END()

}