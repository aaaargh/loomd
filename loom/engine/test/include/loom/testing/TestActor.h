//
// Created by void on 01/06/2020.
//

#ifndef LOOM_TESTING_TESTACTOR_H
#define LOOM_TESTING_TESTACTOR_H

#include <loom/testing/TestEngine.h>
#include <loom/engine/Actor.h>

namespace loom::testing {

class TestActor : public engine::Actor {
public:
    explicit TestActor();

private:
    static TestEngine &getTestEngine();
};

}

#endif //LOOM_TESTING_TESTACTOR_H
