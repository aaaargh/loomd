//
// Created by void on 28/04/2020.
//

#ifndef LOOM_TESTING_TESTDATAPROVIDER_H
#define LOOM_TESTING_TESTDATAPROVIDER_H

#include <loom/engine/ActorBrain.h>

namespace loom::testing {

class TestBrain : public engine::ActorBrain {
public:
    explicit TestBrain(std::shared_ptr<engine::DataTransactionHandler> handler, engine::data_buffer_type data);

    explicit TestBrain(std::shared_ptr<engine::DataTransactionHandler> handler, size_t numBytes = 1024);

public:
    void handleDataRequest(ConfigMap subjectId, ConfigMap::key_type attribute, size_t offset, size_t len) override;

    void handleSubjectOwned(engine::SubjectHandle subject) override;

    void handleSubjectOwnershipLost(engine::SubjectHandle subject) override;

private:
    engine::data_buffer_type data;
    std::shared_ptr<engine::DataTransactionHandler> handler;
};

struct NoBrain : public TestBrain {
    NoBrain() : TestBrain(std::shared_ptr<engine::DataTransactionHandler>()) {}
};

}

#endif //LOOM_TESTING_TESTDATAPROVIDER_H
