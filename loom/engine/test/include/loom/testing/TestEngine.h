//
// Created by void on 23/04/2020.
//

#ifndef LOOM_TESTING_TESTENGINE_H
#define LOOM_TESTING_TESTENGINE_H

#include <loom/engine/Engine.h>
#include <loom/testing/TestBrain.h>

#include <memory>

namespace loom::testing {

class TestEngine : public engine::Engine {
public:
    explicit TestEngine(boost::asio::io_context &ioContext);

    explicit TestEngine(
            std::unique_ptr<boost::asio::io_context> ioContext = std::make_unique<boost::asio::io_context>());

public:
    void simulateTick();

    engine::ActorHandle makeTestActor(const std::string &id,
                                      std::shared_ptr<engine::ActorBrain> brain = std::make_shared<NoBrain>());

    engine::ActorHandle makeTestActor(const std::string &id,
                                      std::shared_ptr<engine::DataTransactionHandler> transactionHandler);

private:
    std::unique_ptr<boost::asio::io_context> selfContainedIOContext;
};

}

#endif //LOOM_TESTING_TESTENGINE_H
