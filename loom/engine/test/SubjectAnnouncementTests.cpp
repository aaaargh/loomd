//
// Created by void on 06/06/2020.
//

#include <loom/testing.h>
#include <loom/testing/TestEngine.h>

using namespace loom::testing;

namespace loom::engine::testing {

BOOST_AUTO_TEST_SUITE(SubjectAnnouncementTests)

BOOST_AUTO_TEST_CASE(can_announce_simple_subject) {
    TestEngine engine;

    struct TestExt : engine::Extension {
        std::vector<ConfigMap> announcedSubjects;

        void onPreAnnounceSubject(Engine &engine, const ActorHandle &actor, ConfigMap &subjectConfig,
                                  SubjectAnnouncementFlags &flags) override {
            announcedSubjects.push_back(subjectConfig);
        }
    };

    auto &ext = engine.installExtension<TestExt>();

    auto actor = engine.makeTestActor("test");

    ConfigMap subjectConfig = {
            {"uuid",  "hello-world"},
            {"realm", "default"}
    };

    std::optional<bool> status;
    engine.announceSubject(actor, subjectConfig, SubjectAnnouncementFlags::None,
                           [&](auto handle) { status = true; },
                           [&](const auto &err) { status = false; });
    await(status);

    auto it = std::find(ext.announcedSubjects.begin(), ext.announcedSubjects.end(), subjectConfig);

    BOOST_REQUIRE(status.value());
    BOOST_REQUIRE(it != ext.announcedSubjects.end());
}

/* Disabled for now as this is not implemented correctly. 
BOOST_AUTO_TEST_CASE(announcing_already_present_subject_fails) {
    TestEngine engine;

    auto actor = engine.makeTestActor("test");

    ConfigMap subjectConfig = {
            {"uuid",  "hello-world"},
            {"realm", "default"}
    };

    std::optional<bool> status;
    engine.announceSubject(actor, subjectConfig, SubjectAnnouncementFlags::None,
                           [&](auto handle) { status = true; },
                           [&](const auto &err) { status = false; });
    await(status);
    BOOST_REQUIRE(status.value());

    status.reset();
    engine.announceSubject(actor, subjectConfig, SubjectAnnouncementFlags::None,
                           [&](auto handle) { status = true; },
                           [&](const auto &err) { status = false; });
    await(status);
    BOOST_REQUIRE(!status.value());
}*/

BOOST_AUTO_TEST_SUITE_END()

}