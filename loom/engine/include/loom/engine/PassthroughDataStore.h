//
// Created by void on 20/05/2020.
//

#ifndef LOOM_ENGINE_PASSTHROUGHDATASTORE_H
#define LOOM_ENGINE_PASSTHROUGHDATASTORE_H

#include <loom/engine/DataStore.h>

namespace loom::engine {

class PassthroughDataStore : public DataStore {
public:
    using DataStore::DataStore;

public:
    void requestData(size_t offset, size_t len, const std::shared_ptr<DataTransactionHandler> &handler) override;

    void putData(size_t offset, data_buffer_type data) override;

private:
    struct WaitingHandler {
        size_t offset;
        size_t len;
        std::shared_ptr<DataTransactionHandler> handler;
    };

    std::shared_mutex waitingHandlersMutex;
    std::list<WaitingHandler> waitingHandlers;
};

}

#endif //LOOM_PASSTHROUGHDATASTORE_H
