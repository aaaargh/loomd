//
// Created by void on 16/04/2020.
//

#ifndef LOOM_ENGINE_SUBJECTSTORE_H
#define LOOM_ENGINE_SUBJECTSTORE_H

#include <loom/engine/ConfigMapProvider.h>
#include <loom/engine/Subject.h>
#include <loom/engine/Actor.h>

#include <loom/lql/AST.h>

#include <loom/SubjectViewMode.h>
#include <loom/SubjectAnnouncementFlags.h>
#include <loom/SubjectQueryFlags.h>

#include <vector>

namespace loom::engine {

/**
 * The interface class for as subject store.
 */
class SubjectStore {
public:
    virtual ~SubjectStore() = default;

public:
    /**
     * Puts a new subject in the store.
     * @param subjectConfig The full subject config to store.
     * @param owner The owner of the subject.
     * @return a fully resolved view representing the inserted subject.
     */
    virtual SubjectView put(ConfigMap subjectConfig, SubjectAnnouncementFlags flags, ActorWeakHandle owner) = 0;

    /**
     * Gets all subjects matching the given id, including its extensions.
     * @param subjectId The subject id.
     * @param viewMode The view mode use to resolve the subject.
     * @return The obtained subjects or an empty one if the given subject id does not exist in the store.
     */
    [[nodiscard]] virtual std::vector<SubjectView> get(const ConfigMap &subjectId, SubjectViewMode viewMode) const;

    /**
     * Finds all subjects matching the given query.
     * @param query The query to match the subjects against.
     * @param viewMode The view mode use to resolve the subjects.
     * @return A list of found subjects.
     */
    [[nodiscard]] std::vector<SubjectView> find(const std::string &query, SubjectViewMode viewMode) const;

    /**
     * Finds all subjects matching the given LQL expression.
     * @param expression The expression to match the subjects against.
     * @param viewMode The view mode use to resolve the subjects.
     * @return A list of found subjects.
     */
    [[nodiscard]] virtual std::vector<SubjectView> findEval(
            const lql::ast::SelectExpression &expression,
            SubjectViewMode viewMode) const = 0;

    /**
     * Removes all subjects matching the given query.
     * @param query The query to match the subjects against.
     * @return The number of removed subjects.
     */
    size_t remove(const std::string &query);

    /**
     * Removes a single subject from the store.
     * @param subjectId The subject id.
     * @return Whether the given subject has been found and removed.
     */
    virtual bool remove(const ConfigMap &subjectId);

    /**
     * Removes all subjects matching the given LQL expression.
     * @param expression The expression to match the subjects against.
     * @return The number of removed subjects.
     */
    virtual size_t removeEval(const lql::ast::SelectExpression &expression) = 0;
};

}

#endif //LOOM_ENGINE_SUBJECTSTORE_H
