//
// Created by void on 04/06/2020.
//

#ifndef LOOM_ENGINE_ACTORHANDLE_H
#define LOOM_ENGINE_ACTORHANDLE_H

#include <memory>
#include "Actor.h"


namespace loom::engine {

class ActorBrain;

class ActorMemory;

class Actor;

class SubjectRef;

class Engine;

using SubjectHandle = std::shared_ptr<SubjectRef>;

class ActorRef {
public:
    explicit ActorRef(Actor &actor);

    ~ActorRef();

public:
    [[nodiscard]] ActorBrain &getBrain();

    [[nodiscard]] ActorMemory &getMemory();

    [[nodiscard]] SubjectHandle getIdentitySubject() const;

private:
    friend class Engine;

    Actor &actor;
    bool isValid = true;
};

using ActorHandle = std::shared_ptr<ActorRef>;
using ActorWeakHandle = std::weak_ptr<ActorRef>;

}

#endif //LOOM_ENGINE_ACTORHANDLE_H
