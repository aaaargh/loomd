//
// Created by void on 04/06/2020.
//

#ifndef LOOM_ENGINE_SUBJECTHANDLE_H
#define LOOM_ENGINE_SUBJECTHANDLE_H

#include <loom/ConfigMap.h>

#include <memory>

namespace loom::engine {

class Subject;

class SubjectMemory;

class ActorRef;

class Engine;

using ActorWeakHandle = std::weak_ptr<ActorRef>;

class SubjectRef {
public:
    explicit SubjectRef(Subject &subject);
    ~SubjectRef();

public:
    [[nodiscard]] const ConfigMap &getUniqueId() const;

    [[nodiscard]] SubjectMemory &getMemory();

    [[nodiscard]] const ActorWeakHandle &getOwner() const;

    void setOwner(const ActorWeakHandle &newOwner);

private:
    friend class Engine;

    Subject &subject;
    bool isValid = true;
};

using SubjectHandle = std::shared_ptr<SubjectRef>;
using SubjectWeakHandle = std::weak_ptr<SubjectRef>;

}

#endif //LOOM_ENGINE_SUBJECTHANDLE_H
