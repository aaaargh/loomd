//
// Created by void on 09/05/2020.
//

#ifndef LOOM_ENGINE_EXTENSION_H
#define LOOM_ENGINE_EXTENSION_H

#include <loom/ConfigMap.h>
#include <loom/engine/DataStore.h>
#include <loom/engine/ActorHandle.h>
#include <loom/SubjectAnnouncementFlags.h>
#include <loom/SubjectQueryFlags.h>
#include <loom/lql/AST.h>

#include <memory>

namespace loom::engine {

class Engine;

class Extension {
public:
    virtual ~Extension() = default;

    virtual void onInitialize(Engine &engine) {};

    virtual void onShutdown(Engine &engine) {};

    virtual void onValidateSubject(const Engine &engine, const ConfigMap &subjectConfig) const {}

    virtual void onPreAnnounceSubject(Engine &engine, const ActorHandle &actor, ConfigMap &subjectConfig,
                                      SubjectAnnouncementFlags &flags) {};

    virtual void onPostAnnounceSubject(Engine &engine,
                                       const ActorHandle &actor,
                                       const SubjectHandle &subjectHandle,
                                       bool isUpdated) {};

    virtual void onAlterDataStore(Engine &engine,
                                  const ActorHandle &actor,
                                  std::shared_ptr<DataStore> &store,
                                  const ConfigMapAttributeKey &attributeKey) {}

    virtual void onAlterSubjectQuery(Engine &engine, const ActorHandle &actor, lql::ast::SelectExpression &query,
                                     SubjectQueryFlags &flags) {}

    virtual void onPreDropSubject(Engine &engine, const ActorHandle &actor, const ConfigMap &subjectId) {};

    virtual void onPostDropSubject(Engine &engine, const ActorHandle &actor, const ConfigMap &subjectId) {};

    virtual void onPreRegisterActor(Engine &engine, ConfigMap &identityConfig) {}

    virtual void onPostRegisterActor(Engine &engine, const ActorHandle &actor) {}

    virtual void onPreDropActor(Engine &engine, const ActorHandle &actor) {}

    virtual void onPostDropActor(Engine &engine, const ActorHandle &actor) {}

    virtual void onSubscribe(Engine &engine, const ActorHandle &actor) {}

    virtual void onExtensionError(Engine &engine, const Extension &extension, const std::exception &error) {}
};

}

#endif //LOOM_ENGINE_EXTENSION_H
