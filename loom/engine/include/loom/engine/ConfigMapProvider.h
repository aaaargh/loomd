//
// Created by void on 16/04/2020.
//

#ifndef LOOM_ENGINE_CONFIGMAPPROVIDER_H
#define LOOM_ENGINE_CONFIGMAPPROVIDER_H

#include <loom/ConfigMap.h>

namespace loom::engine {
class ConfigMapProvider {
public:
    virtual ~ConfigMapProvider() = default;

public:
    virtual void fetchAllAttributes(ConfigMap &configMap) const = 0;

    void fetchAttribute(ConfigMap &configMap,
                        const ConfigMap::key_type &attributeName) const;

    virtual void fetchAttributes(ConfigMap &configMap,
                                 std::initializer_list<ConfigMap::key_type> &&attributes) const = 0;
};

class InMemoryConfigMapProvider : public ConfigMapProvider {
public:
    explicit InMemoryConfigMapProvider(ConfigMap sourceConfigMap);

    void fetchAllAttributes(ConfigMap &configMap) const override;

    void fetchAttributes(ConfigMap &configMap,
                         std::initializer_list<ConfigMap::key_type> &&attributes) const override;

    [[nodiscard]] const ConfigMap &getSourceConfigMap() const;

private:
    // Note: Since we store variants now, this is no longer a reference!
    ConfigMap sourceConfigMap;
};

}

#endif //LOOM_ENGINE_CONFIGMAPPROVIDER_H
