//
// Created by void on 06/06/2020.
//

#ifndef LOOM_ENGINE_SYSTEMINFOEXTENSION_H
#define LOOM_ENGINE_SYSTEMINFOEXTENSION_H

#include <loom/engine/Extension.h>

#include <set>

namespace loom::engine::extensions {

class SystemInfoExtension : public Extension {
public:
    void onInitialize(Engine &engine) override;

private:
    std::set<SubjectHandle> persistentSubjects;
};

}

#endif //LOOM_ENGINE_SYSTEMINFOEXTENSION_H
