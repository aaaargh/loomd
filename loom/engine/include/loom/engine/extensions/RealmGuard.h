//
// Created by void on 09/05/2020.
//

#ifndef LOOM_ENGINE_EXTENSIONS_REALMGUARD_H
#define LOOM_ENGINE_EXTENSIONS_REALMGUARD_H

#include <loom/engine/Extension.h>
#include <loom/lql/AST.h>

#include <shared_mutex>
#include <loom/SubjectAnnouncementFlags.h>
#include <loom/SubjectQueryFlags.h>

namespace loom::engine::extensions {

class RealmGuard : public Extension {
public:
    struct Realm {
        lql::ast::SelectExpression writeFilter;
        lql::ast::SelectExpression readFilter;
    };

public:
    void onValidateSubject(const Engine &engine, const ConfigMap &subjectConfig) const override;

    void onPreAnnounceSubject(Engine &engine, const ActorHandle &actor, ConfigMap &subjectConfig,
                              SubjectAnnouncementFlags &flags) override;

private:
    [[nodiscard]] bool canWrite(Engine& engine, const ActorHandle &actor, const std::string &realmId) const;

    [[nodiscard]] bool canRead(Engine& engine, const ActorHandle &actor, const std::string &realmId) const;

public:
    void onInitialize(Engine &engine) override;

    void onAlterSubjectQuery(Engine &engine, const ActorHandle &actor, lql::ast::SelectExpression &query,
                             loom::SubjectQueryFlags &flags) override;

    void onPreRegisterActor(Engine &engine, ConfigMap &identityConfig) override;

    void onPostRegisterActor(Engine &engine, const ActorHandle &actor) override;

    void onPostDropActor(Engine &engine, const ActorHandle &actor) override;

private:
    [[nodiscard]] static bool isSystemRealm(const std::string &realmId);

private:
    static constexpr auto SystemRealmId = "system";
    static constexpr auto ActorsRealmId = "actors";
    static constexpr auto RealmsRealmId = "realms";
    static constexpr auto PrivateRealmId = "private";
    static constexpr auto PublicRealmId = "default";

    mutable std::shared_mutex realmsMutex;
    std::map<std::string, Realm> realms;
};

}

#endif //LOOM_ENGINE_EXTENSIONS_REALMGUARD_H
