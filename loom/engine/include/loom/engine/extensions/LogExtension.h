//
// Created by void on 09/05/2020.
//

#ifndef LOOM_ENGINE_EXTENSIONS_LOGEXTENSION_H
#define LOOM_ENGINE_EXTENSIONS_LOGEXTENSION_H

#include <loom/engine/Extension.h>

namespace loom::engine::extensions {

class LogExtension : public Extension {
public:
    void onInitialize(Engine &engine) override;

    void onShutdown(Engine &engine) override;

    void onPostAnnounceSubject(Engine &engine, const ActorHandle &actor, const SubjectHandle &subjectHandle,
                               bool isUpdated) override;

    void onPreDropSubject(Engine &engine, const ActorHandle &actor, const ConfigMap &subjectId) override;

    void onPostDropSubject(Engine &engine, const ActorHandle &actor, const ConfigMap &subjectId) override;

    void onPostRegisterActor(Engine &engine, const ActorHandle &actor) override;

    void onPostDropActor(Engine &engine, const ActorHandle &actor) override;

    void onSubscribe(Engine &engine, const ActorHandle &actor) override;

    void onExtensionError(Engine &engine, const Extension &extension, const std::exception &error) override;
};

}

#endif //LOOM_ENGINE_LOGEXTENSION_H
