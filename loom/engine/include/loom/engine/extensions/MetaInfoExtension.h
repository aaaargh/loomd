//
// Created by void on 11/05/2020.
//

#ifndef LOOM_ENGINE_EXTENSIONS_METAINFOEXTENSION_H
#define LOOM_ENGINE_EXTENSIONS_METAINFOEXTENSION_H

#include <loom/engine/Extension.h>

namespace loom::engine::extensions {

class MetaInfoExtension : public Extension {
public:
    void onPreDropSubject(Engine &engine, const ActorHandle &actor, const ConfigMap &subjectId) override;

    void onPostAnnounceSubject(Engine &engine, const ActorHandle &actor, const SubjectHandle &subjectHandle,
                               bool isUpdated) override;

public:
    static constexpr auto MetaInfoRealmId = "metainfo";
};

}

#endif //LOOM_METAINFOEXTENSION_H
