//
// Created by void on 13/04/2020.
//

#ifndef LOOM_ENGINE_SUBJECT_H
#define LOOM_ENGINE_SUBJECT_H

#include <loom/ConfigMap.h>
#include <loom/engine/ConfigMapProvider.h>
#include <loom/engine/SubjectHandle.h>
#include <loom/engine/SubjectMemory.h>
#include <loom/engine/ActorHandle.h>

#include <utility>
#include <vector>

namespace loom::engine {

class SubjectView : public ConfigMapProvider {
public:
    SubjectView() = default;

    explicit SubjectView(std::shared_ptr<ConfigMapProvider> provider);

public:
    explicit operator bool() const;

    [[nodiscard]] ConfigMap fetchUniqueId() const;

    void fetchAllAttributes(ConfigMap &configMap) const override;

    void fetchAttributes(ConfigMap &configMap, std::initializer_list<ConfigMap::key_type> &&attributes) const override;

private:
    std::shared_ptr<ConfigMapProvider> provider;
};

class Engine;

class Subject {
public:
    Subject(Engine &engine, ActorWeakHandle owner, ConfigMap uuidMap);

private:
    friend class Engine;

    friend class SubjectRef;

    Engine &engine;
    ConfigMap uuidMap;
    ActorWeakHandle owner;
    SubjectMemory memory;
};

}

#endif //LOOM_ENGINE_SUBJECT_H
