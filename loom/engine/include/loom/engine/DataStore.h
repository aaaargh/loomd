//
// Created by void on 27/04/2020.
//

#ifndef LOOM_ENGINE_DATASTORE_H
#define LOOM_ENGINE_DATASTORE_H

#include <loom/ConfigMap.h>

#include <loom/engine/Actor.h>
#include <loom/engine/ActorBrain.h>

#include <functional>
#include <cstddef>
#include <vector>
#include <list>
#include <memory>
#include <shared_mutex>

namespace loom::engine {

class DataStore {
public:
    explicit DataStore(ConfigMap subjectId, ConfigMap::key_type attribute, ActorBrain &provider);

public:
    [[nodiscard]] const ConfigMap &getSubjectId() const;

    [[nodiscard]] const ConfigMap::key_type &getAttribute() const;

public:
    virtual void requestData(size_t offset, size_t len, const std::shared_ptr<DataTransactionHandler> &handler) = 0;

    virtual void putData(size_t offset, data_buffer_type data) = 0;

public:
    [[nodiscard]] ActorBrain &getProvider();

private:
    ActorBrain &dataProvider;
    ConfigMap subjectId;
    ConfigMap::key_type attribute;
};

class DataStoreManager {
public:
    void put(const ConfigMapAttributeKey &key, ActorHandle owner,
             const std::shared_ptr<DataStore> &store);

    void drop(const ConfigMapAttributeKey &key);

    void dropAll(const ConfigMap &subjectId);

    void requestData(const ConfigMapAttributeKey &key,
                     size_t offset, size_t len,
                     const std::shared_ptr<DataTransactionHandler> &handler
    ) const;

    void putData(const ConfigMapAttributeKey &key,
                 size_t offset,
                 data_buffer_type data
    );

private:
    struct ManagedStore {
        ActorHandle owner;
        std::shared_ptr<DataStore> store;
    };

    mutable std::shared_mutex managedStoresMutex;
    std::map<ConfigMapAttributeKey, ManagedStore> managedStores;
};

}

#endif //LOOM_ENGINE_DATASTORE_H
