//
// Created by void on 26/04/2020.
//

#ifndef LOOM_ENGINE_AUTHPROVIDER_H
#define LOOM_ENGINE_AUTHPROVIDER_H

#include <loom/ConfigMap.h>

namespace loom::engine {

class AuthProvider {
public:
    virtual ~AuthProvider() = default;

public:
    [[nodiscard]] virtual bool canAnnounceSubject(const ConfigMap &identity, const ConfigMap &subjectConfig) const = 0;

    [[nodiscard]] virtual bool canDropSubject(const ConfigMap &identity, const ConfigMap &subjectConfig) const = 0;
};

class YoloAuthProvider : public AuthProvider {
public:
    bool canAnnounceSubject(const ConfigMap &identity, const ConfigMap &subjectConfig) const override;

    bool canDropSubject(const ConfigMap &identity, const ConfigMap &subjectConfig) const override;
};

}

#endif //LOOM_ENGINE_AUTHPROVIDER_H
