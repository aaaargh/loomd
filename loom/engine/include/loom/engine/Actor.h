//
// Created by void on 26/04/2020.
//

#ifndef LOOM_ENGINE_ACTOR_H
#define LOOM_ENGINE_ACTOR_H

#include <loom/engine/ActorBrain.h>
#include <loom/engine/ActorMemory.h>
#include <loom/engine/ActorHandle.h>
#include <loom/engine/SubjectHandle.h>
#include <loom/ConfigMap.h>

#include <utility>
#include <memory>
#include <functional>
#include <set>

namespace loom::engine {

class Engine;

class Actor {
public:
    Actor(Engine &engine,
          std::shared_ptr<ActorBrain> brain,
          SubjectHandle identitySubject
    );

    ~Actor();
    
public:
    friend class ActorRef;

private:
    Engine &engine;
    std::shared_ptr<ActorBrain> brain;
    SubjectHandle identitySubject;
    ActorMemory memory;
};

}

#endif //LOOM_ENGINE_ACTOR_H
