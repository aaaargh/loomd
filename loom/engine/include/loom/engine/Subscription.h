//
// Created by void on 14/04/2020.
//

#ifndef LOOM_ENGINE_SUBSCRIPTION_H
#define LOOM_ENGINE_SUBSCRIPTION_H

#include <loom/engine/SubjectHandle.h>
#include <loom/SubjectSubscriptionFlags.h>
#include <loom/ConfigMap.h>
#include <loom/lql/AST.h>

#include <vector>
#include <functional>

namespace loom::engine {
namespace detail {
template<typename ... ArgsT>
struct Event : public std::vector<std::function<void(ArgsT...)> > {
public:
    using callback_type = std::function<void(ArgsT...)>;
public:
    void operator+=(const callback_type &cb) {
        this->emplace_back(std::move(cb));
    }

    void operator()(ArgsT... args) {
        for (auto &fun : *this) {
            fun(std::forward<ArgsT>(args)...);
        }
    }
};
}

class Subscription {
public:
    explicit Subscription(ActorWeakHandle actor,
                          lql::ast::SelectExpression filterExpression,
                          SubjectSubscriptionFlags flags);

public:
    using SubjectAnnouncedEvent = detail::Event<SubjectHandle, bool>;
    SubjectAnnouncedEvent subjectAnnounced;

    using SubjectDroppedEvent = detail::Event<const ConfigMap &>;
    SubjectDroppedEvent subjectDropped;

    [[nodiscard]] const lql::ast::SelectExpression &getFilterExpression() const;

    [[nodiscard]] const SubjectSubscriptionFlags &getFlags() const;

    const ActorWeakHandle& getActor() const;

private:
    ActorWeakHandle actor;
    lql::ast::SelectExpression filterExpression;
    SubjectSubscriptionFlags flags;
};

}

#endif //LOOM_SUBSCRIPTION_H
