//
// Created by void on 04/06/2020.
//

#ifndef LOOM_ENGINE_SUBJECTMEMORY_H
#define LOOM_ENGINE_SUBJECTMEMORY_H

#include <loom/engine/SubjectHandle.h>

#include <vector>

namespace loom::engine {

struct SubjectMemory {
    std::vector<SubjectHandle> dependentSubjects;
};

}

#endif //LOOM_ENGINE_SUBJECTMEMORY_H
