//
// Created by void on 04/06/2020.
//

#ifndef LOOM_ENGINE_ACTORMEMORY_H
#define LOOM_ENGINE_ACTORMEMORY_H

#include <loom/engine/SubjectHandle.h>

#include <set>

namespace loom::engine {

struct ActorMemory {
    std::set<SubjectHandle> persistentSubjects;
};

}

#endif //LOOM_ENGINE_ACTORMEMORY_H
