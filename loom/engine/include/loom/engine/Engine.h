//
// Created by void on 14/03/2020.
//

#ifndef LOOM_ENGINE_ENGINE_H
#define LOOM_ENGINE_ENGINE_H

#include <loom/engine/Subject.h>
#include <loom/engine/Subscription.h>
#include <loom/ConfigMap.h>
#include <loom/engine/SubjectStore.h>
#include <loom/engine/AuthProvider.h>
#include <loom/engine/Actor.h>
#include <loom/engine/DataStore.h>
#include <loom/engine/Extension.h>
#include <loom/SubjectViewMode.h>
#include <loom/SubjectAnnouncementFlags.h>
#include <loom/SubjectQueryFlags.h>
#include <loom/SubjectSubscriptionFlags.h>

#include <memory>
#include <set>
#include <shared_mutex>
#include <istream>
#include <utility>

#include <boost/asio/io_context.hpp>
#include <boost/asio/deadline_timer.hpp>

#include <tbb/task_arena.h>
#include <tbb/concurrent_unordered_set.h>

#define LOOM_ENGINE_VERSION_MAJOR "0"
#define LOOM_ENGINE_VERSION_MINOR "1"
#define LOOM_ENGINE_VERSION_PATCH "1"
#define LOOM_ENGINE_VERSION_SUFFIX " (Axolotl)"
#define LOOM_ENGINE_VERSION_STRING LOOM_ENGINE_VERSION_MAJOR "." LOOM_ENGINE_VERSION_MINOR "." LOOM_ENGINE_VERSION_PATCH LOOM_ENGINE_VERSION_SUFFIX

namespace loom::engine {
/**
 * The engine that drives a Loom instance.
 */
class Engine {
public:

public:
    explicit Engine(boost::asio::io_context &ioContext);

    virtual ~Engine();

private:
    void invalidateResources();

public:
    [[nodiscard]] static const std::string &getEngineVersionString();

public:
    template<typename ExtensionT, typename... Args>
    ExtensionT &installExtension(Args &&... args) {
        auto extension = std::make_unique<ExtensionT>(std::forward<Args>(args)...);
        auto &extRef = *extension;
        extensions.emplace_back(std::move(extension));
        extRef.onInitialize(*this);
        return extRef;
    }

private:
    std::vector<std::unique_ptr<Extension> > extensions;

public:
    using ErrorCallback = std::function<void(const std::exception_ptr &err)>;

    using SubjectAnnounceSuccessCallback = std::function<void(SubjectHandle)>;

    void announceSubject(const ActorHandle &actor,
                         ConfigMap subjectConfig,
                         SubjectAnnouncementFlags flags,
                         SubjectAnnounceSuccessCallback successCallback,
                         ErrorCallback errorCallback
    );

    using SubjectDropSuccessCallback = std::function<void()>;

    void dropSubject(const ActorHandle &actor,
                     ConfigMap subjectId,
                     SubjectDropSuccessCallback successCallback,
                     ErrorCallback errorCallback);

    using SubscribeSuccessCallback = std::function<void(std::shared_ptr<Subscription>)>;

    void subscribe(const ActorHandle &actor,
                   const std::string &query,
                   SubjectSubscriptionFlags flags,
                   SubscribeSuccessCallback successCallback,
                   ErrorCallback errorCallback
    );

    void subscribe(const ActorHandle &actor,
                   lql::ast::SelectExpression filterExpression,
                   SubjectSubscriptionFlags flags,
                   SubscribeSuccessCallback successCallback,
                   ErrorCallback errorCallback
    );

    using QuerySubjectsSuccessCallback = std::function<void(std::vector<ConfigMap>)>;

    void querySubjects(const ActorHandle &actor,
                       const std::string &query,
                       SubjectQueryFlags flags,
                       SubjectViewMode viewMode,
                       QuerySubjectsSuccessCallback successCallback,
                       ErrorCallback errorCallback
    );

    void querySubjects(const ActorHandle &actor,
                       lql::ast::SelectExpression query,
                       SubjectQueryFlags flags,
                       SubjectViewMode viewMode,
                       QuerySubjectsSuccessCallback successCallback,
                       ErrorCallback errorCallback
    );

    using RegisterActorSuccessCallback = std::function<void(ActorHandle)>;

    void registerActor(ConfigMap actorIdentity,
                       std::shared_ptr<ActorBrain> brain,
                       RegisterActorSuccessCallback successCallback,
                       ErrorCallback errorCallback
    );

    void requestData(const ActorHandle &actor,
                     ConfigMapAttributeKey subjectAttribute,
                     size_t offset,
                     size_t len,
                     std::shared_ptr<DataTransactionHandler> handler
    );

    using PutDataSuccessCallback = std::function<void()>;

    void putData(const ActorHandle &actor,
                 ConfigMapAttributeKey subjectAttribute,
                 size_t offset,
                 data_buffer_type data,
                 PutDataSuccessCallback successCallback,
                 ErrorCallback errorCallback
    );

public:
    void dropActor(Actor &actor);

    ActorHandle getSystemActor() const;

    [[nodiscard]] boost::asio::io_context &getIOContext();

private:
    void forEachSubscriber(const std::function<void(Subscription &)> &callback);

    [[nodiscard]] static std::shared_ptr<DataStore> createNewDataStore(const ConfigMap &subject,
                                                                       const ConfigMap::key_type &attribute,
                                                                       const ActorHandle &actor);

    void createSystemActor();

    void validateSubject(const ConfigMap &cm) const;

protected:
    virtual void handleEngineTick(const boost::system::error_code &ec);

private:
    std::unique_ptr<SubjectStore> subjectStore;
    DataStoreManager dataStoreManager;

    mutable std::shared_mutex activeSubjectsMutex;
    struct ActiveSubject {
        SubjectWeakHandle ref;
        std::unique_ptr<Subject> subject;
    };

    std::map<ConfigMap, ActiveSubject> activeSubjects;

    mutable std::shared_mutex registeredActorsMutex;
    struct RegisteredActor {
        ActorWeakHandle ref;
        std::unique_ptr<Actor> actor;
    };

    std::map<ConfigMap, RegisteredActor> registeredActors;

    mutable std::shared_mutex subscriptionsMutex;
    std::set<std::weak_ptr<Subscription>, std::owner_less<std::weak_ptr<Subscription> > > subscriptions;

    boost::asio::io_context &ioContext;
    boost::asio::deadline_timer engineTickTimer;

    tbb::task_arena taskArena;

    std::unique_ptr<AuthProvider> authProvider;

    ActorHandle engineActor;

    std::set<SubjectHandle> persistentEngineSubjects;
    bool isInShutdownPhase = false;
};

}

#endif //LOOM_ENGINE_ENGINE_H
