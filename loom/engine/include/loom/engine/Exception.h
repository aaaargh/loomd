//
// Created by void on 15/04/2020.
//

#ifndef LOOM_ENGINE_EXCEPTION_H
#define LOOM_ENGINE_EXCEPTION_H

#include <exception>
#include <stdexcept>

namespace loom::engine {
class ValidationException : public std::logic_error {
public:
    using std::logic_error::logic_error;
};

class AuthException : public std::logic_error {
public:
    using std::logic_error::logic_error;
};

class AccessDeniedException : public std::logic_error {
public:
    using std::logic_error::logic_error;
};

class StorageException : public std::logic_error {
public:
    using std::logic_error::logic_error;
};

}

#endif //LOOM_ENGINE_EXCEPTION_H
