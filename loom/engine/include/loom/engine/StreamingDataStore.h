//
// Created by void on 20/05/2020.
//

#ifndef LOOM_ENGINE_STREAMINGDATASTORE_H
#define LOOM_ENGINE_STREAMINGDATASTORE_H

#include <loom/engine/DataStore.h>
#include <atomic>

namespace loom::engine {

class StreamingDataStore : public DataStore {
    using DataStore::DataStore;

public:
    static constexpr size_t DefaultRequestBlockSize = 262140;

public:
    void requestData(size_t offset, size_t len, const std::shared_ptr<DataTransactionHandler> &handler) override;

    void putData(size_t offset, data_buffer_type data) override;

private:
    struct Subscription {
        size_t len; // Can be zero (endless streaming)
        std::shared_ptr<DataTransactionHandler> handler;
    };

    std::shared_mutex subscriptionsMutex;
    std::list<Subscription> subscriptions;
    std::atomic_size_t requestedBytes = 0;
    size_t maxRequestBlockSize = DefaultRequestBlockSize;
};

}

#endif //LOOM_ENGINE_STREAMINGDATASTORE_H
