//
// Created by void on 28/04/2020.
//

#ifndef LOOM_ENGINE_INMEMORYSUBJECTSTORE_H
#define LOOM_ENGINE_INMEMORYSUBJECTSTORE_H

#include <loom/engine/SubjectStore.h>
#include <loom/SubjectAnnouncementFlags.h>

#include <boost/uuid/random_generator.hpp>
#include <shared_mutex>

namespace loom::engine {

class InMemorySubjectStore : public SubjectStore {
public:
    SubjectView put(ConfigMap subjectConfig, SubjectAnnouncementFlags flags, ActorWeakHandle owner) override;

    [[nodiscard]] std::vector<SubjectView> findEval(const lql::ast::SelectExpression &expression,
                                                    SubjectViewMode viewMode) const override;

    size_t removeEval(const lql::ast::SelectExpression &expression) override;

private:
    struct StoredSubject {
        ActorHandle::weak_type owner;
        SubjectAnnouncementFlags flags;
        std::shared_ptr<ConfigMap> subjectConfig;
    };

    mutable std::shared_mutex subjectConfigsMutex;
    std::multimap<ConfigMap, StoredSubject> storedSubjects;
};

}

#endif //LOOM_ENGINE_INMEMORYSUBJECTSTORE_H
