//
// Created by void on 28/04/2020.
//

#ifndef LOOM_ENGINE_ACTORBRAIN_H
#define LOOM_ENGINE_ACTORBRAIN_H

#include <loom/ConfigMap.h>
#include <loom/engine/SubjectHandle.h>

#include <vector>
#include <cstddef>
#include <functional>
#include <memory>

namespace loom::engine {

using data_buffer_type = std::vector<std::byte>;

extern const data_buffer_type dataEOF;

class DataTransactionHandler {
public:
    virtual void handleError(const std::exception_ptr &error) = 0;

    enum class CompleteReason {
        RequestComplete,
        ReachedEOF
    };

    virtual void handleTransactionComplete(CompleteReason reason) = 0;

    virtual void handleTransactionContinue() {};

    virtual void handleDataReceived(data_buffer_type dataChunk, size_t chunkOffset) = 0;
};

class ActorBrain {
public:
    virtual void handleDataRequest(ConfigMap subjectId,
                                   ConfigMap::key_type attribute,
                                   size_t offset, size_t len) = 0;

    virtual void handleSubjectOwned(SubjectHandle subject) = 0;

    virtual void handleSubjectOwnershipLost(SubjectHandle subject) = 0;
};

class NoBrain : public ActorBrain {
public:
    void handleDataRequest(ConfigMap subjectId, ConfigMap::key_type attribute, size_t offset, size_t len) override;

    void handleSubjectOwned(SubjectHandle subject) override;

    void handleSubjectOwnershipLost(SubjectHandle subject) override;
};

}

#endif //LOOM_DATAALLOCATOR_H
