cmake_minimum_required(VERSION 3.13.2)

include(loom-build)

project(loom-ql)
add_library(lql STATIC
        src/loom/lql/AST.cpp
        include/loom/lql/AST.h

        src/loom/lql/Parser.cpp
        include/loom/lql/Parser.h

        src/loom/lql/Visitor.cpp
        include/loom/lql/Visitor.h

        src/loom/lql/SimpleMatcher.cpp
        include/loom/lql/SimpleMatcher.h
        )

find_package(Boost REQUIRED)
target_link_libraries(lql PUBLIC Boost::boost)

target_link_libraries(lql PUBLIC loom-common)

target_include_directories(lql PRIVATE ${CMAKE_CURRENT_BINARY_DIR})
target_include_directories(lql PRIVATE grammar)

loom_set_target_defaults(lql)

loom_setup_tests(
        test/MatcherTests.cpp
        test/ParserTests.cpp
        test/VisitorTests.cpp
        LIBS lql
)