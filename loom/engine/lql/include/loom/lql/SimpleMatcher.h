//
// Created by void on 31/05/2020.
//

#ifndef LOOM_LQL_CONFIGMAPMATCHER_H
#define LOOM_LQL_CONFIGMAPMATCHER_H

#include <loom/lql/AST.h>
#include <loom/ConfigMap.h>

namespace loom::lql {

extern const ast::SelectExpression AnyMatch;

class SimpleMatcher {
public:
    SimpleMatcher() = delete;

public:
    [[nodiscard]] static bool matches(const ConfigMap &map, const ast::SelectExpression &expression);

    [[nodiscard]] static bool matches(const ConfigMap &map, const ast::AttributeFilter &filter);

    [[nodiscard]] static bool matches(const ConfigMap &map, const ast::BoolExpression &expression);

    [[nodiscard]] static std::vector<ConfigMap::const_iterator> getAttributeMatches(
            const ConfigMap &map,
            const std::string &attributeNamePatternOrExactMatch);
};

}

#endif //LOOM_LQL_CONFIGMAPMATCHER_H
