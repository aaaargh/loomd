//
// Created by void on 13/04/2020.
//

#ifndef LOOM_LQL_PARSER_H
#define LOOM_LQL_PARSER_H

#include <istream>
#include <loom/lql/AST.h>

namespace loom::lql {

class Parser {
public:
    Parser() = delete;

public:
    [[nodiscard]] static ast::SelectExpression parse(const std::string &input);
};

}

#endif //LOOM_LQL_PARSER_H
