//
// Created by void on 30/05/2020.
//

#ifndef LOOM_LQL_VISITOR_H
#define LOOM_LQL_VISITOR_H

#include <loom/lql/AST.h>

namespace loom::lql {

class Visitor {
public:
    void visit(const ast::SelectExpression &expression);

    virtual void enter(const ast::SelectExpression &expression) {}

    virtual void exit(const ast::SelectExpression &expression) {}

    void visit(const ast::AttributeSelector &selector);

    virtual void enter(const ast::AttributeSelector &selector) {}

    virtual void exit(const ast::AttributeSelector &selector) {}

    void visit(const ast::Term &term);

    virtual void enter(const ast::Term &expression) {}

    virtual void exit(const ast::Term &expression) {}

    void visit(const ast::AttributeFilter &filter);

    virtual void enter(const ast::AttributeFilter &expression) {}

    virtual void exit(const ast::AttributeFilter &expression) {}

    void visit(const ast::AnyExpression &expression);

    virtual void enter(const ast::AnyExpression &expression) {}

    virtual void exit(const ast::AnyExpression &expression) {}

    void visit(const ast::BoolExpression &expression);

    virtual void enter(const ast::BoolExpression &expression) {}

    virtual void exit(const ast::BoolExpression &expression) {}
};

}

#endif //LOOM_VISITOR_H
