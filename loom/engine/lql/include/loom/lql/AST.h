//
// Created by void on 14/04/2020.
//

#ifndef LOOM_LQL_AST_H
#define LOOM_LQL_AST_H

#include <string>
#include <ostream>
#include <boost/variant.hpp>
#include <boost/spirit/home/x3/support/ast/position_tagged.hpp>

namespace loom::lql::ast {
enum class ComparisonOperator {
    Equal,
    NotEqual,
    Greater,
    GreaterEqual,
    Less,
    LessEqual,
};

enum class BoolOperator {
    Not,
    And,
    Or
};

struct AttributeFilter;
struct BoolExpression;
struct AnyExpression;

using Term = boost::variant<
        boost::recursive_wrapper<AttributeFilter>,
        boost::recursive_wrapper<BoolExpression>,
        boost::recursive_wrapper<AnyExpression>
>;

struct AttributeSelector : boost::spirit::x3::position_tagged {
    std::string attribute;
    bool excluded = false;
};

struct SelectExpression : boost::spirit::x3::position_tagged {
    Term term;
    std::vector<AttributeSelector> attributes;

    friend std::ostream &operator<<(std::ostream &os, const SelectExpression &expression);
};

struct AnyExpression : boost::spirit::x3::position_tagged {
    AnyExpression() noexcept = default;
};

struct AttributeFilter : boost::spirit::x3::position_tagged {
    ComparisonOperator operation;
    std::string attribute;
    std::string value;
};

struct BoolExpression : boost::spirit::x3::position_tagged {
    BoolOperator operation;
    std::vector<Term> terms;
};

}

#endif //LOOM_LQL_AST_H
