//
// Created by void on 13/04/2020.
//

#include <loom/lql/Parser.h>
#include <loom/Exception.h>

#include <boost/spirit/home/x3.hpp>
#include <boost/spirit/home/x3/support/utility/error_reporting.hpp>
#include <boost/spirit/home/x3/support/utility/annotate_on_success.hpp>
#include <boost/spirit/include/support_istream_iterator.hpp>

#include <boost/fusion/adapted/boost_tuple.hpp>
#include <boost/fusion/include/adapt_struct.hpp>

BOOST_FUSION_ADAPT_STRUCT(
        loom::lql::ast::AttributeFilter,
        (std::string, attribute)
                (loom::lql::ast::ComparisonOperator, operation)
                (std::string, value)
)

BOOST_FUSION_ADAPT_STRUCT(
        loom::lql::ast::BoolExpression,
        (loom::lql::ast::BoolOperator, operation)
                (std::vector<loom::lql::ast::Term>, terms)
)

BOOST_FUSION_ADAPT_STRUCT(
        loom::lql::ast::SelectExpression,
        (loom::lql::ast::Term, term)
                (std::vector<loom::lql::ast::AttributeSelector>, attributes)
)

BOOST_FUSION_ADAPT_STRUCT(
        loom::lql::ast::AttributeSelector,
        (bool, excluded)
                (std::string, attribute)
)

namespace x3 = boost::spirit::x3;

namespace loom::lql {

using namespace ast;

namespace parser_impl {

namespace ascii = boost::spirit::x3::ascii;
using x3::lexeme;
using x3::attr;
using x3::alnum;
using x3::lit;
using x3::char_;
using x3::omit;
using x3::eoi;
using x3::string;

// Error handling
struct error_handler {
    template<typename Iterator, typename Exception, typename Context>
    x3::error_handler_result on_error(
            Iterator &first, Iterator const &last, Exception const &x, Context const &context) {
        auto &error_handler = x3::get<x3::error_handler_tag>(context).get();
        std::string message = "Error! Expecting: " + x.which() + " here:";
        error_handler(x.where(), message);
        return x3::error_handler_result::fail;
    }
};

// Symbols
struct comparison_operator : x3::symbols<ComparisonOperator> {
    comparison_operator() : x3::symbols<ComparisonOperator>(std::string("comparison operator")) {
        add
                ("=", ComparisonOperator::Equal)
                ("!=", ComparisonOperator::NotEqual)
                (">", ComparisonOperator::Greater)
                (">=", ComparisonOperator::GreaterEqual)
                ("<", ComparisonOperator::Less)
                ("<=", ComparisonOperator::LessEqual);
    }
} comparison_operator;

struct bool_operator : x3::symbols<BoolOperator> {
    bool_operator() : x3::symbols<BoolOperator>(std::string("bool operator")) {
        add
                ("!", BoolOperator::Not)
                ("&", BoolOperator::And)
                ("|", BoolOperator::Or);
    }
} bool_operator;

// Rules
struct select_expression_tag;
const x3::rule<select_expression_tag, SelectExpression> select_expression{"select expression"};

struct term_tag;
const x3::rule<term_tag, Term> term{"term"};

struct attribute_filter_tag;
const x3::rule<attribute_filter_tag, AttributeFilter> attribute_filter{"attribute filter"};

struct bool_expression_tag;
const x3::rule<bool_expression_tag, BoolExpression> bool_expression{"bool expression"};

struct any_expression_tag;
const x3::rule<any_expression_tag, AnyExpression> any_expression{"any expression"};

struct attribute_selector_tag;
const x3::rule<attribute_selector_tag, AttributeSelector> attribute_selector{"attribute selector"};


// Simple rules

const auto double_quoted_string = lexeme['"' >> *("\"\"" >> attr('"') | ~char_('"')) >> '"'];
const auto single_quoted_string = lexeme['\'' >> *("''" >> attr('\'') | ~char_('\'')) >> '\''];

struct quoted_string_tag;
const auto quoted_string = x3::rule<quoted_string_tag, std::string>{"quoted string"} =
                                   single_quoted_string | double_quoted_string;
const auto brace_open = x3::rule<struct brace_open_tag>{"opening brace"} = omit[char_('(')];
const auto brace_close = x3::rule<struct brace_open_tag>{"closing brace"} = omit[char_(')')];
const auto bracket_open = x3::rule<struct brace_open_tag>{"opening bracket"} = omit[char_('[')];
const auto bracket_close = x3::rule<struct brace_open_tag>{"closing bracket"} = omit[char_(']')];
const auto comma = x3::rule<struct brace_open_tag>{"comma"} = omit[char_(',')];

struct unquoted_string_tag;
const auto unquoted_string = x3::rule<unquoted_string_tag, std::string>{"unquoted string"} =
                                     lexeme[+(alnum | char_("*_-"))];

struct number_tag;
const auto number = x3::rule<number_tag, std::string>{"number"} =
                            lexeme[char_("1-9") >> *char_("0-9") >> -(char_('.') >> *char_("0-9"))];

struct symbol_tag;
const auto symbol = x3::rule<symbol_tag, std::string>{"symbol"} = lexeme[char_("a-zA-Z_")
        >> *(alnum | char_('_'))];

struct attribute_value_tag;
const auto attribute_value = x3::rule<attribute_value_tag, std::string>{"string or *"} =
                                     (number | unquoted_string | quoted_string | string("*"));

// Definitions

const auto any_expression_def = x3::rule<attribute_value_tag, AnyExpression>{"any"} = omit['*'];

const auto attribute_filter_def = symbol > comparison_operator > attribute_value;

const auto bool_expression_def = bool_operator > +term;

const auto attribute_selector_def = -('!' > attr(true)) > (symbol | '*');

const auto attribute_list = bracket_open
        >> attribute_selector % comma
        >> bracket_close;

const auto term_def = brace_open > (attribute_filter | bool_expression | any_expression) > brace_close;

const auto select_expression_def = term > -attribute_list;

BOOST_SPIRIT_DEFINE(select_expression, term, attribute_filter, bool_expression, any_expression, attribute_selector);

struct symbol_tag : x3::annotate_on_success {
};
struct any_expression_tag : x3::annotate_on_success {
};
struct bool_expression_tag : x3::annotate_on_success {
};
struct attribute_filter_tag : x3::annotate_on_success {
};
struct attribute_selector_tag : x3::annotate_on_success {
};
struct term_tag : x3::annotate_on_success {
};
struct select_expression_tag : x3::annotate_on_success, error_handler {
};

}

SelectExpression Parser::parse(const std::string &input) {
    using x3::space;
    using iterator_type = std::string::const_iterator;

    SelectExpression result{};

    iterator_type begin_it = input.begin();
    const iterator_type end_it = input.end();

    using error_handler_type = x3::error_handler<iterator_type>;
    std::stringstream err;

    error_handler_type error_handler(begin_it, end_it, err);

    auto const parser = x3::with<x3::error_handler_tag>(std::ref(error_handler))[parser_impl::select_expression];

    bool r = x3::phrase_parse(begin_it, end_it, parser, space, result);

    if (!r || begin_it != end_it) {
        auto errStr = err.str();
        throw ParseException(errStr);
    }

    return result;
}


}
