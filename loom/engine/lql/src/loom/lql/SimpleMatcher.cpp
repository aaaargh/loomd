//
// Created by void on 31/05/2020.
//

#include <loom/lql/SimpleMatcher.h>
#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>
#include <regex>

namespace loom::lql {

const ast::SelectExpression AnyMatch = {
        .term = ast::AnyExpression()
};

namespace detail {
struct matcher_visitor : boost::static_visitor<bool> {
    explicit matcher_visitor(const ConfigMap &cm) : cm(cm) {}

    [[nodiscard]]  bool operator()(const ast::AnyExpression &) const {
        return true;
    }

    [[nodiscard]] bool operator()(const ast::BoolExpression &exp) const {
        for (auto &term : exp.terms) {
            if (exp.operation == ast::BoolOperator::And) {
                if (!term.apply_visitor(*this)) {
                    return false;
                }
            }
            if (exp.operation == ast::BoolOperator::Or) {
                if (term.apply_visitor(*this)) {
                    return true;
                }
            }
            if (exp.operation == ast::BoolOperator::Not) {
                if (term.apply_visitor(*this)) {
                    return false;
                }
            }
        }

        if (exp.operation == ast::BoolOperator::Or) {
            return false;
        }

        return true;
    }

    [[nodiscard]] bool operator()(const ast::AttributeFilter &filter) const {
        for (auto it : getAttributeMatches(cm, filter.attribute)) {
            auto &value = it->second;

            switch (filter.operation) {
                case ast::ComparisonOperator::Equal:
                    if (checkValueEqual(value, filter.value)) {
                        return true;
                    }
                    break;
                case ast::ComparisonOperator::NotEqual:
                    if (!checkValueEqual(value, filter.value)) {
                        return true;
                    }
                    break;
                case ast::ComparisonOperator::Greater:
                    if (!checkValueEqual(value, filter.value) && !checkValueLess(value, filter.value)) {
                        return true;
                    }
                    break;
                case ast::ComparisonOperator::GreaterEqual:
                    if (!checkValueLess(value, filter.value)) {
                        return true;
                    }
                    break;
                case ast::ComparisonOperator::Less:
                    if (checkValueLess(value, filter.value)) {
                        return true;
                    }
                    break;
                case ast::ComparisonOperator::LessEqual:
                    if (checkValueEqual(value, filter.value) || checkValueLess(value, filter.value)) {
                        return true;
                    }
                    break;
            }
        }

        return false;
    }

    [[nodiscard]] static bool checkValueEqual(const ConfigMapValue &value, const std::string &patternOrExactValue) {
        return std::visit([&](auto &&v) -> bool {
            using T = std::decay_t<decltype(v)>;
            if constexpr(std::is_same_v<T, std::string>) {
                // String comparison
                auto pattern = patternOrExactValue;
                boost::algorithm::replace_all(pattern, "*", ".*");
                std::smatch match;
                return std::regex_match(v, match, std::regex(pattern));
            }

            if constexpr(std::is_same_v<T, double> || std::is_same_v<T, long>) {
                // Numeric comparison
                auto testValue = boost::lexical_cast<T>(patternOrExactValue);
                return std::fabs(v - testValue) <= std::numeric_limits<T>::epsilon();
            }

            if constexpr(std::is_same_v<T, Variant>) {
                // Variant options comparison
                for (auto &value : v.values) {
                    if (value == "*") {
                        return true;
                    }

                    if (value == patternOrExactValue) {
                        return true;
                    }
                }
            }

            return false;
        }, value);
    }

    [[nodiscard]] static bool checkValueLess(const ConfigMapValue &value, const std::string &patternOrExactValue) {
        return std::visit([&](auto &&v) -> bool {
            using T = std::decay_t<decltype(v)>;
            if constexpr(std::is_same_v<T, std::string>) {
                // String length comparison
                return v < patternOrExactValue;
            }

            if constexpr(std::is_same_v<T, double> || std::is_same_v<T, long>) {
                // Numeric comparison
                auto testValue = boost::lexical_cast<T>(patternOrExactValue);
                return v < testValue;
            }

            return false;
        }, value);
    }

    [[nodiscard]] static std::vector<ConfigMap::const_iterator> getAttributeMatches(
            const ConfigMap &cm,
            const std::string &attributeNamePatternOrExactMatch) {
        std::vector<ConfigMap::const_iterator> matches;

        for (auto it = cm.begin(); it != cm.end(); ++it) {
            auto &key = it->first;

            // TODO: Maybe add a regex test here?
            if (key == attributeNamePatternOrExactMatch) {
                matches.push_back(it);
            }
        }

        return std::move(matches);
    }

    const ConfigMap &cm;
};
}

bool SimpleMatcher::matches(const ConfigMap &map, const ast::SelectExpression &expression) {
    detail::matcher_visitor visitor(map);
    return expression.term.apply_visitor(visitor);
}

bool SimpleMatcher::matches(const ConfigMap &map, const ast::AttributeFilter &filter) {
    detail::matcher_visitor visitor(map);
    return visitor(filter);
}

bool SimpleMatcher::matches(const ConfigMap &map, const ast::BoolExpression &expression) {
    detail::matcher_visitor visitor(map);
    return visitor(expression);
}

std::vector<ConfigMap::const_iterator> SimpleMatcher::getAttributeMatches(const ConfigMap &map,
                                                                          const std::string &attributeNamePatternOrExactMatch) {
    return detail::matcher_visitor::getAttributeMatches(map, attributeNamePatternOrExactMatch);
}

}
