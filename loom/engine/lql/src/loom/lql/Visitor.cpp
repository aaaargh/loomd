//
// Created by void on 30/05/2020.
//

#include <loom/lql/Visitor.h>

namespace loom::lql {

namespace detail {
struct expression_visitor : boost::static_visitor<void> {
    Visitor &visitor;

    template<typename ExpressionT>
    void operator()(const ExpressionT &exp) const {
        visitor.visit(exp);
    };
};

}

void Visitor::visit(const ast::SelectExpression &expression) {
    enter(expression);
    visit(expression.term);
    for (auto &attib : expression.attributes) {
        visit(attib);
    }
    exit(expression);
}

void Visitor::visit(const ast::Term &term) {
    enter(term);
    detail::expression_visitor visitor{.visitor = *this};
    term.apply_visitor(visitor);
    exit(term);
}

void Visitor::visit(const ast::AnyExpression &expression) {
    enter(expression);
    exit(expression);
}

void Visitor::visit(const ast::BoolExpression &expression) {
    enter(expression);
    for (auto &term : expression.terms) {
        visit(term);
    }
    exit(expression);
}

void Visitor::visit(const ast::AttributeFilter &filter) {
    enter(filter);
    exit(filter);
}

void Visitor::visit(const ast::AttributeSelector &selector) {
    enter(selector);
    exit(selector);
}

}
