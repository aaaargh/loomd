//
// Created by void on 16/04/2020.
//

#include <ostream>
#include <loom/lql/AST.h>

#include <boost/algorithm/string/replace.hpp>

namespace loom::lql::ast {
namespace detail {
struct print_expression_visitor : boost::static_visitor<std::string> {
    std::string operator()(const AnyExpression &) const {
        return "(*)";
    }

    [[nodiscard]] static std::string getBoolOperator(const BoolOperator &op) {
        switch (op) {
            case BoolOperator::Not:
                return "!";
            case BoolOperator::And:
                return "&";
            case BoolOperator::Or:
                return "|";
        }

        throw std::runtime_error("Invalid boolean operator");
    }

    [[nodiscard]] static std::string getOperator(const ComparisonOperator &op) {
        switch (op) {
            case ComparisonOperator::NotEqual:
                return "!=";
            case ComparisonOperator::Equal:
                return "=";
            case ComparisonOperator::GreaterEqual:
                return ">=";
            case ComparisonOperator::Greater:
                return ">";
            case ComparisonOperator::Less:
                return "<";
            case ComparisonOperator::LessEqual:
                return "<=";
        }

        throw std::runtime_error("Invalid operator");
    }

    std::string operator()(const BoolExpression &expr) const {
        std::stringstream ss;
        ss << "(";
        ss << getBoolOperator(expr.operation);
        for (auto &term : expr.terms) {
            ss << term.apply_visitor(*this);
        }
        ss << ")";
        return ss.str();
    }

    std::string operator()(const AttributeFilter &expr) const {
        std::stringstream ss;
        ss << "(";
        ss << expr.attribute;
        ss << getOperator(expr.operation);
        ss << expr.value;
        ss << ")";
        return ss.str();
    }
};

struct print_value_visitor : boost::static_visitor<std::string> {
    std::string operator()(std::string v) const {
        std::stringstream ss;
        boost::replace_all(v, "\"", "\\\"");
        ss << "\"" << v << "\"";
        return ss.str();
    }

    std::string operator()(const AnyExpression &v) const {
        return "*";
    }

    template<typename T>
    std::string operator()(const T &v) const {
        std::stringstream ss;
        ss << v;
        return ss.str();
    }
};

}

std::ostream &operator<<(std::ostream &os, const Term &expression) {
    detail::print_expression_visitor visitor;
    os << expression.apply_visitor(visitor);
    return os;
}

std::ostream &operator<<(std::ostream &os, const SelectExpression &expression) {
    os << expression.term;
    if (expression.attributes.empty()) {
        return os;
    }

    os << "[";

    bool first = true;
    for (auto &attrib : expression.attributes) {
        if (first) {
            first = false;
        } else {
            os << ",";
        }

        if (attrib.excluded) {
            os << "!";
        }
        os << attrib.attribute;
    }

    return os;
}

}