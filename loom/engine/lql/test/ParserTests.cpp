//
// Created by void on 13/04/2020.
//

#include <boost/test/unit_test.hpp>

#include <loom/lql/Parser.h>
#include <loom/Exception.h>

namespace loom::lql::testing {

using namespace loom::lql::ast;

template<typename RequiredT>
class TestVisitor : boost::static_visitor<> {
public:
    std::vector<RequiredT> found;

public:
    void operator()(RequiredT &val) {
        found.emplace_back(val);
    }

    template<typename IgnoredT>
    void operator()(IgnoredT val) const {}
};

BOOST_AUTO_TEST_SUITE(LQLParserTests)

BOOST_AUTO_TEST_CASE(can_parse_any_expression) {
    auto result = Parser::parse("(*)");
    TestVisitor<AnyExpression> visitor;
    boost::apply_visitor(visitor, result.term);

    BOOST_REQUIRE_EQUAL(1, visitor.found.size());
}

BOOST_AUTO_TEST_CASE(can_parse_any_value_expression) {
    auto result = Parser::parse("(alpha=*)");
    TestVisitor<AttributeFilter> visitor;
    boost::apply_visitor(visitor, result.term);

    BOOST_REQUIRE_EQUAL(1, visitor.found.size());
    BOOST_REQUIRE_EQUAL("alpha", visitor.found.front().attribute);
    BOOST_REQUIRE(ComparisonOperator::Equal == visitor.found.front().operation);
    BOOST_REQUIRE_EQUAL("*", visitor.found.front().value);
}

BOOST_AUTO_TEST_CASE(can_parse_unquoted_value) {
    auto result = Parser::parse("(alpha=beta)");
    TestVisitor<AttributeFilter> visitor;
    boost::apply_visitor(visitor, result.term);

    BOOST_REQUIRE_EQUAL(1, visitor.found.size());
    BOOST_REQUIRE_EQUAL("alpha", visitor.found.front().attribute);
    BOOST_REQUIRE(ComparisonOperator::Equal == visitor.found.front().operation);
    BOOST_REQUIRE_EQUAL("beta", visitor.found.front().value);
}

BOOST_AUTO_TEST_CASE(can_parse_unquoted_value_with_special_chars) {
    auto result = Parser::parse("(alpha=this-is-a_very_special-value----01234)");
    TestVisitor<AttributeFilter> visitor;
    boost::apply_visitor(visitor, result.term);

    BOOST_REQUIRE_EQUAL(1, visitor.found.size());
    BOOST_REQUIRE_EQUAL("alpha", visitor.found.front().attribute);
    BOOST_REQUIRE(ComparisonOperator::Equal == visitor.found.front().operation);
    BOOST_REQUIRE_EQUAL("this-is-a_very_special-value----01234", visitor.found.front().value);
}

BOOST_AUTO_TEST_CASE(can_parse_quoted_value_with_whitespaces) {
    auto result = Parser::parse("(alpha='beta gamma ')");
    TestVisitor<AttributeFilter> visitor;
    boost::apply_visitor(visitor, result.term);

    BOOST_REQUIRE_EQUAL(1, visitor.found.size());
    BOOST_REQUIRE_EQUAL("alpha", visitor.found.front().attribute);
    BOOST_REQUIRE(ComparisonOperator::Equal == visitor.found.front().operation);
    BOOST_REQUIRE_EQUAL("beta gamma ", visitor.found.front().value);
}

BOOST_AUTO_TEST_CASE(can_parse_attribute_expression) {
    auto result = Parser::parse("(alpha>='beta')");
    TestVisitor<AttributeFilter> visitor;
    boost::apply_visitor(visitor, result.term);

    BOOST_REQUIRE_EQUAL(1, visitor.found.size());
    BOOST_REQUIRE_EQUAL("alpha", visitor.found.front().attribute);
    BOOST_REQUIRE(ComparisonOperator::GreaterEqual == visitor.found.front().operation);
    BOOST_REQUIRE_EQUAL("beta", visitor.found.front().value);
}

BOOST_AUTO_TEST_CASE(can_parse_boolean_or) {
    auto result = Parser::parse("(|(alpha<=\"beta\")(theta!=nil))");
    TestVisitor<BoolExpression> visitor;
    boost::apply_visitor(visitor, result.term);

    BOOST_REQUIRE_EQUAL(1, visitor.found.size());
    BOOST_REQUIRE(BoolOperator::Or == visitor.found.front().operation);
    BOOST_REQUIRE_EQUAL(2, visitor.found.front().terms.size());

    TestVisitor<AttributeFilter> termVisitor;
    boost::apply_visitor(termVisitor, visitor.found.front().terms[0]);
    boost::apply_visitor(termVisitor, visitor.found.front().terms[1]);

    BOOST_REQUIRE_EQUAL(2, termVisitor.found.size());
    BOOST_REQUIRE_EQUAL("alpha", termVisitor.found[0].attribute);
    BOOST_REQUIRE(ComparisonOperator::LessEqual == termVisitor.found[0].operation);
    BOOST_REQUIRE_EQUAL("beta", termVisitor.found[0].value);

    BOOST_REQUIRE_EQUAL("theta", termVisitor.found[1].attribute);
    BOOST_REQUIRE(ComparisonOperator::NotEqual == termVisitor.found[1].operation);
    BOOST_REQUIRE_EQUAL("nil", termVisitor.found[1].value);
}

BOOST_AUTO_TEST_CASE(can_parse_symbol_with_underscore) {
    auto result = Parser::parse("(under_score=*)");
    TestVisitor<AttributeFilter> visitor;
    boost::apply_visitor(visitor, result.term);

    BOOST_REQUIRE_EQUAL(1, visitor.found.size());
    BOOST_REQUIRE_EQUAL("under_score", visitor.found.front().attribute);
    BOOST_REQUIRE(ComparisonOperator::Equal == visitor.found.front().operation);
    BOOST_REQUIRE_EQUAL("*", visitor.found.front().value);
}

BOOST_AUTO_TEST_CASE(can_parse_numeric_values) {
    auto result = Parser::parse("(number=2.3)");
    TestVisitor<AttributeFilter> visitor;
    boost::apply_visitor(visitor, result.term);

    BOOST_REQUIRE_EQUAL(1, visitor.found.size());
    BOOST_REQUIRE_EQUAL("number", visitor.found.front().attribute);
    BOOST_REQUIRE(ComparisonOperator::Equal == visitor.found.front().operation);
    BOOST_REQUIRE_EQUAL("2.3", visitor.found.front().value);
}

BOOST_AUTO_TEST_CASE(can_use_attribute_selectors) {
    auto result = Parser::parse("(*)[a,b,c,d]");
    BOOST_REQUIRE_EQUAL(4, result.attributes.size());
    BOOST_REQUIRE_EQUAL("a", result.attributes[0].attribute);
    BOOST_REQUIRE_EQUAL("b", result.attributes[1].attribute);
    BOOST_REQUIRE_EQUAL("c", result.attributes[2].attribute);
    BOOST_REQUIRE_EQUAL("d", result.attributes[3].attribute);
}

BOOST_AUTO_TEST_CASE(is_throwing_parse_exception_for_invalid_expression) {
    BOOST_REQUIRE_THROW(auto result = Parser::parse("(xy"), ParseException);
}

BOOST_AUTO_TEST_CASE(is_throwing_parse_exception_for_invalid_attribute_list) {
    BOOST_REQUIRE_THROW(auto result = Parser::parse("(*)[a"), ParseException);
}

BOOST_AUTO_TEST_SUITE_END()
}
