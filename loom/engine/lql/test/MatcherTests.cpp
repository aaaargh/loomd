//
// Created by void on 15/04/2020.
//

#include <loom/testing.h>

#include <loom/lql/SimpleMatcher.h>
#include <loom/lql/Parser.h>

using namespace loom::testing;

namespace loom::engine::testing {

BOOST_AUTO_TEST_SUITE(LQLMatcherTests)

BOOST_AUTO_TEST_CASE(any_filter_works) {
    ConfigMap configMap{
            {"a", "b"}
    };

    auto filter = lql::AnyMatch;

    BOOST_REQUIRE(lql::SimpleMatcher::matches(configMap, filter));
}

BOOST_AUTO_TEST_CASE(any_filter_works_for_empty_maps) {
    ConfigMap configMap{};

    auto filter = lql::AnyMatch;

    BOOST_REQUIRE(lql::SimpleMatcher::matches(configMap, filter));
}

BOOST_AUTO_TEST_CASE(equality_filter_works) {
    ConfigMap configMap{
            {"foo", "bar"}
    };

    auto filter = lql::Parser::parse("(foo=bar)");

    BOOST_REQUIRE(lql::SimpleMatcher::matches(configMap, filter));
}

BOOST_AUTO_TEST_CASE(equality_filter_with_one_matching_works) {
    ConfigMap configMap{
            {"foo", "baz"},
            {"foo", "bar"}
    };

    auto filter = lql::Parser::parse("(foo=bar)");

    BOOST_REQUIRE(lql::SimpleMatcher::matches(configMap, filter));
}

BOOST_AUTO_TEST_CASE(greater_filter_works) {
    ConfigMap configMap{
            {"foo", "baz"},
            {"foo", "bar"}
    };

    auto filter = lql::Parser::parse("(foo>ba)");

    BOOST_REQUIRE(lql::SimpleMatcher::matches(configMap, filter));
}

BOOST_AUTO_TEST_CASE(greater_filter_works_with_numbers) {
    ConfigMap configMap{
            {"foo", (long)100},
            {"foo", (long)200},
    };

    auto filter = lql::Parser::parse("(foo>150)");

    BOOST_REQUIRE(lql::SimpleMatcher::matches(configMap, filter));
}

BOOST_AUTO_TEST_CASE(equality_filter_works_with_numbers) {
    ConfigMap configMap{
            {"foo", (long)200},
    };

    auto filter = lql::Parser::parse("(foo=200)");

    BOOST_REQUIRE(lql::SimpleMatcher::matches(configMap, filter));
}

BOOST_AUTO_TEST_CASE(equality_filter_works_with_floating_precision_numbers) {
    ConfigMap configMap{
            {"foo", 200.1},
    };

    auto filter = lql::Parser::parse("(foo=200.1)");

    BOOST_REQUIRE(lql::SimpleMatcher::matches(configMap, filter));
}

BOOST_AUTO_TEST_CASE(inequality_filter_works_with_numbers) {
    ConfigMap configMap{
            {"foo", (long)200},
    };

    auto filter = lql::Parser::parse("(foo!=200)");

    BOOST_REQUIRE(!lql::SimpleMatcher::matches(configMap, filter));
}

BOOST_AUTO_TEST_CASE(inequality_filter_works_with_floating_precision_numbers) {
    ConfigMap configMap{
            {"foo", 200.1},
    };

    auto filter = lql::Parser::parse("(foo!=200.1)");

    BOOST_REQUIRE(!lql::SimpleMatcher::matches(configMap, filter));
}

BOOST_AUTO_TEST_CASE(equality_filter_works_with_pattern) {
    ConfigMap configMap{
            {"foo", "baz"},
            {"foo", "bar"}
    };

    auto filter = lql::Parser::parse("(foo=ba*)");

    BOOST_REQUIRE(lql::SimpleMatcher::matches(configMap, filter));
}

BOOST_AUTO_TEST_CASE(equality_filter_works_with_variant) {
    ConfigMap configMap{
            {"foo", "baz"},
            {"foo", Variant("x", "y")}
    };

    auto filter = lql::Parser::parse("(foo=y)");

    BOOST_REQUIRE(lql::SimpleMatcher::matches(configMap, filter));
}

BOOST_AUTO_TEST_SUITE_END()
}