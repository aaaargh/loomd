//
// Created by void on 30/05/2020.
//

#include <boost/test/unit_test.hpp>

#include <loom/lql/Parser.h>
#include <loom/lql/Visitor.h>

namespace loom::lql::testing {

using namespace loom::lql::ast;

BOOST_AUTO_TEST_SUITE(LQLVisitorTests)

BOOST_AUTO_TEST_CASE(can_visit_any) {
    struct : Visitor {
        size_t entered = 0;
        size_t exited = 0;

        void enter(const AnyExpression &) override {
            entered++;
        }

        void exit(const AnyExpression &) override {
            exited++;
        }
    } visitor;

    auto result = Parser::parse("(*)");
    visitor.visit(result);

    BOOST_REQUIRE_EQUAL(1, visitor.entered);
    BOOST_REQUIRE_EQUAL(visitor.entered, visitor.exited);
}

BOOST_AUTO_TEST_CASE(can_visit_terms_in_boolean) {
    struct : Visitor {
        size_t entered = 0;
        size_t exited = 0;

        void enter(const AttributeFilter &) override {
            entered++;
        }

        void exit(const AttributeFilter &) override {
            exited++;
        }
    } visitor;

    auto result = Parser::parse("(&(a=*)(b=*)(c=*))");
    visitor.visit(result);

    BOOST_REQUIRE_EQUAL(3, visitor.entered);
    BOOST_REQUIRE_EQUAL(visitor.entered, visitor.exited);
}

BOOST_AUTO_TEST_SUITE_END()

}