//
// Created by void on 28/04/2020.
//

#include <loom/engine/ActorBrain.h>

namespace loom::engine {

const data_buffer_type dataEOF{};

void NoBrain::handleDataRequest(ConfigMap subjectId, ConfigMap::key_type attribute, size_t offset, size_t len) {}

void NoBrain::handleSubjectOwned(SubjectHandle subject) {}

void NoBrain::handleSubjectOwnershipLost(SubjectHandle subject) {}

}
