//
// Created by void on 16/04/2020.
//

#include <loom/engine/SubjectStore.h>
#include <loom/engine/ConfigMapProvider.h>
#include <loom/lql/Parser.h>

#include <iomanip>
#include <boost/format.hpp>

namespace loom::engine {

std::vector<SubjectView> SubjectStore::find(const std::string &query, SubjectViewMode viewMode) const {
    return findEval(lql::Parser::parse(query), viewMode);
}

size_t SubjectStore::remove(const std::string &query) {
    return removeEval(lql::Parser::parse(query));
}

bool SubjectStore::remove(const ConfigMap &subjectId) {
    auto uuid = std::quoted(std::get<std::string>(subjectId.find("uuid")->second));
    auto realm = std::quoted(std::get<std::string>(subjectId.find("realm")->second));

    return remove((boost::format("(&(uuid=%1%)(realm=%2%))") % uuid % realm).str());
}

std::vector<SubjectView> SubjectStore::get(const ConfigMap &subjectId, SubjectViewMode viewMode) const {
    auto uuid = std::quoted(std::get<std::string>(subjectId.find("uuid")->second));
    auto realm = std::quoted(std::get<std::string>(subjectId.find("realm")->second));

    return find((boost::format("(&(uuid=%1%)(realm=%2%))") % uuid % realm).str(), viewMode);
}

}

