//
// Created by void on 26/04/2020.
//

#include <loom/engine/AuthProvider.h>

namespace loom::engine {

bool YoloAuthProvider::canAnnounceSubject(const ConfigMap &identity, const ConfigMap &subjectConfig) const {
    return true;
}

bool YoloAuthProvider::canDropSubject(const ConfigMap &identity, const ConfigMap &subjectConfig) const {
    return true;
}

}
