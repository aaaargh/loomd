//
// Created by void on 13/04/2020.
//

#include <loom/engine/Subject.h>
#include <loom/engine/Exception.h>
#include <loom/engine/Engine.h>
#include <loom/Exception.h>

#include <utility>
#include <loguru.hpp>
#include <loom/engine/SubjectHandle.h>


namespace loom::engine {

SubjectView::SubjectView(std::shared_ptr<ConfigMapProvider> provider) :
        provider(std::move(provider)) {}

ConfigMap SubjectView::fetchUniqueId() const {
    ConfigMap cm{};
    provider->fetchAttributes(cm, {"uuid", "realm"});
    return cm;
}

void SubjectView::fetchAllAttributes(ConfigMap &configMap) const {
    provider->fetchAllAttributes(configMap);
}

void SubjectView::fetchAttributes(ConfigMap &configMap, std::initializer_list<ConfigMap::key_type> &&attributes) const {
    provider->fetchAttributes(configMap,
                              std::forward<std::initializer_list<ConfigMap::key_type> &&>(attributes));
}

SubjectView::operator bool() const {
    return provider.operator bool();
}

Subject::Subject(Engine &engine, ActorWeakHandle owner, ConfigMap uuidMap)
        : engine(engine),
          uuidMap(std::move(uuidMap)),
          owner(std::move(owner)) {}

SubjectRef::SubjectRef(Subject &subject) : subject(subject) {}

const ConfigMap &SubjectRef::getUniqueId() const {
    return subject.uuidMap;
}

SubjectMemory &SubjectRef::getMemory() {
    return subject.memory;
}

void SubjectRef::setOwner(const ActorWeakHandle &newOwner) {
    subject.owner = newOwner;
}

const ActorWeakHandle &SubjectRef::getOwner() const {
    return subject.owner;
}

SubjectRef::~SubjectRef() {
    if (isValid) {
        isValid = false;
        subject.engine.dropSubject(subject.engine.getSystemActor(), subject.uuidMap,
                                   {}, [](const auto &err) {
                    std::rethrow_exception(err);
                });
    }
}

}
