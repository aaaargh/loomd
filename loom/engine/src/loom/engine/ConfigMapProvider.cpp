//
// Created by void on 16/04/2020.
//

#include <loom/engine/ConfigMapProvider.h>
#include <iostream>

namespace loom::engine {

InMemoryConfigMapProvider::InMemoryConfigMapProvider(ConfigMap sourceConfigMap) :
        sourceConfigMap(std::move(sourceConfigMap)) {}

void InMemoryConfigMapProvider::fetchAttributes(ConfigMap &configMap,
                                                std::initializer_list<ConfigMap::key_type> &&attributes) const {
    // Copy over all matching attributes from our source map.
    for (auto &attributeName : attributes) {
        auto range = sourceConfigMap.equal_range(attributeName);

        while (range.first != range.second) {
            auto item = range.first++;
            auto &key = item->first;

            auto value = item->second;

            configMap.emplace(std::make_pair(key, std::move(value)));
        }
    }
}

void InMemoryConfigMapProvider::fetchAllAttributes(ConfigMap &configMap) const {
    for (auto &entry: sourceConfigMap) {
        configMap.insert(entry);
    }
}

const ConfigMap &InMemoryConfigMapProvider::getSourceConfigMap() const {
    return sourceConfigMap;
}

void ConfigMapProvider::fetchAttribute(ConfigMap &configMap,
                                       const ConfigMap::key_type &attributeName) const {
    return fetchAttributes(configMap, {attributeName});
}

}
