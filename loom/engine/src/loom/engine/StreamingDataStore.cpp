//
// Created by void on 20/05/2020.
//

#include <loom/engine/StreamingDataStore.h>

#include <loguru.hpp>
#include <cstring>

#include <loguru.hpp>

namespace loom::engine {

void StreamingDataStore::requestData(size_t offset, size_t len,
                                     const std::shared_ptr<DataTransactionHandler> &handler) {
    std::unique_lock lk(subscriptionsMutex);
    Subscription subscription{
            .len = len,
            .handler = handler
    };
    // Start the transaction
    subscriptions.emplace_back(std::move(subscription));

    if (requestedBytes > 0) {
        return;
    }

    requestedBytes = maxRequestBlockSize;

    lk.unlock();

    getProvider().handleDataRequest(getSubjectId(), getAttribute(), 0, requestedBytes);
}

void StreamingDataStore::putData(size_t offset, data_buffer_type data) {
    std::unique_lock lk(subscriptionsMutex);

    bool isEOF = data == dataEOF;

    if (isEOF) {
        // TODO: This is called multiple times for some reason. Check why this is happening.
        DLOG_S(INFO) << "Stream has reached EOF!";
    }

    bool wantsMore = false;

    for (auto it = subscriptions.begin(); it != subscriptions.end();) {
        auto &subscription = *it;

        auto toRead = subscription.len > 0 ? std::min(data.size(), subscription.len) : data.size();
        data_buffer_type localData;
        localData.resize(toRead);
        std::memcpy(localData.data(), data.data(), toRead);

        subscription.handler->handleDataReceived(std::move(localData), 0);
        if (subscription.len > 0) {
            subscription.len -= toRead;

            if (subscription.len == 0) {
                // Subscription is completed.
                subscription.handler->handleTransactionComplete(
                        DataTransactionHandler::CompleteReason::RequestComplete);
                it = subscriptions.erase(it);
                continue;
            }
        }
        wantsMore = true;

        if (isEOF) {
            subscription.handler->handleTransactionComplete(DataTransactionHandler::CompleteReason::ReachedEOF);
            it = subscriptions.erase(it);
            continue;
        } else {
            subscription.handler->handleTransactionContinue();
        }

        ++it;
    }
    lk.unlock();

    if (wantsMore && !isEOF) {
        // Request some more data.
        getProvider().handleDataRequest(getSubjectId(), getAttribute(), 0, requestedBytes);
    }
}

}