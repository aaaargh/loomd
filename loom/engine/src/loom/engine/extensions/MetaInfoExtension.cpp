//
// Created by void on 11/05/2020.
//

#include <loom/engine/extensions/MetaInfoExtension.h>
#include <loom/engine/Engine.h>
#include <loom/Exception.h>

#include <loguru.hpp>

namespace loom::engine::extensions {

void MetaInfoExtension::onPostAnnounceSubject(Engine &engine, const ActorHandle &actor, const SubjectHandle &subject,
                                              bool isUpdated) {
    auto &subjectId = subject->getUniqueId();
    auto uuid = std::get<std::string>(subjectId.find("uuid")->second);
    auto realm = std::get<std::string>(subjectId.find("realm")->second);

    if (realm == MetaInfoRealmId) {
        // This is a meta info already, ignore.
        return;
    }

    ConfigMap metaConfig = {
            {"uuid",          realm + ":" + uuid},
            {"realm",         "metainfo"},
            {"subject-realm", realm},
            {"subject-uuid",  uuid}
    };

    engine.announceSubject(
            engine.getSystemActor(),
            metaConfig,
            SubjectAnnouncementFlags::Defaults,
            [subject](const auto &subjectHandle) {
                subject->getMemory().dependentSubjects.emplace_back(subjectHandle);
            }, [](const auto &err) {
                LOG_S(ERROR) << "Unable to announce realm meta info: " << get_exception_message(err);
                std::rethrow_exception(err);
            });
}

void MetaInfoExtension::onPreDropSubject(Engine &engine, const ActorHandle &actor, const ConfigMap &subjectId) {
    auto uuid = std::get<std::string>(subjectId.find("uuid")->second);
    auto realm = std::get<std::string>(subjectId.find("realm")->second);

    if (realm == MetaInfoRealmId) {
        // This is a meta info already, ignore.
        return;
    }


}

}
