//
// Created by void on 06/06/2020.
//

#include <loom/engine/extensions/SystemInfoExtension.h>
#include <loom/engine/Engine.h>

#include <loguru.hpp>

namespace loom::engine::extensions {

void SystemInfoExtension::onInitialize(Engine &engine) {
    // Add some system subjects
    engine.announceSubject(
            engine.getSystemActor(),
            {
                    {"realm",   "system"},
                    {"uuid",    "engineInfo"},
                    {"version", engine.getEngineVersionString()},
            },
            SubjectAnnouncementFlags::Defaults,
            [this](SubjectHandle handle) { persistentSubjects.emplace(std::move(handle)); },
            [](const auto &err) {
                LOG_S(ERROR) << "Unable to create engine info: " << get_exception_message(err);
                std::rethrow_exception(err);
            }
    );
}
}