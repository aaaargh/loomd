//
// Created by void on 09/05/2020.
//

#include <loom/engine/extensions/LogExtension.h>

#include <loguru.hpp>

namespace loom::engine::extensions {

void LogExtension::onInitialize(Engine &engine) {
    LOG_S(INFO) << "Engine logging enabled.";
}

void LogExtension::onShutdown(Engine &engine) {
    LOG_S(INFO) << "Engine shutting down.";
}

void LogExtension::onPostAnnounceSubject(Engine &engine, const ActorHandle &actor, const SubjectHandle &subjectHandle,
                                         bool isUpdated) {
    LOG_S(INFO) << "New subject announced: " << subjectHandle->getUniqueId();
}

void LogExtension::onPreDropSubject(Engine &engine, const ActorHandle &actor, const ConfigMap &subjectId) {
}

void LogExtension::onPostDropSubject(Engine &engine, const ActorHandle &actor, const ConfigMap &subjectId) {
    LOG_S(INFO) << "Subject dropped: " << subjectId;
}

void LogExtension::onPostRegisterActor(Engine &engine, const ActorHandle &actor) {
    LOG_S(INFO) << "Actor registered: " << actor->getIdentitySubject()->getUniqueId();
}

void LogExtension::onPostDropActor(Engine &engine, const ActorHandle &actor) {
    LOG_S(INFO) << "Actor dropped: " << actor->getIdentitySubject()->getUniqueId();
}

void LogExtension::onExtensionError(Engine &engine, const Extension &extension, const std::exception &error) {
    LOG_S(ERROR) << "Extension invocation failed: " << error.what();
}

void LogExtension::onSubscribe(Engine &engine, const ActorHandle &actor) {
    LOG_S(INFO) << "Added new subscription for actor " << actor->getIdentitySubject()->getUniqueId() << "\n";
}

}
