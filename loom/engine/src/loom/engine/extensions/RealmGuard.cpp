//
// Created by void on 09/05/2020.
//

#include <loom/engine/extensions/RealmGuard.h>
#include <loom/engine/Exception.h>
#include <loom/engine/Engine.h>
#include <loom/Exception.h>
#include <loom/lql/SimpleMatcher.h>
#include <loom/lql/Parser.h>

#include <loguru.hpp>

namespace loom::engine::extensions {

void RealmGuard::onPreAnnounceSubject(Engine &engine,
                                      const ActorHandle &actor,
                                      ConfigMap &subjectConfig,
                                      SubjectAnnouncementFlags &flags) {
    auto it = subjectConfig.find("realm");

    if (it == subjectConfig.end()) {
        throw ValidationException("Subject realm is undefined");
    }

    auto &realm = std::get<std::string>(it->second);

    if (realm == PrivateRealmId) {
        //realm = actor.getUUID();
    }

    if (!canWrite(engine, actor, realm)) {
        throw AccessDeniedException("Cannot access realm '" + realm + "' for writing.");
    }

    DLOG_S(INFO) << "Authorized subject announcement in realm " << realm << " for actor "
                 << actor->getIdentitySubject()->getUniqueId();
}

bool RealmGuard::canWrite(Engine &engine, const ActorHandle &actor, const std::string &realmId) const {
    if (actor == engine.getSystemActor()) {
        // System actor can do anything.
        return true;
    }

    if (isSystemRealm(realmId)) {
        return false;
    }

    std::shared_lock lk(realmsMutex);

    auto it = realms.find(realmId);
    if (it == realms.end()) {
        // Realm not found.
        return false;
    }

    auto &realm = it->second;

    auto actorIdentity = actor->getIdentitySubject()->getUniqueId();

    return lql::SimpleMatcher::matches(actorIdentity, realm.writeFilter);

}

bool RealmGuard::canRead(Engine &engine, const ActorHandle &actor, const std::string &realmId) const {
    if (actor == engine.getSystemActor()) {
        // System actor can do anything.
        return true;
    }

    if (isSystemRealm(realmId)) {
        return false;
    }

    std::shared_lock lk(realmsMutex);

    auto it = realms.find(realmId);
    if (it == realms.end()) {
        // Realm not found.
        return false;
    }

    auto &realm = it->second;
    auto actorIdentity = actor->getIdentitySubject()->getUniqueId();
    return lql::SimpleMatcher::matches(actorIdentity, realm.readFilter);
}

bool RealmGuard::isSystemRealm(const std::string &realmId) {
    return
            realmId == SystemRealmId
            || realmId == ActorsRealmId
            || realmId == RealmsRealmId;
}

void RealmGuard::onAlterSubjectQuery(Engine &engine, const ActorHandle &actor, lql::ast::SelectExpression &query,
                                     SubjectQueryFlags &flags) {
    // TODO: This can be used to restrict the query.
}

void RealmGuard::onPostRegisterActor(Engine &engine, const ActorHandle &actor) {
    const auto &actorId = actor->getIdentitySubject()->getUniqueId();
    auto uuid = std::get<std::string>(actorId.find("uuid")->second);

    Realm privateActorRealm = {
            .writeFilter = lql::Parser::parse("(uuid='" + uuid + "')"),
            .readFilter = lql::Parser::parse("(uuid='" + uuid + "')"),
    };

    LOG_S(INFO) << "Created actor realm '" << uuid << "'";

    std::unique_lock lk(realmsMutex);
    realms.emplace(std::make_pair(uuid, std::move(privateActorRealm)));

    ConfigMap realmConfig = {
            {"uuid",  uuid},
            {"realm", RealmsRealmId}
    };

    engine.announceSubject(
            engine.getSystemActor(),
            std::move(realmConfig),
            SubjectAnnouncementFlags::Defaults,
            [actor](const SubjectHandle &subject) {
                assert(actor);
                actor->getMemory().persistentSubjects.insert(subject);
            }, [](const auto &err) {
                LOG_S(ERROR) << "Error storing realm guard info: " << get_exception_message(err);
                std::rethrow_exception(err);
            });
}

void RealmGuard::onPostDropActor(Engine &engine, const ActorHandle &actor) {
    const auto &actorId = actor->getIdentitySubject()->getUniqueId();
    auto uuid = std::get<std::string>(actorId.find("uuid")->second);

    LOG_S(INFO) << "Dropped actor realm '" << uuid << "'";

    std::unique_lock lk(realmsMutex);
    realms.erase(uuid);
}

void RealmGuard::onInitialize(Engine &engine) {
    LOG_S(INFO) << "Creating public realm '" << PublicRealmId << "'.";

    std::unique_lock lk(realmsMutex);

    Realm realm = {
            .writeFilter = lql::Parser::parse("(*)"),
            .readFilter = lql::Parser::parse("(*)"),
    };

    realms.emplace(std::make_pair(PublicRealmId, std::move(realm)));
}

void RealmGuard::onValidateSubject(const Engine &engine, const ConfigMap &subjectConfig) const {
    auto range = subjectConfig.equal_range("realm");
    auto count = std::distance(range.first, range.second);

    if (count == 0) {
        throw NotFoundException("Missing required attribute 'realm' in subject config map");
    }

    if (count > 1) {
        throw NotFoundException("Attribute 'realm' must be unique");
    }

    auto &realm = range.first->second;

    std::visit([&](auto &&v) {
        using T = std::decay_t<decltype(v)>;

        if constexpr(std::is_same_v<T, std::string>) {
            if (v.empty()) {
                throw ValidationException("Attribute 'realm' must not be empty");
            }
        } else {
            throw NotFoundException("Attribute 'realm' must be a string");
        }
    }, realm);

    range = subjectConfig.equal_range("uuid");
    count = std::distance(range.first, range.second);

    if (count == 0) {
        throw NotFoundException("Missing required attribute 'uuid' in subject config map");
    }

    if (count > 1) {
        throw NotFoundException("Attribute 'uuid' must be unique");
    }

    auto &uuid = range.first->second;

    std::visit([&](auto &&v) {
        using T = std::decay_t<decltype(v)>;

        if constexpr(std::is_same_v<T, std::string>) {
            if (v.empty()) {
                throw ValidationException("Attribute 'uuid' must not be empty");
            }
        } else {
            throw NotFoundException("Attribute 'uuid' must be a string");
        }
    }, uuid);
}

void RealmGuard::onPreRegisterActor(Engine &engine, ConfigMap &identityConfig) {
    identityConfig.erase("realm");
    identityConfig.insert(std::make_pair("realm", ActorsRealmId));
}

}
