//
// Created by void on 14/03/2020.
//

#include <loom/engine/Engine.h>
#include <loom/engine/Exception.h>
#include <loom/Exception.h>
#include <loom/ConfigMapExtractor.h>
#include <loom/lql/Parser.h>
#include <loom/lql/SimpleMatcher.h>

#include <loom/engine/InMemorySubjectStore.h>
#include <loom/engine/extensions/LogExtension.h>
#include <loom/engine/extensions/RealmGuard.h>
#include <loom/engine/extensions/MetaInfoExtension.h>
#include <loom/engine/extensions/SystemInfoExtension.h>

#include <loom/engine/StreamingDataStore.h>
#include <loom/engine/PassthroughDataStore.h>

#include <utility>
#include <chrono>

#include <boost/uuid/random_generator.hpp>
#include <boost/uuid/uuid_io.hpp>

#include <loguru.hpp>

#define INVOKE_EXTENSION_INTERRUPTABLE(fun, ...) \
for (auto &ext : extensions) { \
try { ext->fun(*this, __VA_ARGS__); } \
catch (const std::exception &error) { \
for (auto &e : extensions) { e->onExtensionError(*this, *ext, error); } \
errorCallback(std::make_exception_ptr(error)); \
return; } }

#define INVOKE_EXTENSION(fun, ...) \
for (auto &ext : extensions) { \
try { ext->fun(*this, __VA_ARGS__); } \
catch (const std::exception &error) { \
for (auto &e : extensions) { e->onExtensionError(*this, *ext, error); } \
errorCallback(std::make_exception_ptr(error)); } }

namespace loom::engine {

const long DefaultTickFreqMillis = 1000L;

Engine::Engine(boost::asio::io_context &ioContext)
        : ioContext(ioContext),
          engineTickTimer(ioContext),
          subjectStore(std::make_unique<InMemorySubjectStore>()),
          authProvider(std::make_unique<YoloAuthProvider>()) {
    // Install the level 0 core extensions.
    installExtension<extensions::RealmGuard>();

    // Install the engine actor.
    createSystemActor();

    // Install the level 1 core extensions
    installExtension<extensions::MetaInfoExtension>();
    installExtension<extensions::SystemInfoExtension>();
    installExtension<extensions::LogExtension>();
}

Engine::~Engine() {
    isInShutdownPhase = true;

    // Shutdown the extensions
    for (auto &ext : extensions) {
        ext->onShutdown(*this);
    }

    extensions.clear();
    subjectStore.reset();

    invalidateResources();
}

void Engine::invalidateResources() {
    for (auto &actor : registeredActors) {
        auto ptr = actor.second.ref.lock();
        if (ptr) {
            ptr->isValid = false;
        }
    }

    for (auto &subject : activeSubjects) {
        auto ptr = subject.second.ref.lock();
        if (ptr) {
            ptr->isValid = false;
        }
    }

    registeredActors.clear();
    activeSubjects.clear();

    engineActor->isValid = false;
    engineActor.reset();
}

void Engine::createSystemActor() {
    ConfigMap engineActorId = {
            {"uuid", "system"}
    };

    for (auto &ext : extensions) {
        ext->onPreRegisterActor(*this, engineActorId);
    }

    ActiveSubject activeEngineActorSubject = {
            .subject = std::make_unique<Subject>(*this, ActorWeakHandle(), engineActorId)
    };

    SubjectHandle engineActorSubjectHandle(new SubjectRef(*activeEngineActorSubject.subject));
    activeEngineActorSubject.ref = engineActorSubjectHandle;

    RegisteredActor registeredEngineActor = {
            .actor =std::make_unique<Actor>(*this, std::make_shared<NoBrain>(), engineActorSubjectHandle),
    };

    engineActor = ActorHandle(new ActorRef(*registeredEngineActor.actor));
    registeredEngineActor.ref = engineActor;

    activeSubjects.emplace(std::make_pair(engineActorId, std::move(activeEngineActorSubject)));
    registeredActors.emplace(std::make_pair(engineActorId, std::move(registeredEngineActor)));

    for (auto &ext : extensions) {
        ext->onPostRegisterActor(*this, engineActor);
    }
}

const std::string &Engine::getEngineVersionString() {
    static const std::string engineVersion = LOOM_ENGINE_VERSION_STRING;
    return engineVersion;
}

void Engine::handleEngineTick(const boost::system::error_code &ec) {
    if (ec) {
        // Something bad happened.
        return;
    }

    engineTickTimer.expires_from_now(boost::posix_time::milliseconds(DefaultTickFreqMillis));
    engineTickTimer.async_wait([this](const boost::system::error_code &ec) { handleEngineTick(ec); });
}

void Engine::announceSubject(const ActorHandle &actor,
                             ConfigMap subjectConfig,
                             SubjectAnnouncementFlags flags,
                             SubjectAnnounceSuccessCallback successCallback,
                             ErrorCallback errorCallback) {
    if (!actor) {
        if (errorCallback) {
            errorCallback(std::make_exception_ptr(AuthException("Not authorized")));
        }
        return;
    }

    auto fun = [this,
            actor = actor,
            subjectConfig = std::move(subjectConfig),
            flags,
            successCallback = std::move(successCallback),
            errorCallback = std::move(errorCallback)]() mutable {
        try {
            validateSubject(subjectConfig);
        } catch (std::exception &ex) {
            if (errorCallback) {
                errorCallback(std::make_exception_ptr(ex));
            }
            return;
        }

        // Invoke the extension callbacks.
        INVOKE_EXTENSION_INTERRUPTABLE(onPreAnnounceSubject, actor, subjectConfig, flags);

        // Store the subject.
        auto subjectView = subjectStore->put(subjectConfig, flags, actor);
        ConfigMap subjectId;
        subjectView.fetchAllAttributes(subjectId);
        ConfigMapExtractors::get_subject_id_with_variants_extractor()(subjectId);

        // Scan for data sources.
        for (auto &entry : subjectConfig) {
            if (std::holds_alternative<DataSourceInfo>(entry.second)) {
                auto type = std::get<DataSourceInfo>(entry.second);

                std::shared_ptr<DataStore> store;

                ConfigMapAttributeKey key(subjectId, entry.first);

                // Invoke the extension callbacks.
                INVOKE_EXTENSION(onAlterDataStore, actor, store, key);

                if (!store) {
                    store = createNewDataStore(subjectConfig, entry.first, actor);
                }

                dataStoreManager.put(key, actor, store);
            }
        }

        std::unique_lock lk(activeSubjectsMutex);
        auto subjectIt = activeSubjects.find(subjectId);

        SubjectHandle subjectHandle;
        bool isUpdated;

        if (subjectIt != activeSubjects.end()) {
            // Subject already exists.
            subjectHandle = subjectIt->second.ref.lock();

            auto owner = subjectHandle->getOwner().lock();

            if (actor != owner) {
                LOG_S(WARNING) << "Actor " << owner << " lost ownership over subject " << subjectId;
                owner->getBrain().handleSubjectOwnershipLost(subjectHandle);
                subjectIt->second.subject->owner = owner;
            }

            isUpdated = true;
        } else {
            // Subject is new.
            ActiveSubject activeSubject = {
                    .subject = std::make_unique<Subject>(*this, actor, subjectId),
            };

            subjectHandle.reset(new SubjectRef(*activeSubject.subject));
            activeSubject.ref = subjectHandle;

            activeSubjects.emplace(std::make_pair(subjectId, std::move(activeSubject)));

            isUpdated = false;
        }
        lk.unlock();

        LOG_S(INFO) << "Actor " << actor->getIdentitySubject()->getUniqueId() << " is now owning " << subjectId;
        actor->getBrain().handleSubjectOwned(subjectHandle);

        // Invoke the extension callbacks.
        INVOKE_EXTENSION(onPostAnnounceSubject, actor, subjectHandle, isUpdated);

        if (successCallback) {
            successCallback(subjectHandle);
        }

        forEachSubscriber([&](Subscription &subscription) {
            if (check_flag(subscription.getFlags(), SubjectSubscriptionFlags::IgnoreOwnSubjects)) {
                if (actor == subscription.getActor().lock()) {
                    // Ignore own subjects.
                    return;
                }
            }

            if (lql::SimpleMatcher::matches(subjectConfig, subscription.getFilterExpression())) {
                subscription.subjectAnnounced(subjectHandle, isUpdated);
            }
        });
    };

    taskArena.execute(fun);
}

void Engine::dropSubject(const ActorHandle &actor,
                         ConfigMap subjectId,
                         SubjectDropSuccessCallback successCallback,
                         ErrorCallback errorCallback) {
    if (isInShutdownPhase) {
        return;
    }

    if (!actor) {
        if (errorCallback) {
            errorCallback(std::make_exception_ptr(AuthException("Not authorized")));
        }
        return;
    }

    auto fun = [
            this,
            actor = actor,
            subjectId = std::move(subjectId),
            successCallback = std::move(successCallback),
            errorCallback = std::move(errorCallback)]() mutable {

        try {
            validateSubject(subjectId);
        } catch (std::exception &ex) {
            if (errorCallback) {
                errorCallback(std::make_exception_ptr(ex));
            }
            return;
        }

        // Invoke the extension callbacks.
        INVOKE_EXTENSION_INTERRUPTABLE(onPreDropSubject, actor, subjectId);

        subjectStore->remove(subjectId);

        std::unique_lock lk(activeSubjectsMutex);

        auto it = activeSubjects.find(subjectId);

        if (it == activeSubjects.end()) {
            lk.unlock();
            if (errorCallback) {
                errorCallback(std::make_exception_ptr(
                        std::move(std::logic_error("Subject instance does not exist"))
                ));
            }
            return;
        }

        auto subjectInstance = std::move(it->second);
        activeSubjects.erase(it);
        lk.unlock();

        // Delete the stores
        dataStoreManager.dropAll(subjectId);

        // Invoke the extension callbacks.
        INVOKE_EXTENSION(onPostDropSubject, actor, subjectId);

        if (successCallback) {
            successCallback();
        }

        forEachSubscriber([&](Subscription &subscription) {
            if (lql::SimpleMatcher::matches(subjectId, subscription.getFilterExpression())) {
                subscription.subjectDropped(subjectId);
            }
        });
    };

    taskArena.execute(fun);
}

void Engine::subscribe(const ActorHandle &actor,
                       const std::string &query,
                       SubjectSubscriptionFlags flags,
                       SubscribeSuccessCallback successCallback,
                       ErrorCallback errorCallback) {
    subscribe(actor, lql::Parser::parse(query), flags, std::move(successCallback), std::move(errorCallback));
}

void Engine::subscribe(const ActorHandle &actor,
                       lql::ast::SelectExpression filterExpression,
                       SubjectSubscriptionFlags flags,
                       SubscribeSuccessCallback successCallback,
                       ErrorCallback errorCallback) {
    if (!actor) {
        if (errorCallback) {
            errorCallback(std::make_exception_ptr(AuthException("Not authorized")));
        }
        return;
    }

    auto fun = [
            this,
            actor = actor,
            filterExpression = std::move(filterExpression),
            flags,
            successCallback = std::move(successCallback),
            errorCallback = std::move(errorCallback)] {
        INVOKE_EXTENSION_INTERRUPTABLE(onSubscribe, actor);

        auto subscription = std::make_shared<Subscription>(actor, filterExpression, flags);

        std::unique_lock lk(subscriptionsMutex);
        subscriptions.emplace(std::weak_ptr(subscription));
        lk.unlock();

        if (check_flag(flags, SubjectSubscriptionFlags::EmitExistingSubjects)) {
            std::shared_lock subjectsLk(activeSubjectsMutex);
            for (auto &view : subjectStore->findEval(filterExpression, SubjectViewMode::ResolveRequired)) {
                auto subjectId = view.fetchUniqueId();
                auto subjectIt = activeSubjects.find(subjectId);

                if (check_flag(flags, SubjectSubscriptionFlags::IgnoreOwnSubjects)) {
                    auto owner = subjectIt->second.subject->owner.lock();
                    if (owner == actor) {
                        // Ignore own subjects.
                        continue;
                    }
                }

                auto handle = subjectIt->second.ref.lock();

                subscription->subjectAnnounced(handle, false);
            }
        }

        if (successCallback) {
            successCallback(subscription);
        }
    };

    taskArena.execute(fun);
}

void Engine::querySubjects(const ActorHandle &actor,
                           const std::string &query,
                           SubjectQueryFlags flags,
                           SubjectViewMode viewMode,
                           Engine::QuerySubjectsSuccessCallback successCallback,
                           Engine::ErrorCallback errorCallback) {
    querySubjects(actor, lql::Parser::parse(query), flags, viewMode,
                  std::move(successCallback),
                  std::move(errorCallback));
}

void Engine::querySubjects(const ActorHandle &actor,
                           lql::ast::SelectExpression query,
                           SubjectQueryFlags flags,
                           SubjectViewMode viewMode,
                           QuerySubjectsSuccessCallback successCallback,
                           ErrorCallback errorCallback) {
    if (!actor) {
        if (errorCallback) {
            errorCallback(std::make_exception_ptr(AuthException("Not authorized")));
        }
        return;
    }

    auto fun = [
            this,
            query = std::move(query),
            viewMode,
            actor = actor,
            flags,
            successCallback = std::move(successCallback),
            errorCallback = std::move(errorCallback)]() mutable {

        // Invoke the extension callbacks.
        INVOKE_EXTENSION_INTERRUPTABLE(onAlterSubjectQuery, actor, query, flags);

        std::vector<ConfigMap> results;

        try {
            for (const auto &subjectView : subjectStore->findEval(query, viewMode)) {
                ConfigMap cm;
                subjectView.fetchAllAttributes(cm);

                if (check_flag(flags, SubjectQueryFlags::IgnoreOwnSubjects)) {
                    // Todo: Exclude actor here.
                }

                results.emplace_back(std::move(cm));
            }
            if (successCallback) {
                successCallback(std::move(results));
            }
        } catch (const std::logic_error &error) {
            if (errorCallback) {
                errorCallback(std::make_exception_ptr(error));
            }
        }
    };

    taskArena.execute(fun);
}

void Engine::registerActor(ConfigMap identityConfig,
                           std::shared_ptr<ActorBrain> brain,
                           RegisterActorSuccessCallback successCallback,
                           ErrorCallback errorCallback) {
    auto fun = [this,
            identityConfig = std::move(identityConfig),
            brain = std::move(brain),
            successCallback = std::move(successCallback),
            errorCallback = std::move(errorCallback)]() mutable {
        // Invoke the extension callbacks.
        INVOKE_EXTENSION_INTERRUPTABLE(onPreRegisterActor, identityConfig);

        try {
            validateSubject(identityConfig);
        } catch (std::exception &ex) {
            if (errorCallback) {
                errorCallback(std::make_exception_ptr(ex));
            }
            return;
        }

        // Create the actor subject
        announceSubject(
                engineActor,
                identityConfig,
                SubjectAnnouncementFlags::Defaults,
                [this,
                        brain,
                        identityConfig = std::move(identityConfig),
                        errorCallback, successCallback]
                        (const auto &subjectHandle) {
                    std::unique_lock lk(registeredActorsMutex);

                    if (registeredActors.find(identityConfig) != registeredActors.end()) {
                        if (errorCallback) {
                            lk.unlock();
                            errorCallback(std::make_exception_ptr(InsertException("Actor is already registered")));
                        }
                        return;
                    }

                    auto actor = std::make_unique<Actor>(*this, brain, subjectHandle);

                    ActorHandle actorHandle(new ActorRef(*actor));

                    RegisteredActor registeredActor = {
                            .ref = actorHandle,
                            .actor = std::move(actor),
                    };

                    registeredActors.emplace(std::move(std::make_pair(identityConfig, std::move(registeredActor))));

                    lk.unlock();

                    if (successCallback) {
                        successCallback(actorHandle);
                    }

                    // Invoke the extension callbacks.
                    INVOKE_EXTENSION(onPostRegisterActor, actorHandle);
                }, [errorCallback](const auto &error) {
                    if (errorCallback) {
                        errorCallback(error);
                    }
                });
    };

    taskArena.execute(fun);
}

void Engine::dropActor(Actor &actor) {
    if (isInShutdownPhase) {
        return;
    }

    ActorHandle actorPtr(new ActorRef(actor));
    actorPtr->isValid = false;

    // Invoke the extension callbacks.
    for (auto &ext : extensions) {
        ext->onPreDropActor(*this, actorPtr);
    }

    // Find the actor.
    std::unique_lock lk(registeredActorsMutex);
    auto it = std::find_if(registeredActors.begin(), registeredActors.end(), [&](auto &a) {
        return a.second.actor.get() == &actor;
    });

    if (it == registeredActors.end()) {
        throw NotFoundException("No such actor");
    }

    // Keep the actor alive until the mutex has been unlocked.
    auto registeredActor = std::move(std::move(registeredActors.extract(it)).mapped());
    lk.unlock();
    assert(registeredActor.ref.expired());

    // Drop the owned subjects of the actor.

    for (auto &ext : extensions) {
        ext->onPostDropActor(*this, actorPtr);
    }

    registeredActor.actor.reset();
}

void Engine::requestData(const ActorHandle &actor,
                         ConfigMapAttributeKey subjectAttribute,
                         size_t offset, size_t len,
                         std::shared_ptr<DataTransactionHandler> handler) {
    if (!actor) {
        // Not authorized.
        LOG_S(WARNING) << "Unauthorized data request.";
        return;
    }

    auto fun = [this,
            actor = actor,
            subjectAttribute = std::move(subjectAttribute),
            offset, len,
            handler = std::move(handler)]() mutable {
        ConfigMapExtractors::get_normalized_subject_extractor()(subjectAttribute.configMap);

        try {
            validateSubject(subjectAttribute.configMap);
        } catch (std::exception &ex) {
            handler->handleError(std::make_exception_ptr(ex));
            return;
        }

        auto results = subjectStore->get(subjectAttribute.configMap, SubjectViewMode::ResolveRequired);

        if (results.empty()) {
            handler->handleError(std::make_exception_ptr(NotFoundException("No such subject.")));
            return;
        }

        auto &subjectView = results[0];

        // Fetch all attributes to make sure we also transport the selected variants.
        //subjectAttribute.configMap.clear();
        //subjectView.fetchAllAttributes(subjectAttribute.configMap);
        // TODO: Generate the minimal unique config map containing only uuid, realm and required variants.

        dataStoreManager.requestData(subjectAttribute, offset, len, handler);
    };

    taskArena.execute(fun);
}

void Engine::putData(const ActorHandle &actor,
                     ConfigMapAttributeKey subjectAttribute,
                     size_t offset, data_buffer_type data,
                     Engine::PutDataSuccessCallback successCallback,
                     Engine::ErrorCallback errorCallback) {
    if (!actor) {
        if (errorCallback) {
            errorCallback(std::make_exception_ptr(AuthException("Not authorized")));
        }
        return;
    }

    auto fun = [this,
            actor = actor,
            subjectAttribute = std::move(subjectAttribute),
            offset,
            data = std::move(data),
            successCallback = std::move(successCallback),
            errorCallback = std::move(errorCallback)]() mutable {
        ConfigMap &subjectId = subjectAttribute.configMap;

        try {
            validateSubject(subjectId);
        } catch (std::exception &ex) {
            if (errorCallback) {
                errorCallback(std::make_exception_ptr(ex));
            }
            return;
        }

        auto results = subjectStore->get(subjectId, SubjectViewMode::ResolveRequired);

        if (results.empty()) {
            if (errorCallback) {
                errorCallback(
                        std::make_exception_ptr(
                                NotFoundException("Subject does not exist.")));
            }
            return;
        }

        // Make sure we're requesting using the unique id.
        auto &subjectView = results[0];

        subjectAttribute.configMap = subjectView.fetchUniqueId();

        dataStoreManager.putData(subjectAttribute, offset, data);
        if (successCallback) {
            successCallback();
        }
    };

    taskArena.execute(fun);
}

void Engine::forEachSubscriber(const std::function<void(Subscription &)> &callback) {
    if (isInShutdownPhase) {
        // Ignore subscriptions while in shutdown phase.
        return;
    }

    bool eraseLoop = false;
    {
        std::shared_lock lk(subscriptionsMutex);

        for (const auto &subscription : subscriptions) {
            if (subscription.expired()) {
                eraseLoop = true;
            } else {
                auto ptr = subscription.lock();
                callback(*ptr);
            }
        }
    }

    if (eraseLoop) {
        // Erase invalid subscriptions.
        std::unique_lock lk(subscriptionsMutex);

        for (auto it = subscriptions.begin(); it != subscriptions.end();) {
            if (it->expired()) {
                it = subscriptions.erase(it);
            } else {
                ++it;
            }
        }
    }
}

boost::asio::io_context &Engine::getIOContext() {
    return ioContext;
}

ActorHandle Engine::getSystemActor() const {
    return engineActor;
}

std::shared_ptr<DataStore> Engine::createNewDataStore(const ConfigMap &subject,
                                                      const ConfigMap::key_type &attribute,
                                                      const ActorHandle &actor) {
    auto dsInfo = std::get<DataSourceInfo>(subject.find(attribute)->second);

    if (dsInfo.accessType == DataSourceInfo::AccessType::Streamed) {
        return std::make_shared<StreamingDataStore>(subject, attribute, actor->getBrain());
    }

    return std::make_shared<PassthroughDataStore>(subject, attribute, actor->getBrain());
}

void Engine::validateSubject(const ConfigMap &cm) const {
    // Invoke the extension callbacks.
    for (auto &extension : extensions) {
        extension->onValidateSubject(*this, cm);
    }
}

}
