//
// Created by void on 28/04/2020.
//

#include <loom/engine/InMemorySubjectStore.h>
#include <loom/SubjectViewMode.h>
#include <loom/lql/Visitor.h>
#include <loom/lql/SimpleMatcher.h>
#include <loom/ConfigMapExtractor.h>

#include <stack>
#include <regex>
#include <utility>
#include <loom/engine/Exception.h>

namespace loom::engine {

namespace detail {
struct ConfigMapVisitor : lql::Visitor {
    explicit ConfigMapVisitor(const ConfigMap &cm) : cm(cm) {}

    const ConfigMap &cm;

    struct VariantMatch {
        ConfigMap::const_iterator it;
        ConfigMapValue value;
    };

    struct Context {
        bool matchResult = false;
        std::vector<VariantMatch> variantMatches;
    };

    std::stack<Context> contextStack;

    void exit(const lql::ast::AnyExpression &) override {
        auto &ctx = contextStack.top();
        ctx.matchResult = true;
    }

    void exit(const lql::ast::AttributeFilter &expression) override {
        if (!lql::SimpleMatcher::matches(cm, expression)) {
            return;
        }

        auto &ctx = contextStack.top();
        ctx.matchResult = true;
        auto matches = lql::SimpleMatcher::getAttributeMatches(cm, expression.attribute);

        for (auto &matchIt : matches) {
            std::visit([&](auto &&v) {
                using T = std::decay_t<decltype(v)>;
                if constexpr(std::is_same_v<T, Variant>) {
                    VariantMatch match = {
                            .it = matchIt,
                            .value = expression.value
                    };

                    ctx.variantMatches.emplace_back(std::move(match));
                }
            }, matchIt->second);
        }
    }

    void exit(const lql::ast::BoolExpression &expression) override {
        auto &ctx = contextStack.top();

        if (expression.operation == lql::ast::BoolOperator::Not) {
            ctx.matchResult = !ctx.matchResult;
        }

        if (!ctx.matchResult) {
            ctx.variantMatches.clear();
        }
    }

    void enter(const lql::ast::Term &term) override {
        contextStack.emplace(Context{});
    }

    void exit(const lql::ast::Term &term) override {
        auto &ctx = contextStack.top();
        auto variantMatches = std::move(ctx.variantMatches);
        bool isMatch = ctx.matchResult;

        contextStack.pop();

        if (!contextStack.empty()) {
            auto &parentCtx = contextStack.top();
            parentCtx.matchResult = isMatch;

            if (isMatch) {
                for (auto &match : variantMatches) {
                    parentCtx.variantMatches.emplace_back(std::move(match));
                }
            }
        }
    }

    void enter(const lql::ast::SelectExpression &expression) override {
        contextStack.emplace(Context{});
    }
};

struct SharedConfigMapProvider : ConfigMapProvider {
    explicit SharedConfigMapProvider(std::shared_ptr<ConfigMap> cm) : cm(std::move(cm)) {}

    void fetchAllAttributes(ConfigMap &configMap) const override {
        for (auto &item : *cm) {
            configMap.emplace(item);
        }
    }

    void fetchAttributes(ConfigMap &configMap, std::initializer_list<ConfigMap::key_type> &&attributes) const override {
        for (auto &attrib : attributes) {
            auto range = cm->equal_range(attrib);
            while (range.first != range.second) {
                configMap.emplace(*range.first++);
            }
        }
    }

    std::shared_ptr<ConfigMap> cm;
};

}

SubjectView InMemorySubjectStore::put(ConfigMap subjectConfig, SubjectAnnouncementFlags flags, ActorWeakHandle owner) {
    ConfigMap subjectId;
    ConfigMapExtractors::get_subject_id_extractor()(subjectConfig, subjectId);

    std::unique_lock lk(subjectConfigsMutex);

    auto range = storedSubjects.equal_range(subjectId);

    bool isExtending = check_flag(flags, SubjectAnnouncementFlags::ExtendAll)
                       || check_flag(flags, SubjectAnnouncementFlags::ExtendMissing)
                       || check_flag(flags, SubjectAnnouncementFlags::ExtendOverride);

    StoredSubject storedSubject{
            .owner = std::move(owner),
            .flags = flags,
            .subjectConfig = std::make_shared<ConfigMap>(std::move(subjectConfig))
    };

    if (range.first == range.second || isExtending) {
        // Store a new subject.
        // This will happen when either
        //  - the subject is not in store yet
        //  - the subject is about to be extended.

        auto cmPtr = storedSubjects.insert(std::make_pair(subjectId, std::move(storedSubject)))->second.subjectConfig;

        return SubjectView(std::make_shared<detail::SharedConfigMapProvider>(cmPtr));
    }

    throw StorageException("Subject is already present in store, but should not be extended");
}

std::vector<SubjectView> InMemorySubjectStore::findEval(const lql::ast::SelectExpression &expression,
                                                        SubjectViewMode viewMode) const {
    std::shared_lock lk(subjectConfigsMutex);

    std::map<ConfigMap, ConfigMap> extendedMaps;

    for (auto &item : storedSubjects) {
        auto &cm = *item.second.subjectConfig;
        auto &subjectId = item.first;
        auto &flags = item.second.flags;

        detail::ConfigMapVisitor visitor(cm);
        visitor.visit(expression);

        auto &ctx = visitor.contextStack.top();

        if (!ctx.matchResult) {
            // Not a match.
            continue;
        }

        auto &target = extendedMaps[item.first];

        if (viewMode == SubjectViewMode::ResolveFully || viewMode == SubjectViewMode::ResolveRequired) {
            // Resolve the variants.
            for (auto it = cm.begin(); it != cm.end(); ++it) {
                auto &attrib = it->first;

                if (check_flag(flags, SubjectAnnouncementFlags::ExtendMissing) && target.find(attrib) != target.end()) {
                    // Already present, but we only want to extend missing parameters, so we ignore it here.
                    continue;
                }

                ConfigMapValue valueToInsert;

                auto matchIt = std::find_if(
                        ctx.variantMatches.begin(),
                        ctx.variantMatches.end(),
                        [&](const auto &match) {
                            return match.it == it;
                        });

                if (matchIt != ctx.variantMatches.end()) {
                    // Found a replacement, use instead of source value.
                    valueToInsert = std::move(matchIt->value);
                } else if (viewMode == SubjectViewMode::ResolveFully) {
                    valueToInsert = std::visit([&](auto &&v) -> ConfigMapValue {
                        using T = std::decay_t<decltype(v)>;
                        if constexpr(std::is_same_v<T, Variant>) {
                            // Use the default value of the variant.
                            auto &defaultValue = v.values.front();
                            if (defaultValue != "*") {
                                return defaultValue;
                            }
                            // We cannot use 'any' as default value.
                        }

                        return v;
                    }, it->second);
                } else {
                    // Not found the variant while requested partially, use raw option.
                    valueToInsert = it->second;
                }

                // Insert or overwrite the value based on the flag.
                bool shouldErase = check_flag(flags, SubjectAnnouncementFlags::ExtendOverride);

                bool shouldInsert = check_flag(flags, SubjectAnnouncementFlags::ExtendAll) ||
                                    check_flag(flags, SubjectAnnouncementFlags::ExtendMissing) ||
                                    shouldErase;

                if (shouldErase) {
                    target.erase(attrib);
                }

                if (shouldInsert) {
                    target.emplace(std::make_pair(attrib, std::move(valueToInsert)));
                }
            }
        } else if (viewMode == SubjectViewMode::RawOptions || ctx.variantMatches.empty()) {
            // Raw options have been requested or no variants matched, just extend the result.
            if (check_flag(flags, SubjectAnnouncementFlags::ExtendAll)) {
                // Add the complete map.
                target += *item.second.subjectConfig;
            } else if (check_flag(flags, SubjectAnnouncementFlags::ExtendMissing)) {
                for (auto &attrib : cm) {
                    if (target.find(attrib.first) == target.end()) {
                        continue;
                    }

                    target.insert(attrib);
                }
            } else if (check_flag(flags, SubjectAnnouncementFlags::ExtendOverride)) {
                for (auto &attrib : cm) {
                    auto it = target.find(attrib.first);

                    if (it == target.end()) {
                        target.insert(attrib);
                    } else {
                        it->second = attrib.second;
                    }
                }
            }
        }
    }

    std::vector<SubjectView> results;

    auto &extractor = ConfigMapExtractors::get_normalized_subject_extractor();

    for (auto &cm : extendedMaps) {
        extractor(cm.second);
        results.emplace_back(SubjectView(std::make_shared<detail::SharedConfigMapProvider>(
                std::make_shared<ConfigMap>(std::move(cm.second)))));
    }

    return results;
}

size_t InMemorySubjectStore::removeEval(const lql::ast::SelectExpression &expression) {
    size_t removed = 0;

    std::unique_lock lk(subjectConfigsMutex);
    for (auto it = storedSubjects.begin(); it != storedSubjects.end();) {
        auto &cm = *it->second.subjectConfig;
        if (lql::SimpleMatcher::matches(cm, expression)) {
            it = storedSubjects.erase(it);
            removed++;
        } else {
            ++it;
        }
    }

    return removed;
}

}
