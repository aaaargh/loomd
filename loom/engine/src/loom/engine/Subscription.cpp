//
// Created by void on 14/04/2020.
//

#include <loom/engine/Subscription.h>

#include <utility>

namespace loom::engine {

Subscription::Subscription(ActorWeakHandle actor,
                           lql::ast::SelectExpression filterExpression,
                           SubjectSubscriptionFlags flags)
        : filterExpression(std::move(filterExpression)),
          flags(flags), actor(std::move(actor)) {}

const lql::ast::SelectExpression &Subscription::getFilterExpression() const {
    return filterExpression;
}

const SubjectSubscriptionFlags &Subscription::getFlags() const {
    return flags;
}

const ActorWeakHandle &Subscription::getActor() const {
    return actor;
}

}
