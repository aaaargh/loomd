//
// Created by void on 27/04/2020.
//

#include <loom/engine/DataStore.h>
#include <loom/ConfigMap.h>
#include <loom/engine/Actor.h>
#include <loom/Exception.h>

#include <utility>
#include <boost/format.hpp>

#include <loguru.hpp>

namespace loom::engine {

DataStore::DataStore(ConfigMap subjectId, ConfigMap::key_type attribute, ActorBrain &provider)
        : subjectId(std::move(subjectId)), attribute(std::move(attribute)), dataProvider(provider) {}

ActorBrain &DataStore::getProvider() {
    return dataProvider;
}

const ConfigMap &DataStore::getSubjectId() const {
    return subjectId;
}

const ConfigMap::key_type &DataStore::getAttribute() const {
    return attribute;
}

void DataStoreManager::put(const ConfigMapAttributeKey &key,
                           ActorHandle owner,
                           const std::shared_ptr<DataStore> &store) {
    ManagedStore managedStore = {
            .owner = std::move(owner),
            .store = store
    };

    std::unique_lock lk(managedStoresMutex);
    auto it = managedStores.find(key);
    if (it != managedStores.end()) {
        managedStores.erase(it);
    }

    managedStores.emplace(std::make_pair(key, std::move(managedStore)));
}

void DataStoreManager::requestData(const ConfigMapAttributeKey &key,
                                   size_t offset, size_t len,
                                   const std::shared_ptr<DataTransactionHandler> &handler) const {
    std::shared_lock lk(managedStoresMutex);
    auto it = managedStores.find(key);

    if (it == managedStores.end()) {
        lk.unlock();
        handler->handleError(std::make_exception_ptr(NotFoundException(
                (boost::format("No store found for the given key (%1%:%2%)") % key.configMap % key.attribute).str()
        )));
        return;
    }

    auto store = it->second.store;

    store->requestData(offset, len, handler);
}

void DataStoreManager::putData(const ConfigMapAttributeKey &key,
                               size_t offset,
                               data_buffer_type data) {
    std::shared_lock lk(managedStoresMutex);
    auto it = managedStores.find(key);

    if (it == managedStores.end()) {
        lk.unlock();
        throw NotFoundException("No such store");
        return;
    }

    auto store = it->second.store;

    store->putData(offset, std::move(data));
}

void DataStoreManager::drop(const ConfigMapAttributeKey &key) {
    std::unique_lock lk(managedStoresMutex);
    auto it = managedStores.find(key);

    if (it == managedStores.end()) {
        lk.unlock();
        throw NotFoundException("No such store");
        return;
    }

    managedStores.erase(it);
}

void DataStoreManager::dropAll(const ConfigMap &subjectId) {
    for (auto &store : managedStores) {
        if (store.first.configMap != subjectId) {
            continue;
        }

        managedStores.erase(store.first);
        return;
    }
}

}