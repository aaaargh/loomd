//
// Created by void on 26/04/2020.
//

#include <loom/engine/Actor.h>
#include <loom/engine/Engine.h>

#include <utility>

#include <loguru.hpp>
#include <loom/engine/ActorHandle.h>


namespace loom::engine {

Actor::Actor(Engine &engine, std::shared_ptr<ActorBrain> brain, SubjectHandle identitySubject)
        : engine(engine),
          brain(std::move(brain)),
          identitySubject(std::move(identitySubject)) {}

Actor::~Actor() {

}

ActorRef::ActorRef(Actor &actor) : actor(actor) {}

ActorBrain &ActorRef::getBrain() {
    return *actor.brain;
}

ActorMemory &ActorRef::getMemory() {
    return actor.memory;
}

SubjectHandle ActorRef::getIdentitySubject() const {
    return actor.identitySubject;
}

ActorRef::~ActorRef() {
    if (isValid) {
        isValid = false;
        actor.engine.dropActor(actor);
    }
}

}
