//
// Created by void on 20/05/2020.
//

#include <loom/engine/PassthroughDataStore.h>

#include <loguru.hpp>
#include <cstring>

namespace loom::engine {

void PassthroughDataStore::requestData(size_t offset, size_t len,
                                       const std::shared_ptr<DataTransactionHandler> &handler) {
    std::unique_lock lk(waitingHandlersMutex);
    WaitingHandler waitingHandler = {
            .offset = offset,
            .len = len,
            .handler = handler,
    };

    waitingHandlers.emplace_back(std::move(waitingHandler));
    lk.unlock();

    getProvider().handleDataRequest(getSubjectId(), getAttribute(), offset, len);
}

void PassthroughDataStore::putData(size_t offset, data_buffer_type data) {
    std::vector<decltype(waitingHandlers)::iterator> handlersToErase;

    bool isEOF = data == dataEOF;

    if (isEOF) {
        DLOG_S(INFO) << "Put EOF in store @offset " << offset;
    } else {
        DLOG_S(INFO) << "Put " << data.size() << " byte(s) in store, offset := " << offset;
    }

    std::shared_lock slk(waitingHandlersMutex);

    for (auto it = waitingHandlers.begin(); it != waitingHandlers.end(); ++it) {
        auto &handler = *it;

        if (handler.offset == offset) {
            auto bytes = std::min(handler.len, data.size());

            data_buffer_type buf{bytes};
            std::memcpy(buf.data(), data.data(), bytes);

            DLOG_S(INFO) << "Waiting handler received " << bytes
                         << " byte(s) of data, remaining := " << handler.len - bytes;

            handler.handler->handleDataReceived(std::move(buf), offset);

            handler.offset += bytes;
            handler.len -= bytes;

            if (handler.len == 0) {
                handlersToErase.push_back(it);

                DLOG_S(INFO) << "Waiting handler transaction is complete.";
                handler.handler->handleTransactionComplete(DataTransactionHandler::CompleteReason::RequestComplete);
            } else {
                if (isEOF) {
                    // The handler is still waiting, but we reached EOF, so we close the transaction here.
                    handlersToErase.push_back(it);

                    DLOG_S(INFO) << "Waiting handler transaction is complete (reached EOF)";
                    handler.handler->handleTransactionComplete(DataTransactionHandler::CompleteReason::ReachedEOF);
                    continue;
                } else {
                    // Transaction is not finished yet.
                    handler.handler->handleTransactionContinue();
                }
            }
        }
    }
    slk.unlock();

    std::unique_lock lk(waitingHandlersMutex);

    for (auto &handler : handlersToErase) {
        waitingHandlers.erase(handler);
    }
}

}
