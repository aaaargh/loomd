//
// Created by void on 15/05/2020.
//

#ifndef PYLOOM_SYNCABLE_H
#define PYLOOM_SYNCABLE_H

#include <boost/asio/io_context.hpp>
#include <future>

#include <boost/python/object.hpp>

namespace pyloom {

namespace bpy = boost::python;

class SyncedInvoker {
public:
    SyncedInvoker() = delete;

public:
    template<typename ... Args>
    static void pyInvoke(boost::asio::io_context &ioContext, boost::python::object callback, Args &&...args) {
        if (callback.is_none()) {
            return;
        }

        ioContext.dispatch(
                [callback = std::move(callback), args = std::make_tuple(std::forward<Args>(args) ...)]()mutable {
                    return std::apply([&callback](auto &&... args) {
                        callback(std::forward<Args>(args)...);
                    }, std::move(args));
                });
    }

    template<typename...Args>
    static std::function<void(Args...)> pyCallback(boost::asio::io_context &ioContext, boost::python::object callback) {
        return [&ioContext, callback = std::move(callback)](Args &&...  args) mutable {
            pyInvoke(ioContext, callback, std::forward<Args>(args)...);
        };
    }
};

}

#endif //PYLOOM_SYNCABLE_H
