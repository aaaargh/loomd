//
// Created by void on 13/05/2020.
//

#ifndef PYLOOM_CLIENTTCP_H
#define PYLOOM_CLIENTTCP_H

#include <loom/client/ClientTCP.h>

#include <boost/python/object.hpp>

#include <pyloom/ClientEventListener.h>

namespace pyloom {

namespace bpy = boost::python;

class ClientTCP : public loom::client::ClientTCP {
public:
    using loom::client::ClientTCP::ClientTCP;

public:
    static std::shared_ptr<ClientTCP> ctor(const std::string &daemonAddress, unsigned short port);

    virtual ~ClientTCP();

    void start();

    size_t poll();

    void stop();

public:
    void pyAuthenticate(const loom::ConfigMap &identity,
                        bpy::object successCallback,
                        bpy::object errorCallback);

    void pySubscribeToSubjects(const std::string &filter,
                               std::underlying_type_t<loom::SubjectSubscriptionFlags> flags,
                               bpy::object successCallback,
                               bpy::object errorCallback);

    void pyAnnounceSubject(const loom::ConfigMap &subject,
                           std::underlying_type_t<loom::SubjectAnnouncementFlags> flags,
                           bpy::object successCallback,
                           bpy::object errorCallback);

    void pyDropSubject(const loom::ConfigMap &subjectId,
                       bpy::object successCallback,
                       bpy::object errorCallback);

    void pyQuerySubjects(const std::string &query,
                         loom::SubjectViewMode viewMode,
                         std::underlying_type_t<loom::SubjectQueryFlags> flags,
                         bpy::object successCallback,
                         bpy::object errorCallback);

    void pyRequestData(const loom::ConfigMap &subjectId,
                       const loom::ConfigMap::key_type &attribute,
                       size_t offset,
                       size_t len,
                       std::shared_ptr<loom::DataSourceWriter> writer,
                       bpy::object successCallback,
                       bpy::object errorCallback);

    [[nodiscard]] std::shared_ptr<ClientEventListener> pyGetEventListener() const;

    void pySetEventListener(const std::shared_ptr<ClientEventListener> &listener);

    void setEventListener(std::unique_ptr<loom::ClientEventListener> listener) override;

    [[nodiscard]] bool hasEventListener() const override;

    [[nodiscard]] loom::ClientEventListener &getEventListener() const override;

protected:
    void handleDisconnect(const std::exception_ptr &error) override;

private:
    std::unique_ptr<boost::asio::io_context> ioContext;
    std::shared_ptr<ClientEventListener> pyEventListener;
};

}

#endif //PYLOOM_CLIENTTCP_H
