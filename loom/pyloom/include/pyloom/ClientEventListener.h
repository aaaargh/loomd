//
// Created by void on 20/05/2020.
//

#ifndef PYLOOM_CLIENTEVENTLISTENER_H
#define PYLOOM_CLIENTEVENTLISTENER_H

#include <loom/client/Client.h>

#include <boost/python/wrapper.hpp>

namespace pyloom {

namespace bpy = boost::python;

struct ClientEventListener : loom::ClientEventListener, bpy::wrapper<pyloom::ClientEventListener> {
public:
    void onSubjectAnnounced(loom::Client &client, const loom::ConfigMap &subject, bool isUpdate) override;

    void onSubjectDropped(loom::Client &client, const loom::ConfigMap &subject) override;

    void onAuthenticationFailed(loom::Client &client, const std::string &reason) override;

    void onAuthenticated(loom::Client &client, const loom::ConfigMap &identity) override;

    void onConnected(loom::Client &client) override;

    void onConnectionLost(loom::Client &client, const std::exception_ptr &error) override;

    void pyOnConnectionLost(loom::Client &client, const std::string& error);
};

}

#endif //PYLOOM_CLIENTEVENTLISTENER_H
