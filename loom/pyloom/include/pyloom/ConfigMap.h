//
// Created by void on 14/05/2020.
//

#ifndef PYLOOM_CONFIGMAP_H
#define PYLOOM_CONFIGMAP_H

#include <boost/python/to_python_converter.hpp>
#include <boost/python/object.hpp>
#include <boost/python/dict.hpp>

#include <loom/ConfigMap.h>

namespace pyloom {

namespace bpy = boost::python;

extern loom::Variant *Variant_ctor(const bpy::list &values);

extern loom::Variant *Variant_raw_ctor(bpy::tuple args, bpy::dict kwargs);

struct ConfigMapValueConverter {

    static void pyRegister();

    static PyObject *convert(const loom::ConfigMapValue &v);

    static void *convertible(PyObject *obj_ptr);

    static void construct(PyObject *obj_ptr, bpy::converter::rvalue_from_python_stage1_data *data);
};

struct ConfigMapConverter {

    static void pyRegister();

    static PyObject *convert(const loom::ConfigMap &cm);

    static void *convertible(PyObject *obj_ptr);

    static void construct(PyObject *obj_ptr, bpy::converter::rvalue_from_python_stage1_data *data);
};

}


#endif //PYLOOM_CONFIGMAP_H
