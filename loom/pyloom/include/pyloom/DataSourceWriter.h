//
// Created by void on 15/05/2020.
//

#ifndef PYLOOM_DATASOURCEWRITER_H
#define PYLOOM_DATASOURCEWRITER_H

#include <boost/python/wrapper.hpp>

#include <loom/client/Client.h>

namespace pyloom {

namespace bpy = boost::python;

struct DataSourceWriter : loom::DataSourceWriter, bpy::wrapper<loom::DataSourceWriter> {
    using wrapper::wrapper;

    void beginTransaction() override;

    void write(const loom::ImmutableBuffer &buffer, size_t offset) override;

    void setEOF(size_t offset) override;

    void endTransaction() override;
};

}

#endif //PYLOOM_DATASOURCEWRITER_H
