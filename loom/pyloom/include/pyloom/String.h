//
// Created by void on 14/05/2020.
//

#ifndef PYLOOM_STRING_H
#define PYLOOM_STRING_H

#include <boost/python/to_python_converter.hpp>
#include <boost/python/object.hpp>

#include <string>
#include <vector>

namespace pyloom {

namespace bpy = boost::python;

struct StringListConverter {
    static void pyRegister();

    static PyObject *convert(const std::vector<std::string> &s);

    static void *convertible(PyObject *obj_ptr);

    static void construct(PyObject *obj_ptr, bpy::converter::rvalue_from_python_stage1_data *data);
};

}

#endif //PYLOOM_STRING_H
