//
// Created by void on 14/05/2020.
//

#ifndef PYLOOM_DATASOURCEREADER_H
#define PYLOOM_DATASOURCEREADER_H

#include <boost/python/wrapper.hpp>

#include <loom/ConfigMap.h>

namespace pyloom {

namespace bpy = boost::python;

struct DataSourceReader : loom::DataSourceReader, bpy::wrapper<loom::DataSourceReader> {
    ReadDataResult read(loom::MutableBuffer &target, size_t offset) override;
};

}

#endif //PYLOOM_DATASOURCEREADER_H
