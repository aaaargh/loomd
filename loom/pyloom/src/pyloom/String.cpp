//
// Created by void on 14/05/2020.
//

#include <pyloom/String.h>
#include <boost/python/list.hpp>
#include <boost/python/extract.hpp>

#include <pyconfig.h>

#include <loguru.hpp>

namespace pyloom {

namespace bpy = boost::python;

void StringListConverter::pyRegister() {
    bpy::to_python_converter<
            std::vector<std::string>,
            StringListConverter>();

    bpy::converter::registry::push_back(
            &convertible,
            &construct,
            bpy::type_id<std::vector<std::string> >()
    );
}

PyObject *StringListConverter::convert(const std::vector<std::string> &vec) {
    bpy::list result;

    for (auto &val : vec) {
        result.append(val);
    }

    return bpy::incref(result.ptr());
}

void *StringListConverter::convertible(PyObject *obj_ptr) {
    if (!bpy::extract<bpy::list>(obj_ptr).check()) {
        return nullptr;
    }

    bpy::list list(bpy::borrowed(obj_ptr));

    auto len = bpy::len(list);
    for (int i = 0; i < len; ++i) {
        if (!bpy::extract<std::string>(list[i]).check()) {
            return nullptr;
        }
    }
    return obj_ptr;
}

void StringListConverter::construct(PyObject *obj_ptr, bpy::converter::rvalue_from_python_stage1_data *data) {
    // Allocate the memory.
    using storage_type = bpy::converter::rvalue_from_python_storage<std::vector<std::string> >;
    void *storage = reinterpret_cast<storage_type *>(data)->storage.bytes;
    std::vector<std::string> *vec;
    data->convertible = vec = new(storage)std::vector<std::string>();

    // Initialize the object.
    bpy::handle<> handle(bpy::borrowed(obj_ptr));
    bpy::list list(handle);

    auto len = bpy::len(list);
    for (int i = 0; i < len; ++i) {
        vec->emplace_back(bpy::extract<std::string>(list[i])());
    }
}

}