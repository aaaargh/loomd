//
// Created by void on 14/05/2020.
//

#include <boost/python.hpp>
#include <boost/python/module.hpp>
#include <boost/python/str.hpp>
#include <boost/python/suite/indexing/vector_indexing_suite.hpp>

#include <pyloom/Client.h>
#include <pyloom/ConfigMap.h>
#include <pyloom/DataSourceReader.h>
#include <pyloom/DataSourceWriter.h>
#include <pyloom/ClientEventListener.h>
#include <pyloom/String.h>

namespace pyloom {

BOOST_PYTHON_MODULE (pyloom) {
    using namespace boost::python;

    pyloom::ConfigMapValueConverter::pyRegister();

    pyloom::ConfigMapConverter::pyRegister();

    pyloom::StringListConverter::pyRegister();

    class_<loom::Variant>("Variant")
            .def("__init__",
                 make_constructor(&Variant_ctor, default_call_policies(),
                                  arg("options")))
            .def(self_ns::str(self_ns::self));

    enum_<loom::SubjectAnnouncementFlags>("SubjectAnnouncementFlags")
            .value("none", loom::SubjectAnnouncementFlags::None)
            .value("extend_existing_subject", loom::SubjectAnnouncementFlags::ExtendAll)
            .value("defaults", loom::SubjectAnnouncementFlags::Defaults)
            .export_values();

    enum_<loom::SubjectQueryFlags>("SubjectQueryFlags")
            .value("none", loom::SubjectQueryFlags::None)
            .value("ignore_own_subjects", loom::SubjectQueryFlags::IgnoreOwnSubjects)
            .value("defaults", loom::SubjectQueryFlags::Defaults)
            .export_values();

    enum_<loom::SubjectSubscriptionFlags>("SubjectSubscriptionFlags")
            .value("none", loom::SubjectSubscriptionFlags::None)
            .value("emit_existing_subjects", loom::SubjectSubscriptionFlags::EmitExistingSubjects)
            .value("ignore_own_subjects", loom::SubjectSubscriptionFlags::IgnoreOwnSubjects)
            .value("defaults", loom::SubjectSubscriptionFlags::Defaults)
            .export_values();

    enum_<loom::SubjectViewMode>("SubjectViewMode")
            .value("resolve_fully", loom::SubjectViewMode::ResolveFully)
            .value("resolve_required", loom::SubjectViewMode::ResolveRequired)
            .value("raw_options", loom::SubjectViewMode::RawOptions)
            .export_values();

    enum_<loom::DataSourceInfo::AccessType>("AccessType")
            .value("streamed", loom::DataSourceInfo::AccessType::Streamed)
            .value("random", loom::DataSourceInfo::AccessType::Random)
            .export_values();

    enum_<loom::DataSourceInfo::SizeHint>("SizeHint")
            .value("at_least", loom::DataSourceInfo::SizeHint::AtLeast)
            .value("at_most", loom::DataSourceInfo::SizeHint::AtMost)
            .value("estimated", loom::DataSourceInfo::SizeHint::Estimated)
            .value("exact", loom::DataSourceInfo::SizeHint::Exact)
            .export_values();

    class_<loom::DataSourceInfo>("DataSourceInfo")
            .def_readwrite("length", &loom::DataSourceInfo::length)
            .def_readwrite("size_hint", &loom::DataSourceInfo::sizeHint)
            .def_readwrite("access_type", &loom::DataSourceInfo::accessType)
            .def_readwrite("reader", &loom::DataSourceInfo::reader);

    class_<pyloom::DataSourceReader, std::shared_ptr<pyloom::DataSourceReader>, boost::noncopyable>(
            "DataSourceReader")
            .def("read", pure_virtual(&loom::DataSourceReader::read));

    class_<pyloom::DataSourceWriter, std::shared_ptr<pyloom::DataSourceWriter>, boost::noncopyable>("DataSourceWriter")
            .def("begin_transaction", pure_virtual(&loom::DataSourceWriter::beginTransaction))
            .def("write", pure_virtual(&loom::DataSourceWriter::write))
            .def("set_eof", pure_virtual(&loom::DataSourceWriter::setEOF))
            .def("end_transaction", pure_virtual(&loom::DataSourceWriter::endTransaction));

    class_<pyloom::ClientEventListener,
            std::shared_ptr<pyloom::ClientEventListener>,
            boost::noncopyable>("ClientEventListener")
            .def("on_subject_announced", pure_virtual(&pyloom::ClientEventListener::onSubjectAnnounced))
            .def("on_subject_dropped", pure_virtual(&pyloom::ClientEventListener::onSubjectDropped))
            .def("on_authentication_failed", pure_virtual(&pyloom::ClientEventListener::pyOnConnectionLost))
            .def("on_authenticated", pure_virtual(&pyloom::ClientEventListener::onAuthenticated))
            .def("on_connected", pure_virtual(&pyloom::ClientEventListener::onConnected))
            .def("on_connection_lost", pure_virtual(&pyloom::ClientEventListener::onConnectionLost));

    class_<pyloom::ClientTCP, std::shared_ptr<pyloom::ClientTCP>, boost::noncopyable>("ClientTCP", no_init)
            .def("__init__", make_constructor(&pyloom::ClientTCP::ctor, default_call_policies(),
                                              (arg("daemon_address"), arg("port") = 6788)))
            .def("start", &pyloom::ClientTCP::start)
            .def("stop", &pyloom::ClientTCP::stop)
            .def("poll", &pyloom::ClientTCP::poll)

            .def("authenticate", &pyloom::ClientTCP::pyAuthenticate,
                 (arg("identity"),
                         arg("on_success") = object(),
                         arg("on_error") = object()))

            .def("announce_subject", &pyloom::ClientTCP::pyAnnounceSubject,
                 (arg("subject_config"),
                         arg("flags") = loom::SubjectAnnouncementFlags::Defaults,
                         arg("on_success") = object(),
                         arg("on_error") = object()))

            .def("drop_subject", &pyloom::ClientTCP::pyDropSubject,
                 (arg("subject_id"),
                         arg("on_success") = object(),
                         arg("on_error") = object()))

            .def("query_subjects", &pyloom::ClientTCP::pyQuerySubjects,
                 (arg("query"),
                         arg("view_mode") = loom::SubjectViewMode::ResolveFully,
                         arg("flags") = loom::SubjectQueryFlags::Defaults,
                         arg("on_results") = object(),
                         arg("on_error") = object()))

            .def("subscribe_to_subjects", &pyloom::ClientTCP::pySubscribeToSubjects,
                 (arg("filter"),
                         arg("flags") = loom::SubjectSubscriptionFlags::Defaults,
                         arg("on_success") = object(),
                         arg("on_error") = object()))

            .def("request_data", &pyloom::ClientTCP::pyRequestData,
                 (arg("subject_id"),
                         arg("attribute"),
                         arg("offset"),
                         arg("len"),
                         arg("writer"),
                         arg("on_success") = object(),
                         arg("on_error") = object()))

            .add_property("event_listener",
                          &pyloom::ClientTCP::pyGetEventListener,
                          &pyloom::ClientTCP::pySetEventListener);

}

}
