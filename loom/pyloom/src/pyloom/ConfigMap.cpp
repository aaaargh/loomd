//
// Created by void on 14/05/2020.
//

#include <pyloom/ConfigMap.h>

#include <boost/python/dict.hpp>
#include <boost/python/tuple.hpp>
#include <boost/python/extract.hpp>

#include <boost/mpl/for_each.hpp>
#include <boost/mpl/vector.hpp>

#include <loom/Exception.h>

namespace pyloom {

namespace detail {
template<class Variant>
struct mpl_types_impl;

template<class... Ts>
struct mpl_types_impl<std::variant<Ts...>> {
    using type = boost::mpl::vector<Ts...>;
};
}

using config_map_value_types = detail::mpl_types_impl<loom::ConfigMapValue>::type;

void ConfigMapConverter::pyRegister() {
    bpy::to_python_converter<
            loom::ConfigMap,
            ConfigMapConverter>();

    bpy::converter::registry::push_back(
            &convertible,
            &construct,
            bpy::type_id<loom::ConfigMap>()
    );
}

PyObject *ConfigMapConverter::convert(const loom::ConfigMap &cm) {
    bpy::dict dict;

    for (auto &item : cm) {
        if (dict.has_key(item.first)) {
            // Already in there, turn item into list if required and append.
            auto i = dict[item.first];
            auto x_list = bpy::extract<bpy::list>(i);

            if (!x_list.check()) {
                bpy::list l;
                l.append(i);
                dict[item.first] = l;
            }

            auto list = x_list();
            list.append(item.second);
        } else {
            dict[item.first] = item.second;
        }
    }

    return bpy::incref(dict.ptr());
}

void *ConfigMapConverter::convertible(PyObject *obj_ptr) {
    auto x = bpy::extract<bpy::dict>(obj_ptr);

    if (!x.check()) {
        return nullptr;
    }

    auto dict = x();
    auto items = bpy::list(dict.items());
    auto len = bpy::len(items);
    for (size_t i = 0; i < len; ++i) {
        auto x_list = bpy::extract<bpy::list>(items[i][1]);

        if (x_list.check()) {
            // This is a list of values.
            auto list = x_list();
            auto items_len = bpy::len(list);

            for (size_t j = 0; j < items_len; ++j) {
                auto x_val = bpy::extract<loom::ConfigMapValue>(list[j]);
                if (!x_val.check()) {
                    return nullptr;
                }
            }
            continue;
        }

        auto x_val = bpy::extract<loom::ConfigMapValue>(items[i][1]);

        if (x_val.check()) {
            continue;
        }

        return nullptr;
    }

    return obj_ptr;
}

void ConfigMapConverter::construct(PyObject *obj_ptr, bpy::converter::rvalue_from_python_stage1_data *data) {
    // Allocate the memory.
    using storage_type = bpy::converter::rvalue_from_python_storage<loom::ConfigMap>;
    void *storage = reinterpret_cast<storage_type *>(data)->storage.bytes;
    loom::ConfigMap *cm;
    data->convertible = cm = new(storage)loom::ConfigMap();

    // Initialize the object.
    bpy::handle<> handle(bpy::borrowed(obj_ptr));
    bpy::dict o(handle);

    auto items = bpy::list(o.items());
    auto len = bpy::len(items);

    for (size_t i = 0; i < len; ++i) {
        auto key = bpy::extract<std::string>(items[i][0])();

        auto x_list = bpy::extract<bpy::list>(items[i][1]);

        if (x_list.check()) {
            // This is a list of values.
            auto list = x_list();
            auto items_len = bpy::len(list);

            for (size_t j = 0; j < items_len; ++j) {
                auto value = bpy::extract<loom::ConfigMapValue>(list[j])();
                cm->insert(std::make_pair(key, value));
            }
        } else {
            auto value = bpy::extract<loom::ConfigMapValue>(items[i][1])();
            cm->insert(std::make_pair(key, value));
        }
    }
}

void ConfigMapValueConverter::pyRegister() {
    bpy::to_python_converter<
            loom::ConfigMapValue,
            ConfigMapValueConverter>();

    bpy::converter::registry::push_back(
            &convertible,
            &construct,
            bpy::type_id<loom::ConfigMapValue>()
    );
}

PyObject *ConfigMapValueConverter::convert(const loom::ConfigMapValue &value) {
    return std::visit([](auto &&v) -> PyObject * {
        return bpy::incref(bpy::object(v).ptr());
    }, value);
}

void *ConfigMapValueConverter::convertible(PyObject *obj_ptr) {
    PyObject * result = nullptr;
    boost::mpl::for_each<config_map_value_types>([&](auto t) {
        if (result) {
            return;
        }

        using Type = decltype(t);

        if (bpy::extract<Type>(obj_ptr).check()) {
            result = obj_ptr;
        }
    });

    return obj_ptr;
}

void ConfigMapValueConverter::construct(PyObject *obj_ptr, bpy::converter::rvalue_from_python_stage1_data *data) {
    // Allocate the memory.
    using storage_type = bpy::converter::rvalue_from_python_storage<loom::ConfigMapValue>;
    void *storage = reinterpret_cast<storage_type *>(data)->storage.bytes;
    loom::ConfigMapValue *v;
    data->convertible = v = new(storage)loom::ConfigMapValue();

    // Initialize the object.
    bpy::handle<> handle(bpy::borrowed(obj_ptr));
    bpy::object o(handle);

    bool found = false;
    boost::mpl::for_each<config_map_value_types>([&](auto t) {
        if (found) {
            return;
        }

        using Type = decltype(t);

        auto x = bpy::extract<Type>(o);
        if (x.check()) {
            *v = x();
            found = true;
            return;
        }
    });

    if (!found) {
        throw loom::NotSupportedException("Cannot use this type as config map value");
    }
}

loom::Variant *Variant_ctor(const bpy::list &values) {
    auto variant = new loom::Variant();

    auto size = len(values);
    for (int i = 0; i < size; ++i) {
        auto x = bpy::extract<std::string>(values[i]);

        if (x.check()) {
            variant->values.emplace_back(x());
            continue;
        }

        delete variant;
        throw loom::NotSupportedException("Variants accept only strings");
    }
    return variant;

    return nullptr;
}

}
