//
// Created by void on 13/05/2020.
//

#include <pyloom/Client.h>
#include <pyloom/SyncedInvoker.h>

#include <boost/asio/signal_set.hpp>

#include <pyloom/ConfigMap.h>
#include <boost/python/dict.hpp>

#include <loguru.hpp>
#include <utility>
#include <string>

namespace pyloom {

std::shared_ptr<ClientTCP> ClientTCP::ctor(const std::string &daemonAddress, unsigned short port) {
    auto ioContext = std::make_unique<boost::asio::io_context>();
    auto client = std::make_shared<ClientTCP>(*ioContext, daemonAddress, port, true);
    client->ioContext = std::move(ioContext);
    client->connect();

    return client;
}


ClientTCP::~ClientTCP() {
    ioContext->stop();
    LOG_S(INFO) << "Client instance destroyed.";
}

void ClientTCP::start() {
    boost::asio::signal_set signals(*ioContext, SIGINT, SIGTERM);
    signals.async_wait([&](const auto &ec, int sig) {
        LOG_S(INFO) << "Received signal " << sig << ", gracefully shutting down client.";
        ioContext->stop();
    });

    LOG_S(INFO) << "Entering client event loop.";

    ioContext->run();

    LOG_S(INFO) << "Client event loop shutdown complete.";
}

size_t ClientTCP::poll() {
    return ioContext->poll();
}

void ClientTCP::stop() {
    if (!ioContext->stopped()) {
        LOG_S(INFO) << "Shutting down client event loop.";
        ioContext->stop();
    }
}

void ClientTCP::handleDisconnect(const std::exception_ptr &error) {
    if (error) {
        try {
            std::rethrow_exception(error);
        } catch (const std::exception &err) {
            LOG_S(ERROR) << "Connection closed (" << err.what() << ")";
        }
    } else {
        LOG_S(INFO) << "Connection closed.";
    }

    ioContext->stop();
}

void ClientTCP::pyAuthenticate(const loom::ConfigMap &identity,
                               bpy::object successCallback,
                               bpy::object errorCallback) {
    authenticate(identity,
                 SyncedInvoker::pyCallback(*ioContext, std::move(successCallback)),
                 SyncedInvoker::pyCallback<std::exception_ptr>(*ioContext, std::move(errorCallback))
    );
}

void ClientTCP::pyAnnounceSubject(const loom::ConfigMap &subject,
                                  std::underlying_type_t<loom::SubjectAnnouncementFlags> flags,
                                  bpy::object successCallback,
                                  bpy::object errorCallback) {
    announceSubject(subject,
                    static_cast<loom::SubjectAnnouncementFlags>(flags),
                    SyncedInvoker::pyCallback(*ioContext, std::move(successCallback)),
                    SyncedInvoker::pyCallback<std::exception_ptr>(*ioContext, std::move(errorCallback))
    );
}

void ClientTCP::pyQuerySubjects(const std::string &query,
                                loom::SubjectViewMode viewMode,
                                std::underlying_type_t<loom::SubjectQueryFlags> flags,
                                bpy::object successCallback,
                                bpy::object errorCallback) {
    querySubjects(query,
                  viewMode,
                  static_cast<loom::SubjectQueryFlags>(flags),
                  [this, successCallback = std::move(successCallback)](const std::vector<loom::ConfigMap> &results) {
                      bpy::list pyResults;

                      for (auto &result : results) {
                          pyResults.append(bpy::dict(result));
                      }

                      SyncedInvoker::pyInvoke(*ioContext, successCallback, pyResults);
                  },
                  SyncedInvoker::pyCallback<std::exception_ptr>(*ioContext, std::move(errorCallback))
    );
}

void ClientTCP::pyDropSubject(const loom::ConfigMap &subjectId, bpy::object successCallback,
                              bpy::object errorCallback) {
    dropSubject(subjectId,
                SyncedInvoker::pyCallback(*ioContext, std::move(successCallback)),
                SyncedInvoker::pyCallback<std::exception_ptr>(*ioContext, std::move(errorCallback))
    );
}

void ClientTCP::pyRequestData(const loom::ConfigMap &subjectId,
                              const std::basic_string<char> &attribute,
                              size_t offset, size_t len,
                              std::shared_ptr<loom::DataSourceWriter> writer,
                              bpy::object successCallback,
                              bpy::object errorCallback) {
    requestData(subjectId,
                attribute,
                offset, len,
                std::move(writer),
                SyncedInvoker::pyCallback(*ioContext, std::move(successCallback)),
                SyncedInvoker::pyCallback<std::exception_ptr>(*ioContext, std::move(errorCallback))
    );
}

void ClientTCP::pySubscribeToSubjects(const std::string &filter,
                                      std::underlying_type_t<loom::SubjectQueryFlags> flags,
                                      bpy::object successCallback,
                                      bpy::object errorCallback) {
    subscribeToSubjects(filter,
                        static_cast<loom::SubjectSubscriptionFlags>(flags),
                        SyncedInvoker::pyCallback(*ioContext, std::move(successCallback)),
                        SyncedInvoker::pyCallback<std::exception_ptr>(*ioContext, std::move(errorCallback)));
}

std::shared_ptr<ClientEventListener> ClientTCP::pyGetEventListener() const {
    return pyEventListener;
}

void ClientTCP::pySetEventListener(const std::shared_ptr<ClientEventListener> &listener) {
    pyEventListener = listener;
}

void ClientTCP::setEventListener(std::unique_ptr<loom::ClientEventListener> listener) {
    throw loom::NotSupportedException(
            "setEventListener ist not support for Python clients. use pySetEventListener() instead.");
}

bool ClientTCP::hasEventListener() const {
    return pyEventListener.operator bool();
}

loom::ClientEventListener &ClientTCP::getEventListener() const {
    if (!pyEventListener) {
        throw loom::NotFoundException("No event listener registered to this client.");
    }

    return *pyEventListener;
}

}
