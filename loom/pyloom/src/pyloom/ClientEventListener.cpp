//
// Created by void on 20/05/2020.
//

#include <pyloom/ClientEventListener.h>
#include <pyloom/ConfigMap.h>

#include <loguru.hpp>

namespace pyloom {

void ClientEventListener::onSubjectAnnounced(loom::Client &client, const loom::ConfigMap &subject, bool isUpdate) {
    auto override = get_override("on_subject_announced");

    if (!override.is_none()) {
        override(subject, isUpdate);
    }
}

void ClientEventListener::onSubjectDropped(loom::Client &client, const loom::ConfigMap &subject) {
    auto override = get_override("on_subject_dropped");

    if (!override.is_none()) {
        override(subject);
    }
}

void ClientEventListener::onAuthenticationFailed(loom::Client &client, const std::string &reason) {
    auto override = get_override("on_authentication_failed");

    if (!override.is_none()) {
        override(reason);
    }
}

void ClientEventListener::onAuthenticated(loom::Client &client, const loom::ConfigMap &identity) {
    auto override = get_override("on_authenticated");

    if (!override.is_none()) {
        override(identity);
    }
}

void ClientEventListener::onConnected(loom::Client &client) {
    auto override = get_override("on_connected");

    if (!override.is_none()) {
        override();
    }
}

void ClientEventListener::pyOnConnectionLost(loom::Client &client, const std::string &error) {
    auto override = get_override("on_connection_lost");

    if (!override.is_none()) {
        override(error);
    }
}

void ClientEventListener::onConnectionLost(loom::Client &client, const std::exception_ptr &error) {
    std::string errStr;

    if (error) {
        try {
            std::rethrow_exception(error);
        } catch (const std::exception &err) {
            errStr = err.what();
        }
    }

    pyOnConnectionLost(client, errStr);
}

}
