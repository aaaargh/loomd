//
// Created by void on 14/05/2020.
//

#include <pyloom/DataSourceReader.h>
#include <loom/Exception.h>

#include <loguru.hpp>

namespace pyloom {

DataSourceReader::ReadDataResult DataSourceReader::read(loom::MutableBuffer &target, size_t offset) {
    auto override = get_override("read");

    if (override.is_none()) {
        throw loom::NotImplementedException("Missing read() implementation for DataSourceReader.");
    }

    bpy::object memoryView(bpy::handle<>(
            PyMemoryView_FromMemory(static_cast<char *>(target.data()), target.size(), PyBUF_WRITE)));

    auto read = override(memoryView, target.size(), offset).as<size_t>();

    // This is not perfect, but in case we read less data than expected we consider this ot be EOF.
    return {
            .bytesRead = read,
            .isEOF = read < target.size()
    };
}

}
