//
// Created by void on 15/05/2020.
//

#include <pyloom/DataSourceWriter.h>
#include <boost/python/str.hpp>

#include <pyconfig.h>

namespace pyloom {

void DataSourceWriter::beginTransaction() {
    auto override = get_override("begin_transaction");

    if (!override.is_none()) {
        override();
    }
}

void DataSourceWriter::write(const loom::ImmutableBuffer &buffer, size_t offset) {
    bpy::object memoryView(bpy::handle<>(
            PyMemoryView_FromMemory(static_cast<char *>(const_cast<void *>(buffer.data())), buffer.size(),
                                    PyBUF_READ)));

    auto override = get_override("write");

    if (!override.is_none()) {
        override(memoryView, offset);
    }
}

void DataSourceWriter::endTransaction() {
    auto override = get_override("end_transaction");

    if (!override.is_none()) {
        override();
    }
}

void DataSourceWriter::setEOF(size_t offset) {
    auto override = get_override("set_eof");

    if (!override.is_none()) {
        override(offset);
    }
}

}