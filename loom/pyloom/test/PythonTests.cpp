//
// Created by void on 15/05/2020.
//

#include <loom/testing.h>
#include <Python.h>

#include <pyloom/testing/PythonFixture.h>

#include <thread>

using namespace loom::testing;

namespace pyloom::testing {

BOOST_AUTO_TEST_SUITE(PythonTests)

BOOST_FIXTURE_TEST_CASE(can_run_interpreter, PythonFixture) {
    BOOST_REQUIRE_EQUAL(0, PyRun_SimpleString("print('Hello from python!')"));
}

BOOST_FIXTURE_TEST_CASE(can_load_pyloom_via_api, PythonFixture) {
    BOOST_REQUIRE(PyImport_ImportModule("pyloom"));
}

BOOST_FIXTURE_TEST_CASE(can_load_pyloom_via_script, PythonFixture) {
    BOOST_REQUIRE_EQUAL(0, PyRun_SimpleString("import pyloom"));
}

BOOST_AUTO_TEST_SUITE_END()

}