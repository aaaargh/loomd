//
// Created by void on 25/05/2020.
//

#include <loom/testing.h>
#include <Python.h>

#include <pyloom/testing/PythonFixture.h>

#include <thread>

using namespace loom::testing;

namespace pyloom::testing {

BOOST_AUTO_TEST_SUITE(PythonConversionTests)

BOOST_FIXTURE_TEST_CASE(can_create_data_source_info, PythonFixture) {
    BOOST_REQUIRE_EQUAL(0, PyRun_SimpleString(
            "import pyloom\n"
            "dsi = pyloom.DataSourceInfo()"
    ));
}

BOOST_FIXTURE_TEST_CASE(can_access_data_source_info, PythonFixture) {
    BOOST_REQUIRE_EQUAL(0, PyRun_SimpleString(
            "import pyloom\n"
            "dsi = pyloom.DataSourceInfo()\n"
            "dsi.isStream = True\n"
            "dsi.length = 1234\n"
    ));
}

BOOST_AUTO_TEST_SUITE_END()

}

