//
// Created by void on 25/05/2020.
//

#include <loom/testing.h>
#include <Python.h>

#include <pyloom/testing/PythonFixture.h>

#include <loom/engine/Engine.h>
#include <loom/daemon/DaemonTCP.h>
#include <thread>

using namespace loom::testing;

namespace pyloom::testing {

BOOST_AUTO_TEST_SUITE(PythonIntegrationTests)

BOOST_FIXTURE_TEST_CASE(can_announce_subject, PythonDaemonFixture) {
    BOOST_REQUIRE_EQUAL(0, runScript(
            "from pyloom import *\n"
            "client = ClientTCP(\"localhost\", 6788)\n"

            // Authenticate the client
            "client.authenticate({'uuid': 'some dude'})\n"

            // Announce a subject with variants
            "config = { "
            "   'uuid': 'Chair',"
            "   'realm': 'default',"
            "   'size': Variant(['*']),"
            "   'color': Variant(['green', 'red', 'blue', 'purple', 'lime', 'orange', 'yellow', 'white']),"
            "   'style': Variant(['wood', 'metal', 'plastic']),"
            "}\n"

            "client.announce_subject(config)\n"

            // Announce another subject that shares some of the variants with the first subject.
            "config = { "
            "   'uuid': 'Table',"
            "   'realm': 'default',"
            "   'height': Variant(['130', '100']),"
            "   'color': Variant(['brown', 'white']),"
            "   'style': Variant(['wood']),"
            "}\n"

            "client.announce_subject(config, flags=SubjectAnnouncementFlags.defaults)\n"

            // Query the subject by using a variant
            "def handle_results(results: list):\n"
            "   print('Results: ', results)\n"
            "   client.stop()\n"

            "client.query_subjects('(&(style=wood))', on_results=handle_results)\n"

            // Enter the main event loop.
            "client.start()\n"
    ));
}

BOOST_AUTO_TEST_SUITE_END()

}