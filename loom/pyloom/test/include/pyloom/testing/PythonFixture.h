//
// Created by void on 27/05/2020.
//

#ifndef LOOM_PYTHONFIXTURE_H
#define LOOM_PYTHONFIXTURE_H

#include <loom/daemon/DaemonTCP.h>
#include <loom/engine/Engine.h>

#include <thread>

namespace pyloom::testing {

struct PythonFixture {
    PythonFixture();

    ~PythonFixture();

    virtual int runScript(const std::string &script);
};

struct PythonDaemonFixture : PythonFixture {
    using PythonFixture::PythonFixture;

    ~PythonDaemonFixture();

    template<typename EngineT = loom::engine::Engine, typename...Args>
    EngineT &createEngine(Args &&... args) {
        engine = std::make_unique<EngineT>(ioContext, std::forward<Args>(args)...);
        return static_cast<EngineT &>(*engine);
    }

    template<typename DaemonT = loom::daemon::DaemonTCP, typename...Args>
    DaemonT &startDaemon(Args &&... args) {
        if (!engine) {
            createEngine<>();
        }
        daemon = std::make_unique<DaemonT>(*engine, daemonAddress, daemonPort, std::forward<Args>(args)...);

        daemonThread = std::thread([this] {
            ioContext.run();
        });

        return static_cast<DaemonT &>(*daemon);
    }

    template<typename DaemonT = loom::daemon::DaemonTCP>
    DaemonT &getDaemon() {
        return static_cast<DaemonT &>(*daemon);
    }

    int runScript(const std::string &script) override;

    void shutdownDaemon();

    boost::asio::io_context ioContext;
    std::unique_ptr<loom::daemon::Daemon> daemon;
    std::unique_ptr<loom::engine::Engine> engine;
    const boost::asio::ip::address daemonAddress = boost::asio::ip::make_address("0.0.0.0");
    const unsigned short daemonPort = 6788;

private:
    std::thread daemonThread;
};

}


#endif //LOOM_PYTHONFIXTURE_H
