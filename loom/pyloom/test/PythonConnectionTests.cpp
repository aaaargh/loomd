//
// Created by void on 25/05/2020.
//

#include <loom/testing.h>
#include <Python.h>

#include <pyloom/testing/PythonFixture.h>

#include <loom/engine/Engine.h>
#include <loom/daemon/DaemonTCP.h>
#include <thread>

using namespace loom::testing;

namespace pyloom::testing {

BOOST_AUTO_TEST_SUITE(PythonConnectionTests)

struct MyDaemon : loom::daemon::DaemonTCP {
    struct TestConditions {
        bool peerWasConnected = false;
        bool peerWasDisconnected = false;
    };

    TestConditions &conditions;

    MyDaemon(loom::engine::Engine &engine, const boost::asio::ip::address &address, unsigned short port,
             TestConditions &conditions) : DaemonTCP(engine, address, port), conditions(conditions) {}

    void handlePeerConnected(const std::shared_ptr<loom::daemon::Peer> &peer) override {
        conditions.peerWasConnected = true;
        DaemonTCP::handlePeerConnected(peer);
    }

    void handlePeerDisconnected(loom::daemon::Peer &peer) override {
        conditions.peerWasDisconnected = true;
        Daemon::handlePeerDisconnected(peer);
    }
};

BOOST_FIXTURE_TEST_CASE(client_can_connect_and_disconnect, PythonDaemonFixture) {
    MyDaemon::TestConditions conditions;
    startDaemon<MyDaemon>(conditions);

    BOOST_REQUIRE_EQUAL(0, runScript(
            "import pyloom\n"
            "client = pyloom.ClientTCP(\"localhost\", 6788)\n"
            // This disconnects the client.
            "client = None\n"
    ));

    ioContext.poll();

    shutdownDaemon();

    auto &daemon = getDaemon<MyDaemon>();

    BOOST_REQUIRE(conditions.peerWasConnected);
    BOOST_REQUIRE(conditions.peerWasDisconnected);
}

BOOST_FIXTURE_TEST_CASE(client_can_authenticate, PythonDaemonFixture) {
    BOOST_REQUIRE_EQUAL(0, runScript(
            "import pyloom\n"
            "client = pyloom.ClientTCP(\"localhost\", 6788)\n"
            "client.authenticate({'uuid': 'client'})\n"
            "client.poll()\n"
    ));
}

BOOST_AUTO_TEST_SUITE_END()

}