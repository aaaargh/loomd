//
// Created by void on 27/05/2020.
//

#include <pyloom/testing/PythonFixture.h>
#include <pyloom/testing/PyloomTesting.h>

#include <boost/test/unit_test.hpp>
#include <Python.h>

namespace pyloom::testing {
PythonFixture::PythonFixture() {
    BOOST_TEST_MESSAGE("Setting up python environment...");

    //Py_SetPythonHome(L"" PYLOOM_TEST_RUN_PATH);
    Py_Initialize();
    PySys_SetPath(L"" PYLOOM_TEST_RUN_PATH);
}

PythonFixture::~PythonFixture() {
    BOOST_TEST_MESSAGE("Tearing down python environment...");


}

int PythonFixture::runScript(const std::string &script) {
    return PyRun_SimpleString(script.c_str());
}

PythonDaemonFixture::~PythonDaemonFixture() {
    shutdownDaemon();
}

int PythonDaemonFixture::runScript(const std::string &script) {
    if (!daemon) {
        startDaemon<>();
    }

    return PythonFixture::runScript(script);
}

void PythonDaemonFixture::shutdownDaemon() {
    BOOST_TEST_MESSAGE("Shutting down engine and daemon...");

    if (!ioContext.stopped()) {
        ioContext.stop();
    }

    if (daemonThread.joinable()) {
        daemonThread.join();
    }

    daemon.reset();
    engine.reset();
}


}