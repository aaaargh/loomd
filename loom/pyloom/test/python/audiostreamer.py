#!/usr/bin/env python3

import math
import queue
import simpleaudio as sa
import threading

import pyloom

daemon_host = "localhost"
daemon_port = 6788

file_to_send = "8bit_tunnel.wav"

audio_sample_rate = 44100
audio_channels = 2
audio_bytes_per_sample = 2


# Define a couple of generic handlers.
def handle_client_error(e):
    print("Error executing request:", e)


def handle_client_authenticated():
    print("Client successfully authenticated.")


def handle_subject_announced():
    print("Client announced a subject.")


# A reader implementation that gets data from a file.
class MyReader(pyloom.DataSourceReader):

    def __init__(self, file_path: str):
        super(MyReader, self).__init__()
        self.file_path = file_path
        # self.file_size = os.path.getsize(self.file_path)
        self.file_size = 0  # Pretend we don't know the file size.
        self.file = open(self.file_path, "rb")

    def read(self, buffer: bytes, size: int, offset: int):
        bytes_read = self.file.read(size)
        # print("Read", len(bytes_read), "byte(s) @offset", offset, "from file", self.file_path)
        buffer[:len(bytes_read)] = bytes_read
        return len(bytes_read)


# This writer implementation really just logs stuff to stdout.
class WavPlayer(pyloom.DataSourceWriter):
    buffered: bytearray
    thread_handle: threading.Thread
    playback_q: queue.Queue
    done: bool = False

    def playback_worker(self):
        while (not self.done) or (self.playback_q.qsize() > 0):
            play_buf = self.playback_q.get()
            play_obj = sa.play_buffer(play_buf, audio_channels, audio_bytes_per_sample, audio_sample_rate)
            play_obj.wait_done()

    def begin_transaction(self):
        self.buffered = bytearray()
        print("Transaction started.")

        self.playback_q = queue.Queue()

        self.thread_handle = threading.Thread(target=self.playback_worker)
        self.thread_handle.start()

    def write(self, data: bytearray, offset: int):
        play_buf = self.buffered[:]
        play_buf += data

        nearestMultiple = math.floor(len(play_buf) / (audio_channels * audio_bytes_per_sample)) * (
                audio_channels * audio_bytes_per_sample)
        self.buffered = play_buf[nearestMultiple:]
        play_buf = play_buf[0:nearestMultiple]

        self.playback_q.put(play_buf)

    def set_eof(self, offset: int):
        self.done = True
        if self.thread_handle:
            self.thread_handle.join()
        print("Stream closed")


client = pyloom.ClientTCP(daemon_host, daemon_port)

identity = {
    "name": "pyLoom audio player",
    "uuid": "pyloom-audio",
}

client.authenticate(identity, handle_client_authenticated, handle_client_error)

dsInfo = pyloom.DataSourceInfo()
dsInfo.isStream = True
dsInfo.reader = MyReader(file_to_send)

subject = {
    "uuid": file_to_send,
    "realm": "default",
    "mimeType": "audio/wav",
    "data": dsInfo
}

client.announce_subject(subject, handle_subject_announced, handle_client_error)

client.request_data(
    {"uuid": file_to_send, "realm": "default"},
    "data",
    0, 0,
    WavPlayer(),
    None,
    handle_client_error
)

client.start()
