#!/usr/bin/env python3

"""
 This is a short demonstration of how the client works.
 What we do here is connecting to a daemon running at localhost:6788 using an authenticated client.
 After that we announce a new subject (an image containing the python logo) and then we query all existing
 subjects found in the engine. We also request the data sources of any subject that has been found,
 which results in downloading our own provided python logo and storing it somewhere in /tmp.
"""

import pathlib
import random
import string

import pyloom

daemon_host = "localhost"
daemon_port = 6788

file_to_send = "./python.png"

write_directory = "/tmp/pyloomclient-received"


# Define a couple of generic handlers.
def handle_client_error(e):
    print("Error executing request:", e)


def handle_client_authenticated():
    print("Client successfully authenticated.")


def handle_subject_announced():
    print("Client announced a subject.")


def handle_subscription_success():
    print("Successfully subscribed to subject events")


# Prints out a single subject result and requests data if possible.
def handle_query_result(result: dict):
    print("uuid=", result.get("uuid"), ",realm=", result.get("realm"))

    data_key = "data"
    data_attr: pyloom.DataSourceInfo = result.get(data_key)
    if data_attr:
        # Request the data just for fun. We also request way more data than available. This is not necessary but for demonstration purposes we do it anyway.
        request_len = max(data_attr.length * 10, 1024 * 1024 * 1024)
        print("Found a data attribute (size :=", data_attr.length, "), will request at a chunk of", request_len)
        client.request_data(
            {"uuid": result.get("uuid"), "realm": result.get("realm")},
            data_key,
            0, request_len,
            MyWriter(result.get("uuid"), result.get("realm")),
            None,  # We don't care about the results.
            handle_client_error
        )


# Invoked when the client has received information about existing subjects.
# This logs the subject info to stdout.
def handle_query_results(results: list):
    count = len(results)

    print("Received", count, "results.")
    if count == 0:
        return

    for i in range(count):
        handle_query_result(results[i])


# A reader implementation that gets data from a file.
class MyReader(pyloom.DataSourceReader):
    def __init__(self, file_path: str):
        super(MyReader, self).__init__()
        self.file_path = file_path
        # self.file_size = os.path.getsize(self.file_path)
        self.file_size = 0  # Pretend we don't know the file size.
        self.file = open(self.file_path, "rb")

    def read(self, buffer: bytes, size: int, offset: int):
        self.file.seek(offset, 0)
        read = self.file.read(size)
        print("Read", len(read), "byte(s) @offset", offset, "from file", self.file_path)
        buffer[:len(read)] = read
        return len(read)


# This writer implementation really just logs stuff to stdout.
class MyWriter(pyloom.DataSourceWriter):
    file = None

    def __init__(self, uuid: str, realm: str):
        super(MyWriter, self).__init__()
        self.uuid = uuid
        self.realm = realm
        pathlib.Path(write_directory + "/" + self.realm + "/").mkdir(parents=True, exist_ok=True)
        self.file_path = write_directory + "/" + self.realm + "/" + self.uuid
        print("New writer instance created for subject", uuid, "@realm", realm, ", file path:", self.file_path)

    def begin_transaction(self):
        self.file = open(self.file_path, "wb")
        print("Transaction started.")

    def write(self, data: bytes, offset: int):
        print("Writing", len(data), "byte(s) @offset", offset)
        self.file.seek(offset)
        self.file.write(data)

    def end_transaction(self):
        self.file.close()
        print("Transaction complete.")

    def set_eof(self, offset: int):
        print("EOF set @", offset)


# Define an event listener that just prints out stuff.
class MyEventListener(pyloom.ClientEventListener):
    def on_subject_announced(self, new_subject: dict, is_updated: bool):
        print("New subject announced:", new_subject, " update :=", is_updated)

    def on_subject_dropped(self, subject_id: dict):
        print("Subject has been dropped:", subject_id)


# Connect to the daemon running at localhost.
client = pyloom.ClientTCP(daemon_host, daemon_port)

# Register an instance of the event listener.
client.event_listener = MyEventListener()

# This is our client identity. There is no specific schema about what is required to identify, but other clients
# and the engine can use this to communicate with us.
identity = {
    "name": "pyLoom test client",
    "uuid": "pyloom",
}

client.authenticate(identity, handle_client_authenticated, handle_client_error)

# Listen for subject changes remotely
client.subscribe_to_subjects("(*)", handle_subscription_success, handle_client_error)

# Create an info structure for the file we cant to expose to the engine.
dsInfo = pyloom.DataSourceInfo()
dsInfo.isStream = False  # The file is not streamed, meaning it provides random access.
dsInfo.reader = MyReader(file_to_send)  # An in stance of the reade we implemented previously.
dsInfo.length = dsInfo.reader.file_size  # The size of the data source, in this case the file size.

# Announce the python logo as an image subject.
# Subject information is usually provided and received as a dict that contains strings, numbers or DataSourceInfo's.
# The only mandatory fields for a subject are 'uuid' and 'realm' (see comments below)
subject = {
    # A UUID is always required. It must be unique in the given realm.
    "uuid": "Python logo - " + ''.join(random.choices(string.ascii_uppercase + string.digits, k=4)),
    # A realm where the client has write access to. In this example we use the default public realm.
    "realm": "default",
    # The mime time is not a mandatoy field but helps other clients identity the type of the subject.
    "mimeType": "image/png",
    # Here we register
    "data": dsInfo
}

# Announce the subject.
client.announce_subject(subject, handle_subject_announced, handle_client_error)

# Query subjects in the engine using a filter query. Most of this is identical to LDAP search query syntax.
client.query_subjects("(*)", handle_query_results, handle_client_error)

# Enter the client's blocking main loop.
client.start()
