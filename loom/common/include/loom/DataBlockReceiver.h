//
// Created by void on 20/05/2020.
//

#ifndef LOOM_DATABLOCKRECEIVER_H
#define LOOM_DATABLOCKRECEIVER_H

#include <loom/ConfigMap.h>
#include <list>
#include <shared_mutex>

namespace loom {

class DataBlockReceiver {
public:
    using BlockCompleteCallback = std::function<void()>;

    void openBlock(const ConfigMapAttributeKey &key, size_t offset, size_t len,
                   std::shared_ptr<DataSourceWriter> writer);

    void closeAllBlocks(const ConfigMap &configMap);

    [[nodiscard]] bool isClean() const;

    size_t pushReceivedData(const ConfigMapAttributeKey &key, size_t offset, ImmutableBuffer buffer);

private:
    struct OpenBlock {
        using Range = std::pair<size_t, size_t>;
        size_t offset;
        size_t end;
        std::list<Range> writtenRanges;
        std::shared_ptr<DataSourceWriter> writer;

        void mergeRanges();
    };

    std::shared_mutex openBlocksMutex;
    std::multimap<ConfigMapAttributeKey, OpenBlock> openBlocks;
};

}

#endif //LOOM_DATABLOCKRECEIVER_H
