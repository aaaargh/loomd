//
// Created by void on 01/06/2020.
//

#ifndef LOOM_SUBJECTQUERYFLAGS_H
#define LOOM_SUBJECTQUERYFLAGS_H

#include <loom/BitFlags.h>

namespace loom {

enum class SubjectQueryFlags {
    None = 1u << 0u,
    IgnoreOwnSubjects = 1u << 1u,
    Defaults = IgnoreOwnSubjects,
};

}

#endif //LOOM_SUBJECTQUERYFLAGS_H
