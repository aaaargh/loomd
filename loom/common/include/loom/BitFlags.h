//
// Created by void on 01/06/2020.
//

#ifndef LOOM_BITFLAGS_H
#define LOOM_BITFLAGS_H

#include <type_traits>

namespace loom {

/**
 * A helper method that supports the bitwise | operator for an enum class.
 * @tparam EnumT The enum class type.
 * @param lhs The left hand operand.
 * @param rhs The right hand operand.
 * @return The result of the operation.
 */
template<typename EnumT>
EnumT operator|(EnumT lhs, EnumT rhs) {
    static_assert(std::is_enum<EnumT>::value,
                  "template parameter is not an enum type");

    using underlying = typename std::underlying_type<EnumT>::type;

    return static_cast<EnumT> (
            static_cast<underlying>(lhs) |
            static_cast<underlying>(rhs)
    );
}

/**
 * A helper method that supports the bitwise & operator for an enum class.
 * @tparam EnumT The enum class type.
 * @param lhs The left hand operand.
 * @param rhs The right hand operand.
 * @return The result of the operation.
 */
template<typename EnumT>
EnumT operator&(EnumT lhs, EnumT rhs) {
    static_assert(std::is_enum<EnumT>::value,
                  "template parameter is not an enum type");

    using underlying = typename std::underlying_type<EnumT>::type;

    return static_cast<EnumT> (
            static_cast<underlying>(lhs) &
            static_cast<underlying>(rhs)
    );
}

/**
 * A helper method that supports the bitwise ~ operator for an enum class.
 * @tparam EnumT The enum class type.
 * @param lhs The left hand operand.
 * @param rhs The right hand operand.
 * @return The result of the operation.
 */
template<typename EnumT>
EnumT operator~(EnumT rhs) {
    static_assert(std::is_enum<EnumT>::value,
                  "template parameter is not an enum type");

    using underlying = typename std::underlying_type<EnumT>::type;

    return static_cast<EnumT> (
            ~static_cast<underlying>(rhs)
    );
}

/**
 * Checks for the presence of a flag in a bitset.
 * @tparam EnumT The enum class type.
 * @param flags The bitset containing the flags.
 * @param subject The enum value in question.
 * @return Whether the subject is present in the flags.
 */
template<typename EnumT>
bool check_flag(EnumT flags, EnumT subject) {
    return (flags & subject) == subject;
}

}

#endif //LOOM_BITFLAGS_H
