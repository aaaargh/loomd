//
// Created by void on 29/05/2020.
//

#ifndef LOOM_VARIANT_H
#define LOOM_VARIANT_H

#include <vector>
#include <ostream>

namespace loom {

struct Variant {
    using value_type = std::string;

    std::vector<value_type> values;

    template<typename ...Args>
    explicit Variant(Args &&... args) : Variant({args...}) {}

    Variant(std::initializer_list<value_type> vals);

    explicit Variant(std::vector<value_type> vals);

    bool operator<(const Variant &other) const;

    bool operator==(const Variant &other) const;
};

std::ostream &operator<<(std::ostream &os, const Variant &v);

}

#endif //LOOM_VARIANT_H
