//
// Created by void on 20/05/2020.
//

#ifndef LOOM_CHUNKREADER_H
#define LOOM_CHUNKREADER_H

#include <loom/DataSource.h>

#include <cstddef>
#include <atomic>

namespace loom {

class ChunkReader {
public:
    using chunk_buffer_type = std::vector<std::byte>;
    using ReadDataResult = DataSourceReader::ReadDataResult;

public:
    ChunkReader(std::shared_ptr<DataSourceReader> reader,
                size_t maxChunkSize,
                size_t offset, size_t len);

protected:
    virtual void handleChunkRead(ImmutableBuffer buffer, size_t offset, const ReadDataResult &result) = 0;

    virtual void handleComplete() = 0;

    virtual void handleInterrupted() = 0;

    virtual void handleError(const std::exception_ptr &error) = 0;

public:
    void start();

    void interrupt();

    [[nodiscard]] bool isDone() const;

private:
    void readNext(size_t offset, size_t len);

private:
    std::shared_ptr<DataSourceReader> reader;
    size_t maxChunkSize;
    size_t offset;
    size_t len;
    chunk_buffer_type data;
    std::atomic_bool interrupted = false;
    std::atomic_bool done = false;
};

}


#endif //LOOM_CHUNKREADER_H
