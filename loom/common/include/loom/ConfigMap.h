//
// Created by void on 05/04/2020.
//

#ifndef LOOM_CONFIGMAP_H
#define LOOM_CONFIGMAP_H

#include <loom/loom.h>
#include <loom/Exception.h>
#include <loom/DataSource.h>
#include <loom/Variant.h>

#include <variant>

#include <map>
#include <string>
#include <ostream>
#include <memory>
#include <chrono>

namespace loom {

/**
 * The value stored in a config map.
 */
using ConfigMapValue = std::variant<
        std::string,
        double,
        long,
        DataSourceInfo,
        Variant
>;

/**
 * The config map is the main data structure to exchange data in Loom.
 */
LOOMAPI struct ConfigMap : std::multimap<std::string, ConfigMapValue> {
    using map_type = std::multimap<std::string, ConfigMapValue>;
    using map_type::map_type;

    /**
     * Creates a normalized version of this map that only contains the given keys.
     * @param keys The keys to retain if present.
     * @return The normalized map.
     */
    [[nodiscard]] ConfigMap normalized(std::initializer_list<ConfigMap::key_type> keys) const;

    /**
     * Normalizes the map.
     * @param keys The keys to retain if present.
     * @return This map after normalization.
     */
    ConfigMap &normalize(std::initializer_list<ConfigMap::key_type> keys);

    /**
     * Checks whether this config map contains another map or is equal.
     * @param other The possibly contained map.
     * @return Whether the map is contained in this or equal.
     */
    bool operator%=(const ConfigMap &other) const;

    /**
     * Checks whether this config map does not contain another map or is equal.
     * @param other The possibly contained map.
     * @return Whether the map is not contained in this or equal.
     */
    bool operator^=(const ConfigMap &other) const;

    /**
     * Checks whether this config map is equal to another map.
     * @param other The containing map.
     * @return Whether this is equal to the other map.
     */
    bool operator==(const ConfigMap &other) const;

    /**
     * Checks whether this config map is unequal to another map.
     * @param other The containing map.
     * @return Whether this is unequal to the other map.
     */
    bool operator!=(const ConfigMap &other) const;

    /**
     * Removes all redundant items contained in both this and another map by value comparison.
     * @param other The map containing the items to remove.
     * @return This map after the removal.
     */
    ConfigMap &operator-=(const ConfigMap &other);

    /**
     * Creates a map that does not contain values of another map.
     * @param other The map containing the items to exclude.
     * @return A map containing the items of this map excluding the items of the other map.
     */
    ConfigMap operator-(const ConfigMap &other) const;

    /**
     * Adds all items of another map, excluding duplicates for a given key-value pair.
     * @param other The map to add.
     * @return This map after the insertion.
     */
    ConfigMap &operator+=(const ConfigMap &other);

    /**
     * Creates a map that contains this maps items and the non-duplicate key-value pairs of another map.
     * @param other The map containing the items to add.
     * @return A map containing the items of this map including the non-duplicate items of the other map.
     */
    ConfigMap operator+(const ConfigMap &other) const;

    [[nodiscard]] iterator findValue(const key_type &key, const mapped_type &value);

    [[nodiscard]] const_iterator findValue(const key_type &key, const mapped_type &value) const;

    [[nodiscard]] iterator findValue(const value_type &value);

    [[nodiscard]] const_iterator findValue(const value_type &value) const;
};

LOOMAPI std::ostream &operator<<(std::ostream &os, const ConfigMap &cm);

LOOMAPI std::ostream &operator<<(std::ostream &os, const ConfigMap::value_type &val);

LOOMAPI std::ostream &operator<<(std::ostream &os, const ConfigMapValue &val);

LOOMAPI struct ConfigMapAttributeKey {
    ConfigMapAttributeKey() = default;

    ConfigMapAttributeKey(ConfigMap configMap, ConfigMap::key_type attribute);

    ConfigMap configMap;
    ConfigMap::key_type attribute;

    bool operator<(const ConfigMapAttributeKey &other) const;

    bool operator==(const ConfigMapAttributeKey &other) const;

    bool operator==(const ConfigMap &configMap) const;
};

}

#endif //LOOM_CONFIGMAP_H
