//
// Created by void on 19/05/2020.
//

#ifndef LOOM_BUFFER_H
#define LOOM_BUFFER_H

#include <cstddef>

namespace loom {

/**
 * A simple non-owning buffer implementation.
 * @tparam T The type of data to hold.
 * @tparam BufferT The buffer interface type.
 */
template<typename T, typename BufferT>
class BaseBuffer {
public:
    using type = BufferT;
    using self_type = BaseBuffer<T, BufferT>;

    /**
     * Creates a new buffer.
     * @param data The data this buffer points to.
     * @param size The size of the buffer.
     */
    BaseBuffer(T *data, size_t size) : _data(data), _size(size) {}

    /**
     * @return A raw pointer to the data.
     */
    [[nodiscard]] T *data() const {
        return _data;
    }

    /**
     * @return The size of the buffer.
     */
    [[nodiscard]] size_t size() const {
        return _size;
    }

    /**
     * Creates a new buffer offset by n bytes from this one.
     * @param n The relative offset of the new buffer.
     * @return The created buffer.
     */
    [[nodiscard]] type operator+(size_t n) const {
        return type(const_cast<char *>(static_cast<const char *>(_data)) + n, _size - n);
    }

protected:
    T *_data;
    size_t _size;
};

/**
 * A mutable buffer implementation.
 */
class MutableBuffer : public BaseBuffer<void, MutableBuffer> {
public:
    using BaseBuffer<void, MutableBuffer>::BaseBuffer;
    using type = MutableBuffer;
    using self_type = type;

    [[nodiscard]] void *data() {
        return _data;
    }
};

/**
 * A buffer implementation that only supports immutable (read-only) access.
 */
class ImmutableBuffer : public BaseBuffer<void, ImmutableBuffer> {
public:
    using BaseBuffer<void, ImmutableBuffer>::BaseBuffer;
    using type = MutableBuffer;
    using self_type = type;
};

extern const ImmutableBuffer NullBuffer; //< The empty buffer instance.

}

#endif //LOOM_BUFFER_H
