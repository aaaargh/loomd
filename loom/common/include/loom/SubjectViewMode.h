//
// Created by void on 28/05/2020.
//

#ifndef LOOM_SUBJECTVIEWMODE_H
#define LOOM_SUBJECTVIEWMODE_H

namespace loom {

enum class SubjectViewMode {
    ResolveFully, //< Fully resolve the variant, falling back to its default values if not specified.
    ResolveRequired, //< Only resolve options that have been queried.
    RawOptions, //< Return the raw options instead of the resolved values.
};

}

#endif //LOOM_SUBJECTVIEWMODE_H
