//
// Created by void on 01/06/2020.
//

#ifndef LOOM_CONFIGMAPEXTRACTOR_H
#define LOOM_CONFIGMAPEXTRACTOR_H

#include <loom/ConfigMap.h>

#include <utility>
#include <boost/lexical_cast.hpp>

namespace loom {

namespace ConfigMapExtractorOperators {

namespace detail {
template<typename MatcherT>
static bool invokeMatcher(MatcherT &&matcher, const ConfigMap &source, ConfigMap &target,
                          ConfigMap::value_type &value) {
    if constexpr(std::is_same_v<decltype(matcher(source, target, value)), bool>) {
        return matcher(source, target, value);
    } else {
        matcher(source, target, value);
        return true;
    }
}

template<typename MatchersTupleT, typename TransformersTupleT, typename WhenT>
struct when_with_then {
    when_with_then(MatchersTupleT &&matchers, TransformersTupleT &&transformers) noexcept: matchers(
            std::move(matchers)),
                                                                                           transformers(std::move(
                                                                                                   transformers)) {}

    bool operator()(const ConfigMap &source, ConfigMap &target, ConfigMap::value_type &value) const {
        if (!std::apply([&](auto &&...matcher) {
            return (detail::invokeMatcher(matcher, source, target, value) && ...);
        }, matchers)) {
            return true;
        }

        return std::apply([&](auto &&...transformer) {
            return (detail::invokeMatcher(transformer, source, target, value) && ...);
        }, transformers);
    }

    MatchersTupleT matchers;
    TransformersTupleT transformers;
};
}

template<typename T>
struct as {
    void operator()(const ConfigMap &source, const ConfigMap &target, ConfigMap::value_type &value) const {
        std::visit([&](auto &&v) {
            using U = std::decay_t<decltype(v)>;
            if constexpr(std::is_same_v<U, std::string> || std::is_same_v<U, double> || std::is_same_v<U, long>) {
                value.second = boost::lexical_cast<T>(v);
            }
        }, value.second);
    }
};

struct distinct {
    bool operator()(const ConfigMap &source, ConfigMap &target, ConfigMap::value_type &value) const;
};

struct any {
    bool operator()(const ConfigMap &source, ConfigMap &target, ConfigMap::value_type &value) const;
};

struct key {
    explicit key(ConfigMap::key_type k) noexcept;

    bool operator()(const ConfigMap &source, ConfigMap &target, const ConfigMap::value_type &value) const;

    ConfigMap::key_type k;
};

template<typename... Keys>
struct keys {
    explicit keys(Keys... keys) noexcept {
        size_t i = 0;
        ((ks[i++] = keys), ...);
    }

    bool operator()(const ConfigMap &source, ConfigMap &target, const ConfigMap::value_type &value) const {
        for (size_t i = 0; i < sizeof...(Keys); ++i) {
            key k(ks[i]);
            if (k(source, target, value)) {
                return true;
            }
        }

        return false;
    }

    ConfigMap::key_type ks[sizeof...(Keys)];
};

template<typename T>
struct ofType {
    using type = std::decay_t<T>;

    bool operator()(const ConfigMap &source, ConfigMap &target, const ConfigMap::value_type &value) const {
        return (std::visit([](auto &&v) {
            using value_type = std::decay_t<decltype(v)>;
            return std::is_same_v<value_type, type>;
        }, value.second));
    }
};

template<typename... Types>
struct ofTypes {
    bool operator()(const ConfigMap &source, ConfigMap &target, const ConfigMap::value_type &value) const {
        return (... || ofType<Types>()(source, target, value));
    }
};

struct numeric : ofTypes<double, long> {
};

template<typename... Matchers>
struct allOf {
    explicit allOf(Matchers... matchers) noexcept: matchers(std::forward<Matchers>(matchers)...) {}

    bool operator()(const ConfigMap &source, ConfigMap &target, ConfigMap::value_type &value) const {
        return std::apply([&](auto &&...matcher) {
            return (detail::invokeMatcher(matcher, source, target, value) && ...);
        }, matchers);
    }

    std::tuple<Matchers...> matchers;
};

template<typename... Matchers>
struct oneOf {
    explicit oneOf(Matchers... matchers) noexcept: matchers(std::forward<Matchers>(matchers)...) {}

    bool operator()(const ConfigMap &source, ConfigMap &target, ConfigMap::value_type &value) const {
        return std::apply([&](auto &&...matcher) {
            return (detail::invokeMatcher(matcher, source, target, value) || ...);
        }, matchers);
    }

    std::tuple<Matchers...> matchers;
};

template<typename... Matchers>
struct when {
    explicit when(Matchers... matchers) noexcept: matchers(std::forward<Matchers>(matchers)...) {}

    template<typename... Transformers>
    using then_type = detail::when_with_then<std::tuple<Matchers...>, std::tuple<Transformers...>, when<Matchers...> >;

    template<typename... Transformers>
    then_type<Transformers...> operator()(Transformers... transformers) {
        std::tuple<Transformers...> transformersTuple(std::forward<Transformers>(transformers)...);
        return then_type<Transformers...>(std::move(matchers), std::move(transformersTuple));
    }

    std::tuple<Matchers...> matchers;
};

}

class ConfigMapExtractorBase {
public:
    virtual void operator()(const ConfigMap &source, ConfigMap &target) const = 0;

    virtual void operator()(ConfigMap &target) const = 0;
};

template<typename... Matchers>
class ConfigMapExtractor : public ConfigMapExtractorBase {
public:
    explicit ConfigMapExtractor(Matchers &&...matchers) noexcept: baseMatcher(std::forward<Matchers>(matchers)...) {}

    void operator()(const ConfigMap &source, ConfigMap &target) const override {
        for (auto &item : source) {
            ConfigMap::value_type transformedItem = item;
            if (baseMatcher(source, target, transformedItem)) {
                target.insert(std::move(transformedItem));
            }
        }
    }

    void operator()(ConfigMap &target) const override {
        ConfigMap tmp;
        operator()(target, tmp);
        target = std::move(tmp);
    };

private:
    ConfigMapExtractorOperators::oneOf<Matchers...> baseMatcher;
};

template<typename... Matchers>
constexpr ConfigMapExtractor<Matchers...> make_config_map_extractor(Matchers &&...matchers) noexcept {
    return ConfigMapExtractor<Matchers...>(std::forward<Matchers>(matchers)...);
}

template<typename... Matchers>
void apply_config_map_extractor(const ConfigMap &source, ConfigMap &target, Matchers &&...matchers) noexcept {
    auto extractor = make_config_map_extractor(std::forward<Matchers>(matchers)...);
    extractor(source, target);
}

namespace ConfigMapExtractors {
const ConfigMapExtractorBase &get_subject_id_extractor();

const ConfigMapExtractorBase &get_subject_id_with_variants_extractor();

const ConfigMapExtractorBase &get_normalized_subject_extractor();

}

}

#endif //LOOM_CONFIGMAPEXTRACTOR_H
