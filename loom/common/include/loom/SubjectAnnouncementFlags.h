//
// Created by void on 01/06/2020.
//

#ifndef LOOM_SUBJECTANNOUNCEMENTFLAGS_H
#define LOOM_SUBJECTANNOUNCEMENTFLAGS_H

#include <loom/BitFlags.h>

namespace loom {

enum SubjectAnnouncementFlags {
    None = 1u << 0u,
    ExtendAll = 1u << 1u, //< If the subject is already in store, extend it by adding all attributes.
    ExtendMissing = 1u << 2u, //< If the subject is already in store, extend it by adding all missing attributes.
    ExtendOverride = 1u << 3u, //< If the subject is already in store, extend it by overriding the existing attributes.
    Defaults = ExtendAll,
};

}

#endif //LOOM_SUBJECTANNOUNCEMENTFLAGS_H
