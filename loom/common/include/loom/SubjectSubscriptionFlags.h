//
// Created by void on 01/06/2020.
//

#ifndef LOOM_SUBJECTSUBSCRIPTIONFLAGS_H
#define LOOM_SUBJECTSUBSCRIPTIONFLAGS_H

#include <loom/BitFlags.h>

namespace loom {

enum class SubjectSubscriptionFlags {
    None = 1u << 0u,
    EmitExistingSubjects = 1u << 1u,
    IgnoreOwnSubjects = 2u << 1u,
    Defaults = IgnoreOwnSubjects
};

}

#endif //LOOM_SUBJECTSUBSCRIPTIONFLAGS_H
