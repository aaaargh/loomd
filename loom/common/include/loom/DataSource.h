//
// Created by void on 19/05/2020.
//

#ifndef LOOM_DATASOURCE_H
#define LOOM_DATASOURCE_H

#include <loom/loom.h>
#include <loom/Buffer.h>

#include <memory>
#include <functional>
#include <ostream>

namespace loom {

class LOOMAPI DataSourceWriter {
public:
    virtual ~DataSourceWriter() = default;

    virtual void beginTransaction() = 0;

    virtual void write(const ImmutableBuffer &buffer, size_t offset) = 0;

    virtual void setEOF(size_t offset) = 0;

    virtual void endTransaction() = 0;
};

class LOOMAPI DataSourceReader {
public:
    virtual ~DataSourceReader() = default;

    struct ReadDataResult {
        size_t bytesRead{};
        bool isEOF = false;
        std::exception_ptr error;
    };

    using ReadCompleteCallback = std::function<void(ReadDataResult)>;

    virtual void readAsync(MutableBuffer target, size_t offset, const ReadCompleteCallback &callback);

    virtual ReadDataResult read(loom::MutableBuffer &target, size_t offset);
};

LOOMAPI struct DataSourceInfo {
    size_t length = 0;

    enum class SizeHint {
        Exact,
        AtLeast,
        AtMost,
        Estimated
    };

    SizeHint sizeHint = SizeHint::Exact;

    enum class AccessType {
        Random,
        Streamed
    };

    AccessType accessType = AccessType::Random;

    std::shared_ptr<DataSourceReader> reader;

    using hash_type = std::tuple<const decltype(length), const decltype(sizeHint), const decltype(accessType)>;

    [[nodiscard]] hash_type hash() const;

    [[nodiscard]] bool operator==(const DataSourceInfo &other) const;

    [[nodiscard]] bool operator<(const DataSourceInfo &other) const;
};

LOOMAPI std::ostream &operator<<(std::ostream &os, const DataSourceInfo &val);

}

#endif //LOOM_DATASOURCE_H
