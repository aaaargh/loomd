//
// Created by void on 22/04/2020.
//

#ifndef LOOM_EXCEPTION_H
#define LOOM_EXCEPTION_H

#include <loom/loom.h>

#include <exception>
#include <stdexcept>

namespace loom {
class LOOMAPI NotImplementedException : public std::runtime_error {
public:
    using runtime_error::runtime_error;
};

class LOOMAPI NotSupportedException : public std::domain_error {
public:
    using domain_error::domain_error;
};

class LOOMAPI NotFoundException : public std::domain_error {
public:
    using domain_error::domain_error;
};

class LOOMAPI InsertException : public std::domain_error {
    using domain_error::domain_error;
};

class LOOMAPI ParseException : public std::logic_error {
public:
    using logic_error::logic_error;
};

LOOMAPI std::string get_exception_message(const std::exception_ptr &eptr = std::current_exception());

}

#endif //LOOM_EXCEPTION_H
