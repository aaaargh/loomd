//
// Created by void on 13/05/2020.
//

#ifndef LOOM_H
#define LOOM_H

#ifdef __cplusplus
extern "C" {
#endif

#if (defined(LOOM_SHARED) && defined(WIN32))
#ifdef LOOM_EXPORT
#define LOOMAPI __declspec(dllexport)
#else
#define LOOMAPI __declspec(dllimport)
#endif
#else
#define LOOMAPI
#endif

#ifdef __cplusplus
}
#endif

#endif //LOOM_H
