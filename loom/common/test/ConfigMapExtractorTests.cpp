//
// Created by void on 01/06/2020.
//

#include <loom/testing.h>

#include <loom/ConfigMapExtractor.h>

using namespace loom::testing;

namespace loom::testing {

BOOST_AUTO_TEST_SUITE(ConfigMapExtractorTests)

BOOST_AUTO_TEST_CASE(all_matcher_is_working) {
    ConfigMap cm = {
            {"a", (long)1},
            {"a", "2"},
            {"b", (long)3},
            {"b", "4"},
    };

    ConfigMap expected = {
            {"b", (long)3}
    };

    ConfigMap result;

    namespace op = ConfigMapExtractorOperators;

    apply_config_map_extractor(cm, result,
                               op::allOf(
                                       op::key("b"),
                                       op::ofType<long>()
                               ));

    BOOST_REQUIRE_EQUAL(expected, result);
}

BOOST_AUTO_TEST_CASE(can_extract_by_type) {
    ConfigMap cm = {
            {"a", 1.0},
            {"b", "2"},
            {"c", 3.0},
            {"d", "4"},
            {"e", (long)5}
    };

    ConfigMap expected = {
            {"a", 1.0},
            {"c", 3.0},
            {"e", (long)5}
    };

    ConfigMap result;

    namespace op = ConfigMapExtractorOperators;

    apply_config_map_extractor(cm, result,
                               op::numeric()
    );

    BOOST_REQUIRE_EQUAL(expected, result);
}

BOOST_AUTO_TEST_CASE(can_extract_by_type_multiple) {
    ConfigMap cm = {
            {"a", (long)1},
            {"b", "2"},
            {"c", (long)3},
            {"d", "4"},
            {"e", (long)5},
            {"f", DataSourceInfo{}}
    };

    ConfigMap expected = {
            {"a", (long)1},
            {"c", (long)3},
            {"e", (long)5},
            {"f", DataSourceInfo{}}
    };

    ConfigMap result;

    namespace op = ConfigMapExtractorOperators;

    apply_config_map_extractor(cm, result,
                               op::ofTypes<long, DataSourceInfo>()
    );

    BOOST_REQUIRE_EQUAL(expected, result);
}

BOOST_AUTO_TEST_CASE(can_get_key) {
    ConfigMap cm = {
            {"a", (long)1},
            {"b", (long)2},
            {"c", (long)3},
            {"d", (long)4},
    };

    ConfigMap expected = {
            {"a", (long)1}
    };

    ConfigMap result{};

    namespace op = ConfigMapExtractorOperators;

    apply_config_map_extractor(cm, result,
                               op::key("a")
    );

    BOOST_REQUIRE_EQUAL(expected, result);
}

BOOST_AUTO_TEST_CASE(can_get_multiple_keys) {
    ConfigMap cm = {
            {"a", (long)1},
            {"b", (long)2},
            {"c", (long)3},
            {"d", (long)4},
    };

    ConfigMap expected = {
            {"a", (long)1},
            {"c", (long)3},
    };

    ConfigMap result{};

    namespace op = ConfigMapExtractorOperators;

    apply_config_map_extractor(cm, result,
                               op::keys("a", "c")
    );

    BOOST_REQUIRE_EQUAL(expected, result);
}

BOOST_AUTO_TEST_CASE(can_get_distinct) {
    ConfigMap cm = {
            {"a", (long)1},
            {"a", (long)2},
            {"a", (long)3},
            {"a", (long)4},
    };

    ConfigMap expected = {
            {"a", (long)1},
    };

    ConfigMap result{};

    namespace op = ConfigMapExtractorOperators;

    apply_config_map_extractor(cm, result,
                               op::when(op::key("a"))(op::distinct())
    );

    BOOST_REQUIRE_EQUAL(expected, result);
}

BOOST_AUTO_TEST_CASE(can_transform_value) {
    ConfigMap cm = {
            {"a", "1"}
    };

    ConfigMap expected = {
            {"a", (long)1},
    };

    ConfigMap result{};

    namespace op = ConfigMapExtractorOperators;

    apply_config_map_extractor(cm, result,
                               op::as<long>()
    );

    BOOST_REQUIRE_EQUAL(expected, result);
}

BOOST_AUTO_TEST_CASE(can_transform_value_without_explicit_apply) {
    ConfigMap cm = {
            {"a", "1"},
            {"b", "2"},
    };

    ConfigMap expected = {
            {"a", 1.0},
            {"b", 2.0},
    };

    ConfigMap result{};

    namespace op = ConfigMapExtractorOperators;

    apply_config_map_extractor(cm, result,
                               op::as<double>()
    );

    BOOST_REQUIRE_EQUAL(expected, result);
}

BOOST_AUTO_TEST_CASE(can_transform_value_conditionally) {
    ConfigMap cm = {
            {"a", "1"},
            {"b", "2"}
    };

    ConfigMap expected = {
            {"a", "1"},
            {"b", (long)2},
    };

    ConfigMap result{};

    namespace op = ConfigMapExtractorOperators;

    apply_config_map_extractor(cm, result,
                               op::key("a"),
                               op::when(op::key("b")
                               )(op::as<long>())
    );

    BOOST_REQUIRE_EQUAL(expected, result);
}

BOOST_AUTO_TEST_CASE(can_use_subject_id_extractor) {
    ConfigMap cm = {
            {"uuid",           "alpha"},
            {"realm",          "default"},
            {"obsolete",       "this is not needed"},
            {"obsolete2",      "also this one"},
            {"ignored_number", (long)12}
    };

    ConfigMap expected = {
            {"uuid",  "alpha"},
            {"realm", "default"}
    };

    ConfigMap result{};

    ConfigMapExtractors::get_subject_id_extractor()(cm, result);

    BOOST_REQUIRE_EQUAL(expected, result);
}

BOOST_AUTO_TEST_CASE(can_use_subject_id_extractor_that_converts) {
    ConfigMap cm = {
            {"uuid",           "alpha"},
            {"realm",          (long)1234},
            {"realm",          "duplicate"},
            {"obsolete",       "this is not needed"},
            {"obsolete2",      "also this one"},
            {"ignored_number", (long)12}
    };

    ConfigMap expected = {
            {"uuid",  "alpha"},
            {"realm", "1234"}
    };

    ConfigMap result{};

    ConfigMapExtractors::get_subject_id_extractor()(cm, result);

    BOOST_REQUIRE_EQUAL(expected, result);
}

BOOST_AUTO_TEST_CASE(can_use_subject_id_extractor_with_variants) {
    ConfigMap cm = {
            {"uuid",     "alpha"},
            {"realm",    "default"},
            {"obsolete", "this is not needed"},
            {"variant",  Variant("a", "b")}
    };

    ConfigMap expected = {
            {"uuid",    "alpha"},
            {"realm",   "default"},
            {"variant", Variant("a", "b")}
    };

    ConfigMap result{};

    ConfigMapExtractors::get_subject_id_with_variants_extractor()(cm, result);

    BOOST_REQUIRE_EQUAL(expected, result);
}

BOOST_AUTO_TEST_CASE(can_use_subject_normalizer) {
    ConfigMap cm = {
            {"uuid",  "alpha"},
            {"uuid",  "duplicate"},
            {"realm", "default"},
            {"realm", "duplicate"},
            {"other", (long)2}
    };

    ConfigMap expected = {
            {"uuid",  "alpha"},
            {"realm", "default"},
            {"other", (long)2}
    };

    ConfigMap result{};

    ConfigMapExtractors::get_normalized_subject_extractor()(cm, result);

    BOOST_REQUIRE_EQUAL(expected, result);
}

BOOST_AUTO_TEST_SUITE_END()

}