//
// Created by void on 05/04/2020.
//

#include <loom/testing.h>

#include <loom/ConfigMap.h>

using namespace loom::testing;

namespace loom::testing {

BOOST_AUTO_TEST_SUITE(ConfigMapTests)

BOOST_AUTO_TEST_CASE(initialization_is_working) {
    ConfigMap cm{
            {"a", "1"},
            {"b", "2"}
    };

    BOOST_REQUIRE_EQUAL(2, cm.size());
}

BOOST_AUTO_TEST_CASE(can_put_non_string_values) {
    ConfigMap cm;
    cm.insert(std::make_pair("test", (double) 12));
    BOOST_REQUIRE(cm.find("test") != cm.end());
}

BOOST_AUTO_TEST_CASE(can_get_non_string_values) {
    ConfigMap cm;
    cm.insert(std::make_pair("test", (double) 42));
    auto val = std::get<double>(cm.find("test")->second);
    BOOST_REQUIRE(val);
    BOOST_REQUIRE_EQUAL(42, val);
}

BOOST_AUTO_TEST_CASE(can_put_and_get_non_string_values) {
    ConfigMap cm;
    cm.insert(std::make_pair("test", (double) 42));
    auto val = std::get<double>(cm.find("test")->second);
    BOOST_REQUIRE(val);
    BOOST_REQUIRE_EQUAL(42, val);
}

BOOST_AUTO_TEST_CASE(type_checking_is_correct) {
    ConfigMap cm{
            {"string", "hello"},
            {"number", (double) 123},
            {"source", DataSourceInfo{.length = 0, .accessType = DataSourceInfo::AccessType::Streamed}}
    };

    BOOST_REQUIRE(std::holds_alternative<std::string>(cm.find("string")->second));
    BOOST_REQUIRE(std::holds_alternative<double>(cm.find("number")->second));
    BOOST_REQUIRE(std::holds_alternative<DataSourceInfo>(cm.find("source")->second));
}

BOOST_AUTO_TEST_CASE(can_subtract_maps) {
    ConfigMap a{
            {"x", "1"},
            {"y", "2"},
            {"y", "3"}
    };

    ConfigMap b{
            {"y", "3"}
    };

    ConfigMap result = a - b;

    ConfigMap expected{
            {"x", "1"},
            {"y", "2"}
    };

    BOOST_REQUIRE_EQUAL(expected, result);
}

BOOST_AUTO_TEST_CASE(can_subtract_maps_inplace) {
    ConfigMap a{
            {"x", "1"},
            {"y", "2"},
            {"y", "3"}
    };

    ConfigMap b{
            {"x", "1"},
            {"y", "4"},
    };

    a -= b;

    ConfigMap expected{
            {"y", "2"},
            {"y", "3"},
    };

    BOOST_REQUIRE_EQUAL(expected, a);
}

BOOST_AUTO_TEST_CASE(can_add_maps) {
    ConfigMap a{
            {"x", "1"},
    };

    ConfigMap b{
            {"y", "2"}
    };

    ConfigMap result = a + b;

    ConfigMap expected{
            {"x", "1"},
            {"y", "2"}
    };

    BOOST_REQUIRE_EQUAL(expected, result);
}

BOOST_AUTO_TEST_CASE(can_add_maps_inplace) {
    ConfigMap a{
            {"x", "1"},
    };

    ConfigMap b{
            {"x", "1"},
            {"y", "2"},
    };

    a += b;

    ConfigMap expected{
            {"x", "1"},
            {"y", "2"}
    };

    BOOST_REQUIRE_EQUAL(expected, a);
}

BOOST_AUTO_TEST_CASE(can_find_value) {
    ConfigMap a{
            {"x", "1"},
    };

    BOOST_REQUIRE(a.end() != a.findValue({"x", "1"}));
    BOOST_REQUIRE(a.end() != a.findValue("x", "1"));
    BOOST_REQUIRE(a.end() == a.findValue("x", (long)1));
}

BOOST_AUTO_TEST_SUITE_END()

}
