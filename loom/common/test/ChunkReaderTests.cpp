//
// Created by void on 22/05/2020.
//

#include <loom/testing.h>

#include <loom/ChunkReader.h>

using namespace loom::testing;

namespace loom::testing {

BOOST_AUTO_TEST_SUITE(ChunkReaderTests)

struct TestReader : DataSourceReader {
    std::vector<std::pair<size_t, size_t> > readRanges;
    size_t readBytes = 0;
    bool eofHit = false;

    size_t eofPosition = 511;
    size_t maxChunkSize = std::numeric_limits<size_t>::max();
    bool fakeAsyncDelay = false;
    bool returnError = false;

    ReadCompleteCallback delayedCallback;
    ReadDataResult delayedResult;

    void completeAsyncCall() const {
        delayedCallback(delayedResult);
    }

    void readAsync(MutableBuffer target, size_t offset, const ReadCompleteCallback &callback) override {
        auto bytesRead = std::min(maxChunkSize, target.size());
        bool isEOF = false;

        if (offset + bytesRead > eofPosition) {
            bytesRead = eofPosition - offset;
            isEOF = true;
            eofHit = true;
        }

        readBytes += bytesRead;
        readRanges.emplace_back(offset, bytesRead);

        ReadDataResult result = {
                .bytesRead = bytesRead,
                .isEOF = isEOF,
        };

        if (returnError) {
            result.error = std::make_exception_ptr(std::runtime_error("Just a test error"));
        }

        if (fakeAsyncDelay) {
            delayedCallback = callback;
            delayedResult = result;
            return;
        }

        callback(result);
    }
};

struct TestChunkReader : ChunkReader {
    using ChunkReader::ChunkReader;

public:
    size_t handleChunkReadInvocations = 0;
    bool handleCompleteCalled = false;
    bool handleInterruptedCalled = false;
    bool handleErrorCalled = false;

protected:
    void handleChunkRead(ImmutableBuffer buffer, size_t offset, const ReadDataResult &result) override {
        handleChunkReadInvocations++;
    }

    void handleComplete() override {
        handleCompleteCalled = true;
    }

    void handleInterrupted() override {
        handleInterruptedCalled = true;
    }

    void handleError(const std::exception_ptr &error) override {
        handleErrorCalled = true;
    }
};

BOOST_AUTO_TEST_CASE(can_read_single_chunk) {
    auto reader = std::make_shared<TestReader>();
    TestChunkReader chunkReader(reader, 100, 0, 100);

    chunkReader.start();

    BOOST_REQUIRE(chunkReader.isDone());
    BOOST_REQUIRE(!reader->eofHit);
    BOOST_REQUIRE_EQUAL(1, reader->readRanges.size());
    BOOST_REQUIRE_EQUAL(0, reader->readRanges[0].first);
    BOOST_REQUIRE_EQUAL(100, reader->readRanges[0].second);

    BOOST_REQUIRE_EQUAL(1, chunkReader.handleChunkReadInvocations);
    BOOST_REQUIRE(chunkReader.handleCompleteCalled);
    BOOST_REQUIRE(!chunkReader.handleInterruptedCalled);
    BOOST_REQUIRE(!chunkReader.handleErrorCalled);
}

BOOST_AUTO_TEST_CASE(can_read_single_chunk_with_early_eof) {
    auto reader = std::make_shared<TestReader>();
    TestChunkReader chunkReader(reader, 1000, 0, 1000);

    chunkReader.start();

    BOOST_REQUIRE(chunkReader.isDone());
    BOOST_REQUIRE(reader->eofHit);
    BOOST_REQUIRE_EQUAL(1, reader->readRanges.size());
    BOOST_REQUIRE_EQUAL(0, reader->readRanges[0].first);
    BOOST_REQUIRE_EQUAL(reader->eofPosition, reader->readRanges[0].second);
}

BOOST_AUTO_TEST_CASE(can_read_multiple_chunks) {
    auto reader = std::make_shared<TestReader>();
    reader->eofPosition = std::numeric_limits<size_t>::max();

    TestChunkReader chunkReader(reader, 300, 0, 1000);

    chunkReader.start();

    BOOST_REQUIRE(chunkReader.isDone());
    BOOST_REQUIRE(!reader->eofHit);
    BOOST_REQUIRE_EQUAL(4, reader->readRanges.size());
    BOOST_REQUIRE_EQUAL(1000, reader->readBytes);
}

BOOST_AUTO_TEST_CASE(can_read_single_chunk_delayed) {
    auto reader = std::make_shared<TestReader>();
    reader->fakeAsyncDelay = true;

    TestChunkReader chunkReader(reader, 100, 0, 100);

    chunkReader.start();

    BOOST_REQUIRE(!chunkReader.isDone());

    reader->completeAsyncCall();

    BOOST_REQUIRE(chunkReader.isDone());

    BOOST_REQUIRE(!reader->eofHit);
    BOOST_REQUIRE_EQUAL(1, reader->readRanges.size());
    BOOST_REQUIRE_EQUAL(0, reader->readRanges[0].first);
    BOOST_REQUIRE_EQUAL(100, reader->readRanges[0].second);
}

BOOST_AUTO_TEST_CASE(can_read_single_chunk_with_early_eof_delayed) {
    auto reader = std::make_shared<TestReader>();
    reader->fakeAsyncDelay = true;

    TestChunkReader chunkReader(reader, 1000, 0, 1000);

    chunkReader.start();

    BOOST_REQUIRE(!chunkReader.isDone());

    reader->completeAsyncCall();

    BOOST_REQUIRE(chunkReader.isDone());
    BOOST_REQUIRE(reader->eofHit);
    BOOST_REQUIRE_EQUAL(1, reader->readRanges.size());
    BOOST_REQUIRE_EQUAL(0, reader->readRanges[0].first);
    BOOST_REQUIRE_EQUAL(reader->eofPosition, reader->readRanges[0].second);
}

BOOST_AUTO_TEST_CASE(can_read_multiple_chunks_delayed) {
    auto reader = std::make_shared<TestReader>();
    reader->eofPosition = std::numeric_limits<size_t>::max();
    reader->fakeAsyncDelay = true;

    TestChunkReader chunkReader(reader, 300, 0, 1000);

    chunkReader.start();

    size_t calls = 0;

    while (!chunkReader.isDone()) {
        reader->completeAsyncCall();
        calls++;
    }

    BOOST_REQUIRE_EQUAL(4, calls);
    BOOST_REQUIRE(!reader->eofHit);
    BOOST_REQUIRE_EQUAL(4, reader->readRanges.size());
    BOOST_REQUIRE_EQUAL(1000, reader->readBytes);
}

BOOST_AUTO_TEST_CASE(can_interrupt) {
    auto reader = std::make_shared<TestReader>();
    reader->fakeAsyncDelay = true;

    TestChunkReader chunkReader(reader, 1000, 0, 1000);

    chunkReader.start();

    BOOST_REQUIRE(!chunkReader.isDone());

    chunkReader.interrupt();

    reader->completeAsyncCall();

    BOOST_REQUIRE(!chunkReader.isDone());
    BOOST_REQUIRE(chunkReader.handleInterruptedCalled);
}

BOOST_AUTO_TEST_CASE(can_handle_errors) {
    auto reader = std::make_shared<TestReader>();
    reader->returnError = true;

    TestChunkReader chunkReader(reader, 1000, 0, 1000);

    chunkReader.start();

    BOOST_REQUIRE(chunkReader.handleErrorCalled);
}

BOOST_AUTO_TEST_SUITE_END()

}