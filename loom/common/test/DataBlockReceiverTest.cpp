//
// Created by void on 22/05/2020.
//

#include <loom/testing.h>

#include <loom/DataBlockReceiver.h>

using namespace loom::testing;

namespace loom::testing {

BOOST_AUTO_TEST_SUITE(DataBlockReceiverTests)

struct TestWriter : DataSourceWriter {
    bool beginTransactionCalled = false;
    std::vector<std::pair<size_t, size_t> > rangesWritten;
    bool eofSet = false;
    size_t eof = 0;
    bool endTransactionCalled = false;

    void beginTransaction() override {
        beginTransactionCalled = true;
    }

    void write(const ImmutableBuffer &buffer, size_t offset) override {
        rangesWritten.emplace_back(std::make_pair(offset, buffer.size()));
    }

    void setEOF(size_t offset) override {
        eofSet = true;
        eof = offset;
    }

    void endTransaction() override {
        endTransactionCalled = true;
    }
};

BOOST_AUTO_TEST_CASE(reports_zero_open_blocks) {

    DataBlockReceiver receiver;
    auto writer = std::make_shared<TestWriter>();

    auto key = ConfigMapAttributeKey({{"uuid", "test"}}, "file");

    ImmutableBuffer noBuffer(nullptr, 100);
    BOOST_REQUIRE_EQUAL(0, receiver.pushReceivedData(key, 0, noBuffer));
}

BOOST_AUTO_TEST_CASE(can_receive_complete_block_in_single_pass) {

    DataBlockReceiver receiver;
    auto writer = std::make_shared<TestWriter>();

    auto key = ConfigMapAttributeKey({{"uuid", "test"}}, "file");

    receiver.openBlock(key, 0, 100, writer);

    BOOST_REQUIRE(!receiver.isClean());

    ImmutableBuffer noBuffer(nullptr, 100);
    BOOST_REQUIRE_EQUAL(1, receiver.pushReceivedData(key, 0, noBuffer));

    BOOST_REQUIRE(receiver.isClean());

    BOOST_REQUIRE(writer->beginTransactionCalled);
    BOOST_REQUIRE(writer->endTransactionCalled);
    BOOST_REQUIRE(!writer->eofSet);
    BOOST_REQUIRE_EQUAL(1, writer->rangesWritten.size());
    BOOST_REQUIRE_EQUAL(0, writer->rangesWritten[0].first);
    BOOST_REQUIRE_EQUAL(100, writer->rangesWritten[0].second);
}

BOOST_AUTO_TEST_CASE(can_receive_complete_block_in_multiple_passes) {

    DataBlockReceiver receiver;
    auto writer = std::make_shared<TestWriter>();

    auto key = ConfigMapAttributeKey({{"uuid", "test"}}, "file");

    receiver.openBlock(key, 0, 100, writer);

    BOOST_REQUIRE(!receiver.isClean());

    ImmutableBuffer noBuffer(nullptr, 50);
    BOOST_REQUIRE_EQUAL(1, receiver.pushReceivedData(key, 0, noBuffer));

    BOOST_REQUIRE(!receiver.isClean());
    BOOST_REQUIRE(writer->beginTransactionCalled);
    BOOST_REQUIRE(!writer->endTransactionCalled);

    BOOST_REQUIRE_EQUAL(1, receiver.pushReceivedData(key, 50, noBuffer));

    BOOST_REQUIRE(writer->endTransactionCalled);

    BOOST_REQUIRE(receiver.isClean());

    BOOST_REQUIRE_EQUAL(2, writer->rangesWritten.size());
    BOOST_REQUIRE_EQUAL(0, writer->rangesWritten[0].first);
    BOOST_REQUIRE_EQUAL(50, writer->rangesWritten[0].second);
    BOOST_REQUIRE_EQUAL(50, writer->rangesWritten[1].first);
    BOOST_REQUIRE_EQUAL(50, writer->rangesWritten[1].second);
}

BOOST_AUTO_TEST_CASE(is_setting_eof) {

    DataBlockReceiver receiver;
    auto writer = std::make_shared<TestWriter>();

    auto key = ConfigMapAttributeKey({{"uuid", "test"}}, "file");

    receiver.openBlock(key, 0, 100, writer);

    BOOST_REQUIRE(!receiver.isClean());

    ImmutableBuffer noBuffer(nullptr, 70);
    BOOST_REQUIRE_EQUAL(1, receiver.pushReceivedData(key, 0, noBuffer));
    BOOST_REQUIRE_EQUAL(1, receiver.pushReceivedData(key, 70, NullBuffer));

    BOOST_REQUIRE(receiver.isClean());
    BOOST_REQUIRE(writer->eofSet);
    BOOST_REQUIRE(writer->endTransactionCalled);
}

BOOST_AUTO_TEST_CASE(is_propagating_over_multiple_blocks) {

    DataBlockReceiver receiver;
    auto writer1 = std::make_shared<TestWriter>();
    auto writer2 = std::make_shared<TestWriter>();

    auto key = ConfigMapAttributeKey({{"uuid", "test"}}, "file");

    receiver.openBlock(key, 0, 100, writer1);
    receiver.openBlock(key, 0, 100, writer2);

    BOOST_REQUIRE(!receiver.isClean());

    ImmutableBuffer noBuffer(nullptr, 100);
    BOOST_REQUIRE_EQUAL(2, receiver.pushReceivedData(key, 0, noBuffer));

    BOOST_REQUIRE(receiver.isClean());
}

BOOST_AUTO_TEST_CASE(is_propagating_over_multiple_blocks_with_multipass) {

    DataBlockReceiver receiver;
    auto writer1 = std::make_shared<TestWriter>();
    auto writer2 = std::make_shared<TestWriter>();

    auto key = ConfigMapAttributeKey({{"uuid", "test"}}, "file");

    receiver.openBlock(key, 0, 100, writer1);
    receiver.openBlock(key, 0, 100, writer2);

    BOOST_REQUIRE(!receiver.isClean());

    ImmutableBuffer noBuffer(nullptr, 50);
    BOOST_REQUIRE_EQUAL(2, receiver.pushReceivedData(key, 0, noBuffer));
    BOOST_REQUIRE_EQUAL(2, receiver.pushReceivedData(key, 50, noBuffer));

    BOOST_REQUIRE(receiver.isClean());
}

BOOST_AUTO_TEST_SUITE_END()

}