//
// Created by void on 02/06/2020.
//

#include <loom/testing.h>

#include <loom/BitFlags.h>

using namespace loom::testing;

namespace loom::testing {

BOOST_AUTO_TEST_SUITE(FlagSetTests)

enum class MyFlag {
    Aldebaran = 1u << 0u,
    Betelgeusian = 1u << 1u,
    Golgafrinchan = 1u << 2u,
    Hooloovoo = 1u << 3u,
    Jatravartid = 1u << 4u,
};

std::ostream &operator<<(std::ostream &os, MyFlag f) {
    os << std::hex << static_cast<size_t>(f);
    return os;
}

BOOST_AUTO_TEST_CASE(bitwise_flags_are_working) {
    MyFlag f = MyFlag::Aldebaran | MyFlag::Hooloovoo | MyFlag::Jatravartid;

    BOOST_REQUIRE_EQUAL(MyFlag::Aldebaran, f & MyFlag::Aldebaran);
    BOOST_REQUIRE_NE(MyFlag::Betelgeusian, f & MyFlag::Betelgeusian);
    BOOST_REQUIRE_NE(MyFlag::Golgafrinchan, f & MyFlag::Golgafrinchan);
    BOOST_REQUIRE_EQUAL(MyFlag::Hooloovoo, f & MyFlag::Hooloovoo);
    BOOST_REQUIRE_EQUAL(MyFlag::Jatravartid, f & MyFlag::Jatravartid);
}

BOOST_AUTO_TEST_CASE(check_flag_is_working) {
    MyFlag f = MyFlag::Betelgeusian | MyFlag::Golgafrinchan;

    BOOST_REQUIRE(!check_flag(f, MyFlag::Aldebaran));
    BOOST_REQUIRE(check_flag(f, MyFlag::Betelgeusian));
    BOOST_REQUIRE(check_flag(f, MyFlag::Golgafrinchan));
    BOOST_REQUIRE(!check_flag(f, MyFlag::Hooloovoo));
    BOOST_REQUIRE(!check_flag(f, MyFlag::Jatravartid));
}

BOOST_AUTO_TEST_SUITE_END()
}