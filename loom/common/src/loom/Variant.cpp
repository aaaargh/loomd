//
// Created by void on 29/05/2020.
//

#include <loom/Variant.h>

namespace loom {

Variant::Variant(std::initializer_list<value_type> vals) {
    values.reserve(vals.size());
    for (auto &value : vals) {
        values.emplace_back(std::move(value));
    }
}

Variant::Variant(std::vector<value_type> vals) : values(std::move(vals)) {}

bool Variant::operator<(const Variant &other) const {
    return values < other.values;
}

bool Variant::operator==(const Variant &other) const {
    return values == other.values;
}

std::ostream &operator<<(std::ostream &os, const Variant &v) {
    os << "Variant";
    return os;
}

}