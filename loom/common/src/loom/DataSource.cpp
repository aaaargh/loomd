//
// Created by void on 19/05/2020.
//

#include <loom/DataSource.h>
#include <loom/Exception.h>

namespace loom {

std::ostream &operator<<(std::ostream &os, const DataSourceInfo::AccessType &accessType) {
    switch (accessType) {
        case DataSourceInfo::AccessType::Random:
            os << "random";
            break;
        case DataSourceInfo::AccessType::Streamed:
            os << "streamed";
            break;
    }
    return os;
}

std::ostream &operator<<(std::ostream &os, const DataSourceInfo::SizeHint &sizeHint) {
    switch (sizeHint) {
        case DataSourceInfo::SizeHint::Exact:
            os << "exact";
            break;
        case DataSourceInfo::SizeHint::AtLeast:
            os << "at least";
            break;
        case DataSourceInfo::SizeHint::AtMost:
            os << "at most";
            break;
        case DataSourceInfo::SizeHint::Estimated:
            os << "estimated";
            break;
    }
    return os;
}


std::ostream &operator<<(std::ostream &os, const DataSourceInfo &dsi) {
    os << "DataSource(length:=" << dsi.length << ", sizeHint:=" << dsi.sizeHint << ", accessType:=" << dsi.accessType
       << ")";
    return os;
}

DataSourceReader::ReadDataResult DataSourceReader::read(loom::MutableBuffer &target, size_t offset) {
    throw NotImplementedException("A DataSourceReader must either implement read() or readAsync().");
}

void DataSourceReader::readAsync(MutableBuffer target, size_t offset, const ReadCompleteCallback &callback) {
    auto result = read(target, offset);
    callback(result);
}

}
