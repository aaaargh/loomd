//
// Created by void on 20/05/2020.
//

#include <loom/DataBlockReceiver.h>

#include <utility>
#include <mutex>
#include <shared_mutex>

namespace loom {

void DataBlockReceiver::openBlock(const ConfigMapAttributeKey &key,
                                  size_t offset, size_t len,
                                  std::shared_ptr<DataSourceWriter> writer) {
    OpenBlock block = {
            .offset = offset,
            .end = offset + len,
            .writer = std::move(writer)
    };

    std::unique_lock lk(openBlocksMutex);
    openBlocks.emplace(std::make_pair(key, std::move(block)));
}

size_t DataBlockReceiver::pushReceivedData(const ConfigMapAttributeKey &key,
                                           size_t offset,
                                           ImmutableBuffer buffer) {
    auto blocks = openBlocks.equal_range(key);
    auto end = offset + buffer.size();
    size_t found = 0;

    // A zero-buffer indicates the EOF.
    bool isEOF = buffer.size() == 0;

    std::vector<decltype(openBlocks)::iterator> toRemove;

    std::shared_lock lk(openBlocksMutex);
    for (; blocks.first != blocks.second; ++blocks.first) {
        auto &block = blocks.first->second;

        OpenBlock::Range blockRange;

        if (block.end >= offset && block.end < end) {
            // Block end is somewhere in the given range.
            blockRange.first = std::max(offset, block.offset);
            blockRange.second = std::min(end, block.end);
        } else if (block.offset >= offset && block.offset < end) {
            // Block offset is somewhere in the given range.
            blockRange.first = std::max(offset, block.offset);
            blockRange.second = std::min(end, block.end);
        } else if (block.offset <= offset && block.end >= end) {
            // Given range is contained within the block completely.
            blockRange.first = offset;
            blockRange.second = end;
        } else {
            // Block is not intersecting.
            continue;
        }

        if (block.writtenRanges.empty()) {
            block.writer->beginTransaction();
        }

        if (block.offset == 0 && block.end == 0) {
            // A zero-byte block usually means we're streaming, extend the buffer range.
            blockRange.second = end;
        }

        if (isEOF) {
            // Limit the block size to the EOF.
            block.end = std::min(block.end, end);
            blockRange.second = std::min(blockRange.second, end);

            block.writer->setEOF(end);
        }

        auto bufferOffset = blockRange.first - offset;
        ImmutableBuffer blockBuffer(static_cast<char *>(buffer.data()) + bufferOffset,
                                    blockRange.second - blockRange.first);
        // TODO: this might write the block data multiple times in case there is more than one request in parallel.
        found++;

        if (!isEOF) {
            block.writer->write(blockBuffer, blockRange.first);
            block.writtenRanges.emplace_back(std::move(blockRange));

            block.mergeRanges();
        }

        if (block.writtenRanges.size() == 1 && block.writtenRanges.front().first == block.offset &&
            block.writtenRanges.front().second == block.end) {
            // Block has been read completely.
            block.writer->endTransaction();
            toRemove.push_back(blocks.first);
        }
    }

    if (!toRemove.empty()) {
        lk.unlock();
        std::unique_lock removeLk(openBlocksMutex);
        for (auto it : toRemove) {
            openBlocks.erase(it);
        }
    }

    return found;
}

void DataBlockReceiver::closeAllBlocks(const ConfigMap &configMap) {
    for (auto it = openBlocks.begin(); it != openBlocks.end();) {
        if (configMap %= it->first.configMap) {
            it = openBlocks.erase(it);
        } else {
            ++it;
        }
    }
}

bool DataBlockReceiver::isClean() const {
    return openBlocks.empty();
}

void DataBlockReceiver::OpenBlock::mergeRanges() {
    for (auto aIt = writtenRanges.begin(); aIt != writtenRanges.end(); ++aIt) {
        auto &a = *aIt;

        for (auto bIt = writtenRanges.begin(); bIt != writtenRanges.end();) {
            if (aIt == bIt) {
                // Ignore self.
                ++bIt;
                continue;
            }

            auto &b = *bIt;

            bool isIntersecting = false;

            if (a.second >= b.first && a.second < b.second) {
                // a's end is somewhere in b.
                isIntersecting = true;
            } else if (a.first >= b.first && a.first < b.second) {
                // a's offset is somewhere in b.
                isIntersecting = true;
            } else if (a.first <= b.first && a.second >= b.second) {
                // b is contained in a
                isIntersecting = true;
            }

            if (!isIntersecting) {
                ++bIt;
                continue;
            }

            // Merge b in a
            a.first = std::min(a.first, b.first);
            a.second = std::max(a.second, b.second);

            bIt = writtenRanges.erase(bIt);
            mergeRanges();
            return;
        }
    }
}
}
