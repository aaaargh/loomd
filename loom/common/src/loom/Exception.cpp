//
// Created by void on 08/05/2020.
//

#include <loom/Exception.h>

namespace loom {

std::string get_exception_message(const std::exception_ptr &eptr) {
    if (!eptr) { throw std::bad_exception(); }

    try { std::rethrow_exception(eptr); }
    catch (const std::exception &e) { return e.what(); }
}

}
