//
// Created by void on 20/05/2020.
//

#include <loom/ChunkReader.h>

#include <memory>
#include <utility>

namespace loom {

void ChunkReader::readNext(size_t chunkOffset, size_t chunkLen) {
    // Create the required buffer for the next chunk.
    auto chunkSize = std::min(chunkLen, maxChunkSize);

    if (chunkSize == 0) {
        done = true;
        handleComplete();
        return;
    }

    reader->readAsync(MutableBuffer(data.data(), chunkSize), chunkOffset,
                      [this, chunkOffset, chunkLen]
                              (const ReadDataResult &result) mutable {
                          if (result.error) {
                              handleError(result.error);
                              return;
                          }

                          if (interrupted) {
                              handleInterrupted();
                              return;
                          }

                          handleChunkRead(ImmutableBuffer(data.data(), result.bytesRead), chunkOffset, result);

                          // Off to the next chunk.
                          chunkOffset += result.bytesRead;
                          chunkLen -= result.bytesRead;

                          if (result.isEOF) {
                              // No more chunks to read.
                              done = true;
                              handleComplete();
                              return;
                          }

                          readNext(chunkOffset, chunkLen);
                      });
}

ChunkReader::ChunkReader(std::shared_ptr<DataSourceReader> reader,
                         size_t maxChunkSize, size_t offset, size_t len)
        : reader(std::move(reader)), maxChunkSize(maxChunkSize),
          offset(offset), len(len),
          data(maxChunkSize) {}

void ChunkReader::start() {
    readNext(offset, len);
}

void ChunkReader::interrupt() {
    interrupted = true;
}

bool ChunkReader::isDone() const {
    return done;
}

}