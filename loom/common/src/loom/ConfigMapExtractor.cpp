//
// Created by void on 02/06/2020.
//

#include <loom/ConfigMapExtractor.h>

namespace loom {

namespace ConfigMapExtractorOperators {

bool distinct::operator()(const ConfigMap &source, ConfigMap &target,
                          std::multimap<std::string, ConfigMapValue>::value_type &value) const {
    bool isMatch = target.find(value.first) == target.end();
    return isMatch;
}

key::key(ConfigMap::key_type k) noexcept: k(std::move(k)) {}

bool key::operator()(const ConfigMap &source, ConfigMap &target,
                     const std::multimap<std::string, ConfigMapValue>::value_type &value) const {
    return value.first == k;
}

bool any::operator()(const ConfigMap &source, ConfigMap &target,
                     std::multimap<std::string, ConfigMapValue>::value_type &value) const {
    return false;
}
}

namespace ConfigMapExtractors {

const ConfigMapExtractorBase &get_subject_id_extractor() {
    namespace ops = ConfigMapExtractorOperators;

    const static auto x = make_config_map_extractor(
            ops::allOf(
                    ops::keys("uuid", "realm"),
                    ops::when(
                            ops::key("uuid")
                    )(ops::distinct(),
                      ops::as<std::string>()),
                    ops::when(
                            ops::key("realm")
                    )(ops::distinct(),
                      ops::as<std::string>())));

    return x;
}

const ConfigMapExtractorBase &get_subject_id_with_variants_extractor() {
    namespace ops = ConfigMapExtractorOperators;

    const static auto x = make_config_map_extractor(
            ops::ofType<Variant>(),
            ops::allOf(
                    ops::keys("uuid", "realm"),
                    ops::when(
                            ops::key("uuid")
                    )(ops::distinct(),
                      ops::as<std::string>()),
                    ops::when(
                            ops::key("realm")
                    )(ops::distinct(),
                      ops::as<std::string>()))
    );

    return x;
}

const ConfigMapExtractorBase &get_normalized_subject_extractor() {
    namespace ops = ConfigMapExtractorOperators;

    const static auto x = make_config_map_extractor(
            ops::allOf(
                    ops::when(
                            ops::key("uuid")
                    )(ops::distinct(),
                      ops::as<std::string>()),
                    ops::when(
                            ops::key("realm")
                    )(ops::distinct(),
                      ops::as<std::string>())));

    return x;
}
}

}