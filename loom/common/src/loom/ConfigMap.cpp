//
// Created by void on 20/04/2020.
//

#include <loom/ConfigMap.h>
#include <loom/DataSource.h>
#include <cmath>


namespace loom {

std::ostream &operator<<(std::ostream &os, const ConfigMap &cm) {
    os << "{";

    bool first = true;

    for (auto &item : cm) {
        if (first) {
            first = false;
        } else {
            os << ",";
        }

        os << item;
    }

    os << "}";
    return os;
}

std::ostream &operator<<(std::ostream &os, const ConfigMap::value_type &cv) {
    os << cv.first << "=";
    os << cv.second;
    return os;
}

std::ostream &operator<<(std::ostream &os, const ConfigMapValue &cv) {
    std::visit([&](auto &&v) {
        using T = std::decay_t<decltype(v)>;
        os << v;
    }, cv);

    return os;
}

bool DataSourceInfo::operator==(const DataSourceInfo &other) const {
    return hash() == other.hash();
}

bool DataSourceInfo::operator<(const DataSourceInfo &other) const {
    return hash() < other.hash();
}

DataSourceInfo::hash_type DataSourceInfo::hash() const {
    return std::tie(length, sizeHint, accessType);
}

ConfigMapAttributeKey::ConfigMapAttributeKey(ConfigMap configMap, ConfigMap::key_type attribute)
        : configMap(std::move(configMap)), attribute(std::move(attribute)) {}

bool ConfigMapAttributeKey::operator<(const ConfigMapAttributeKey &other) const {
    return std::tie(configMap, attribute) < std::tie(other.configMap, other.attribute);
}

bool ConfigMapAttributeKey::operator==(const ConfigMapAttributeKey &other) const {
    return std::tie(configMap, attribute) == std::tie(other.configMap, other.attribute);
}

bool ConfigMapAttributeKey::operator==(const ConfigMap &cm) const {
    return configMap == cm;
}

bool ConfigMap::operator%=(const ConfigMap &other) const {
    for (auto &entry : other) {
        if (std::find(begin(), end(), entry) == end()) {
            // Found at least one key-value-pair that is not contained in this.
            return false;
        }
    }
    return true;
}

bool ConfigMap::operator^=(const ConfigMap &other) const {
    return !operator%=(other);
}

bool ConfigMap::operator==(const ConfigMap &other) const {
    if (other.size() != size()) {
        return false;
    }

    for (auto &entry : other) {
        if (std::find(begin(), end(), entry) == end()) {
            // Found at least one key-value-pair that is not contained in this.
            return false;
        }
    }

    return true;
}

bool ConfigMap::operator!=(const ConfigMap &other) const {
    return !operator==(other);
}

ConfigMap ConfigMap::normalized(std::initializer_list<ConfigMap::key_type> keys) const {
    ConfigMap result;

    for (auto &entry : *this) {
        for (auto &key : keys) {
            if (entry.first == key) {
                result.insert(entry);
                break;
            }
        }
    }

    return result;
}

ConfigMap &ConfigMap::normalize(std::initializer_list<ConfigMap::key_type> keys) {
    for (auto it = begin(); it != end();) {
        bool found = false;
        for (auto &key : keys) {
            if (it->first == key) {
                found = true;
                break;
            }
        }

        if (!found) {
            it = erase(it);
        } else {
            ++it;
        }
    }

    return *this;
}

ConfigMap &ConfigMap::operator-=(const ConfigMap &other) {
    for (auto &item : other) {
        auto range = equal_range(item.first);
        for (auto it = range.first; it != range.second;) {
            if (it->second == item.second) {
                it = erase(it);
            } else {
                ++it;
            }
        }
    }

    return *this;
}

ConfigMap ConfigMap::operator-(const ConfigMap &other) const {
    ConfigMap result;

    for (auto &item : *this) {
        auto range = other.equal_range(item.first);

        bool found = false;
        for (auto it = range.first; it != range.second; ++it) {
            if (it->second == item.second) {
                found = true;
                break;
            }
        }

        if (!found) {
            result.insert(item);
        }
    }

    return result;
}

ConfigMap &ConfigMap::operator+=(const ConfigMap &other) {
    for (auto &item : other) {
        auto range = equal_range(item.first);
        bool found = false;
        for (auto it = range.first; it != range.second; ++it) {
            if (it->second == item.second) {
                found = true;
                break;
            }
        }

        if (!found) {
            insert(item);
        }
    }

    return *this;
}

ConfigMap ConfigMap::operator+(const ConfigMap &other) const {
    ConfigMap result = *this;
    result += other;
    return result;
}

ConfigMap::iterator ConfigMap::findValue(
        const key_type &key,
        const mapped_type &value) {
    auto range = equal_range(key);
    while (range.first != range.second) {
        if (range.first->second == value) {
            return range.first;
        }
        range.first++;
    }

    return end();
}

ConfigMap::iterator ConfigMap::findValue(const value_type &value) {
    return findValue(value.first, value.second);
}

ConfigMap::const_iterator ConfigMap::findValue(
        const key_type &key,
        const mapped_type &value) const {
    auto range = equal_range(key);
    while (range.first != range.second) {
        if (range.first->second == value) {
            return range.first;
        }
        range.first++;
    }

    return cend();
}

ConfigMap::const_iterator ConfigMap::findValue(const value_type &value) const {
    return findValue(value.first, value.second);
}

}

