cmake_minimum_required(VERSION 3.13.2)

include(loom-build)

project(loom-common)
set(SOURCES
        src/loom/Buffer.cpp
        include/loom/Buffer.h

        src/loom/ChunkReader.cpp
        include/loom/ChunkReader.h

        src/loom/DataBlockReceiver.cpp
        include/loom/DataBlockReceiver.h

        src/loom/Exception.cpp
        include/loom/Exception.h

        src/loom/DataSource.cpp
        include/loom/DataSource.h

        src/loom/ConfigMap.cpp
        include/loom/ConfigMap.h

        src/loom/ConfigMapExtractor.cpp
        include/loom/ConfigMapExtractor.h

        src/loom/Variant.cpp
        include/loom/Variant.h

        include/loom/SubjectViewMode.h
        include/loom/SubjectAnnouncementFlags.h
        include/loom/SubjectQueryFlags.h
        include/loom/SubjectSubscriptionFlags.h
        )

add_library(
        loom-common
        STATIC
        ${SOURCES}
)

find_package(Boost REQUIRED COMPONENTS system)
target_link_libraries(loom-common PUBLIC Boost::boost Boost::system)

find_package(MessagePack REQUIRED)
target_link_libraries(loom-common PUBLIC MessagePack::MessagePack)

find_package(Threads REQUIRED)
target_link_libraries(loom-common PUBLIC Threads::Threads)

# DEBUG ONLY
option(LOOM_MANAGEDINSTANCE_DEBUG_TRACE "Enable debug tracing for ManagedInstance objects" OFF)
configure_file(loom-config.h.in loom-config.h @ONLY)
target_include_directories(loom-common PUBLIC ${CMAKE_CURRENT_BINARY_DIR})
if (LOOM_MANAGEDINSTANCE_DEBUG_TRACE)
    target_link_libraries(loom-common PUBLIC dl backtrace)
endif ()

loom_set_target_defaults(loom-common)

loom_setup_tests(
        test/ConfigMapTests.cpp
        test/ConfigMapExtractorTests.cpp
        test/DataBlockReceiverTest.cpp
        test/ChunkReaderTests.cpp
        test/FlagTests.cpp
        LIBS loom-common
)

include(GNUInstallDirs)

install(FILES
        include/loom/loom.h
        include/loom/BitFlags.h
        include/loom/ConfigMap.h
        include/loom/DataSource.h
        include/loom/Buffer.h
        include/loom/Variant.h
        include/loom/SubjectViewMode.h
        include/loom/Exception.h
        include/loom/SubjectAnnouncementFlags.h
        include/loom/SubjectQueryFlags.h
        include/loom/SubjectSubscriptionFlags.h
        DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/loom
        )
