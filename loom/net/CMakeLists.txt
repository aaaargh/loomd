cmake_minimum_required(VERSION 3.13.2)

include(loom-build)

project(loom-net)
set(SOURCES
        include/loom/net/ErrorCodes.h
        src/loom/net/ErrorCodes.cpp

        include/loom/net/Message.h

        include/loom/net/MessageHandler.h
        src/loom/net/MessageHandler.cpp

        include/loom/net/MessageHandlerRegistry.h

        include/loom/net/MessageParts.h

        include/loom/net/MessagePartTraits.h

        include/loom/net/MessageSerializer.h

        include/loom/net/MessageUtils.h

        include/loom/net/Packet.h
        src/loom/net/Packet.cpp

        include/loom/net/PackedMessage.h

        include/loom/net/ProtocolHandler.h
        src/loom/net/ProtocolHandler.cpp

        include/loom/net/SocketProtocolHandler.h

        include/loom/net/topics/Authentication.h
        include/loom/net/topics/DataRequest.h
        include/loom/net/topics/SubjectEvents.h

        src/loom/net/ConfigMapSerialization.cpp
        include/loom/net/ConfigMapSerialization.h

        include/loom/net/TypesSerialization.h
        )

add_library(
        loom-net
        STATIC
        ${SOURCES}
)

target_link_libraries(loom-net PUBLIC loom-common)

find_package(MessagePack REQUIRED)
target_link_libraries(loom-net PUBLIC MessagePack::MessagePack)

find_package(Threads REQUIRED)
target_link_libraries(loom-net PUBLIC Threads::Threads)

find_package(Loguru REQUIRED)
target_link_libraries(loom-net PUBLIC Loguru::Loguru)

loom_set_target_defaults(loom-net)

loom_setup_tests(
        test/ConfigMapSerializationTests.cpp
        test/MessageSerializerTests.cpp
        test/ProtocolTests.cpp
        LIBS loom-net
)
