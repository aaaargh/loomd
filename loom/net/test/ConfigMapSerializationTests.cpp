//
// Created by void on 01/06/2020.
//

#include <loom/testing.h>

#include <loom/ConfigMap.h>
#include <loom/net/ConfigMapSerialization.h>

#include <msgpack/adaptor/map.hpp>
#include <msgpack/adaptor/map_decl.hpp>

using namespace loom::testing;

namespace loom::testing {

BOOST_AUTO_TEST_SUITE(ConfigMapSerializationTests)

BOOST_AUTO_TEST_CASE(can_convert_to_and_back_from_msgpack) {
    DataSourceInfo dsInfo = {
            .length = 12345,
            .sizeHint = DataSourceInfo::SizeHint::Estimated,
            .accessType = DataSourceInfo::AccessType::Streamed,
    };

    ConfigMap cm{
            {"string",      "hello"},
            {"number",      (double) 123},
            {"data_source", dsInfo},
            {"variant",     Variant("a", "b", "c")}
    };

    msgpack::sbuffer sbuf;
    BOOST_REQUIRE_NO_THROW(msgpack::pack(sbuf, cm));

    auto oh = msgpack::unpack(sbuf.data(), sbuf.size());
    ConfigMap cm2;
    oh->convert(cm2);

    BOOST_REQUIRE_EQUAL(cm, cm2);
}

BOOST_AUTO_TEST_CASE(can_use_msgpack_for_variants) {
    ConfigMap cm{
            {"opts", Variant("a", "b", "c")}
    };

    msgpack::sbuffer sbuf;
    BOOST_REQUIRE_NO_THROW(msgpack::pack(sbuf, cm));

    auto oh = msgpack::unpack(sbuf.data(), sbuf.size());
    ConfigMap cm2;
    oh->convert(cm2);

    BOOST_REQUIRE_EQUAL(cm, cm2);
}

BOOST_AUTO_TEST_SUITE_END()

}
