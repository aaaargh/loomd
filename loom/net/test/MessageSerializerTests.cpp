//
// Created by void on 17/04/2020.
//

#include <loom/testing.h>

#include <loom/net/MessageSerializer.h>

using namespace loom::testing;

namespace loom::net::testing {

BOOST_AUTO_TEST_SUITE(MessageSerializerTests)

class TestMessage : public TypedMessage<0x123> {
public:
    bool bVar{};
    int iVar{};
    short sVar{};
    long lVar{};
    float fVar{};
    double dVar{};

    std::string stringVar = "a simple string";
    std::wstring wstringVar = L"a wide string";

    struct type_with_constructor {
        type_with_constructor() = delete;

        explicit type_with_constructor(int i) {}
    } sTypeWithConstructor{1};

    struct msgpack_intrusive_impl {
        int a = 100;
        MSGPACK_DEFINE (a);
    } sTypeWithIntrusiveMessagePackSupport{};

    bool operator==(const TestMessage &other) const {
        return bVar == other.bVar
               && iVar == other.iVar
               && sVar == other.sVar
               && lVar == other.lVar
               && fVar == other.fVar
               && dVar == other.dVar
               && stringVar == other.stringVar
               && wstringVar == other.wstringVar
               && sTypeWithIntrusiveMessagePackSupport.a == other.sTypeWithIntrusiveMessagePackSupport.a;
    }
};

template<typename T>
size_t getExpectedObjectSize(const T &val) {
    msgpack::sbuffer buf;
    msgpack::pack(buf, val);
    return buf.size();
}

const size_t testBufferSize = 65535;

[[nodiscard]] std::vector<char> quickSerialize(MessageSerializer &serializer, TestMessage &msg) {
    std::vector<char> buffer(testBufferSize);
    MutableBuffer buf(buffer.data(), buffer.size());

    auto written = serializer.serialize(buf, msg);

    BOOST_REQUIRE_LT(0, written);

    buffer.resize(written);
    return std::move(buffer);
}

BOOST_AUTO_TEST_CASE(message_size_is_correct_for_primitives) {
    TestMessage msg;

    class : public StaticMessageSerializer<
            message::MemberPart<&TestMessage::bVar>
    > {
    } boolSerializer;
    BOOST_REQUIRE_EQUAL(getExpectedObjectSize(msg.bVar), quickSerialize(boolSerializer, msg).size());

    class : public StaticMessageSerializer<
            message::MemberPart<&TestMessage::iVar>
    > {
    } intSerializer;
    BOOST_REQUIRE_EQUAL(getExpectedObjectSize(msg.iVar), quickSerialize(intSerializer, msg).size());

    class : public StaticMessageSerializer<
            message::MemberPart<&TestMessage::sVar>
    > {
    } shortSerializer;
    BOOST_REQUIRE_EQUAL(getExpectedObjectSize(msg.sVar), quickSerialize(shortSerializer, msg).size());

    class : public StaticMessageSerializer<
            message::MemberPart<&TestMessage::lVar>
    > {
    } longSerializer;
    BOOST_REQUIRE_EQUAL(getExpectedObjectSize(msg.lVar), quickSerialize(longSerializer, msg).size());

    class : public StaticMessageSerializer<
            message::MemberPart<&TestMessage::fVar>
    > {
    } floatSerializer;
    BOOST_REQUIRE_EQUAL(getExpectedObjectSize(msg.fVar), quickSerialize(floatSerializer, msg).size());

    class : public StaticMessageSerializer<
            message::MemberPart<&TestMessage::dVar>
    > {
    } doubleSerializer;
    BOOST_REQUIRE_EQUAL(getExpectedObjectSize(msg.dVar), quickSerialize(doubleSerializer, msg).size());
}

BOOST_AUTO_TEST_CASE(message_size_is_correct_for_strings) {
    TestMessage msg;

    class : public StaticMessageSerializer<
            message::MemberPart<&TestMessage::stringVar>
    > {
    } stringSerializer;
    BOOST_REQUIRE_EQUAL(getExpectedObjectSize(msg.stringVar), quickSerialize(stringSerializer, msg).size());

    class : public StaticMessageSerializer<
            message::MemberPart<&TestMessage::wstringVar>
    > {
    } wstringSerializer;
    BOOST_REQUIRE_EQUAL(getExpectedObjectSize(msg.wstringVar), quickSerialize(wstringSerializer, msg).size());
}

BOOST_AUTO_TEST_CASE(message_traits_for_custom_types_are_used) {
    TestMessage msg;

    static const std::string val = "This is the string to transmit";

    struct CustomSerializerTraits {
        static size_t getSerializedMemberSize(const TestMessage::type_with_constructor &t) {
            return getExpectedObjectSize(val);
        }

        static size_t serializeMember(const TestMessage::type_with_constructor &c, MutableBuffer &target) {
            return getExpectedObjectSize(val);
        }

        static size_t deserializeMember(TestMessage::type_with_constructor &c, const ImmutableBuffer &source) {
            return getExpectedObjectSize(val);
        }
    };

    class CustomSerializer : public StaticMessageSerializer<
            message::MemberPart<&TestMessage::sTypeWithConstructor, CustomSerializerTraits>
    > {

    } customSerializer;
    BOOST_REQUIRE_EQUAL(getExpectedObjectSize(val), quickSerialize(customSerializer, msg).size());
}

BOOST_AUTO_TEST_CASE(message_traits_for_serializable_types_are_used) {
    TestMessage msg;

    class CustomSerializer : public StaticMessageSerializer<
            message::MemberPart<&TestMessage::sTypeWithIntrusiveMessagePackSupport>
    > {

    } customSerializer;
    BOOST_REQUIRE_EQUAL(getExpectedObjectSize(msg.sTypeWithIntrusiveMessagePackSupport),
                        quickSerialize(customSerializer, msg).size());
}

BOOST_AUTO_TEST_CASE(can_use_custom_part_traits) {
    TestMessage msg;

    struct CustomTrait {
        static size_t getSerializedMemberSize(const TestMessage::msgpack_intrusive_impl &c) {
            return 1;
        }

        static size_t serializeMember(const TestMessage::msgpack_intrusive_impl &c, MutableBuffer &target) {
            return 1;
        }

        static size_t deserializeMember(TestMessage::msgpack_intrusive_impl &c, const ImmutableBuffer &source) {
            return 1;
        }
    };

    class CustomSerializer : public StaticMessageSerializer<
            message::MemberPart<&TestMessage::sTypeWithIntrusiveMessagePackSupport, CustomTrait> > {
    } customSerializer;

    BOOST_REQUIRE_EQUAL(1,
                        quickSerialize(customSerializer, msg).size());
}

BOOST_AUTO_TEST_CASE(can_read_single_part_in_packet) {
    class : public StaticMessageSerializer<
            message::MemberPart<&TestMessage::bVar>,
            message::MemberPart<&TestMessage::stringVar>,
            message::MemberPart<&TestMessage::wstringVar>
    > {
    } serializer;

    TestMessage msg;

    std::vector<char> buffer(testBufferSize);
    MutableBuffer buf(buffer.data(), buffer.size());

    BOOST_REQUIRE_LT(0, serializer.serialize(buf, msg));

    auto oh = msgpack::unpack(static_cast<const char *>(buffer.data()), buffer.size());
    bool b;
    oh->convert(b);

    BOOST_REQUIRE_EQUAL(msg.bVar, b);
}

BOOST_AUTO_TEST_CASE(can_read_multiple_parts_in_single_packet) {
    class : public StaticMessageSerializer<
            message::MemberPart<&TestMessage::bVar>,
            message::MemberPart<&TestMessage::stringVar>,
            message::MemberPart<&TestMessage::wstringVar>
    > {
    } serializer;

    TestMessage msg;

    auto buffer = quickSerialize(serializer, msg);

    auto oh = msgpack::unpack(buffer.data() + 1, buffer.size() - 1);
    std::string str;
    oh->convert(str);

    BOOST_REQUIRE_EQUAL(msg.stringVar, str);
}

BOOST_AUTO_TEST_CASE(can_read_whole_message_in_single_packet) {
    class : public StaticMessageSerializer<
            message::MemberPart<&TestMessage::bVar>,
            message::MemberPart<&TestMessage::stringVar>,
            message::MemberPart<&TestMessage::wstringVar>
    > {
    } serializer;

    TestMessage msg;

    std::vector<char> buffer(testBufferSize);
    MutableBuffer buf(buffer.data(), buffer.size());
    BOOST_REQUIRE_LT(0, serializer.serialize(buf, msg));
}

BOOST_AUTO_TEST_CASE(can_deserialize_message_from_single_packet) {
    class : public StaticMessageSerializer<
            message::MemberPart<&TestMessage::bVar>,
            message::MemberPart<&TestMessage::stringVar>,
            message::MemberPart<&TestMessage::wstringVar>
    > {
    } serializer;

    TestMessage msg;

    // Build the packet.
    std::vector<char> buffer(testBufferSize);
    MutableBuffer buf(buffer.data(), buffer.size());
    auto written = serializer.serialize(buf, msg);
    BOOST_REQUIRE_LT(0, written);

    TestMessage msg2;
    auto read = serializer.deserialize(msg2, ImmutableBuffer(buffer.data(), written));

    BOOST_REQUIRE(msg == msg2);
    BOOST_REQUIRE_EQUAL(read, written);
}

BOOST_AUTO_TEST_SUITE_END()

}
