//
// Created by void on 01/05/2020.
//

#include <loom/testing.h>

#include <loom/net/SocketProtocolHandler.h>
#include <loom/net/MessageHandlerRegistry.h>

#include <boost/asio/local/stream_protocol.hpp>
#include <boost/asio/local/connect_pair.hpp>

using namespace loom::testing;

namespace loom::net::testing {

BOOST_AUTO_TEST_SUITE(ProtocolTests)

struct TestMessage : public TypedMessage<0x1234> {
public:
    explicit TestMessage(size_t len = 0) {
        if (len == 0) {
            content = testContent;
        } else {
            size_t i = 0;;
            content.resize(len);

            while (i < len) {
                auto toWrite = std::min(sizeof(testContent), len - i);
                i += sizeof(testContent);
                std::copy(testContent, testContent + toWrite, content.data() + i);
            }
        }
    }

    std::string content;
    std::string otherContent;
    static const char *testContent;
};

const char *TestMessage::testContent = "Hello World!";

class TestSerializer : public MemberSerializer<&TestMessage::content, &TestMessage::otherContent> {
};

class TestMessageHandler : public TypedMessageHandler<TestMessage> {
public:
    [[nodiscard]] bool canSerialize(Message::id_type type) const override { return true; }

    [[nodiscard]] bool canDeserialize(Message::id_type type) const override { return true; }

    [[nodiscard]] const MessageSerializer &getTypedSerializer() const override {
        static const TestSerializer serializer;
        return serializer;
    }
};

class TestProtocolHandler
        : public LocalSocketProtocolHandler,
          public MessageHandlerRegistry<>,
          public ProtocolInvoker {
public:
    explicit TestProtocolHandler(io_context &ioContext) :
            LocalSocketProtocolHandler(*this, ioContext) {
        registerMessageHandler<TestMessageHandler>();
    }

protected:
    const MessageHandlerInvoker &getMessageHandlerInvoker() const override {
        return *this;
    }

    void handleMessageReceived(std::unique_ptr<net::Message> msg) override {}

    void handleDisconnect(const std::exception_ptr &error) override {}

public:
    void connect(socket_type &remote) {
        assert(!getSocket().is_open());
        boost::asio::local::connect_pair(getSocket(), remote);
        assert(getSocket().is_open());

        notifyConnected();
    }
};

BOOST_AUTO_TEST_CASE(can_use_unix_domain_sockets) {
    boost::asio::io_context ioContext;
    using socket_type = boost::asio::local::stream_protocol::socket;

    BOOST_CHECK_NO_THROW(auto handler = std::make_shared<TestProtocolHandler>(ioContext););
}

BOOST_AUTO_TEST_CASE(can_send_message) {
    boost::asio::io_context ioContext;
    using socket_type = boost::asio::local::stream_protocol::socket;

    auto handler = std::make_shared<TestProtocolHandler>(ioContext);

    socket_type remoteSocket(ioContext);
    handler->connect(remoteSocket);

    bool invoked = false;
    handler->sendMessage(std::make_unique<TestMessage>(), [&] { invoked = true; },
                         [&](const auto &err) {
                             BOOST_FAIL(get_exception_message(err));
                         });

    ioContext.run_one();
    BOOST_REQUIRE(invoked);

    handler->notifyClosed();
    ioContext.run();
}

BOOST_AUTO_TEST_CASE(can_send_big_message) {
    boost::asio::io_context ioContext;
    using socket_type = boost::asio::local::stream_protocol::socket;

    auto handler = std::make_shared<TestProtocolHandler>(ioContext);

    socket_type remoteSocket(ioContext);

    handler->connect(remoteSocket);

    auto msg = std::make_unique<TestMessage>(1024 * 1024);
    auto msgSize = msg->content.size();

    std::string receiveBuffer;
    receiveBuffer.resize(msg->content.size());
    size_t numReceived = 0;

    boost::asio::async_read(remoteSocket, boost::asio::buffer(receiveBuffer.data(), receiveBuffer.size()),
                            [&](const auto &ec, size_t read) {
                                numReceived = read;
                                throw std::runtime_error("Always look on the bright side of life.");
                            });

    bool invoked = false;
    handler->sendMessage(std::move(msg), [&] { invoked = true; },
                         [&](const auto &err) {
                             BOOST_FAIL(get_exception_message(err));
                         });

    try {
        ioContext.run();
    } catch (const std::runtime_error &e) {}

    BOOST_REQUIRE(invoked);
    BOOST_REQUIRE_EQUAL(msgSize, numReceived);

    handler->notifyClosed();
    ioContext.run();
}

BOOST_AUTO_TEST_CASE(can_receive_message) {
    boost::asio::io_context ioContext;
    using socket_type = boost::asio::local::stream_protocol::socket;

    class MyTestProtocolHandler : public TestProtocolHandler {
        using TestProtocolHandler::TestProtocolHandler;

        void handleMessageReceived(std::unique_ptr<Message> msg) override {
            received = std::move(msg);
        }

    public:
        std::unique_ptr<Message> received;
    };

    auto handler = std::make_shared<MyTestProtocolHandler>(ioContext);

    socket_type remoteSocket(ioContext);
    handler->connect(remoteSocket);

    auto msg = std::make_unique<TestMessage>();
    TestSerializer serializer;
    std::vector<char> buffer(1000);

    TestProtocolHandler::PacketHeader header{};

    MutableBuffer buf(buffer.data() + sizeof(header),
                      buffer.size() - sizeof(header));
    auto written = serializer.serialize(buf, *msg);

    header.length = boost::endian::native_to_little(written);
    header.messageType = boost::endian::native_to_little(msg->getType());

    std::memcpy(buffer.data(), &header, sizeof(header));

    boost::asio::write(remoteSocket, boost::asio::buffer(buffer.data(), header.length + sizeof(header)));

    ioContext.run_one(); // Send tick
    ioContext.run_one(); // Receive tick

    BOOST_REQUIRE(handler->received);
    BOOST_REQUIRE_EQUAL(msg->getType(), handler->received->getType());

    handler->notifyClosed();
    ioContext.run();
}

BOOST_AUTO_TEST_CASE(can_receive_multiple_messages) {
    boost::asio::io_context ioContext;
    using socket_type = boost::asio::local::stream_protocol::socket;

    struct MyTestProtocolHandler : public TestProtocolHandler {
        using TestProtocolHandler::TestProtocolHandler;

        const size_t numPacketsToSend = 10000;

        void handleMessageReceived(std::unique_ptr<Message> msg) override {
            if (++received == numPacketsToSend) {
                throw std::runtime_error("Don't worry, be happy.");
            }
        }

        size_t received = 0;
    };

    auto handler = std::make_shared<MyTestProtocolHandler>(ioContext);

    socket_type remoteSocket(ioContext);
    handler->connect(remoteSocket);

    auto msg = std::make_unique<TestMessage>();
    TestSerializer serializer;

    std::vector<char> buffer(1000);

    TestProtocolHandler::PacketHeader header{};

    MutableBuffer buf(buffer.data() + sizeof(header),
                      buffer.size() - sizeof(header));
    auto written = serializer.serialize(buf, *msg);

    header.length = boost::endian::native_to_little(written);
    header.messageType = boost::endian::native_to_little(msg->getType());

    std::memcpy(buffer.data(), &header, sizeof(header));

    size_t numSent = 0;

    for (size_t i = 0; i < handler->numPacketsToSend; ++i) {
        boost::asio::async_write(
                remoteSocket, boost::asio::buffer(buffer.data(), header.length + sizeof(header)),
                [&](const auto &ec, size_t sent) {
                    numSent++;
                });
        try {
            ioContext.run_one();
        } catch (const std::runtime_error &) {}
    }

    // Make sure all the packets have been transmitted.
    try {
        ioContext.run();
    } catch (const std::runtime_error &) {}
    handler->notifyClosed();

    BOOST_REQUIRE_EQUAL(handler->numPacketsToSend, handler->received);
}

BOOST_AUTO_TEST_SUITE_END()

}
