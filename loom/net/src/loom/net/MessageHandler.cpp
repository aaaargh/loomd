//
// Created by void on 02/04/2020.
//

#include <loom/net/MessageHandler.h>

namespace loom::net {

const size_t MessageHandler::DefaultPriority = 1000;

bool MessageHandler::canProcess(Message::id_type id) const {
    return false;
}

bool MessageHandler::canSerialize(Message::id_type id) const {
    return false;
}

bool MessageHandler::canDeserialize(Message::id_type) const {
    return false;
}

const MessageSerializer &MessageHandler::getSerializer(Message::id_type) const {
    throw std::logic_error("Cannot serialize this message");
}

size_t MessageHandler::getPriority() const {
    return DefaultPriority;
}

}
