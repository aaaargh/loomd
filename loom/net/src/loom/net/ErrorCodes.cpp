//
// Created by void on 03/04/2020.
//

#include <loom/net/ErrorCodes.h>

namespace loom::net::detail {

std::string MessageErrorCategory::message(int i) const {
    switch (static_cast<MessageError>(i)) {
        default:
        case MessageError::Success:
            return "success";
        case MessageError::InvalidPacketSize:
            return "packet size is invalid";
    }
}

const char *MessageErrorCategory::name() const noexcept {
    return "message_error";
}

}

std::error_code loom::net::make_error_code(loom::net::MessageError e) {
    static const detail::MessageErrorCategory cat{};
    return {static_cast<int>(e), cat};
}

