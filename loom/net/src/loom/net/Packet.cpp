//
// Created by void on 20/04/2020.
//

#include <loom/net/Packet.h>

namespace loom::net {

Packet Packet::makeTerminalPacket(sequence_id sequenceId) {
    return Packet{{sequenceId}};
}
}
