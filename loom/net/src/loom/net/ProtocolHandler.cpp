//
// Created by void on 02/05/2020.
//

#include <loom/net/ProtocolHandler.h>

namespace loom::net {

void ProtocolInvoker::DefaultSendMessageErrorCallback(const std::exception_ptr &) {}

void ProtocolInvoker::DefaultSendMessageSuccessCallback() {}

ProtocolHandler::ProtocolHandler(ProtocolInvoker &invoker) : protocolInvoker(invoker) {}

}