//
// Created by void on 26/04/2020.
//

#include <loom/net/ConfigMapSerialization.h>

#include <memory>

namespace loom::detail {
template<>
ValueType get_value_type<std::string>::operator()() const {
    return ValueType::String;
}

template<>
ValueType get_value_type<double>::operator()() const {
    return ValueType::Number;
}

template<>
ValueType get_value_type<DataSourceInfo>::operator()() const {
    return ValueType::DataSourceInfo;
}

template<>
ValueType get_value_type<Variant>::operator()() const {
    return ValueType::Variant;
}
}

namespace msgpack::adaptor {

/**
 * Adaptors for the ConfigMap type.
 */
const msgpack::object &convert<loom::ConfigMap>::operator()(const msgpack::object &o,
                                                            loom::ConfigMap &value) const {
    o.convert<loom::ConfigMap::map_type>(value);
    return o;
}

void object_with_zone<loom::ConfigMap>::operator()(msgpack::object::with_zone &o,
                                                   const loom::ConfigMap &value) const {
    object_with_zone<loom::ConfigMap::map_type>()(o, value);
}

/**
 * Adaptors for the Variant type.
 */
const msgpack::object &convert<loom::Variant>::operator()(const msgpack::object &o,
                                                          loom::Variant &value) const {
    if (o.type != msgpack::type::ARRAY) throw msgpack::type_error();

    auto ary = o.via.array;

    value.values.resize(ary.size);
    for (size_t i = 0; i < ary.size; ++i) {
        value.values[i] = ary.ptr[i].as<std::string>();
    }

    return o;
}

void object_with_zone<loom::Variant>::operator()(msgpack::object::with_zone &o, const loom::Variant &value) const {

}

/**
 * Adaptors for the DataSourceInfo type
 */
const msgpack::object &convert<loom::DataSourceInfo>::operator()(const msgpack::object &o,
                                                                 loom::DataSourceInfo &value) const {
    if (o.type != msgpack::type::ARRAY) throw msgpack::type_error();
    auto ary = o.via.array;
    if (ary.size != 3) throw msgpack::type_error();

    ary.ptr[0].convert(value.length);
    ary.ptr[1].convert(value.accessType);
    ary.ptr[2].convert(value.sizeHint);

    return o;
}

void object_with_zone<loom::DataSourceInfo>::operator()(msgpack::object::with_zone &o,
                                                        const loom::DataSourceInfo &value) const {
}

/**
 * Adaptors for the ConfigMapValue type
 */
const msgpack::object &convert<loom::ConfigMapValue>::operator()(const msgpack::object &o,
                                                                 loom::ConfigMapValue &value) const {
    if (o.type != msgpack::type::ARRAY) throw msgpack::type_error();
    auto ary = o.via.array;
    if (ary.size != 2) throw msgpack::type_error();

    auto type = ary.ptr[0].as<loom::detail::ValueType>();
    auto obj = ary.ptr[1];

    switch (type) {
        case loom::detail::ValueType::String: {
            std::string target;
            obj.convert(target);
            value = target;
            break;
        }
        case loom::detail::ValueType::Number: {
            double target;
            obj.convert(target);
            value = target;
            break;
        }
        case loom::detail::ValueType::DataSourceInfo: {
            loom::DataSourceInfo target;
            obj.convert(target);
            value = target;
            break;
        }
        case loom::detail::ValueType::Variant: {
            loom::Variant target;
            obj.convert(target);
            value = target;
            break;
        }
    }

    return o;
}

void object_with_zone<loom::ConfigMapValue>::operator()(msgpack::object::with_zone &o,
                                                        const loom::ConfigMapValue &value) const {
    std::visit([&](auto &&v) {
        using T = std::decay_t<decltype(v)>;
        object_with_zone<T>()(o, v);
    }, value);
}

}
