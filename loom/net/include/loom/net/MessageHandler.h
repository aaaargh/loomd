//
// Created by void on 02/04/2020.
//

#ifndef LOOM_NET_MESSAGEHANDLER_H
#define LOOM_NET_MESSAGEHANDLER_H

#include <loom/loom.h>

#include <loom/net/Message.h>
#include <loom/net/MessageSerializer.h>
#include <loom/net/MessageUtils.h>

#include <memory>

namespace loom::net {

/**
 * A handler that can be registered to a daemon for extending message support.
 */
class MessageHandler {
public:
    virtual ~MessageHandler() = default;

    /**
     * Checks whether this handler can process the given message.
     *
     * @param type The message type.
     * @return Whether this handler can process the message.
     */
    [[nodiscard]] virtual bool canProcess(Message::id_type type) const;

    /**
     * Checks whether this handler can serialize the given message type.
     *
     * @param type The message type.
     * @return Whether this handler can serialize this message type.
     */
    [[nodiscard]] virtual bool canSerialize(Message::id_type type) const;

    /**
     * Checks whether this handler can deserialize the given message type.
     *
     * @param type The message type.
     * @return Whether this handler can deserialize this message type.
     */
    [[nodiscard]] virtual bool canDeserialize(Message::id_type type) const;

    [[nodiscard]] virtual const MessageSerializer &getSerializer(Message::id_type type) const;

    static const size_t DefaultPriority;

    [[nodiscard]] virtual size_t getPriority() const;
};

template<typename MessageT, typename MessageHandlerT = MessageHandler>
class TypedMessageHandler : public virtual MessageHandlerT {
public:
    using handler_message_type = MessageT;

public:
    [[nodiscard]] virtual const MessageSerializer &getTypedSerializer() const {
        throw std::logic_error("Cannot serialize this message");
    }

public:
    [[nodiscard]] bool canProcess(Message::id_type type) const override {
        return is_message<handler_message_type>(type);
    }

    [[nodiscard]] bool canSerialize(Message::id_type type) const override {
        return is_message<handler_message_type>(type);
    }

    [[nodiscard]] bool canDeserialize(Message::id_type type) const override {
        return is_message<handler_message_type>(type);
    }

    [[nodiscard]] const MessageSerializer &getSerializer(Message::id_type type) const override {
        return getTypedSerializer();
    }
};

class MessageHandlerInvoker {
public:
    virtual ~MessageHandlerInvoker() = default;

public:
    [[nodiscard]] virtual const MessageHandler &findProcessingHandler(Message::id_type) const = 0;

    [[nodiscard]] virtual const MessageHandler &findSerializationHandler(Message::id_type) const = 0;

    [[nodiscard]] virtual const MessageHandler &findDeserializationHandler(Message::id_type) const = 0;
};

}

#endif //LOOM_NET_MESSAGEHANDLER_H
