//
// Created by void on 18/04/2020.
//

#ifndef LOOM_NET_MESSAGEPARTTRAITS_H
#define LOOM_NET_MESSAGEPARTTRAITS_H

#include <loom/Buffer.h>

#include <msgpack.hpp>

namespace loom::net::message::traits {

template<typename... Traits>
struct make_traits;

template<typename T0>
struct make_traits<T0> : T0 {
};

template<typename T0, typename... Traits>
struct make_traits<T0, Traits...> : T0, make_traits<Traits...> {
private:
    using T1 = make_traits<Traits...>;

public:
    using T0::serializeMember;
    using T1::serializeMember;

    using T0::deserializeMember;
    using T1::deserializeMember;

    using T0::getSerializedMemberSize;
    using T1::getSerializedMemberSize;
};

struct msgpack_any {
private:
    template<typename T, typename U = decltype(::msgpack::pack(std::declval<std::ostream &>(),
                                                               std::declval<const T &>()))>
    struct match_cond;

public:
    template<typename T, typename U = match_cond<T> >
    [[nodiscard]] static size_t serializeMember(const T &c, MutableBuffer &dest) {
        std::stringstream stream;
        msgpack::pack(stream, c);
        auto buf = stream.str();

        if (buf.size() > dest.size()) {
            throw std::overflow_error("Buffer overflow when serializing");
        }

        std::memcpy(dest.data(), buf.data(), buf.size());
        return buf.size();
    }

    template<typename T, typename U = match_cond<T> >
    [[nodiscard]] static size_t deserializeMember(T &target, const ImmutableBuffer &source) {
        msgpack::unpacker unpacker;
        msgpack::object_handle oh;

        unpacker.reserve_buffer(source.size());
        std::memcpy(unpacker.buffer(), source.data(), source.size());
        unpacker.buffer_consumed(source.size());
        if (!unpacker.next(oh)) {
            return 0;
        }

        oh->convert(target);
        return source.size() - unpacker.nonparsed_size();
    }

    template<typename T, typename U = match_cond<T> >
    [[nodiscard]] static size_t getSerializedMemberSize(const T &value) {
        std::stringstream stream;
        msgpack::pack(stream, value);
        auto buf = stream.str();

        return buf.size();
    }
};

struct msgpack_types {
    static size_t serializeMember(const msgpack::object_handle &oh,
                                  MutableBuffer &dest) {
        std::stringstream stream;
        msgpack::pack(stream, *oh);
        auto buf = stream.str();

        if (buf.size() > dest.size()) {
            throw std::overflow_error("Buffer overflow when serializing");
        }

        std::memcpy(dest.data(), buf.data(), buf.size());
        return buf.size();
    }

    [[nodiscard]] static size_t deserializeMember(msgpack::object_handle &target,
                                                  const ImmutableBuffer &source) {
        msgpack::unpacker unpacker;
        msgpack::object_handle oh;

        unpacker.reserve_buffer(source.size());
        std::memcpy(unpacker.buffer(), source.data(), source.size());
        unpacker.buffer_consumed(source.size());
        if (!unpacker.next(target)) {
            return 0;
        }

        return source.size() - unpacker.nonparsed_size();
    }

    [[nodiscard]] static size_t getSerializedMemberSize(const msgpack::object_handle &oh) {
        std::stringstream stream;
        msgpack::pack(stream, *oh);
        auto buf = stream.str();
        return buf.size();
    }
};

using msgpack = make_traits<msgpack_any, msgpack_types>;

using defaults = make_traits<msgpack>;

}

#endif //LOOM_NET_MESSAGEPARTTRAITS_H
