//
// Created by void on 18/04/2020.
//

#ifndef LOOM_NET_PACKET_H
#define LOOM_NET_PACKET_H

#include <loom/loom.h>

#include <vector>

namespace loom::net {

struct Packet {
    using sequence_id = unsigned char;

    struct __attribute__ ((packed)) {
        sequence_id sequenceId;
        size_t packetLength;
    } header{};

    std::vector<char> data{};

    static Packet makeTerminalPacket(sequence_id sequenceId);
};

}

#endif //LOOM_NET_PACKET_H
