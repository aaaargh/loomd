//
// Created by void on 03/04/2020.
//

#ifndef LOOM_NET_ERRORCODES_H
#define LOOM_NET_ERRORCODES_H

#include <loom/loom.h>

#include <type_traits>
#include <system_error>
#include <string>

namespace loom::net {

/**
 * Errors related to messaging.
 */
enum class MessageError {
    Success = 0, //< No error occured.
    InvalidPacketSize = 100, //< The packet size received is invalid.
};

namespace detail {
/**
 * The error category for MessageErrors.
 */
class MessageErrorCategory : public std::error_category {
public:
    [[nodiscard]] const char *name() const noexcept override;

    [[nodiscard]] std::string message(int i) const override;
};
}

/**
 * std::make_error_code Specialization for MessageErrors.
 * @param e The error.
 * @return The corresponding std::error_code.
 */
std::error_code make_error_code(MessageError e);
}

namespace std {
template<>
struct is_error_code_enum<loom::net::MessageError> : std::true_type {
};
}

#endif //LOOM_NET_ERRORCODES_H
