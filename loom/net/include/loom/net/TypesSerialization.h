//
// Created by void on 01/06/2020.
//

#ifndef LOOM_NET_TYPESSERIALIZATION_H
#define LOOM_NET_TYPESSERIALIZATION_H

#include <loom/SubjectAnnouncementFlags.h>
#include <loom/SubjectQueryFlags.h>
#include <loom/SubjectSubscriptionFlags.h>
#include <loom/SubjectViewMode.h>
#include <loom/net/ConfigMapSerialization.h>

#include <msgpack.hpp>

MSGPACK_ADD_ENUM(loom::SubjectAnnouncementFlags);
MSGPACK_ADD_ENUM(loom::SubjectQueryFlags);
MSGPACK_ADD_ENUM(loom::SubjectSubscriptionFlags);
MSGPACK_ADD_ENUM(loom::SubjectViewMode);

#endif //LOOM_NET_TYPESSERIALIZATION_H
