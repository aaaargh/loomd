//
// Created by void on 26/04/2020.
//

#ifndef LOOM_NET_PACKEDMESSAGE_H
#define LOOM_NET_PACKEDMESSAGE_H

#include <loom/loom.h>

#include <loom/net/Message.h>

#include <msgpack.hpp>

namespace loom::net {

class PackedMessage : public Message {
public:
    id_type id = 0;
    std::vector<msgpack::object_handle> objects;

public:
    [[nodiscard]] id_type getType() const override {
        return id;
    }

private:
    void initialize() {}

    template<typename T0, typename... Types>
    void initialize(T0 &&object, Types &&... tail) {
        auto zone = std::make_unique<msgpack::zone>();
        msgpack::object o(object, *zone);
        objects.emplace_back(msgpack::object_handle(o, std::move(zone)));
        initialize(tail...);
    }

public:
    [[nodiscard]] static std::unique_ptr<PackedMessage> create(id_type id,
                                                               std::initializer_list<const msgpack::object> &objects) {
        auto msg = std::make_unique<PackedMessage>();

        for (auto &object : objects) {
            auto zone = std::make_unique<msgpack::zone>();

            msg->id = id;
            msg->objects.emplace_back(msgpack::object_handle(object, std::move(zone)));
        }

        return std::move(msg);
    }

    template<typename... Types>
    [[nodiscard]] static std::unique_ptr<PackedMessage> create(id_type id, Types &&... objects) {
        auto msg = std::make_unique<PackedMessage>();

        msg->id = id;
        msg->initialize(objects...);

        return std::move(msg);
    }
};
}

#endif //LOOM_NET_PACKEDMESSAGE_H
