//
// Created by void on 02/04/2020.
//

#ifndef LOOM_NET_MESSAGEUTILS_H
#define LOOM_NET_MESSAGEUTILS_H

#include <loom/net/Message.h>

namespace loom::net {

/**
 * Convenience method that allows quick tests for method types.
 * @tparam MessageT The expected message type. Must provide a static field named 'type' (e.g. a TypedMessage).
 * @param message The message to test.
 * @return Whether message is of the given class.
 */
template<typename MessageT>
bool is_message(const Message &message) {
    return message.getType() == MessageT::type;
}

/**
 * Convenience method that allows quick tests for method types.
 * @tparam MessageT The expected message type. Must provide a static field named 'type' (e.g. a TypedMessage).
 * @param type The message type to test for.
 * @return Whether message is of the given class.
 */
template<typename MessageT>
bool is_message(Message::id_type type) {
    return type == MessageT::type;
}

}

#endif //LOOM_NET_MESSAGEUTILS_H
