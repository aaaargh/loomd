//
// Created by void on 12/03/2020.
//

#ifndef LOOM_NET_MESSAGE_H
#define LOOM_NET_MESSAGE_H

#include <loom/loom.h>

#include <cstddef>
#include <vector>
#include <boost/uuid/uuid.hpp>
#include <boost/uuid/random_generator.hpp>

namespace loom::net {

class MessageSerializer;

/**
 * Basic implementation of a message transported to / from a peer that is connected to the daemon.
 */
class Message {
public:
    using id_type = std::size_t;

public:
    virtual ~Message() = default;

    /**
     * @return The type of this message.
     */
    [[nodiscard]] virtual id_type getType() const = 0;
};

/**
 * A message class that provides a static type.
 * @tparam Type The type of the message.
 */
template<Message::id_type Type, typename MessageT = Message>
class TypedMessage : public MessageT {
public:
    using message_type = TypedMessage<Type, MessageT>;
    using base_type = MessageT;
    using id_type = typename MessageT::id_type;
    constexpr static const id_type type = Type;

public:
    using base_type::base_type;

public:
    [[nodiscard]] id_type getType() const override {
        return Type;
    }
};

}

#endif //LOOM_NET_MESSAGE_H
