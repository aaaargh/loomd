//
// Created by void on 05/05/2020.
//

#ifndef LOOM_NET_TOPICS_AUTHENTICATION_H
#define LOOM_NET_TOPICS_AUTHENTICATION_H

#include <loom/loom.h>

#include <loom/net/Message.h>
#include <loom/net/MessageSerializer.h>

#include <loom/ConfigMap.h>

namespace loom::net::topics {

constexpr Message::id_type MT_AuthRequest = 0xA0;
constexpr Message::id_type MT_AuthResponse = 0xA1;

/**
 * The requesting side starts the conversation by asking for data using this message.
 */
struct AuthRequest : TypedMessage<MT_AuthRequest> {
    ConfigMap identity;
};

class AuthRequestSerializer : public MemberSerializer<
        &AuthRequest::identity> {
};

struct AuthResponse : TypedMessage<MT_AuthResponse> {
    std::string error;
    ConfigMap identity;
};

class AuthResponseSerializer : public MemberSerializer<
        &AuthResponse::error,
        &AuthResponse::identity> {
};

}

#endif //LOOM_NET_TOPICS_AUTHENTICATION_H
