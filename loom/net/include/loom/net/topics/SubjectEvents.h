//
// Created by void on 05/05/2020.
//

#ifndef LOOM_NET_TOPICS_SUBJECTEVENTS_H
#define LOOM_NET_TOPICS_SUBJECTEVENTS_H

#include <loom/loom.h>

#include <loom/net/Message.h>
#include <loom/net/MessageSerializer.h>
#include <loom/SubjectSubscriptionFlags.h>

#include <loom/ConfigMap.h>

#include <msgpack.hpp>

namespace loom::net::topics {

constexpr Message::id_type MT_SubjectSubscription = 0xC0;
constexpr Message::id_type MT_SubjectAnnounced = 0xC11;
constexpr Message::id_type MT_SubjectDropped = 0xC12;

/**
 * Message sent by the client to subscribe to subjects in the engine.
 */
struct SubjectSubscription : TypedMessage<MT_SubjectSubscription> {
    std::string filter;
    SubjectSubscriptionFlags flags = SubjectSubscriptionFlags::Defaults;
};

class SubjectSubscriptionSerializer : public MemberSerializer<
        &SubjectSubscription::filter,
        &SubjectSubscription::flags> {
};

/**
 * Event that is sent when a new subject has been announced that matches the clients subscription criteria.
 */
struct SubjectAnnouncedEvent : TypedMessage<MT_SubjectAnnounced> {
    ConfigMap subjectId;
    bool isUpdated{};
};

class SubjectAnnouncedEventSerializer : public MemberSerializer<
        &SubjectAnnouncedEvent::subjectId,
        &SubjectAnnouncedEvent::isUpdated> {
};

/**
 * Event that is sent when a subscribed subject has been dropped.
 */
struct SubjectDroppedEvent : TypedMessage<MT_SubjectDropped> {
    ConfigMap subjectId;
};

class SubjectDroppedEventSerializer : public MemberSerializer<
        &SubjectDroppedEvent::subjectId> {
};

}

#endif //LOOM_NET_TOPICS_SUBJECTEVENTS_H
