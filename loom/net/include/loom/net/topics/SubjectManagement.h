//
// Created by void on 05/05/2020.
//

#ifndef LOOM_NET_TOPICS_SUBJECTMANAGEMENT_H
#define LOOM_NET_TOPICS_SUBJECTMANAGEMENT_H

#include <loom/loom.h>

#include <loom/net/Message.h>
#include <loom/net/MessageSerializer.h>
#include <loom/SubjectAnnouncementFlags.h>
#include <loom/SubjectQueryFlags.h>

#include <loom/ConfigMap.h>

#include <functional>
#include <vector>
#include <loom/SubjectViewMode.h>

namespace loom::net::topics {

constexpr Message::id_type MT_SubjectAnnouncement = 0xA10;
constexpr Message::id_type MT_SubjectDrop = 0xA11;

constexpr Message::id_type MT_SubjectQuery = 0xA20;
constexpr Message::id_type MT_SubjectQueryResults = 0xA21;

/**
 * Message sent by the client to announce a local subject to the engine.
 */
struct SubjectAnnouncement : TypedMessage<MT_SubjectAnnouncement> {
    ConfigMap subject;
    SubjectAnnouncementFlags flags = SubjectAnnouncementFlags::Defaults;
};

class SubjectAnnouncementSerializer : public MemberSerializer<
        &SubjectAnnouncement::subject,
        &SubjectAnnouncement::flags> {
};

/**
 * Message sent by the client to drop a local subject from the engine.
 */
struct SubjectDrop : TypedMessage<MT_SubjectDrop> {
    ConfigMap subjectId;
};

class SubjectDropSerializer : public MemberSerializer<
        &SubjectDrop::subjectId> {
};

/**
 * Message sent by the client to query for subjects in the engine.
 */
struct SubjectQuery : TypedMessage<MT_SubjectQuery> {
    std::string query;
    SubjectViewMode viewMode;
    SubjectQueryFlags flags = SubjectQueryFlags::Defaults;

    // Handler payload
    using ResultsCallback = std::function<void(const std::vector<ConfigMap> &)>;
    ResultsCallback resultsCallback;
};

class SubjectQuerySerializer : public MemberSerializer<
        &SubjectQuery::query,
        &SubjectQuery::viewMode,
        &SubjectQuery::flags> {
};

/**
 * Message sent by the daemon that contains the results of a subject query.
 */
struct SubjectQueryResults : TypedMessage<MT_SubjectQueryResults> {
    std::string query;
    SubjectViewMode viewMode;
    std::string error;
    std::vector<ConfigMap> results;
};

class SubjectQueryResultsSerializer : public MemberSerializer<
        &SubjectQueryResults::query,
        &SubjectQueryResults::viewMode,
        &SubjectQueryResults::error,
        &SubjectQueryResults::results> {
};

}

#endif //LOOM_NET_TOPICS_SUBJECTMANAGEMENT_H
