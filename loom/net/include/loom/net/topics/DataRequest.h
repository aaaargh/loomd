//
// Created by void on 29/04/2020.
//

#ifndef LOOM_NET_TOPICS_DATAREQUEST_H
#define LOOM_NET_TOPICS_DATAREQUEST_H

#include <loom/loom.h>
#include <loom/DataSource.h>

#include <loom/net/Message.h>
#include <loom/net/MessageSerializer.h>

#include <loom/ConfigMap.h>

namespace loom::net::topics {

constexpr Message::id_type MT_DataRequest = 0xE0;
constexpr Message::id_type MT_DataResponse = 0xE1;
constexpr Message::id_type MT_DataResponseTerminal = 0xE2;

/**
 * The requesting side starts the conversation by asking for data using this message.
 */
struct DataRequest : TypedMessage<MT_DataRequest> {
    short requestId;
    ConfigMap subjectId;
    ConfigMap::key_type attribute;
    size_t offset;
    size_t len;

    // Handler payload
    std::shared_ptr<DataSourceWriter> writer;
};

class DataRequestSerializer : public MemberSerializer<
        &DataRequest::requestId,
        &DataRequest::subjectId,
        &DataRequest::attribute,
        &DataRequest::offset,
        &DataRequest::len> {
};

/**
 * This is the response sent by the remote containing the requested data.
 */
struct DataResponse : TypedMessage<MT_DataResponse> {
    decltype(DataRequest::requestId) requestId{};
    ConfigMap subjectId;
    ConfigMap::key_type attribute;
    size_t offset;
    using data_buffer_type = std::vector<std::byte>;
    data_buffer_type data;
    bool isEOF = false;
};

class DataResponseSerializer : public MemberSerializer<
        &DataResponse::requestId,
        &DataResponse::subjectId,
        &DataResponse::attribute,
        &DataResponse::offset,
        &DataResponse::data,
        &DataResponse::isEOF> {
};

/**
 * This is the terminal signal sent after a request has been fully answered.
 */
struct DataResponseTerminal : TypedMessage<MT_DataResponseTerminal> {
    decltype(DataRequest::requestId) requestId{};
};

class DataResponseTerminalSerializer : public MemberSerializer<
        &DataResponseTerminal::requestId> {
};

}

#endif //LOOM_NET_TOPICS_DATAREQUEST_H
