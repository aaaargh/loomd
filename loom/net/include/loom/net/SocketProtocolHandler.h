//
// Created by void on 08/05/2020.
//

#ifndef LOOM_NET_SOCKETPROTOCOLHANDLER_H
#define LOOM_NET_SOCKETPROTOCOLHANDLER_H

#include <loom/net/ProtocolHandler.h>

#include <boost/asio/io_context.hpp>
#include <boost/asio/write.hpp>
#include <boost/asio/read.hpp>
#include <boost/asio/dispatch.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/local/stream_protocol.hpp>

#include <boost/endian/conversion.hpp>

#include <exception>
#include <vector>
#include <functional>
#include <condition_variable>
#include <queue>

#include <loguru.hpp>

// #define LOOM_SOCKETPROTOCOLHANDLER_DEBUG

namespace loom::net {

template<typename SocketT>
class SocketProtocolHandler : public ProtocolHandler {

public:
    using type = SocketProtocolHandler<SocketT>;
    using io_context = boost::asio::io_context;
    using buffer_type = boost::asio::const_buffer;
    using internal_buffer_type = std::vector<char>;
    using socket_type = SocketT;
    using send_callback = std::function<void()>;

public:
    struct __attribute__ ((packed)) PacketHeader {
        char packetId;
        size_t length;
        Message::id_type messageType;
    };

public:
    explicit SocketProtocolHandler(
            ProtocolInvoker &invoker,
            io_context &ioContext
    ) : ProtocolHandler(invoker),
        socket(ioContext) {};

    ~SocketProtocolHandler() override {
        LOG_S(INFO) << "Shutting down protocol handler.";
    }

    [[nodiscard]] socket_type &getSocket() {
        return socket;
    }

public:
    void sendMessage(std::unique_ptr<Message> message,
                     const SendMessageSuccessCallback &successCallback,
                     const SendMessageErrorCallback &errorCallback) override {
        auto &handler = protocolInvoker.getMessageHandlerInvoker().findSerializationHandler(message->getType());
        auto &serializer = handler.getSerializer(message->getType());
        auto size = serializer.getSerializedSize(*message);

        internal_buffer_type buffer(size + sizeof(PacketHeader));
        MutableBuffer buf(buffer.data() + sizeof(PacketHeader), size);
        auto written = serializer.serialize(buf, *message);

        assert(size == written);

        // Create the packet header.
        PacketHeader header{
                .packetId = nextPacketId++,
                .length = boost::endian::native_to_little(written),
                .messageType = boost::endian::native_to_little(message->getType())
        };
        std::memcpy(buffer.data(), &header, sizeof(PacketHeader));

        SendBufferItem item{
                .data = std::move(buffer),
                .successCallback = successCallback,
                .errorCallback = errorCallback
        };

        std::unique_lock lk(sendBufferQMutex);
        sendBufferQ.emplace(std::move(item));
        lk.unlock();

        if (!isSendDispatched.exchange(true)) {
#ifdef LOOM_SOCKETPROTOCOLHANDLER_DEBUG
            DLOG_S(INFO) << "Send queue processing started.";
#endif
            processSendQueue();
        }
    }

    void notifyClosed() override {
        if (connected) {
            connected = false;

            std::unique_lock lk(sendBufferQMutex);
            while (!sendBufferQ.empty()) {
                sendBufferQ.pop();
            }
            lk.unlock();

            if (socket.is_open()) {
                socket.close();
            }
        }
    }

    void notifyConnected() override {
        {
            boost::asio::socket_base::receive_buffer_size option;
            socket.get_option(option);
            receiveBufferSize = option.value();
        }

        {
            boost::asio::socket_base::send_buffer_size option;
            socket.get_option(option);
            sendBufferSize = option.value();
        }

        connected = true;

        startReceive();
    }

private:
    void startReceive() {
        receiveBuffer.resize(sizeof(PacketHeader));

#ifdef LOOM_SOCKETPROTOCOLHANDLER_DEBUG
        DLOG_S(INFO) << "Waiting for packet header....";
#endif
        boost::asio::async_read(
                socket,
                boost::asio::buffer(receiveBuffer.data(), sizeof(PacketHeader)),
                [this, self = shared_from_this()](const auto &errorCode, size_t read) {
                    if (!connected) {
                        DLOG_S(INFO) << "Connection aborted.";
                        return;
                    }

                    // Read the packet header.
                    if (errorCode) {
                        std::exception_ptr exceptionPtr;

                        if (errorCode != boost::asio::error::eof &&
                            errorCode != boost::asio::error::operation_aborted) {
                            exceptionPtr = std::make_exception_ptr(errorCode);
                        }

                        //DLOG_S(INFO) << "Error receiving packet header: " << errorCode.message();
                        if (connected) {
                            protocolInvoker.handleDisconnect(std::exception_ptr{});
                        }
                        return;
                    }

                    std::memcpy(&receivedHeader, receiveBuffer.data(), sizeof(PacketHeader));
                    receivedHeader.packetId = boost::endian::little_to_native(receivedHeader.packetId);
                    receivedHeader.length = boost::endian::little_to_native(receivedHeader.length);
                    receivedHeader.messageType = boost::endian::little_to_native(receivedHeader.messageType);

#ifdef LOOM_SOCKETPROTOCOLHANDLER_DEBUG
                    DLOG_S(INFO) << "Received packet header. ID := " << receivedHeader.packetId << ", message type := "
                                 << receivedHeader.messageType << ", message length := " << receivedHeader.length;
#endif

                    receiveBuffer.resize(receivedHeader.length);
                    receiveMessageBody(boost::asio::buffer(receiveBuffer.data(), receivedHeader.length));
                });
    }

    void receiveMessageBody(boost::asio::mutable_buffer buffer) {

        if (buffer.size() == 0) {
            // Message is complete.

            auto &serializer = protocolInvoker.getMessageHandlerInvoker().findDeserializationHandler(
                    receivedHeader.messageType).getSerializer(receivedHeader.messageType);
            auto msg = serializer.constructEmptyMessage(receivedHeader.messageType, std::any());

            try {
                auto readMessage = serializer.deserialize(*msg,
                                                          ImmutableBuffer(receiveBuffer.data(), receivedHeader.length));

                //DLOG_S(INFO) << "Message is complete";

                receiveBuffer.clear();
            } catch (const std::exception &error) {
                DLOG_S(INFO) << "Failed de-serializing message: " << error.what();
                if (connected) {
                    protocolInvoker.handleDisconnect(std::make_exception_ptr(error));
                }
                return;
            }

            protocolInvoker.handleMessageReceived(std::move(msg));

            startReceive();
            return;
        }

        auto self = shared_from_this();

        auto toRead = std::min(buffer.size(), receiveBufferSize);

#ifdef LOOM_SOCKETPROTOCOLHANDLER_DEBUG
        DLOG_S(INFO) << "Receiving " << toRead << " byte(s) of message body.";
#endif
        boost::asio::async_read(
                socket,
                boost::asio::buffer(buffer.data(), toRead),
                [this, buffer, self](const auto &errorCode, size_t read) {
                    if (!connected) {
                        DLOG_S(INFO) << "Connection aborted.";
                        return;
                    }

                    if (errorCode) {
                        std::exception_ptr exceptionPtr;

                        if (errorCode != boost::asio::error::eof &&
                            errorCode != boost::asio::error::operation_aborted) {
                            exceptionPtr = std::make_exception_ptr(errorCode);
                        }

                        DLOG_S(INFO) << "Failed receiving message body: " << errorCode.message();
                        if (connected) {
                            protocolInvoker.handleDisconnect(exceptionPtr);
                        }
                        return;
                    }

                    receiveMessageBody(buffer + read);
                });
    }

private:
    struct SendBufferItem {
        internal_buffer_type data;
        SendMessageSuccessCallback successCallback;
        SendMessageErrorCallback errorCallback;
    };

    void writeItem(SendBufferItem item, size_t offset = 0) {
        auto toSend = std::min(sendBufferSize, item.data.size() - offset);
        auto buf = boost::asio::buffer(item.data.data() + offset, toSend);

        if (toSend == 0) {
            // Done.
            if (item.successCallback) {
                item.successCallback();
            }

            std::unique_lock lk(sendBufferQMutex);
            if (sendBufferQ.empty()) {
                // That's all for now.
                isSendDispatched = false;
            } else {
                lk.unlock();
                processSendQueue();
            }
            return;
        }

#ifdef LOOM_SOCKETPROTOCOLHANDLER_DEBUG
        DLOG_S(INFO) << "Sending " << toSend << " byte(s) of data (offset := " << offset << ")";
#endif
        boost::asio::async_write(
                socket,
                buf,
                [this, offset, self = shared_from_this(), item = std::move(item)](const auto &errorCode,
                                                                                  size_t written) mutable {
                    if (!connected) {
                        DLOG_S(INFO) << "Connection aborted.";
                        return;
                    }

                    if (errorCode) {
                        std::exception_ptr exceptionPtr;

                        if (errorCode != boost::asio::error::eof &&
                            errorCode != boost::asio::error::operation_aborted) {
                            exceptionPtr = std::make_exception_ptr(errorCode);
                        }

                        DLOG_S(INFO) << "Failed sending packet: " << errorCode.message();
                        if (connected) {
                            protocolInvoker.handleDisconnect(exceptionPtr);
                        }
                        return;
                    }

#ifdef LOOM_SOCKETPROTOCOLHANDLER_DEBUG
                        DLOG_S(INFO) << "Sent " << written << " byte(s) of data (offset := " << offset << ")";
#endif

                    assert(connected);
                    protocolInvoker.handlePacketSent(written);
                    writeItem(std::move(item), offset + written);
                }
        );
    }

    void processSendQueue() {
        std::unique_lock lk(sendBufferQMutex);
        SendBufferItem item = sendBufferQ.front();
        sendBufferQ.pop();
        lk.unlock();
        writeItem(std::move(item));
    }

private:
    socket_type socket;
    bool connected = false;

    size_t sendBufferSize{};
    std::atomic_bool isSendDispatched = false;

    PacketHeader receivedHeader;
    size_t receiveBufferSize{};
    internal_buffer_type receiveBuffer;

    mutable std::mutex sendBufferQMutex;
    std::queue<SendBufferItem> sendBufferQ;

    char nextPacketId = 0;
};

using TCPSocketProtocolHandler = SocketProtocolHandler<boost::asio::ip::tcp::socket>;
using LocalSocketProtocolHandler = SocketProtocolHandler<boost::asio::local::stream_protocol::socket>;

}

#endif //LOOM_NET_SOCKETPROTOCOLHANDLER_H
