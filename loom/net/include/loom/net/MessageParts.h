//
// Created by void on 18/04/2020.
//

#ifndef LOOM_NET_MESSAGEPARTS_H
#define LOOM_NET_MESSAGEPARTS_H

#include <loom/Buffer.h>
#include <loom/net/MessagePartTraits.h>

#include <boost/mpl/vector.hpp>
#include <boost/mpl/for_each.hpp>

#include <ostream>

namespace loom::net::message {

namespace detail {

template<auto PtrT, typename MessagePartT>
class BasicMemberMessagePart;

template<typename MessageClassT, typename MemberT, MemberT(MessageClassT::*PtrT), typename MessagePartT>
class BasicMemberMessagePart<PtrT, MessagePartT> {
public:
    BasicMemberMessagePart() = delete;

public:
    using type = MessagePartT;
    using message_type = MessageClassT;
    using member_type = MemberT;

public:
    [[nodiscard]] static size_t serialize(const message_type &message,
                                          MutableBuffer &dest) {
        const member_type &member = message.*PtrT;
        auto written = type::serializeMember(member, dest);
        return written;
    }

    [[nodiscard]] static size_t deserialize(message_type &message,
                                            const ImmutableBuffer &source) {
        member_type &member = message.*PtrT;
        return type::deserializeMember(member, source);
    }

    [[nodiscard]] static size_t getSerializedSize(const message_type &message) {
        const member_type &member = message.*PtrT;
        return type::getSerializedMemberSize(member);
    }
};

}

template<typename P0, typename... PartsT>
class PartGroup {
public:
    PartGroup() = delete;

public:
    constexpr static size_t part_count = sizeof...(PartsT) + 1;
    using message_type = typename P0::message_type;

private:
    template<typename T>
    struct PartWrapper {
        using type = T;
    };

    using Parts = boost::mpl::vector<
            PartWrapper<P0>,
            PartWrapper<PartsT>...
    >;

    static constexpr size_t numParts = boost::mpl::size<Parts>::value;

public:
    [[nodiscard]] static size_t deserialize(Message &message,
                                            const ImmutableBuffer &source) {
        auto &msg = static_cast<message_type &>(message);
        size_t cursor = 0;

        boost::mpl::for_each<Parts>([&](auto t) {
            using PartT = typename decltype(t)::type;
            auto partBuffer = source + cursor;

            // This may throw an overflow_error when there is not enough free buffer space.
            auto read = PartT::deserialize(msg, partBuffer);
            cursor += read;
        });

        return cursor;
    }

    [[nodiscard]] static size_t serialize(const Message &message,
                                          MutableBuffer &dest) {
        const auto &msg = static_cast<const message_type &>(message);
        size_t cursor = 0;

        boost::mpl::for_each<Parts>([&](auto t) {
            using PartT = typename decltype(t)::type;

            MutableBuffer partBuffer = dest + cursor;

            // This may throw an overflow_error when there is not enough free buffer space.
            size_t written = PartT::serialize(msg, partBuffer);

            cursor += written;
        });

        return cursor;
    }

    [[nodiscard]] static size_t getSerializedSize(const Message &message) {
        const auto &msg = static_cast<const message_type &>(message);
        size_t size = 0;

        boost::mpl::for_each<Parts>([&](auto t) {
            using PartT = typename decltype(t)::type;

            size += PartT::getSerializedSize(msg);
        });

        return size;
    }
};

template<auto PtrT, typename Traits = message::traits::make_traits<message::traits::defaults> >
struct MemberPart : detail::BasicMemberMessagePart<PtrT, MemberPart<PtrT, Traits> >,
                    Traits {
public:
    MemberPart() = delete;
};

}

#endif //LOOM_MESSAGEPARTS_H
