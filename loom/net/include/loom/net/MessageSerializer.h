//
// Created by void on 17/04/2020.
//

#ifndef LOOM_NET_MESSAGESERIALIZER_H
#define LOOM_NET_MESSAGESERIALIZER_H

#include <loom/loom.h>

#include <loom/Buffer.h>

#include <loom/net/Message.h>
#include <loom/net/MessageParts.h>
#include <loom/net/MessagePartTraits.h>

#include <boost/asio/buffer.hpp>

#include <msgpack.hpp>
#include <sstream>
#include <memory>
#include <any>

#include <loom/net/TypesSerialization.h>

namespace loom::net {

class MessageSerializer {
public:
    [[nodiscard]] virtual size_t serialize(MutableBuffer &buffer,
                                           const Message &message) const = 0;

public:
    [[nodiscard]] virtual size_t deserialize(Message &targetMessage,
                                             const ImmutableBuffer &buffer) const = 0;

    [[nodiscard]] virtual std::unique_ptr<Message> constructEmptyMessage(Message::id_type,
                                                                         const std::any &userData) const = 0;

    [[nodiscard]] virtual size_t getSerializedSize(const Message &message) const = 0;
};

template<typename ... PartsT>
class StaticMessageSerializer : public MessageSerializer {
private:
    using part_group = message::PartGroup<PartsT...>;

public:
    using message_type = typename part_group::message_type;

public:
    [[nodiscard]] size_t serialize(MutableBuffer &buffer,
                                   const Message &message) const override {
        return part_group::serialize(message, buffer);
    }

    [[nodiscard]] size_t deserialize(Message &targetMessage, const ImmutableBuffer &buffer) const override {
        return part_group::deserialize(targetMessage, buffer);
    }

    [[nodiscard]] size_t getSerializedSize(const Message &message) const override {
        return part_group::getSerializedSize(message);
    }

    [[nodiscard]] std::unique_ptr<Message> constructEmptyMessage(Message::id_type type,
                                                                 const std::any &userData) const override {
        return std::make_unique<message_type>();
    }
};

template<auto ...Members>
using MemberSerializer = StaticMessageSerializer<message::MemberPart<Members>...>;

}

#endif //LOOM_NET_MESSAGESERIALIZER_H
