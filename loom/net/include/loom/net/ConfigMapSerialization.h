//
// Created by void on 24/04/2020.
//

#ifndef LOOM_CONFIGMAPSERIALIZATION_H
#define LOOM_CONFIGMAPSERIALIZATION_H

#include <loom/ConfigMap.h>
#include <loom/Exception.h>
#include <loom/SubjectViewMode.h>

#define MSGPACK_USE_BOOST

#include <msgpack.hpp>
#include <msgpack/adaptor/cpp11/chrono.hpp>

#include <boost/uuid/string_generator.hpp>
#include <boost/uuid/uuid_io.hpp>

namespace loom::detail {

enum class ValueType {
    String = 100,
    Number = 101,
    DataSourceInfo = 102,
    Variant = 103,
};

template<typename T>
struct get_value_type {
    ValueType operator()() const {
        throw NotSupportedException("Type is not supported for serialization");
    }
};

template<>
ValueType get_value_type<std::string>::operator()() const;

template<>
ValueType get_value_type<double>::operator()() const;

template<>
ValueType get_value_type<DataSourceInfo>::operator()() const;

template<>
ValueType get_value_type<Variant>::operator()() const;

}

MSGPACK_ADD_ENUM(loom::detail::ValueType);
MSGPACK_ADD_ENUM(loom::DataSourceInfo::SizeHint)
MSGPACK_ADD_ENUM(loom::DataSourceInfo::AccessType)

namespace msgpack::adaptor {
/**
 * Adaptors for the ConfigMap type.
 */
template<>
struct convert<loom::ConfigMap> {
    const msgpack::object &operator()(const msgpack::object &o, loom::ConfigMap &value) const;
};

template<>
struct pack<loom::ConfigMap> {
    template<typename Stream>
    msgpack::packer<Stream> &operator()(msgpack::packer<Stream> &stream,
                                        const loom::ConfigMap &value) const {
        return stream.pack(static_cast<const loom::ConfigMap::map_type &>(value));
    }
};

template<>
struct object_with_zone<loom::ConfigMap> {
    void operator()(msgpack::object::with_zone &o, const loom::ConfigMap &value) const;
};

/**
 * Adaptors for the Variant<> type.
 */
template<>
struct convert<loom::Variant> {
    const msgpack::object &operator()(const msgpack::object &o, loom::Variant &value) const;
};

template<>
struct pack<loom::Variant> {
    template<typename Stream>
    msgpack::packer<Stream> &operator()(msgpack::packer<Stream> &stream,
                                        const loom::Variant &value) const {
        stream.pack_array(value.values.size());
        for (auto &v : value.values) {
            stream.pack(v);
        }

        return stream;
    }
};

template<>
struct object_with_zone<loom::Variant> {
    void operator()(msgpack::object::with_zone &o, const loom::Variant &value) const;
};

/**
 * Adaptors for the DataSourceInfo type.
 */
template<>
struct convert<loom::DataSourceInfo> {
    const msgpack::object &operator()(const msgpack::object &o, loom::DataSourceInfo &value) const;
};

template<>
struct pack<loom::DataSourceInfo> {
    template<typename Stream>
    msgpack::packer<Stream> &operator()(msgpack::packer<Stream> &stream,
                                        const loom::DataSourceInfo &value) const {
        stream.pack_array(3);
        stream.pack(value.length);
        stream.pack(value.accessType);
        stream.pack(value.sizeHint);
        return stream;
    }
};

template<>
struct object_with_zone<loom::DataSourceInfo> {
    void operator()(msgpack::object::with_zone &o, const loom::DataSourceInfo &value) const;
};

/**
 * Adaptors for the ConfigMapValue type.
 */
template<>
struct convert<loom::ConfigMapValue> {
    const msgpack::object &operator()(const msgpack::object &o, loom::ConfigMapValue &value) const;
};

template<>
struct pack<loom::ConfigMapValue> {
    template<typename Stream>
    msgpack::packer<Stream> &operator()(msgpack::packer<Stream> &stream,
                                        const loom::ConfigMapValue &value) const {
        stream.pack_array(2);
        std::visit([&](auto &&v) {
            using T = std::decay_t<decltype(v)>;
            loom::detail::ValueType type = loom::detail::get_value_type<T>()();
            stream.pack(type);
            stream.pack(v);
        }, value);

        return stream;
    }
};

template<>
struct object_with_zone<loom::ConfigMapValue> {
    void operator()(msgpack::object::with_zone &o, const loom::ConfigMapValue &value) const;
};

}

#endif //LOOM_CONFIGMAPSERIALIZATION_H
