//
// Created by void on 16/04/2020.
//

#ifndef LOOM_NET_MESSAGEHANDLERREGISTRY_H
#define LOOM_NET_MESSAGEHANDLERREGISTRY_H

#include <loom/net/MessageHandler.h>

#include <vector>
#include <memory>

#include <boost/format.hpp>

namespace loom::net {

template<typename MessageHandlerT = MessageHandler>
class MessageHandlerRegistry : public MessageHandlerInvoker {
public:
    using message_handler_type = MessageHandlerT;

private:
    using message_handler_ptr_type = std::unique_ptr<message_handler_type>;
    std::vector<message_handler_ptr_type> messageHandlers;

public:
    /**
     * Registers a new message handler.
     * @tparam MessageHandlerT The handler type.
     * @tparam Args The construction argument types of the handler.
     * @param args The construction arguments of the handler.
     * @return A reference to the newly created handler.
     */
    template<typename HandlerT, typename... Args>
    HandlerT &registerMessageHandler(Args &&... args) {
        auto handler = std::make_unique<HandlerT>(args...);
        auto &handlerRef = *handler;
        messageHandlers.emplace_back(std::move(handler));

        std::sort(messageHandlers.begin(), messageHandlers.end(), [](const auto &a, const auto &b) {
            return (a->getPriority()) < (b->getPriority());
        });

        return handlerRef;
    }

    /**
     * Unregisters a message handler and destructs it.
     * @param handler The handler to unregister.
     */
    void unregisterMessageHandler(message_handler_type &handler) {
        messageHandlers.erase(std::find_if(messageHandlers.begin(), messageHandlers.end(), [&](const auto &handlerPtr) {
            return handlerPtr.get() == &handler;
        }));
    }

    [[nodiscard]] MessageHandlerT &getProcessingHandler(Message::id_type type) const {
        return static_cast<MessageHandlerT & >(findProcessingHandler(type));
    }

    [[nodiscard]] MessageHandlerT &getSerializationHandler(Message::id_type type) const {
        return static_cast<const MessageHandlerT & >(findSerializationHandler(type));
    }

    [[nodiscard]] MessageHandlerT &getDeserializationHandler(Message::id_type type) const {
        return static_cast<const MessageHandlerT & >(findDeserializationHandler(type));
    }

protected:
    [[nodiscard]] MessageHandler &findProcessingHandler(Message::id_type type) const override {
        for (auto &handler : messageHandlers) {
            if (handler->canProcess(type)) {
                return *handler;
            }
        }

        throw std::domain_error(
                (boost::format("No processing handler defined for given message type (%1$#x).") % type).str());
    }

    [[nodiscard]] MessageHandler &findSerializationHandler(Message::id_type type) const override {
        for (auto &handler : messageHandlers) {
            if (handler->canSerialize(type)) {
                return *handler;
            }
        }

        throw std::domain_error(
                (boost::format("No serialization handler defined for given message type (%1$#x).") % type).str());
    }

    [[nodiscard]] MessageHandler &findDeserializationHandler(Message::id_type type) const override {
        for (auto &handler : messageHandlers) {
            if (handler->canDeserialize(type)) {
                return *handler;
            }
        }

        throw std::domain_error(
                (boost::format("No deserialization handler defined for given message type (%1$#x).") % type).str());
    }
};

}

#endif //LOOM_NET_MESSAGEHANDLERREGISTRY_H
