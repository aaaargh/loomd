//
// Created by void on 01/05/2020.
//

#ifndef LOOM_NET_PROTOCOLHANDLER_H
#define LOOM_NET_PROTOCOLHANDLER_H

#include <loom/loom.h>

#include <loom/net/Message.h>
#include <loom/net/MessageHandler.h>

namespace loom::net {

class ProtocolInvoker {
public:
    using SendMessageSuccessCallback = std::function<void()>;
    using SendMessageErrorCallback = std::function<void(const std::exception_ptr &)>;

    static void DefaultSendMessageErrorCallback(const std::exception_ptr &);

    static void DefaultSendMessageSuccessCallback();

public:
    [[nodiscard]] virtual const MessageHandlerInvoker &getMessageHandlerInvoker() const = 0;

    virtual void handleMessageReceived(std::unique_ptr<net::Message> msg) = 0;

    virtual void handlePacketSent(size_t numBytes) {}

    virtual void handlePacketReceived(size_t numBytes) {}

    virtual void handleDisconnect(const std::exception_ptr &error) = 0;
};

class ProtocolHandler : public std::enable_shared_from_this<ProtocolHandler> {
public:
    using SendMessageSuccessCallback = ProtocolInvoker::SendMessageSuccessCallback;
    using SendMessageErrorCallback = ProtocolInvoker::SendMessageErrorCallback;

public:
    explicit ProtocolHandler(ProtocolInvoker &protocolInvoker);

    virtual ~ProtocolHandler() = default;

    virtual void notifyClosed() = 0;

    virtual void notifyConnected() = 0;

    virtual void sendMessage(std::unique_ptr<Message> message,
                             const SendMessageSuccessCallback &successCallback,
                             const SendMessageErrorCallback &errorCallback) = 0;

protected:
    ProtocolInvoker &protocolInvoker;
};

}

#endif //LOOM_NET_PROTOCOLHANDLER_H
