cmake_minimum_required(VERSION 3.13.2)

project(loom)

set(CMAKE_MODULE_PATH "${CMAKE_CURRENT_LIST_DIR}/cmake;${CMAKE_MODULE_PATH}")

include(loom-build)
loom_basic_setup()

include(CTest)

add_subdirectory(loom)
